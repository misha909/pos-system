# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20160321_1807'),
        ('dealerexchange', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShippingInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('FedEx', models.BooleanField(default=False)),
                ('UPS', models.BooleanField(default=False)),
                ('USPS', models.BooleanField(default=False)),
                ('none', models.BooleanField(default=False)),
                ('FedEx_drop', models.BooleanField(default=False)),
                ('UPS_drop', models.BooleanField(default=False)),
                ('USPS_drop', models.BooleanField(default=False)),
                ('none_drop', models.BooleanField(default=False)),
                ('user_profile', models.OneToOneField(related_name='shipping_info', to='accounts.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
