window.onload=function(){
    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var add_listing_btn = document.getElementsByClassName('add-listing-btn')[0];
    var item_img = document.getElementsByClassName('item-img');
    var listing_price = document.getElementsByClassName('listing-price')[0];

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    function addCommasToListingPrice(){
        var a = listing_price.innerHTML;
        var b = a.replace('$','');
        listing_price.innerHTML = '$' + numberWithCommas(b);
    }

    addCommasToListingPrice();

    function edit_item(){
        var i, a,b;
        for(i=0; i<item_img.length; i++){
            item_img[i].parentNode.onmouseover = function(){
                //a = this.parentNode;
                this.getElementsByClassName('edit-icon')[0].style.cssText="display: block;";
                this.getElementsByClassName('item-img')[0].style.cssText = "opacity: 0.5;";
            };

            item_img[i].parentNode.onmouseout = function(){
                //a = this.parentNode;
                this.getElementsByClassName('edit-icon')[0].style.cssText="display: none;";
                this.getElementsByClassName('item-img')[0].style.cssText = "opacity: 1;";
            }
        }
    }

    edit_item();

    add_listing_btn.onclick = function(){
        window.location = "/dealerexchange/add-listing/";
    };

    $('html').click(function() {
        $('#wrapper').css('display', 'none');
    });

    $('#processing').click(function(event){
        event.stopPropagation();
    });

    var cancel_handler = function() {
        $('#wrapper-delete-popup').css('display', 'none');
    };

    $('#delete-popup .cancel').click(cancel_handler);

    $('.deactivate-button').click(function(e){
        var pk = $(this).data('pk');
        var title = $(this).data('title');
        var is_active = $(this).data('is-active');

        $.ajax('/listing/activation/' + pk + '/',{
            method: 'POST',
            returnType: 'text',
            data: {
                'is_active': is_active,
            },
            success: function(data){
                if(data == 'success'){
                        console.log(e, e.target)
                    if(e.target.innerHTML == "Deactivate"){
                        e.target.innerHTML = 'Activate';
                    } else {
                        e.target.innerHTML = 'Deactivate';
                    }
                } else if(data == 'not approved') {
                    alert('This product hasn\'t been approved yet.');
                } else {
                    alert('There was an error processing your request');
                }
            }
        });
    });

    $('.delete-button').click(function(event) {
        var pk = $(this).data('pk');
        var title = $(this).data('title');

        $('#wrapper-delete-popup').css('display', 'block');
        $('#delete-popup p.last').html("Are you sure you want to delete product '"+title+"'?");

        var confirm_handler = function(event) {
            $('#delete-popup .confirm').unbind('click', confirm_handler);
            $('#delete-popup .cancel').unbind('click', cancel_handler)
            $('#delete-popup button').css('background', 'rgba(251, 158, 76, 0.75) none repeat scroll 0% 0%');

            $.ajax('/dealerexchange/delete/'+pk+'/', {
                method: 'POST',
                returnType: 'text',
                success: function(data) {
                    if(data === 'true') {
                        window.location = "/dealerexchange/store/";
                    }
                    else {
                        $('#delete-popup p.error').html(data);
                    }
                },
                error: function(data) {
                    console.log(data);
                },
                complete: function() {
                    $('#delete-popup .confirm').bind('click', confirm_handler);
                    $('#delete-popup .cancel').bind('click', cancel_handler);
                    $('#delete-popup button').css('background', '#FB9E4C none repeat scroll 0% 0%');
                }
            });
        };

        $('#delete-popup .confirm').click(confirm_handler);
    });
};
