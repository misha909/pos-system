$(document).ready( function() {

    var listing_type = document.getElementsByClassName('select-type')[0].getElementsByTagName('input');
    var firearm_radio = $('input[value="Firearm"]');
    var accessory_radio = $('input[value="Accssry"]');
    var ammo_radio = $('input[value="Ammo"]');
    var optics_radio = $('input[value="Optics"]'); 
    var apparel_radio = $('input[value="Apparel"]'); 
    var blades_radio = $('input[value="Blades"]');
    var parts_radio = $('input[value="PrtGear"]');

    var listing_type_firearm = document.getElementsByClassName('type-firearm')[0];
    var listing_type_ammo = document.getElementsByClassName('type-ammo')[0];

    var listing_type_blades = document.getElementsByClassName('type-blades')[0];
    var listing_type_optics = document.getElementsByClassName('type-optics')[0];
    var listing_type_apparel = document.getElementsByClassName('type-apparel')[0];

    var required = document.getElementsByClassName("req");
    var submit_btn = document.getElementsByClassName("submit-btn")[0];
    var i;

    var cost = document.getElementById('id_price');
    var price = document.getElementById('id_list_price');
    var list_price = document.getElementById('list_price_value');

    var msrp = document.getElementById('id_msrp');
    var map_price = document.getElementById('id_map');

    var required_select = document.getElementById("id_category");
    var zoom = document.getElementsByClassName('magnify');
    var wrapper = document.getElementsByClassName('wrapper')[0];

    var item_image = document.getElementsByClassName('item-image');
    var close_box = document.getElementsByClassName('wrapper')[0].getElementsByTagName('button')[0];
    //var item_color;

    var tag_input = document.getElementsByClassName('tag-input')[0];
    var title_listing = document.getElementsByClassName('title-listing')[0];
    var title_error = document.getElementsByClassName('title-error')[0];

    var tags_wrapper = document.getElementsByClassName('tags-wrapper')[0];
    var tags_list = document.getElementById('tag-list');
    //var add_tags_button = document.getElementsByClassName('add')[0];
    var tags_left = document.getElementsByClassName('left')[0];
    var dimension_inputs = document.getElementsByClassName('dimensions')[0].getElementsByTagName('input');
    var dimensions_error = document.getElementsByClassName('dimensions')[0].getElementsByTagName('span')[0];

    //var dragSrcEl = null;
    //var add_color = document.getElementsByClassName("add-color")[0];
    //var colors_div = document.getElementsByClassName("colors-div")[0];

    var zoom_arr = Array.prototype.slice.call(zoom);
    //var select_color = document.getElementsByClassName("select-color")[0];
    //var y = select_color.options;

    //select_color.onchange = function(){
    //    var x = this.selectedIndex;
    //    if(x==0){
    //    }
    //    else {
    //        var a = colors_div.innerHTML;
    //        var b = "<ul class='item-color' draggable='true'><li>"+y[x].value+"</li><li><label><input type='text' class='dollar'></label></li><li><label><input type='checkbox' checked></label></li></ul>";
    //        colors_div.innerHTML = a+b;
    //        item_color = document.getElementsByClassName('item-color');
    //        function drag_func(){
    //            [].forEach.call(item_color, function(col) {
    //                col.addEventListener('dragstart', handleDragStart, false);
    //                col.addEventListener('dragenter', handleDragEnter, false);
    //                col.addEventListener('dragover', handleDragOver, false);
    //                col.addEventListener('dragleave', handleDragLeave, false);
    //                col.addEventListener('drop', handleDrop, false);
    //                col.addEventListener('dragend', handleDragEnd, false);
    //            });
    //        }
    //
    //        drag_func();
    //    }
    //};

    var check_input = function(){
        var images_exist = false;
        var img_input = $(".image-input");
        for (i=0; i<img_input.length; i++) {
            if(img_input[i].value) {
                images_exist = true;
            }
        }
        if (!images_exist) {
            $('#photos-desc').css('color', "#FF0000");
            return 2;
        } else {
            $('#photos-desc').css('color', "#767676");
        }
        for(i=0; i<required.length; i++){
            if(required[i].value == ""){
                if(getComputedStyle(required[i].parentElement.parentElement,null).display !== "none"){ //check to see if these inputs are displayed or not
                    required[i].style.cssText = "border: thin red solid;";
                    return 1;
                }
            }

            else {
                required[i].style.cssText = "border: thin silver solid;";
            }
        }
    };

    //function handleDragStart(e) {
    //    //this.style.opacity = '0.4';  // this / e.target is the source node.
    //
    //    dragSrcEl = this;
    //
    //    e.dataTransfer.effectAllowed = 'move';
    //    e.dataTransfer.setData('text/html', this.innerHTML);
    //}
    //
    //function handleDragOver(e) {
    //    if (e.preventDefault) {
    //        e.preventDefault(); // Necessary. Allows us to drop.
    //    }
    //
    //    e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
    //
    //    return false;
    //}
    //
    //function handleDragEnter(e) {
    //    // this / e.target is the current hover target.
    //    this.classList.add('over');
    //}
    //
    //function handleDragLeave(e) {
    //    this.classList.remove('over');  // this / e.target is previous target element.
    //}
    //
    //function handleDrop(e) {
    //    // this/e.target is current target element.
    //
    //    if (e.stopPropagation) {
    //        e.stopPropagation(); // Stops some browsers from redirecting.
    //    }
    //
    //    // Don't do anything if dropping the same column we're dragging.
    //    if (dragSrcEl != this) {
    //        // Set the source column's HTML to the HTML of the column we dropped on.
    //        dragSrcEl.innerHTML = this.innerHTML;
    //        this.innerHTML = e.dataTransfer.getData('text/html');
    //    }
    //
    //    return false;
    //}
    //
    //function handleDragEnd(e) {
    //    // this/e.target is the source node.
    //
    //    [].forEach.call(item_color, function (col) {
    //        col.classList.remove('over');
    //    });
    //}

    function img_zoom(){
        var a,b;
        for(i=0;i<zoom_arr.length;i++){
            zoom[i].onclick = function(){
                a = zoom_arr.indexOf(this);
                b = item_image[a].src;
                wrapper.style.cssText = "display: block;";
                wrapper.getElementsByTagName('img')[0].src = b;
            };
        }
    }

    function remove_tag(){
        var tag_name = $(".tag-name");
        var a, b,c;
        $(".tag-remove").each( function(index) {
            $(this).click( function() {
                    $(this).parent().remove();
                    a = tags_left.innerHTML;
                    b = parseInt(a,10);
                    c = b+1;
                    tags_left.innerHTML = c+" left";
                 })
        });
    }

    //function add_tags(){
    //    var tags_array = tag_input.value.split(",");
    //
    //    if(tags_array.length <= 10){
    //        var d = tags_left.innerHTML;
    //        var e = parseInt(d,10);
    //        tags_left.innerHTML = e-tags_array.length + " left";
    //        var a = "";
    //        for(i=0;i<tags_array.length; i++){
    //            a += "<span><button type='button' class='active-button tag-remove'></button><button type='button' class='tag-name'>"+ tags_array[i] +"</button><input type='hidden' value='" + tags_array[i] + "' name='tag_'" + i + "'></span>";
    //        }
    //        tags_wrapper.innerHTML = a;
    //    }
    //
    //    remove_tag();
    //}

    function tag_counter(){
        var tags_array = tag_input.value.split(",");
        var array_length = tags_array.length;
        if (array_length > 10) {
            tag_input.value = tag_input.value.substring(0, tag_input.value.lastIndexOf(','));
            tags_array = tags_array.splice(0, 10);
            array_length -= 1;
        }
        if (tags_array[array_length - 1].trim() === "") {
            array_length -= 1;
        }
        tags_left.innerHTML = 10 - array_length + " left";
        var a = "";
        for (i=0; i<array_length; i++) {
            if (tags_array[i] !== "") {
                a += "<span><button type='button' class='tag-name'>" + tags_array[i] + "</button></span>";
            }
        }
        tags_list.innerHTML = a;
    }

    tag_input.oninput = function(){
        tag_counter();
    };

    //add_tags_button.onclick = function(){
    //    var d = tags_left.innerHTML;
    //    var e = parseInt(d,10);
    //    if(e>0){
    //        add_tags();
    //    }
    //};

    function hide_blocks(){
        var a = document.getElementsByClassName('details');
        var i;

        for(i=0; i< a.length; i++){
            if(i==0 || i==6){
            }
            else {
                a[i].style.cssText="display: none;";
            }
        }
    }

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }


    firearm_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
            listing_type_firearm.style.cssText = "display: block";
        }
    });

    ammo_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
            listing_type_ammo.style.cssText = "display: block";

        }
    });

    optics_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
            listing_type_optics.style.cssText = "display: block";
        }
    });

    apparel_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
            listing_type_apparel.style.cssText = "display: block";
        }
    });

    blades_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
            listing_type_blades.style.cssText = "display: block";
        }
    });

    parts_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
        }
    });

    accessory_radio.change(function(){
        if(this.checked == true){
            hide_blocks();
        }
    });
    submit_btn.onclick = function(){
        var a = required_select.selectedIndex;
        var check = check_input();
        var scrollSpeed = 300;
        if(check === 2) {
            $('html, body').animate({scrollTop: $('#photos-desc').offset().top - 10}, scrollSpeed);
            return false;
        }

        else if(check === 1){
            for(i=0; i<required.length; i++){
                if(required[i].value == ""){
                    if(getComputedStyle(required[i].parentElement.parentElement,null).display !== "none"){
                        $('html, body').animate({
                        scrollTop: $(required[i]).offset().top - 10
                        }, scrollSpeed);
                        break;
                    }
                }
            }
            return false;
        }

        else if (a == 0){
            required_select.style.cssText = "border: thin red solid";
            $('html, body').animate({
            scrollTop: $(required_select).offset().top
            }, scrollSpeed);
            return false;
        }

         //else if (dimension_inputs.value)

        else {
            title_error.style.cssText = "display: none";
            required_select.style.cssText = "border: thin silver solid";
            jQuery("button.continue-btn").fancybox({
                padding: 0,
                modal : true,
                helpers : {
                    overlay : {
                        css : {
                            'background' : 'rgba(58, 42, 45, 0.1)'
                        }
                    }
                },
                css : {
                    'background' : 'rgba(58, 42, 45, 0.2)','opacity':'1'
                },
                href: '#processing-gif',
                afterShow: function () {
                    $('.fancybox-skin').css({'box-shadow' :'none','background':'rgba(255,255,255,0.5)'});
                    $('.processing').css({'background' : 'rgba(204,204,204, 0.7)','opacity' : '1'});
                    document.getElementsByClassName('listing-form')[0].submit();
                }


            });
        }
    };

    close_box.onclick = function(){
        wrapper.style.cssText = "display: none;";
    };

//    title_listing.onkeyup = function(e){
//        var a = 'which' in e ? e.which : e.keyCode;
//       if(a==190){ //do nothing for period
//            title_error.style.cssText = "display: block";
//            this.style.cssText = "border: thin red solid;";
//        }
//        else {
//            title_error.style.cssText = "display: none";
//            this.style.cssText = "border: thin silver solid;";
//        }
//    };

    cost.oninput = function(){
        var value = (1.135 * this.value.replace(',','')).toFixed(2); // replace commas in cost value before calculation
        list_price.innerHTML = numberWithCommas(value);
        price.value = value;

    };

    cost.onblur = function(){
      this.value = numberWithCommas(this.value);
    };

    msrp.onblur = function(){
        this.value = numberWithCommas(this.value);
    };

    map_price.onblur = function(){
        this.value = numberWithCommas(this.value);
    };

    dimension_inputs[1].onblur = function(){
        if(dimension_inputs[1].value > dimension_inputs[0].value) {
            submit_btn.disabled = true;
            submit_btn.style.cssText = "background: #777";
            this.style.cssText = "border: thin red solid";
            dimensions_error.style.cssText = "display: inline-block;";
        }

        else {
            submit_btn.disabled = false;
            submit_btn.style.cssText = "background: #fb9e4c";
            this.style.cssText = "border: thin silver solid";
            dimensions_error.style.cssText = "display: none;";
        }
    };

    dimension_inputs[2].onblur = function(){
        if(dimension_inputs[2].value > dimension_inputs[0].value) {
            submit_btn.disabled = true;
            submit_btn.style.cssText = "background: #777";
            this.style.cssText = "border: thin red solid";
            dimensions_error.style.cssText = "display: inline-block;";
        }

        else {
            submit_btn.disabled = false;
            submit_btn.style.cssText = "background: #fb9e4c";
            this.style.cssText = "border: thin silver solid";
            dimensions_error.style.cssText = "display: none;";
        }
    };

    img_zoom();
   // drag_func();
    remove_tag();

    function add_photo(input) {
        var image_index = parseInt(input.id.slice(-1), 10);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $('#img_view_' + image_index);
                img.attr('src', e.target.result);
                $('#input_photo_' + image_index).hide();
                $('#display_photo_' + image_index).show();
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.image-input').change(function(){
        add_photo(this);
    });

    $('.delete_photo').click(function(){
        var par = $(this).parent();
        var image_index = parseInt(par.attr('id').slice(-1), 10);
        $('#id_img_' + image_index).val('');
        $('#img_view_' + image_index).attr('src', '');
        $('#display_photo_' + image_index).hide();
        $('#input_photo_' + image_index).show();
    });

    $("span.question").hover(function () {
        $(this).append('<div class="tooltip"><p>The "List Price" is automatically calculated with Dealer Direct seller fees. This price is what the dealer will see. The "Cost" field is the amount you will be paid.</p></div>');
    }, function () {
        $("div.tooltip").remove();
    });
});
