window.onload = function(){

    var period_buttons = document.getElementsByClassName('period-buttons');
    var i;

    function highlight(){
        var a,b,j;
        for(i=0; i<period_buttons.length; i++){
            period_buttons[i].onclick = function(){
                if (this.getAttribute("data-active") !== "true") {
                    a = this.parentNode;
                    b = a.getElementsByTagName('button');
                    for(j=0; j<b.length; j++){
                        b[j].style.cssText = "background-color: #bcbcbc;";
                        b[j].setAttribute("data-active", "false");
                    }
                    this.style.cssText = "background-color: #0b0b79;";
                }
            }
        }
    }

    highlight();



    //------------------------------------------//
    //------------ Graph Javascript ------------//
    //------------------------------------------//

    // Sales Line Chart
    var sales_label_list = [["12AM", "1AM", "2AM", "3AM", "4AM", "5AM", "6AM", "7AM", "8AM", "9AM", "10AM", "11AM", "12PM", "1PM", "2PM", "3PM", "4PM", "5PM", "6PM", "7PM", "8PM", "9PM", "10PM", "11PM"],
                            [],
                            [],
                            ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]];

    var sales_data = {
        labels: [],
        datasets: [
            {
                label: "Sales Data",
                fillColor: "rgba(0,0,0,0)",
                strokeColor: "rgba(11,11,121,1)",
                pointColor: "rgba(11,11,121,1)",
                data: []
            }
        ]
    };

    for (i = 0; i < sales_ytd_data.length; i++){
        sales_data.labels.push(sales_label_list[3][i]);
        sales_data.datasets[0].data.push(sales_ytd_data[i].y);
    }

    var sales_options = {
        scaleShowGridLines: false,
        bezierCurve: false,
        pointDotRadius: 2,
        pointHitDetectionRadius: 4,
        tooltipTemplate: "$<%= value %>",
        scaleLabel: "$<%= value %>"
    };

    var sales_line_ctx = document.getElementById('sales-line').getContext("2d");
    if (sales_data.datasets[0].data.length !== 0){
        var sales_line_chart = new Chart(sales_line_ctx).Line(sales_data, sales_options);
    } else {
        $('#sales-line').hide();
    }



    // Top Five Pie Chart
    var pie_data = [
        {
            color: "#0B0B79",
            highlight: "#0B0B79"
        },
        {
            color: "#0E0E96",
            highlight: "#0E0E96"
        },
        {
            color: "#1010B9",
            highlight: "#1010B9"
        },
        {
            color: "#06064A",
            highlight: "#06064A"
        },
        {
            color: "#1111DF",
            highlight: "#1111DF"
        }
    ];

    for (i = 0; i < top_five_ytd_data.length; i++){
        pie_data[i].label = top_five_ytd_data[i].model;
        pie_data[i].value = top_five_ytd_data[i].total;
    }

    var pie_options = {
        animationEasing: "easeOutQuart",
        animationSteps: 60,
        showTooltips: false
    };

    var top_pie_ctx = document.getElementById('top-pie').getContext("2d");
    var top_pie_chart;
    if (top_five_ytd_data.length !== 0){
        top_pie_chart = new Chart(top_pie_ctx).Pie(pie_data.slice(0, top_five_ytd_data.length), pie_options);
    } else {
        var empty_data = [
            {
                color: "#0B0B79",
                highlight: "#0B0B79",
                label: "None",
                value: 1
            }
        ];
        top_pie_chart = new Chart(top_pie_ctx).Pie(empty_data, pie_options);
    }



    // Listing Number Gauge
    var product_gauge = new JustGage({
        id: "gauge",
        value: active_percent,
        min: 0,
        max: 100,
        label: "active listings",
        symbol: "%",
        levelColors: ['#0B0B79']
    });


    //------------------------------------------//
    //------------ API Javascript ------------//
    //------------------------------------------//

    // Variables
    var total_orders = document.getElementById("total-orders");
    var total_sales = document.getElementById("total-sales");

    var total_orders_buttons = document.getElementsByClassName("total-orders-buttons");

    var total_sales_buttons = document.getElementsByClassName("total-sales-buttons");

    var top_five_buttons = document.getElementsByClassName("top-five-buttons");
    var top_five_legends = document.getElementsByClassName("top-five-legend");

    var total_orders_set = [false, false, false];
    var total_orders_value = [0, 0, 0, total_orders.innerHTML];

    var total_sales_set = [false, false, false];
    var total_sales_value = [[], [], [], sales_ytd_data];

    var top_five_set = [false, false, false];
    var top_five_value = [[], [], [], top_five_ytd_data];



    // Get Total Orders API
    var get_total_orders = function(periodIndex) {
        var period = 1;
        if (periodIndex === 1){
            period = 7;
        } else if (periodIndex === 2){
            period = 30;
        }
        $.ajax({
            url: "/dealerexchange/get_total_orders/",
            data: {period: period}
        }).done(function(data){
            total_orders_value[periodIndex] = data;
            total_orders_set[periodIndex] = true;
            total_orders.innerHTML = total_orders_value[periodIndex];
        });
    };

    total_orders_buttons[0].addEventListener('click', function(){
        if (total_orders_set[0] !== true) {
            get_total_orders(0);
        } else {
            total_orders.innerHTML = total_orders_value[0];
        }
    });

    total_orders_buttons[1].addEventListener('click', function(){
        if (total_orders_set[1] !== true) {
            get_total_orders(1);
        } else {
            total_orders.innerHTML = total_orders_value[1];
        }
    });

    total_orders_buttons[2].addEventListener('click', function(){
        if (total_orders_set[2] !== true) {
            get_total_orders(2);
        } else {
            total_orders.innerHTML = total_orders_value[2];
        }
    });

    total_orders_buttons[3].addEventListener('click', function(){
        total_orders.innerHTML = total_orders_value[3];
    });



    // Get Total Sales API
    var get_total_sales = function(periodIndex) {
        var period = 1;
        if (periodIndex === 1) {
            period = 7;
        } else if (periodIndex === 2) {
            period = 30;
        }
        $.ajax({
            url: "/dealerexchange/get_total_sales/",
            data: {period: period}
        }).done(function(data) {
            console.log(data);
            total_sales_value[periodIndex] = data;
            total_sales_set[periodIndex] = true;
            create_sales_graph(periodIndex);
        })
    };

    var create_sales_graph = function(periodIndex) {
        var new_sales_data = [];
        for (i = 0; i < total_sales_value[periodIndex].length; i++){
            new_sales_data.push(total_sales_value[periodIndex][i].y);
        }
        sales_data.datasets[0].data = new_sales_data;
        if (periodIndex === 0 || periodIndex === 3){
            sales_data.labels = sales_label_list[periodIndex].slice(0, new_sales_data.length);
        } else {
            var new_labels = [];
            for (i = 0; i < total_sales_value[periodIndex].length; i++){
                new_labels.push(total_sales_value[periodIndex][i].x);
            }
            sales_data.labels = new_labels;
        }
        sales_line_chart.destroy();
        sales_line_chart = new Chart(sales_line_ctx).Line(sales_data, sales_options);
        total_sales_buttons[periodIndex].setAttribute("data-active", "true");

        var sales_total = 0;
        for (i = 0; i < new_sales_data.length; i++){
            sales_total += new_sales_data[i];
        }
        sales_total = "$" + sales_total.toFixed(2).toString();
        total_sales.innerHTML = sales_total.toString();
    };

    total_sales_buttons[0].addEventListener('click', function(){
        if (total_sales_buttons[0].getAttribute("data-active") !== "true") {
            if (total_sales_set[0] !== true) {
                get_total_sales(0);
            } else {
                create_sales_graph(0);
            }
        }
    });

    total_sales_buttons[1].addEventListener('click', function(){
        if (total_sales_buttons[1].getAttribute("data-active") !== "true") {
            if (total_sales_set[1] !== true) {
                get_total_sales(1);
            } else {
                create_sales_graph(1);
            }
        }
    });

    total_sales_buttons[2].addEventListener('click', function(){
        if (total_sales_buttons[2].getAttribute("data-active") !== "true") {
            if (total_sales_set[2] !== true) {
                get_total_sales(2);
            } else {
                create_sales_graph(2);
            }
        }
    });

    total_sales_buttons[3].addEventListener('click', function(){
        if (total_sales_buttons[3].getAttribute("data-activ") !== "true") {
            create_sales_graph(3);
        }
    });



    // Get Top Five API
    var get_top_five = function(periodIndex) {
        var period = 1;
        if (periodIndex === 1){
            period = 7;
        } else if (periodIndex === 2){
            period = 30;
        }
        $.ajax({
            url: "/dealerexchange/get_top_five/",
            data: {period: period}
        }).done(function(data){
            console.log(data);
             top_five_set[periodIndex] = true;
             top_five_value[periodIndex] = data;
             create_top_five_graph(periodIndex);
        });
    };

    var create_top_five_graph = function(periodIndex){
        var data_length = top_five_value[periodIndex].length;
        for (i = 0; i < data_length; i++){
            pie_data[i].value = top_five_value[periodIndex][i].total;
            pie_data[i].label = top_five_value[periodIndex][i].model;
        }
        for (i = 0; i < 5; i++){
            if (i < data_length) {
                top_five_legends[i].children[1].innerHTML = top_five_value[periodIndex][i].model.substring(0, 17) + "...";
                top_five_legends[i].children[2].innerHTML = "$" + top_five_value[periodIndex][i].total.toFixed(2).toString();
                top_five_legends[i].style.display = "block";
            } else {
                top_five_legends[i].style.display = "none";
            }
        }
        top_pie_chart.destroy();
        if (data_length !== 0){
            $(".no-data").hide();
            top_pie_chart = new Chart(top_pie_ctx).Pie(pie_data.slice(0, data_length), pie_options);
        } else {
            var empty_data = [
                {
                    color: "#0B0B79",
                    highlight: "#4F9260",
                    label: "None",
                    value: 1
                }
            ];
            top_pie_chart = new Chart(top_pie_ctx).Pie(empty_data, pie_options);
            $(".no-data").show();
        }
        top_five_buttons[periodIndex].setAttribute("data-active", "true");
    };

    top_five_buttons[0].addEventListener('click', function(){
        if (top_five_buttons[0].getAttribute("data-active") !== "true") {
            if (top_five_set[0] !== true) {
                get_top_five(0);
            } else {
                create_top_five_graph(0);
            }
        }
    });

    top_five_buttons[1].addEventListener('click', function(){
        if (top_five_buttons[1].getAttribute("data-active") !== "true") {
            if (top_five_set[1] !== true) {
                get_top_five(1);
            } else {
                create_top_five_graph(1);
            }
        }
    });

    top_five_buttons[2].addEventListener('click', function(){
        if (top_five_buttons[2].getAttribute("data-active") !== "true") {
            if (top_five_set[2] !== true) {
                get_top_five(2);
            } else {
                create_top_five_graph(2);
            }
        }
    });

    top_five_buttons[3].addEventListener('click', function(){
        if (top_five_buttons[3].getAttribute("data-active") !== "true") {
            create_top_five_graph(3);
        }
    });
};

