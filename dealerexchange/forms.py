import re

from django import forms
from django.forms.models import inlineformset_factory
from django.core.validators import RegexValidator, validate_email

from rest_framework.authtoken.models import Token

from decimal import Decimal
from accounts.models import Distributor
from api.utils import create_image
from products.models import (
        UPC, 
        Product, 
        ProductListingImage, 
        ProductTag,
        Category,
        )
from saasapp.salesforce import create_product_2_de
from accounts.models import UserProfile, Wallet
from .models import ShippingInfo
from .utils import verify_routing_number, create_payment_info
from products.search_indexes import ProductIndex
from products.constants import categories, conditions


class UsernameField(forms.CharField):

    def validate(self, value):
        super(UsernameField, self).validate(value)
        validate_email(value)
        check_unique = User.objects.filter(username=value)
        if check_unique.count() > 0:
            raise ValidationError(
                ('E-mail %(value)s is already registered to an account.'),
                params={'value': value},
                code='existing'
            )


class PasswordField(forms.CharField):
    validator = RegexValidator(regex=r'^[a-zA-Z0-9]+$')
    default_validators = [validator]

    def __init__(self, *args, **kwargs):
        kwargs['min_length'] = 8
        kwargs['error_messages'] = {
            'required': 'Please enter your password.',
            'invalid': 'Ensure password consists of letters or numbers.'
        }
        super(PasswordField, self).__init__(*args, **kwargs)

    def validate(self, value):
        super(PasswordField, self).validate(value)
        if BlacklistedPassword.objects.filter(text=value).count() > 0:
            raise ValidationError(
                ('Enter a less common password.'),
                code='common'
            )


class SignUpForm(forms.Form):
    first_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'name': 'first_name',
                'id': 'first_name',
                'placeholder': 'First Name',
            }
        ),
        label='First name',
        error_messages={
            'required': 'Please enter your first name.'
        }
    )
    last_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'name': 'last_name',
                'id': 'last_name',
                'placeholder': 'Last Name',
            }
        ),
        label='Last name',
        error_messages={
            'required': 'Please enter your last name.'
        }
    )
    username = UsernameField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'name': 'username',
                'id': 'username',
                'placeholder': 'Email address'
            }
        ),
        label='Email address',
        error_messages={
            'required': 'Please enter your e-mail address.',
            'existing': 'E-mail address is already registered',
        }
    )
    ffl_number_first_three = forms.CharField(
        min_length=3,
        max_length=4,
        widget=forms.TextInput(
            attrs={
                'name': 'ffl_number_first_three',
                'id': 'ffl_number_first_three',
                'placeholder': 'First 3 of FFL',
            }
        ),
        label='First three digits of FFL',
        error_messages={
            'required': 'Please enter the first three digits of your FFL.',
            'min_length': 'Ensure first FFL field has three (3) digits.'
        }
    )
    ffl_number_last_five = forms.CharField(
        min_length=5,
        max_length=5,
        widget=forms.TextInput(
            attrs={
                'name': 'ffl_number_last_five',
                'id': 'ffl_number_last_five',
                'placeholder': 'Last 5 of FFL',
            }
        ),
        label='Last five digits of FFL',
        error_messages={
            'required': 'Please enter the last five digits of your FFL.',
            'min_length': 'Ensure second FFL field has five (5) digits.',
        }
    )
    premise_zip_code = forms.CharField(
        max_length=5,
        widget=forms.TextInput(
            attrs={
                'name': 'premise_zip_code',
                'id': 'premise_zip_code',
                'placeholder': 'Premise Zip Code',
            },
        ),
        label='Premise zip code',
        error_messages={
            'required': 'Please enter premise zip code.'
        }
    )
    password = PasswordField(
        max_length=100,
        widget=forms.PasswordInput(
            attrs={
                'name': 'password',
                'id': 'password',
                'placeholder': 'Password'
            }
        ),
        error_messages={
            'required': 'Please enter your password.',
            'invalid': 'Ensure password consists of letters or numbers.',
            'min_length': 'Ensure password has at least eight (8) characters.',
        }
    )

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        if not self.errors:
            first3 = cleaned_data.get('ffl_number_first_three')
            ffl_number_first_three = ''.join(re.findall(r'[0-9]', first3))
            last5 = cleaned_data.get('ffl_number_last_five')
            first1 = ffl_number_first_three[0]
            first2 = ffl_number_first_three[1:]
            invalid = 'Invalid FFL credentials.'
            fail = 'FFL EZ check is not responding. Please try again.'
            unique = 'FFL number already registered with vArmory.'

            if UserProfile.objects.filter(
                # ffl_number_first_three='%s%s' % (first1, first2),
                ffl_number_last_five=last5
            ).exists():
                self.add_error(
                    'ffl_number_first_three',
                    ValidationError(unique, code='unique')
                )
                return
            try:
                verified = verify_ffl(first1, first2, last5)
                if not verified.get('verified'):
                    self.add_error(
                        'ffl_number_first_three',
                        ValidationError(invalid, code='invalid')
                    )
                self.cleaned_data['verified'] = verified
            except Exception, e:
                print e
                self.add_error(
                    'ffl_number_first_three',
                    ValidationError(fail, code='fail')
                )
        else:
            self.add_error(
                None,
                ValidationError(
                    'FFL will be verified once all other entries are valid.'
                )
            )

    def create_new_user(self):
        data = self.cleaned_data
        first3 = data['ffl_number_first_three']
        verified = data['verified']

        new_user = User.objects.create(
            first_name=data['first_name'],
            last_name=data['last_name'],
            username=data['username'],
            email=data['username'],
            password=make_password(data['password']),
            is_active=True
        )
        userprofile = UserProfile(
            user=new_user,
            ffl_number_first_three='%s%s' % (first3[0], first3[2:]),
            ffl_number_last_five=data['ffl_number_last_five'],
            ffl_expiration_date=verified['expiration_date'],
            ffl_premise_address=verified['premise_address'],
            ffl_mailing_address=verified['mailing_address'],
            company_name=verified['company'],
            premise_zip_code=data['premise_zip_code'],
        )
        userprofile.save()
        # Salesforce leads are generated from the code below
        if userprofile.company_name and not userprofile.from_salesforce:
            create_account(user=new_user)
        return new_user


class ListingForm(forms.ModelForm):
    upc = forms.CharField(required=False)
    quantity = forms.IntegerField()
    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'description req'})
    )
    price = forms.DecimalField(max_digits=10, decimal_places=2)
    caliber_firearm = forms.CharField(
        required=False, widget=forms.TextInput(attrs={'class': 'req'})
    )
    caliber_ammo = forms.CharField(required=False)
    finish_optics = forms.CharField(
        required=False,
        widget=forms.TextInput()
    )
    length_barrel = forms.CharField(required=False)
    overall_length_firearm = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'req', 'placeholder': 'Inches'})
    )
    overall_length_ammo = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'req', 'placeholder': 'Inches'})
    )
    overall_length_optics = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'req', 'placeholder': 'Inches'})
    )
    tags = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'tag-input',
            'placeholder': 'example: ar15, iron sights, glock holster, etc'
        })
    )
    img_0 = forms.ImageField(
        required=False, widget=forms.FileInput(attrs={'class': 'image-input'})
    )
    img_1 = forms.ImageField(
        required=False, widget=forms.FileInput(attrs={'class': 'image-input'})
    )
    img_2 = forms.ImageField(
        required=False, widget=forms.FileInput(attrs={'class': 'image-input'})
    )
    img_3 = forms.ImageField(
        required=False, widget=forms.FileInput(attrs={'class': 'image-input'})
    )
    img_4 = forms.ImageField(
        required=False, widget=forms.FileInput(attrs={'class': 'image-input'})
    )
    minimum_order = forms.IntegerField(
        initial=1,
        widget=forms.NumberInput(attrs={'class': 'req'})
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ListingForm, self).__init__(*args, **kwargs)

    class Meta:
        model = UPC
        exclude = ['copy', 'picture', 'davidson_picture', 'action',
                   'stock', 'sights', 'caliber']
        widgets = {
            'model': forms.TextInput(attrs={'class': 'title-listing'}),
            'listing_type': forms.RadioSelect(),
            'upc': forms.TextInput(attrs={'class': 'upc'}),
            'listing_condition': forms.Select(attrs={'required':'False'}),
            'condition_detail': forms.Select(attrs={'required': 'False'}),
            'listing_category': forms.Select(
                attrs={'class': 'short-input', 'required': 'True'}
            ),
            'price': forms.TextInput(attrs={'class': 'price dollar req'}),
            'msrp': forms.TextInput(attrs={'class': 'dollar'}),
            'map': forms.TextInput(attrs={'class': 'dollar'}),
            'list_price': forms.TextInput(
                attrs={'class': 'list_price_input', 'max_length': '10'}
            ),
            'quantity': forms.TextInput(attrs={'class': 'qty req'}),
            'description': forms.Textarea(attrs={'class': 'description req'}),
            'estimated_ship_days': forms.TextInput(
                attrs={'class': 'qty req', 'placeholder': 'Days'}
            ),
            'capacity': forms.TextInput(attrs={'class': 'req'}),
            'length_overall': forms.TextInput(
                attrs={'class': 'req', 'placeholder': 'Inches'}
            ),
            'weight_pounds': forms.TextInput(
                attrs={'class': 'weight req', 'placeholder': 'Pounds'}
            ),
            'weight_ounces': forms.TextInput(
                attrs={'class': 'weight req', 'placeholder': 'Pounds'}
            ),
            'shipping_zip_code': forms.TextInput(
                attrs={'placeholder': 'Your shipping zip code'}
            ),
        }

    def populate_upc(self, upc, data):
        cat_name = data.get('category')
        if cat_name:
            upc.category = Category.objects.get_or_create(name=cat_name)[0]
        listing_category = data.get('listing_category') or data.get('category')
        upc.listing_condition = data.get('listing_condition')
        upc.condition_detail = data.get('condition_detail')
        upc.listing_category = listing_category or upc.listing_category
        upc.model = data.get('title') or upc.model
        upc.description = data.get('description') or upc.description
        upc.short_description = data.get('short_description') or upc.short_description
        upc.caliber = data.get('caliber') or upc.caliber
        upc.capacity = data.get('capacity') or upc.capacity
        upc.finish = data.get('finish') or upc.finish
        upc.weight = data.get('weight') or upc.weight
        upc.gun_type = data.get('type') or upc.gun_type
        upc.length_barrel = data.get('barrel_length') or upc.length_barrel
        upc.length_overall = data.get('overall_length') or upc.length_barrel
        upc.manufacturer = data.get('manufacturer') or upc.manufacturer
        upc.listing_type = data.get('listing_type') or upc.listing_type
        upc.msrp = data.get('msrp', upc.msrp)
        upc.map = data.get('map', upc.map)
        upc.estimated_ship_days = data.get(
            'estimated_ship_days', upc.estimated_ship_days
        )

        # Extra Data for Ammo
        upc.rounds = data.get('rounds') or upc.rounds
        upc.muzzle_velocity = data.get('muzzle_velocity') or upc.muzzle_velocity
        upc.length = data.get('length') or upc.length
        upc.ammo_type = data.get('ammo_type') or upc.ammo_type

        # Extra Data for Optics
        upc.magnification = data.get('magnification') or upc.magnification
        upc.field_of_view = data.get('field_of_view') or upc.field_of_view
        upc.reticle = data.get('reticle') or upc.reticle
        upc.objective_lens = data.get('objective_lens') or upc.objective_lens
        upc.eye_relief = data.get('eye_relief') or upc.eye_relief
        upc.max_windage_adjustment = data.get('max_windage_adjustment') or upc.max_windage_adjustment
        upc.max_elevation_adjustment = data.get('max_elevation_adjustment') or upc.max_elevation_adjustment
        upc.battery = data.get('battery') or upc.battery
        upc.dot_size = data.get('dot_size') or upc.dot_size

        # Extra Data for Blades
        upc.blade_type = data.get('blade_type') or upc.blade_type
        upc.blade_length = data.get('blade_length') or upc.blade_length
        upc.blade_total_length = data.get('blade_total_length') or upc.blade_total_length
        upc.blade_steel = data.get('blade_steel') or upc.blade_steel
        upc.blade_handle_material = data.get('blade_handle_material') or upc.blade_handle_material
        upc.blade_edge = data.get('blade_edge') or upc.blade_edge

        # Extra Data for Apparel
        upc.shirt_size = data.get('shirt_size') or upc.shirt_size
        upc.shirt_color = data.get('shirt_color') or upc.shirt_color

        upc.save()
        return upc

    def clean(self):
        listing_type = self.cleaned_data.get('listing_type')
        if self.cleaned_data.get('img_0') is None and \
           self.cleaned_data.get('img_1') is None and \
           self.cleaned_data.get('img_2') is None and \
           self.cleaned_data.get('img_3') is None and \
           self.cleaned_data.get('img_4') is None:
           self.add_error('img_0', 'One photo must be uploaded')
        self.cleaned_data['finish'] = self.cleaned_data.get('finish') or \
            self.cleaned_data.get('finish_optics')
        self.cleaned_data['length_overall'] = \
            self.cleaned_data.get('overall_length_firearm') or \
            self.cleaned_data.get('overall_length_ammo') or \
            self.cleaned_data.get('overall_length_optics')

        if listing_type == 'Firearm':
            if self.cleaned_data.get('caliber_firearm', None) is None:
                self.add_error('caliber_firearm', 'This field is required')
            else:
                self.cleaned_data['caliber'] = self.cleaned_data.get('caliber_firearm')
            if self.cleaned_data.get('capacity', None) is None:
                self.add_error('capacity', 'This field is required')
            if self.cleaned_data.get('weight_pounds', None) is None:
                self.add_error('weight_pounds', 'This field is required')
            if self.cleaned_data.get('weight_ounces', None) is None:
                self.add_error('weight_ounces', 'This field is required')
            if self.cleaned_data.get('length_barrel', None) is None:
                self.add_error('length_barrel', 'This field is required')
            if self.cleaned_data.get('length_overall', None) is None:
                self.add_error('overall_length_firearm', 'This field is required')
        elif listing_type == 'Ammo':
            if self.cleaned_data.get('caliber_ammo', None) is not None:
                self.cleaned_data['caliber'] = self.cleaned_data.get('caliber_ammo')
            if self.cleaned_data.get('overall_length_ammo', None) is None:
                self.add_error('overall_length_ammo', 'This field is required')
        elif listing_type == 'Optics':
            if self.cleaned_data.get('overall_length_optics', None) is None:
                self.add_error('overall_length_optics', 'This field is required')

        if self.cleaned_data.get('shipping_zip_code') is None or \
           self.cleaned_data.get('shipping_weight') is None or \
           self.cleaned_data.get('shipping_height') is None or \
           self.cleaned_data.get('shipping_length') is None or \
           self.cleaned_data.get('shipping_width') is None:
            self.add_error('shipping_weight', 'Shipping information is not complete')
        self.cleaned_data['upc'] = self.cleaned_data['upc'] or 'Dummy'
        self.cleaned_data['manufacturer'] = self.user.email
        return self.cleaned_data

    def save(self, commit=True):
        upc = super(ListingForm, self).save(commit=commit)
        distributor = Distributor.objects.get(name='Dealer Exchange')
        product = Product.objects.create(
            user_profile=self.user.userprofile,
            distributor=distributor,
            upc=upc,
            price=self.cleaned_data['price'],
            qty=self.cleaned_data['quantity'],
            is_active=False,
            dealer_exchange=True,
            minimum_order=self.cleaned_data['minimum_order'],
        )
        self.populate_upc(product.upc, self.cleaned_data)

        product_id = create_product_2_de(listing=product, user=self.user)
        upc.upc = product_id
        upc.save()

        pic_info_list = []

        for num in range(5):
            info = self.cleaned_data.get('img_' + str(num))
            if info is not None:
                pic_info_list.append(info)

        for pic in pic_info_list:
            new_image = ProductListingImage.objects.create(
                product=product,
            )
            new_image.image.save(
                '%s.jpg' % product.upc.upc,
                create_image(pic, 460, 400)
            )
            new_image.image_thumbnail.save(
                '%s.jpg' % product.upc.upc,
                create_image(pic, 63, 63)
            )
            new_image.save()

        create_product_2_de(listing=product, user=self.user)

        tag_list = self.cleaned_data.get('tags').split(',')
        tag_list = map(unicode.strip, tag_list)
        for tag_value in tag_list:
            if tag_value != "":
                tag, created = ProductTag.objects.get_or_create(name=tag_value)
                product.tags.add(tag)
        product_index = ProductIndex()
        product_index.update_object(product)
        return product


class ListingDetailForm(forms.ModelForm):
    tags = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': 'example: ar15, iron sights, glock holster, etc',
            'class': 'tag-input'
        })
    )
    video_url = forms.URLField(required=False, label='Video URL')
    photo_primary = forms.IntegerField(required=False)
    photo_extra = forms.ImageField(required=False)
    upc = forms.CharField(required=False)
    map = forms.CharField(required=False)
    msrp = forms.CharField(required=True)
    muzzle_velocity = forms.CharField(required=False)
    sku = forms.CharField(required=False)
    description = forms.CharField(required=True)
    category = forms.ChoiceField(
            choices=categories
            )
    listing_type = forms.ChoiceField(
            choices=UPC.LISTING_CHOICES
            )
    shipping_zip_code = forms.IntegerField()
    shipping_weight = forms.IntegerField()
    shipping_length = forms.IntegerField()
    shipping_width = forms.IntegerField()
    shipping_height = forms.IntegerField()
    qty = forms.IntegerField()

    class Meta:
        model = Product
        exclude = ['company_profile', 'salesforce_id', 'list_price', 
                'upc', 'qty_sold', 'distributor']
        labels = {
            'listing_type': 'Listing Type',
            'upc': 'UPC/ID',
            'listing_condition': forms.Select(attrs={'required':'False'}),
            'condition_detail': forms.Select(attrs={'required': 'False'}),
            'quantity': 'Quantity In-Stock',
            'sku': 'SKU',
            'msrp': 'MSRP',
            'map': 'MAP',
            'minimum_order': 'Minimum Order',
        }
    
    def clean_copy(self):
        self.cleaned_data['copy'] = None

    def clean(self):
        # if self.cleaned_data.get('photo_primary') is None or (
           # self.cleaned_data['photo_primary'] == 0 and
           # self.cleaned_data.get('photo_extra') is None):
            # self.add_error('photo_primary', 'A primary photo is required.')
        try:
            width = float(self.cleaned_data.get('shipping_width'))
            height = float(self.cleaned_data.get('shipping_height'))
            length = float(self.cleaned_data.get('shipping_length'))
            if width > length or height > length:
                self.add_error(
                    'shipping_weight',
                    'The width and height value cannot exceed the length.'
                )

            url_regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
            if len(re.findall(url_regex, self.cleaned_data.get('description'))) > 0:
                self.add_error('description', 'Remove any URL\'s from the description.')
        except ValueError:
            self.add_error(
                'shipping_weight',
                'Enter valid values for length, width, and height.'
            )
        return self.cleaned_data


    def populate_upc(self, upc, data):
        cat_name = data.get('category')
        if cat_name:
            upc.category = Category.objects.get_or_create(name=cat_name)[0]
        listing_category = data.get('listing_category') or data.get('category')
        upc.listing_condition = data.get('listing_condition')
        upc.condition_detail = data.get('condition_detail')
        upc.listing_category = listing_category or upc.listing_category
        upc.model = data.get('title') or upc.model
        upc.description = data.get('description') or upc.description
        upc.short_description = data.get('short_description') or upc.short_description
        upc.caliber = data.get('caliber') or upc.caliber
        upc.capacity = data.get('capacity') or upc.capacity
        upc.finish = data.get('finish') or upc.finish
        upc.weight = data.get('weight') or upc.weight
        upc.gun_type = data.get('type') or upc.gun_type
        upc.length_barrel = data.get('barrel_length') or upc.length_barrel
        upc.length_overall = data.get('overall_length') or upc.length_barrel
        upc.manufacturer = data.get('manufacturer') or upc.manufacturer
        upc.listing_type = data.get('listing_type') or upc.listing_type
        upc.msrp = data.get('msrp', upc.msrp)
        upc.map = data.get('map', upc.map)
        upc.estimated_ship_days = data.get(
            'estimated_ship_days', upc.estimated_ship_days
        )

        # Extra Data for Ammo
        upc.rounds = data.get('rounds') or upc.rounds
        upc.muzzle_velocity = data.get('muzzle_velocity') or upc.muzzle_velocity
        upc.length = data.get('length') or upc.length
        upc.ammo_type = data.get('ammo_type') or upc.ammo_type

        # Extra Data for Optics
        upc.magnification = data.get('magnification') or upc.magnification
        upc.field_of_view = data.get('field_of_view') or upc.field_of_view
        upc.reticle = data.get('reticle') or upc.reticle
        upc.objective_lens = data.get('objective_lens') or upc.objective_lens
        upc.eye_relief = data.get('eye_relief') or upc.eye_relief
        upc.max_windage_adjustment = data.get('max_windage_adjustment') or upc.max_windage_adjustment
        upc.max_elevation_adjustment = data.get('max_elevation_adjustment') or upc.max_elevation_adjustment
        upc.battery = data.get('battery') or upc.battery
        upc.dot_size = data.get('dot_size') or upc.dot_size

        # Extra Data for Blades
        upc.blade_type = data.get('blade_type') or upc.blade_type
        upc.blade_length = data.get('blade_length') or upc.blade_length
        upc.blade_total_length = data.get('blade_total_length') or upc.blade_total_length
        upc.blade_steel = data.get('blade_steel') or upc.blade_steel
        upc.blade_handle_material = data.get('blade_handle_material') or upc.blade_handle_material
        upc.blade_edge = data.get('blade_edge') or upc.blade_edge

        # Extra Data for Apparel
        upc.shirt_size = data.get('shirt_size') or upc.shirt_size
        upc.shirt_color = data.get('shirt_color') or upc.shirt_color

        upc.save()
        return upc

    def save(self, commit=True, userprofile=None):
        data = self.cleaned_data
        product = Product.objects.get(salesforce_id=self.cleaned_data.get('upc'))
        product.qty = data.get('qty') or product.qty
        product.minimum_order = data.get('minimum_order') or product.minimum_order
        product.video_url = data.get('video_url') or product.video_url
        product.price = data.get('price') or product.price

        self.populate_upc(product.upc, data)

        product.tags.clear()
        tag_list = self.cleaned_data.get('tags').split(',')
        tag_list = map(unicode.strip, tag_list)
        for tag_value in tag_list:
            if tag_value != "":
                tag, created = ProductTag.objects.get_or_create(name=tag_value)
                tag.product.add(product)
        product.save()
        product_id = create_product_2_de(product, user=userprofile.user)
        return product


class ImageUpdateForm(forms.ModelForm):

    class Meta:
        model = ProductListingImage
        exclude = ['image_thumbnail', 'product']

    def save(self, commit=True):
        return super(ImageUpdateForm, self).save(commit=True)

ImageFormSet = inlineformset_factory(
    Product, ProductListingImage,
    fields=['image'], extra=1, form=ImageUpdateForm
)


class ShippingInfoForm(forms.ModelForm):
    class Meta:
        model = ShippingInfo 
        exclude = ['user_profile']
        widgets = {
            'FedEx': forms.CheckboxInput(attrs={'name': 'FedEx', }),
            'UPS': forms.CheckboxInput(attrs={'name': 'UPS', }),
            'USPS': forms.CheckboxInput(attrs={'name': 'USPS', }),
            'none': forms.CheckboxInput(attrs={'name': 'None', }),
            'FedEx_drop': forms.CheckboxInput(attrs={'name': 'FedEx_drop', }),
            'UPS_drop': forms.CheckboxInput(attrs={'name': 'UPS_drop', }),
            'USPS_drop': forms.CheckboxInput(attrs={'name': 'USPS_drop', }),
            'none_drop': forms.CheckboxInput(attrs={'name': 'None_drop', }),
        }

    def clean(self):
        return self.cleaned_data

    def save(self, *args, **kwargs):
        user = args[0]
        ShippingInfo.objects.create(user_profile=user,
                                    FedEx=self.cleaned_data['FedEx'],
                                    UPS=self.cleaned_data['UPS'],
                                    USPS=self.cleaned_data['USPS'],
                                    none=self.cleaned_data['none'],
                                    FedEx_drop=self.cleaned_data['FedEx'],
                                    UPS_drop=self.cleaned_data['UPS'],
                                    USPS_drop=self.cleaned_data['USPS'],
                                    none_drop=self.cleaned_data['none'],)


class GetPaidForm(forms.ModelForm):

    class Meta:
        model = Wallet
        fields = [
                'account_number',
                'routing_number',
                ]
        widgets = {
            'account_type': forms.Select(attrs={'class': 'tob'}),
            'owner_name': forms.TextInput(attrs={'placeholder': 'Account Owner\'s Name', 'class': 'get-paid-input'}),
            'routing_number': forms.TextInput(attrs={'placeholder': 'Bank Routing Number', 'class': 'get-paid-input'}),
            'bank_name': forms.TextInput(attrs={'class': 'get-paid-input','readonly': 'True'}),
            'account_number': forms.TextInput(attrs={'placeholder': 'Bank Account Number', 'class': 'get-paid-input'}),
        }

        error_messages = {
            'routing_number': {'required': ''},
        }

    def clean(self):
        routing = self.cleaned_data.get('routing_number').replace(' ', '')
        self.cleaned_data['bank_name'] = routing
        result = verify_routing_number(routing)
        if result['bank_name'] == 'Invalid':
            self.add_error('routing_number', 'Routing number is invalid')
            return
        else:
            return self.cleaned_data

    def save(self, *args, **kwargs):
        user = args[0]
        wallet = Wallet.objects.create(
                user=user,
                wallet_type='Checking',
                routing_number=self.cleaned_data['routing_number'],
                account_number=self.cleaned_data['account_number'])
        create_payment_info(wallet)
        token, created = Token.objects.get_or_create(user=user)
