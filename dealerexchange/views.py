import json
from datetime import timedelta

from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView,
    CreateView,
    UpdateView,
)
from django.utils import timezone

from braces.views import JSONResponseMixin,LoginRequiredMixin

from django.views.generic.edit import FormView
from rest_framework import status
from rest_framework.response import Response

from accounts.models import Distributor, Wallet
from accounts.mixins import ApprovalRequiredMixin
from products.models import Product, ProductListingImage
from saasapp.models import Order, PurchaseHistory
from saasapp.forms import UploadfflForm
from saasapp.salesforce import create_account, activate_product
from .forms import (
        ListingForm, 
        ListingDetailForm, 
        ImageFormSet, 
        SignUpForm,
        ShippingInfoForm,
        GetPaidForm,
        )
from .models import ShippingInfo
from .utils import (
        is_dealer_direct, 
        set_dealer_exchange,
        create_file_path,
        )

DISTRIBUTOR = Distributor.objects.get(name='Dealer Exchange')


class SplashView(ApprovalRequiredMixin,TemplateView):
    template_name = 'dealerexchange/start.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            user_profile = self.request.user.userprofile
            if not user_profile.has_de_profile():
                return super(SplashView, self).dispatch(*args, **kwargs)
            return HttpResponseRedirect(reverse_lazy('dashboard'))
        return HttpResponseRedirect(reverse_lazy('login'))


class ShippingView(ApprovalRequiredMixin, FormView):
    template_name = 'dealerexchange/info.html'
    form_class = ShippingInfoForm

    def get_success_url(self):
        return reverse_lazy('de_get_paid')

    def form_valid(self, form):
        form.save(self.request.user.userprofile)
        return super(ShippingView, self).form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if is_dealer_direct(self.request.user.userprofile):
                set_dealer_exchange(self.request.user.userprofile)
                return HttpResponseRedirect(reverse_lazy('dashboard'))
            return super(ShippingView, self).dispatch(request, *args, **kwargs)


class SignUpView(ApprovalRequiredMixin, FormView):
    template_name = 'signup1.html'
    form_class = SignUpForm
    success_url = reverse_lazy('exchange-signup-2')

    def dispatch(self, request, *args, **kwargs):
        return super(SignUpView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        verified = form.cleaned_data['verified']
        new_user = form.create_new_user()
        if verified.get('company'):
            self.request.session['company'] = verified['company']
            send_intro_email(to_email=[new_user.email])

        user = authenticate(
            username=new_user.username,
            password=form.cleaned_data['password']
        )
        login(self.request, user)
        return super(SignUpView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        initial = self.request.COOKIES.get('username')
        kwargs['sign_up_form'] = kwargs['form']
        del kwargs['form']
        return super(SignUpView, self).get_context_data(**kwargs)




class DashboardView(ApprovalRequiredMixin, TemplateView):
    template_name = 'dealerexchange/dashboard.html'

    def get_context_data(self, **kwargs):
        profile = self.request.user.userprofile
        context = super(DashboardView, self).get_context_data(**kwargs)

        top_five = get_top_five(profile.company_name, period='ytd')
        top_five_list = [item for item in top_five]
        while len(top_five_list) != 5:
                top_five_list.append(None)

        total_sales = get_total_sales(profile.company_name, period='ytd')
        total_sales_amount = 0
        for sale in total_sales['data']:
            total_sales_amount += float(sale['y'])

        list_size = 5 - top_five_list.count(None)

        product_totals = get_product_totals(profile.company_name)

        context['active_percent'] = product_totals['percent']
        context['top_five'] = top_five_list
        context['list_size'] = list_size
        context['product_totals'] = product_totals
        context['total_orders'] = get_total_orders(
            profile.company_name, period='ytd'
        )
        context['total_sales'] = total_sales
        context['total_sales_amount'] = total_sales_amount
        return context


class OrderView(ApprovalRequiredMixin, ListView):
    template_name = 'dealerexchange/ship.html'
    context_object_name = 'order_list'

    def get_queryset(self):
        user = self.request.user
        return Order.objects.filter(user=user).order_by('-date')


class OrderDetailView(ApprovalRequiredMixin, DetailView):
    template_name = 'dealerexchange/order.html'
    context_object_name = 'order_details'

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        self.order = Order.objects.filter(
                user=user,
                id=self.kwargs.get('pk')
                )
        return self.order

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        ffl_url = self.order[0].user.userprofile.ffl_url
        context['ffl_url'] = ffl_url
        return context


class StoreView(ApprovalRequiredMixin, ListView):
    template_name = 'dealerexchange/store.html'
    context_object_name = 'listings'


    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if not self.request.user.userprofile.has_de_profile():
                return HttpResponseRedirect(reverse_lazy('splash'))
        return super(StoreView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        qs = Product.objects.filter(distributor=DISTRIBUTOR)
        return qs.filter(user_profile=user.userprofile)

    def get_context_data(self, **kwargs):
        context = super(StoreView, self).get_context_data(**kwargs)
        if self.request.GET.get('success') == "true":
            context['listing_added'] = True
        else:
            context['listing_added'] = False
        if self.request.session.get('delete_error'):
            context['delete_error'] = self.request.session.get('delete_error')
            del self.request.session['delete_error']
        return context


class CreateListingView(ApprovalRequiredMixin, CreateView):
    template_name = 'dealerexchange/listing.html'
    model = Product
    form_class = ListingForm
    
    def form_invalid(self, form):
        return super(CreateListingView, self).form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('store') + "?success=true"

    def get_form_kwargs(self):
        kwargs = super(CreateListingView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(CreateListingView, self).form_valid(form)


class UpdateListingView(ApprovalRequiredMixin, UpdateView):
    template_name = 'dealerexchange/listing_detail.html'
    form_class = ListingDetailForm
    success_url = reverse_lazy('store')

    def get_object(self):
        product = Product.objects.get(
            distributor__name='Dealer Exchange',
            pk=self.kwargs['pk']
        )
        return product.upc

    def get_initial(self):
        initial = super(UpdateListingView, self).get_initial()
        self.object = self.get_object()
        product = Product.objects.get(salesforce_id=self.object.upc)
        initial['quantity'] = product.qty
        initial['price'] = product.price
        initial['minimum_order'] = product.minimum_order
        value = ''
        for tag in product.tags.all():
            if len(value):
                value = value + ','
            value = value + tag.name
        initial['tags'] = value
        return initial

    def get_context_data(self, **kwargs):
        context = super(UpdateListingView, self).get_context_data(**kwargs)
        self.object = self.get_object()
        product = Product.objects.get(salesforce_id=self.object.upc)
        context['listing_id'] = product.id

        if self.request.POST:
            context['image_form'] = ImageFormSet(
                self.request.POST,
                self.request.FILES,
                instance=product
            )
        else:
            context['image_form'] = ImageFormSet(
                instance=product
            )
        context['photos'] = product.productlistingimage_set.all()
        context['instance'] = product
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        product = Product.objects.get(salesforce_id=self.object.upc)
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        image_form = ImageFormSet(
            self.request.POST,
            self.request.FILES,
            instance=product
        )
        if form.is_valid() and image_form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        context = self.get_context_data()
        image_form = context['image_form']
        if form.is_valid() and image_form.is_valid():
            image_form.save()
            form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self)


def delete_listing(request, pk):
    product = Product.objects.get(pk=pk)
    for image in product.productlistingimage_set.all():
        image.image.delete(False)
        image.image_thumbnail.delete(False)
    upc = product.upc
    upc.delete()
    return HttpResponse('true')


def ajax_total_orders(request):
    period = request.GET.get("period")
    user = request.user
    total_orders = get_total_orders(
        user.userprofile.company_name, period=period
    )
    return HttpResponse(total_orders, content_type='application/json')


def ajax_total_sales(request):
    period = request.GET.get("period")
    user = request.user
    total_sales = get_total_sales(
        user.userprofile.company_name, period=period
    )
    data = json.dumps(total_sales["data"])
    return HttpResponse(data, content_type='application/json')


def ajax_top_five(request):
    period = request.GET.get("period")
    user = request.user
    top_five = get_top_five(user.userprofile.company_name, period=period)
    top_five_list = [item for item in top_five]
    data = json.dumps(top_five_list)
    return HttpResponse(data, content_type='application/json')


def get_product_totals(manufacturer):
    queryset = Product.objects.filter(distributor=DISTRIBUTOR)
    total = queryset.filter(upc__manufacturer=manufacturer)
    active = total.filter(is_active=True)
    try:
        percent = int(100*float(active.count())/float(total.count()))
    except:
        percent = 0
    return {
        'active_products': active.count(),
        'total_products': total.count(),
        'percent': percent
    }


def get_top_five(manufacturer, period='ytd'):
    queryset = PurchaseHistory.objects.filter(
        manufacture=manufacturer,
        product__distributor=DISTRIBUTOR
    )
    today = timezone.now()
    today.replace(hour=0, minute=0, second=0, microsecond=0)
    if period == 'ytd':
        queryset = queryset.filter(date__year=today.year)
    else:
        past = today-timedelta(days=int(period))
        queryset = queryset.filter(date__gte=past)
    items = queryset.values('model', 'product').annotate(
        total=Sum('total_price')).order_by('-total')[:5]
    return items


def get_total_sales(manufacturer, period='ytd'):
    data = []
    today = timezone.now()
    start = timezone.now()
    start = start.replace(hour=0, minute=0, second=0, microsecond=0)
    items = PurchaseHistory.objects.filter(
        manufacture=manufacturer,
        product__distributor=DISTRIBUTOR
    )

    if period == '1':
        delta = timedelta(hours=1)
        while start <= today:
            data.append({
                'x': '%s-%s' % (
                    start.strftime('%H:%M'),
                    (start+delta).strftime('%H:%M')
                ),
                'y': items.filter(
                    date__range=(start, start+delta)
                    ).aggregate(y=Sum('total_price'))['y'] or 0
            })
            start += delta
    elif period == '7':
        start -= timedelta(days=6)
        delta = timedelta(days=1)
        while start <= today:
            data.append({
                'x': start.strftime('%m/%d'),
                'y': items.filter(
                    date__range=(start, start+delta)
                ).aggregate(y=Sum('total_price'))['y'] or 0
            })
            start += delta
    elif period == '30':
        start -= timedelta(days=29)
        delta = timedelta(days=1)
        while start <= today:
            data.append({
                'x': start.strftime('%m/%d'),
                'y': items.filter(
                    date__range=(start, start+delta)
                ).aggregate(y=Sum('total_price'))['y'] or 0
            })
            start += delta
    elif period == 'ytd':
        start = start.replace(month=1, day=1)
        month = 1
        while month <= today.month:
            right_interval = start.replace(month=min(12, month+1))
            if month == 12:
                right_interval = today
            data.append({
                'x': start.strftime('%B %Y'),
                'y': items.filter(
                    date__range=(start, right_interval)
                ).aggregate(y=Sum('total_price'))['y'] or 0
            })
            month += 1
            start = start.replace(month=min(12, month))
    return {'data': data}


def get_total_orders(manufacturer, period='ytd'):
    queryset = PurchaseHistory.objects.filter(
        manufacture=manufacturer,
        product__distributor=DISTRIBUTOR
    )
    today = timezone.now()
    today.replace(hour=0, minute=0, second=0, microsecond=0)
    if period == 'ytd':
        queryset = queryset.filter(date__year=today.year)
    else:
        past = today-timedelta(days=int(period))
        queryset = queryset.filter(date__gte=past)
    return queryset.count()


class GetPaidView(ApprovalRequiredMixin, FormView):
    template_name = 'dealerexchange/get-paid.html'
    form_class = GetPaidForm

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def form_valid(self, form):
        user = self.request.user
        form.save(user)
        set_dealer_exchange(user.userprofile)
        return super(GetPaidView, self).form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if self.request.user.userprofile.has_de_profile():
                return HttpResponseRedirect(reverse_lazy('dashboard'))
            else:
                return super(GetPaidView, self).dispatch(request, *args, **kwargs)


def activation(request, pk):
    product = Product.objects.get(pk=pk)
    response = activate_product(product)
    return HttpResponse(response)


class ListingDetailView(ApprovalRequiredMixin, UpdateView):
    model = Product
    template_name = 'dealerexchange/listing_detail.html'
    form_class = ListingDetailForm

    def get_success_url(self):
        return reverse_lazy('store')

    def get_initial(self):
        self.object = self.get_object()
        title = self.object.upc.model
        upc = self.object.upc
        value = ''
        for tag in self.object.tags.all():
            if len(value):
                value = value + ','
            value = value + tag.name
        return {
                'sku': upc.sku,
                'map': upc.map,
                'msrp': upc.msrp,
                'muzzle_velocity': upc.muzzle_velocity,
                'tags': value,
                'title': title,
                'listing_type': upc.listing_type,
                'category': upc.listing_category,
                'upc': upc,
                'shipping_zip_code': upc.shipping_zip_code,
                'shipping_height': upc.shipping_height,
                'shipping_width': upc.shipping_width,
                'shipping_length': upc.shipping_length,
                'shipping_weight': upc.shipping_weight,
                'video_url': self.object.video_url,
                'description': upc.description,
                }

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(ListingDetailView, self).get_context_data(**kwargs)
        context['listing_id'] = self.object.id

        if self.request.POST:
            context['form'] = ListingDetailForm(
                self.request.POST,
                instance=self.object,
                initial=self.get_initial()
            )
        else:
            context['form'] = ListingDetailForm(
                instance=self.object,
                initial=self.get_initial()
            )
            # context['photos'] = self.object.photos.all()
            context['photos'] = ProductListingImage.objects.\
                    filter(product=self.object).all()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        context = self.get_context_data()
        base_form = context['form']
        if base_form.is_valid():
            primary_photo = None
            photos = ProductListingImage.objects.filter(product_id=self.object.id)
            for photo in photos.all():
                primary = self.request.POST.get('photo_%s_primary' % photo.id)
                delete = self.request.POST.get('photo_%s_delete' % photo.id)
                if self.request.FILES.get('photo_%s' % photo.id):
                    photo.image = self.request.FILES.get('photo_%s' % photo.id)
                if primary:
                    if primary_photo:
                        primary_photo.primary = False
                    photo.primary = True
                else:
                    photo.primary = False
                photo.save()
                if delete and not primary:
                    photo.delete()
            if self.request.FILES.get('photo_extra'):
                primary = self.request.POST.get('photo_extra_primary', False)
                if primary_photo:
                    primary_photo.primary = False
                    primary_photo.save()
                ProductListingImage.objects.create(
                    product=self.object,
                    image=self.request.FILES.get('photo_extra'),
                )
            listing = self.get_object()
            form.save(False, self.request.user.userprofile)
            return HttpResponseRedirect(reverse_lazy('store'))
        else:
            return self.render_to_response(self)

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if not self.request.user.userprofile.has_de_profile():
                return HttpResponseRedirect(reverse_lazy('splash'))
            return super(ListingDetailView, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse_lazy('login'))


@login_required
def copy(request, pk):
    product_to_copy = Product.objects.get(pk=pk)
    product_photo = product_to_copy.photos.filter(primary=True).first() or \
        product_to_copy.photos.first()

    tag_dict_list = []
    for tag in product_to_copy.tag_set.all():
            tag_dict_list.append({'name': tag.tag})

    listing_to_copy.pk = None
    listing_to_copy.salesforce_id = None
    listing_to_copy.save()

    listing_photo.pk = None
    listing_photo.save()
    listing_photo.listing = listing_to_copy

    new_image_path = create_file_path(listing_photo, listing_photo.image.url)

    # tmp_file = StringIO(listing_photo.image.read())
    # tmp_file = ContentFile(listing_photo.image.getvalue())
    listing_photo.image.save(new_image_path, listing_photo.image, save=True)

    listing_to_copy.copy = set_copy_number(listing_to_copy)
    listing_to_copy.save()

    product_id = create_product_2_de(listing_to_copy)

    api_data = {
        'price': str(listing_to_copy.list_price),
        'qty': listing_to_copy.quantity,
        'upc': listing_to_copy.upc,
        'salesforce_id': product_id,
        'title': listing_to_copy.title,
        'category': '',
        'listing_category': listing_to_copy.category,
        'description': listing_to_copy.description,
        'name': listing_to_copy.company_profile.name,
        'manufacturer': listing_to_copy.company_profile.name
    }
    if listing_to_copy.listing_type == Listing.FIREARM:
        api_data['capacity'] = str(listing_to_copy.capacity)
        api_data['caliber'] = listing_to_copy.caliber
        api_data['finish'] = listing_to_copy.finish
        api_data['weight'] = str(listing_to_copy.weight_pounds) + \
            "lbs., " + str(listing_to_copy.weight_ounces) + "oz."
        api_data['overall_length'] = listing_to_copy.overall_length_firearm
        api_data['barrel_length'] = listing_to_copy.barrel_length
    elif listing_to_copy.listing_type == Listing.AMMO:
        api_data['weight'] = listing_to_copy.weight
        api_data['caliber'] = listing_to_copy.caliber
        api_data['rounds'] = listing_to_copy.rounds
        api_data['muzzle_velocity'] = listing_to_copy.muzzle_velocity
        api_data['ammo_type'] = listing_to_copy.ammo_type
    elif listing_to_copy.listing_type == Listing.OPTICS:
        api_data['magnification'] = listing_to_copy.magnification
        api_data['field_of_view'] = listing_to_copy.field_of_view
        api_data['reticle'] = listing_to_copy.reticle
        api_data['objective_lens'] = listing_to_copy.objective_lens
        api_data['eye_relief'] = listing_to_copy.eye_relief
        api_data['max_windage_adjustment'] = listing_to_copy.max_windage_adjustment
        api_data['max_elevation_adjustment'] = listing_to_copy.max_elevation_adjustment
        api_data['battery'] = listing_to_copy.battery
        api_data['dot_size'] = listing_to_copy.dot_size
    elif listing_to_copy.listing_type == Listing.BLADES:
        api_data['blade_type'] = listing_to_copy.blade_type
        api_data['blade_length'] = listing_to_copy.blade_length
        api_data['blade_total_length'] = listing_to_copy.blade_total_length
        api_data['blade_steel'] = listing_to_copy.blade_steel
        api_data['blade_handle_material'] = listing_to_copy.blade_handle_material
        api_data['blade_edge'] = listing_to_copy.blade_edge
    elif listing_to_copy.listing_type == Listing.APPAREL:
        api_data['shirt_size'] = listing_to_copy.shirt_size
        api_data['shirt_color'] = listing_to_copy.shirt_color

    if len(tag_dict_list) > 0:
        api_data['tags'] = tag_dict_list

    token = "Token " + listing_to_copy.company_profile.user.varmory_token
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    response = requests.post(
        settings.API_PREFIX + "add-listing/",
        headers=headers,
        data=json.dumps(api_data),
        verify=False,
    )
    print "Response:", response

    if response.status_code == 201:
        current_site = Site.objects.get_current()
        res_json = response.json()
        photo_data = {'product_id': res_json['id']}
        for photo in listing_to_copy.photos.all():
            photo_data['image_url'] = "http://" + current_site.domain \
             + photo.image.url
            requests.post(
                settings.API_PREFIX + "add-listing-image/",
                headers=headers,
                verify=False,
                data=json.dumps(photo_data)
            )
    return HttpResponseRedirect(reverse('store'))


