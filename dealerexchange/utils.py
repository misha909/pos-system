import csv
import os
import uuid

from pyforce import pyforce

from saas import settings
from saasapp.salesforce import get_account_id


def verify_routing_number(number):
    verified = False
    bank_name = 'Invalid'
    file_path = settings.ROUTING_NUMBER_CSV_FILE

    with open(file_path, 'r') as csvfile:
        file_reader = csv.reader(csvfile)
        for row in file_reader:
            if row[2] == str(number):
                bank_name = row[0]
                verified = True

        return {
                'verified': verified,
                'bank_name': bank_name
                }

        
def create_payment_info(wallet=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )     
    
    account_id = svc.query(
        "SELECT Id FROM Account WHERE Primary_Contact_Email__c ='%s' LIMIT 1"
         % (
            wallet.user.email
        )
    )
    
    payment_info = {
        'type': 'Payment_Info__c',
        'Name' : wallet.user.email,
        'Account__c': account_id[0].Id,
        'E_Check__c': wallet.account_number,
        'Routing_Number__c': wallet.routing_number,
        'Account_Type__c': wallet.wallet_type
    }

    res = svc.create(payment_info)
    if not res[0]['errors']:
        payment_info_id = res[0]['id']
    else:
        raise Exception('Payment Info creation failed {0}'.format(res[0]['errors']))

    return payment_info_id


def is_dealer_direct(user_profile):
    q = "SELECT Dealer_Direct_User__c, Dealer_Exchange_User__c from Account where id='{}' LIMIT 1"
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (
                settings.SALESFORCE_LOGIN_PASSWORD, 
                settings.SALESFORCE_SECURITY_TOKEN)
            )
    salesforce_id = get_account_id(user_profile.user)
    res = svc.query(q.format(salesforce_id))
    if res and res[0]:
        return (
                res[0]['Dealer_Direct_User__c'] 
                or 
                res[0]['Dealer_Exchange_User__c']
                )
    return False


def set_dealer_exchange(user_profile):
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (
                settings.SALESFORCE_LOGIN_PASSWORD, 
                settings.SALESFORCE_SECURITY_TOKEN)
            )

    user_profile.dealer_exchange = True
    user_profile.save()
    userprofile_dict = {
        'type': 'Account',
        'Id': user_profile.account_id,
        'Dealer_Exchange_User__c': True,
    }
    svc.update(userprofile_dict)


def create_file_path(instance, filename):
    file_ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), file_ext)
    if not os.path.isfile(os.path.join('images', filename)):
        return os.path.join('images', filename)
    else:
        return create_file_path(instance, filename)
