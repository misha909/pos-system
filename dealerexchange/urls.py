from django.conf.urls import url, patterns

import dealerexchange.views as views

urlpatterns = patterns(
    '',
    url(r'^$', views.SplashView.as_view(), name='splash'),
    # url(r'^step-2', views.SignupTwo.as_view(), name='exchange-signup-2'),
    # url(r'^upload-ffl/$', views.UploadfflView.as_view(), name='upload_ffl'),
    url(r'^dashboard/$', views.DashboardView.as_view(), name='dashboard'),
    url(r'^store/$', views.StoreView.as_view(), name='store'),
    url(r'^orders/$', views.OrderView.as_view(), name='order_list'),
    url(r'shipping/$', views.ShippingView.as_view(), name='shipping'),
    url(r'payment/$', views.GetPaidView.as_view(), name='de_get_paid'),
    url(r'^copy/(?P<pk>\d+)/$', views.copy, name='copy'),
    url(r'^listing/activation/(?P<pk>\d+)/?$', views.activation, name='activation'),
    url(r'^listing/(?P<pk>\d+)/?$', views.ListingDetailView.as_view(), name='listing_detail'),
    url(
        r'^order/(?P<pk>\d+)/$',
        views.OrderDetailView.as_view(),
        name='order_detail'
    ),
    url(
        r'^add-listing/$',
        views.CreateListingView.as_view(),
        name='listing_add'
    ),
    url(
        r'^update/(?P<pk>\d+)/$',
        views.UpdateListingView.as_view(),
        name='update'
    ),
    url(r'^delete/(?P<pk>\d+)/$', views.delete_listing, name='delete'),
    url(
        r'^get_total_orders/?$', views.ajax_total_orders,
        name='ajax_total_orders'
    ),
    url(
        r'^get_total_sales/?$', views.ajax_total_sales,
        name='ajax_total_sales'
    ),
    url(r'^get_top_five/?$', views.ajax_top_five, name='ajax_top_five'),
)
