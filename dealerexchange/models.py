from django.db import models
from accounts.models import UserProfile
# Create your models here.


class ShippingInfo(models.Model):
    user_profile = models.OneToOneField(UserProfile, related_name='shipping_info')
    FedEx = models.BooleanField(default=False)
    UPS = models.BooleanField(default=False)
    USPS = models.BooleanField(default=False)
    none = models.BooleanField(default=False)
    FedEx_drop = models.BooleanField(default=False)
    UPS_drop = models.BooleanField(default=False)
    USPS_drop = models.BooleanField(default=False)
    none_drop = models.BooleanField(default=False)

    def __unicode__(self):
        return self.company_profile.name



