from django.db import models

from .constants import RESULT_CHOICES


class ImportStat(models.Model):
    class Meta:
        db_table = 'import_stats'

    time_start = models.DateTimeField()
    time_finish = models.DateTimeField(blank=True, null=True)
    result = models.CharField(max_length=15, choices=RESULT_CHOICES)


class Log(models.Model):
    """
    TODO:
    - It seems that django models require a primary key field.
      The schema for the logs in the sql dump does not include
      a primary key.

      So this line should be added in the CREATE TABLE for logs.
      `id` int(11) NOT NULL AUTO_INCREMENT,
    """
    class Meta:
        db_table = 'logs'

    import_stat = models.ForeignKey(ImportStat, db_column='import_id')
    log_time = models.DateTimeField()
    log_text = models.TextField()
