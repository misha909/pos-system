SUCCESS = 'success'
WARNING = 'warning'
ERROR = 'error'
IN_PROGRESS = 'in progress'

RESULT_CHOICES = (
    (SUCCESS, SUCCESS),
    (WARNING, WARNING),
    (ERROR, ERROR),
    (IN_PROGRESS, IN_PROGRESS)
)

"""
Sports Constants

Possible field names returned by a category in
the Sports API for a given field.
"""
MODEL_FIELDS = (
    'model',
    'model or style',
    'firearm model',
    'gun model',
    'scope model'
)

CALIBER_FIELDS = (
    'caliber',
    'caliber or gauge',
    'bullet caliber',
    'gun caliber'
)

ACTION_FIELDS = (
    'action',
)

CAPACITY_FIELDS = (
    'capacity',
    'weight capacity',
    'rated capacity mah',
    'wick capacity'
)

FINISH_FIELDS = (
    'finish',
    'frame finish',
    'metal finish',
    'material finish',
    'finish color',
    'stock finish'
)

STOCK_FIELDS = (
    'stock',
)

SIGHTS_FIELDS = (
    'sights',
)

LENGTH_BARREL_FIELDS = (
    'barrel length',
)

LENGTH_OVERALL_FIELDS = (
    'length',
)

WEIGHT_FIELDS = (
    'weight',
)
