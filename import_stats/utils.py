import memcache

from django.conf import settings

from products.models import Product, UPC


def get_product_from_cache(data):
    """
    Update data entry from values stored in memcache
    from a given upc in data and if is_upc is True.
    
    Also store data in memcache if upc is not yet in memcache.
    Or if the value for a field is blank.
    """
    upc = data.get('upc')
    is_upc = data.get('is_upc')
    
    if is_upc and upc and upc != ' ':
        # Update values of data
        mc = memcache.Client([settings.MEMCACHED_URL], debug=0)
        product = mc.get(upc)
        
        caliber = data.get('caliber', ' ')
        capacity = data.get('capacity', ' ')
        finish = data.get('finish', ' ')
        stock = data.get('stock', ' ')
        sights = data.get('sights', ' ')
        length_barrel = data.get('length_barrel', ' ')
        length_overall = data.get('length_overall', ' ')
        weight = data.get('weight', ' ')
        
        if product:
            if not caliber or caliber == ' ':
                caliber = product.get('caliber', ' ')
                # Fix for caliber error in lipseys
                if caliber and (type(caliber) is not str and type(caliber) is not unicode):
                    caliber = ' '
            
            if not capacity or capacity == ' ':
                capacity = product.get('capacity', ' ')
            
            if not finish or finish == ' ':
                finish = product.get('finish', ' ')
            
            if not stock or stock == ' ':
                stock = product.get('stock', ' ')
            
            if not sights or sights == ' ':
                sights = product.get('sights', ' ')
            
            if not length_barrel or length_barrel == ' ':
                length_barrel = product.get('length_barrel', ' ')
            
            if not length_overall or length_overall == ' ':
                length_overall = product.get('length_overall', ' ')
            
            if not weight or weight == ' ':
                weight = product.get('weight', ' ')
        
        product_dict = {
            'caliber': caliber,
            'capacity': capacity,
            'finish': finish,
            'stock': stock,
            'sights': sights,
            'length_barrel': length_barrel,
            'length_overall': length_overall,
            'weight': weight
        }
        
        # Save product_dict to memcached
        mc.set(upc, product_dict)
        
        data['caliber'] = caliber
        data['capacity'] = capacity
        data['finish'] = finish
        data['stock'] = stock
        data['sights'] = sights
        data['length_barrel'] = length_barrel
        data['length_overall'] = length_overall
        data['weight'] = weight
    
    return data


def update_product_from_cache(product, upc, mc=None):
    if not mc:
        mc = memcache.Client([settings.MEMCACHED_URL], debug=0)
    if upc:
        data = mc.get(upc)
        if data:
            caliber = data.get('caliber', ' ')
            capacity = data.get('capacity', ' ')
            finish = data.get('finish', ' ')
            stock = data.get('stock', ' ')
            sights = data.get('sights', ' ')
            length_barrel = data.get('length_barrel', ' ')
            length_overall = data.get('length_overall', ' ')
            weight = data.get('weight', ' ')
        
            if not product.caliber or product.caliber == ' ':
                # Fix for caliber error in lipseys
                if caliber and (type(caliber) is not str and type(caliber) is not unicode):
                    caliber = ' '
                product.caliber = caliber
            else:
                caliber = product.caliber
            if not product.capacity or product.capacity == ' ':
                product.capacity = capacity
            else:
                capacity = product.capacity
            if not product.finish or product.finish == ' ':
                product.finish = finish
            else:
                finish = product.finish
            if not product.stock or product.stock == ' ':
                product.stock = stock
            else:
                stock = product.stock
            if not product.sights or product.sights == ' ':
                product.sights = sights
            else:
                sights = product.sights
            if not product.length_barrel or product.length_barrel == ' ':
                product.length_barrel = length_barrel
            else:
                length_barrel = product.length_barrel
            if not product.length_overall or product.length_overall == ' ':
                product.length_overall = length_overall
            else:
                length_overall = product.length_overall
            if not product.weight or product.weight == ' ':
                product.weight = weight
            else:
                weight = product.weight
            product.save()
            
            product_dict = {
                'caliber': caliber,
                'capacity': capacity,
                'finish': finish,
                'stock': stock,
                'sights': sights,
                'length_barrel': length_barrel,
                'length_overall': length_overall,
                'weight': weight
            }
            
            # Save product_dict to memcached
            mc.set(upc, product_dict)


def update_cache(product, mc=None):
    if not mc:
        mc = memcache.Client([settings.MEMCACHED_URL], debug=0)
    upc = None
    if product.upc:
        upc = str(product.upc.upc)
    if upc:
        data = mc.get(upc)
        
        caliber = product.caliber
        capacity = product.capacity
        finish = product.finish
        stock = product.stock
        sights = product.sights
        length_barrel = product.length_barrel
        length_overall = product.length_overall
        weight = product.weight
        
        if data:
            if not caliber or caliber.strip() == '':
                caliber = data.get('caliber', ' ')
            if not capacity or capacity.strip() == '':
                capacity = data.get('capacity', ' ')
            if not finish or finish.strip() == '':
                finish = data.get('finish', ' ')
            if not stock or stock.strip() == '':
                stock = data.get('stock', ' ')
            if not sights or sights.strip() == '':
                sights = data.get('sights', ' ')
            if not length_barrel or length_barrel.strip() == '':
                length_barrel = data.get('length_barrel', ' ')
            if not length_overall or length_overall.strip() == '':
                length_overall = data.get('length_overall', ' ')
            if not weight or weight.strip() == '':
                weight = data.get('weight', ' ')
        
        product_dict = {
            'caliber': caliber,
            'capacity': capacity,
            'finish': finish,
            'stock': stock,
            'sights': sights,
            'length_barrel': length_barrel,
            'length_overall': length_overall,
            'weight': weight
        }
        
        # Save product_dict to memcached
        mc.set(upc, product_dict)
