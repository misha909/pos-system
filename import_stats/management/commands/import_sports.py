import string
import random
import requests
import re
import csv
import os
import xml.etree.ElementTree as ET

from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import
from xml.sax.saxutils import unescape
from datetime import datetime, date
from django.utils import timezone
from django.core.management.base import BaseCommand
from django.conf import settings
from ftplib import FTP
from kitchen.text.converters import to_unicode

from products.models import UPC, Product
from import_stats.models import ImportStat, Log
from import_stats.constants import (
    MODEL_FIELDS,
    CALIBER_FIELDS,
    ACTION_FIELDS,
    CAPACITY_FIELDS,
    FINISH_FIELDS,
    STOCK_FIELDS,
    SIGHTS_FIELDS,
    LENGTH_BARREL_FIELDS,
    LENGTH_OVERALL_FIELDS,
    WEIGHT_FIELDS
)
from accounts.models import Distributor


def generate_random_name(length=20, chars=string.ascii_lowercase):
        return ''.join(random.choice(chars) for _ in range(length))


class Logger:
    def __init__(self, time_started):
        self.time_started = time_started

    def log(self, message, result='in progress', time_finished=None, distributor=None):
        print '%s - %s' %(datetime.utcnow(), message)
        import_stat = ImportStat.objects.create(
            time_start=self.time_started,
            time_finish=time_finished,
            result=result
        )
        Log.objects.create(
            import_stat=import_stat,
            log_time=timezone.now(),
            log_text=message
        )

        if result != 'in progress':
            # need to update distributor's main log
            distributor.update_result = result
            distributor.update_msg = message
            distributor.last_updated = timezone.now()
            distributor.save()


class Command(BaseCommand):
    args = ""
    help = "Imports products and UPCs from distributors."

    def handle(self,*args,**options):
        log = Logger(timezone.now())
        import_result = 'in progress'
        # First, get a list of all existing UPCs and distributors
        list_upc = set()
        upcs = UPC.objects.all()
        for u in upcs:
            list_upc.add(u.upc)
        log.log('Found %s unique UPC values in master database.' % upcs.count())
        distributor = Distributor.objects.get(name="Sport's South")
        update_date = distributor.update_date
        if not update_date:
            update_date = date(1990, 1, 1)
        
        # Create temp file to store data from each distributor
        tmp_file = '%s.%s' % (generate_random_name(), distributor.file_type)
        tmp_file_price = 'price_%s' % tmp_file
        tmp_file_cat = 'cat_%s' % tmp_file
        tmp_file_man = 'man_%s' % tmp_file
        f = open(tmp_file, 'w')
        f_price = open(tmp_file_price, 'w')
        f_cat = open(tmp_file_cat, 'w')
        f_man = open(tmp_file_man, 'w')
        
        log.log('Starting import from distributor %s' % distributor.name)
        
        # Get product xml file using DailyItemUpdate
        schema_url = 'http://www.w3.org/2001/XMLSchema'
        namespace = 'http://webservices.theshootingwarehouse.com/smart/Inventory.asmx'
        wsdl = 'http://webservices.theshootingwarehouse.com/smart/inventory.asmx?wsdl'
        imp = Import(schema_url)
        imp.filter.add(namespace)
        doctor = ImportDoctor(imp)
        client = Client(wsdl, doctor=doctor)
        log.log("Downloading data to local file %s" % tmp_file)
        try:
            response = client.service.DailyItemUpdate(
                CustomerNumber=distributor.login,
                UserName=distributor.login,
                Password=distributor.password,
                LastUpdate='%d/%d/%d' % (update_date.month, update_date.day, update_date.year),
                LastItem=settings.LAST_ITEM,
                Source=distributor.login
            )
            f.write(response.encode('utf-8'))
            log.log("Product list from distributor %s downloaded" % distributor.name)
        except Exception,e:
            log.log("Error downloading file via HTTP request: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
            import_result = 'error'
            
        # Get price xml file using OnhandUpdate
        try:
            response = client.service.OnhandUpdate(
                CustomerNumber=distributor.login,
                UserName=distributor.login,
                Password=distributor.password,
                Source=distributor.login
            )
            f_price.write(response)
            log.log("Price information from distributor %s downloaded" % distributor.name)
        except Exception, e:
            log.log("Error downloading file via HTTP request: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
            import_result = 'error'
            
        # Get category file using CategoryUpdate
        try:
            response = client.service.CategoryUpdate(
                CustomerNumber=distributor.login,
                UserName=distributor.login,
                Password=distributor.password,
                Source=distributor.login
            )
            f_cat.write(response)
            log.log("Category information from distributor %s downloaded" % distributor.name)
        except Exception, e:
            log.log("Error downloading file via HTTP request: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
            import_result = 'error'
            
        # Get manufacture file using ManufactureUpdate
        try:
            response = client.service.ManufacturerUpdate(
                CustomerNumber=distributor.login,
                UserName=distributor.login,
                Password=distributor.password,
                Source=distributor.login
            )
            f_man.write(response)
            log.log("Manufacturer information from distributor %s downloaded" % distributor.name)
        except Exception, e:
            log.log("Error downloading file via HTTP request: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
            import_result = 'error'
        
        f.close()
        f_price.close()
        f_cat.close()
        f_man.close()
        
        data = {}
        if import_result != 'error':
            # Get Manufacturers
            manufacturers = {}
            tree = ET.parse(tmp_file_man)
            root = tree.getroot()
            for item in root.findall('Table'):
                mfg_no = item.find('MFGNO').text
                mfg_name = item.find('MFGNM').text
                manufacturers[mfg_no] = mfg_name
            
            # Get Categories
            categories = {}
            tree = ET.parse(tmp_file_cat)
            root = tree.getroot()
            for item in root.findall('Table'):
                cat_no = item.find('CATID').text
                cat_name = item.find('CATDES').text
                categories[cat_no] = {
                    'category_name' :cat_name,
                    'itatr': {}
                }
                # Populate ITATR fields
                for ctr in range(0,21):
                    field_name = item.find('ATTR%d' % ctr)
                    if field_name is not None:
                        field_name = field_name.text.lower().strip()
                        if field_name in MODEL_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'model'
                        elif field_name in CALIBER_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'caliber'
                        elif field_name in ACTION_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'action'
                        elif field_name in CAPACITY_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'capacity'
                        elif field_name in FINISH_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'finish'
                        elif field_name in STOCK_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'stock'
                        elif field_name in SIGHTS_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'sights'
                        elif field_name in LENGTH_BARREL_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'length_barrel'
                        elif field_name in LENGTH_OVERALL_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'length_overall'
                        elif field_name in WEIGHT_FIELDS:
                            categories[cat_no]['itatr'][ctr] = 'weight'

            # Get price and quantity
            onhand_updates = {}
            tree = ET.parse(tmp_file_price)
            root = tree.getroot()
            for item in root:
                item_no = item.find('I')
                if item_no is not None:
                    item_no = item_no.text
                    catalog_price = item.find('P').text
                    customer_price = item.find('C').text
                    quantity = item.find('Q').text
                    onhand_updates[item_no] = {
                        'catalog_price': catalog_price,
                        'customer_price': customer_price,
                        'quantity': quantity
                    }
                
            tree = ET.parse(tmp_file)
            root = tree.getroot()
            for item in root.findall('Table'):
                item_no = item.find('ITEMNO')
                if item_no is not None:
                    item_no = item_no.text.strip()
                
                    is_upc = True
                    upc = item.find('ITUPC').text
                    if upc is None:
                        upc = item_no
                        is_upc = False
                    else:
                        upc = str(to_unicode(upc, 'utf-8').strip())
                
                    model = item.find('IMODEL').text
                    if model is None:
                        model = ''
                    else:
                        model = model.strip()
                
                    manufacturer = item.find('IMFGNO').text
                    if manufacturer is None:
                        manufacturer = ''
                    else:
                        manufacturer = manufacturer.strip()
                
                    category = item.find('CATID').text
                    if category is None:
                        category = ''
                    else:
                        category = category.strip()
                        
                    # Set default values for fields
                    caliber = ''
                    action = ''
                    capacity = ''
                    finish = ''
                    stock = ''
                    sights = ''
                    length_barrel = ''
                    length_overall = ''
                    weight = ''
                    
                    # Fill-Up fields base on category
                    category_entry = categories.get(category, None)
                    if category_entry:
                        itatr = category_entry.get('itatr', None)
                        for ctr in range(0,21):
                            # Get field name
                            if itatr.get(ctr, None):
                                field_name = itatr.get(ctr)
                                if field_name == 'caliber':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        caliber = field_value
                                elif field_name == 'action':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        action = field_value
                                elif field_name == 'capacity':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        capacity = field_value
                                elif field_name == 'finish':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        finish = field_value
                                elif field_name == 'stock':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        stock = field_value
                                elif field_name == 'sights':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        sights = field_value
                                elif field_name == 'length_barrel':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        length_barrel = field_value
                                elif field_name == 'length_overall':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        length_overall = field_value
                                elif field_name == 'weight':
                                    # Get value of field
                                    field_value = item.find('ITATR%d' % ctr).text
                                    if field_value is not None:
                                        weight = field_value
                
                    price = None
                    if onhand_updates.get(item_no):
                        price = float(onhand_updates.get(item_no).get('customer_price', '').strip())
                        if not price:
                            price = float(onhand_updates.get(item_no).get('catalog_price', '').strip())
                
                    quantity = None
                    if onhand_updates.get(item_no):
                        quantity = onhand_updates.get(item_no).get('quantity', '').strip()
                    else:
                        quantity = ''

                    upc = upc.strip()                
                    if price and upc:
                        data_entry = {
                            'upc': upc,
                            'is_upc': is_upc,
                            'item_number': item_no,
                            'model': model,
                            'manufacturer': manufacturers.get(manufacturer, None),
                            'price': price,
                            'qty': quantity,
                            'action': action,
                            'caliber': caliber,
                            'finish': finish,
                            'stock': stock,
                            'sights': sights,
                            'length_barrel': length_barrel,
                            'capacity': capacity,
                            'length_overall': length_overall,
                            'weight': weight,
                            'picture': item_no + '.jpg'
                        }

                        # Get category_name
                        category_entry = categories.get(category, None)
                        if category_entry:
                            data_entry['gun_type'] = category_entry.get('category_name', None)
                        
                        data[item_no] = data_entry

        #------------------------------------------------------------#
        # Insert Data to DB.                                         #
        # Compare UPC codes of downloaded list to existing UPC codes #
        #------------------------------------------------------------#
        log.log("Processing UPC codes.")
        count_new_upc = 0
        for key in data:
            upc_obj = None
            if not data[key]['upc'] in list_upc:
                upc_data = data[key]['upc']
                upc_obj = UPC.objects.create(
                    upc=upc_data,
                    is_upc=1 if upc_data else 0,
                    category=data[key].get('category'),
                    gun_type=data[key].get('gun_type'),
                    manufacturer=data[key].get('manufacturer'),
                    model=data[key].get('model'),
                    caliber=data[key].get('caliber'),
                    action=data[key].get('action'),
                    description=data[key].get('description'),
                    capacity=data[key].get('capacity'),
                    finish=data[key].get('finish'),
                    stock=data[key].get('stock'),
                    sights=data[key].get('sights'),
                    length_barrel=data[key].get('length_barrel'),
                    length_overall=data[key].get('length_overall'),
                    weight=data[key].get('weight'),
                    picture=data[key].get('picture')
                )
                list_upc.add(upc_data)
                log.log("Added new UPC code %s from distributor %s" % (upc_data, distributor.name))
                count_new_upc += 1
            else:
                upc_obj = UPC.objects.filter(upc=data[key]['upc'])[0]
                upc_obj.category = upc_obj.category or data[key].get('category')
                upc_obj.gun_type = upc_obj.gun_type or data[key].get('gun_type')
                upc_obj.manufacturer = upc_obj.manufacturer or data[key].get('manufacturer')
                upc_obj.model = upc_obj.model or data[key].get('model')
                upc_obj.caliber = upc_obj.caliber or data[key].get('caliber')
                upc_obj.action = upc_obj.action or data[key].get('action')
                upc_obj.description = upc_obj.description or data[key].get('description')
                upc_obj.capacity = upc_obj.capacity or data[key].get('capacity')
                upc_obj.finish = upc_obj.finish or data[key].get('finish')
                upc_obj.stock = upc_obj.stock or data[key].get('stock')
                upc_obj.sights = upc_obj.sights or data[key].get('sights')
                upc_obj.length_barrel = upc_obj.length_barrel or data[key].get('length_barrel')
                upc_obj.length_overall = upc_obj.length_overall or data[key].get('length_overall')
                upc_obj.weight = upc_obj.weight or data[key].get('weight')
                upc_obj.picture = upc_obj.picture or data[key].get('picture')
                upc_obj.save()

            try:
                Product.objects.update_or_create(
                    upc=upc_obj,
                    distributor=distributor,
                    defaults={
                        'item_number':data[key].get('item_number'),
                        'qty':data[key].get('qty'),
                        'price':data[key].get('price')
                    }
                )
            except Exception, e:
                log.log("Error adding product to database: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
                import_result = 'error'
                break
                
        log.log("UPC codes processed. Added %s new UPC codes." % count_new_upc)

        #---------------------------------------#
        # Update import results for distributor #
        #---------------------------------------#
        log.log("Parsing finished.", result='success', time_finished=timezone.now(), distributor=distributor)

        #--------------------#
        # Delete temp files. #
        #--------------------#
        os.remove(tmp_file)
        os.remove(tmp_file_price)
        os.remove(tmp_file_cat)
        os.remove(tmp_file_man)
        
        #-----------------------------#
        # All done! Finalize logging. #
        #-----------------------------#
        if import_result == 'in progress':
            import_result = 'success'
        if import_result == 'success':
            distributor.update_date = date.today()
            distributor.save()
        log.log("Import process complete. Result: %s" % import_result)
