from django.core.management.base import BaseCommand

import paramiko
import string
import random
import requests
import re
import csv
import os
import zipfile
import xml.etree.ElementTree as ET
from datetime import datetime, date
from django.utils import timezone
from ftplib import FTP
from kitchen.text.converters import to_unicode

from products.models import UPC, Product
from import_stats.models import ImportStat, Log
from accounts.models import Distributor


def generate_random_name(length=20, chars=string.ascii_lowercase):
        return ''.join(random.choice(chars) for _ in range(length))


def unzip(source_filename, dest_dir):
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            # Path traversal defense copied from
            # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                drive, word = os.path.splitdrive(word)
                head, word = os.path.split(word)
                if word in (os.curdir, os.pardir, ''): continue
                path = os.path.join(path, word)
            zf.extract(member, path)


class Logger:
    def __init__(self, time_started):
        self.time_started = time_started

    def log(self, message, result='in progress', time_finished=None, distributor=None):
        print '%s - %s' %(datetime.utcnow(), message)
        import_stat = ImportStat.objects.create(
            time_start=self.time_started,
            time_finish=time_finished,
            result=result
        )
        Log.objects.create(
            import_stat=import_stat,
            log_time=timezone.now(),
            log_text=message
        )

        if result != 'in progress':
            # need to update distributor's main log
            distributor.update_result = result
            distributor.update_msg = message
            distributor.last_updated = timezone.now()
            distributor.save()


class Command(BaseCommand):
    args = ""
    help = "Imports products and UPCs from distributors."

    def handle(self,*args,**options):
        log = Logger(timezone.now())
        import_result = 'in progress'
        # First, get a list of all existing UPCs and distributors
        list_upc = set()
        upcs = UPC.objects.all()
        for u in upcs:
            list_upc.add(u.upc)
        log.log('Found %s unique UPC values in master database.' % upcs.count())

        distributor = Distributor.objects.get(name="Jerry's Sports Center")

        log.log('Starting import from distributor %s' % distributor.name)

        # Connect to sftp server
        # Files required:
        # 1.  CatInfoEB.zip
        # 2.  ItemAvailEB.zip
        # 3.  METADataEB.txt
        host = distributor.url
        port = 8022
        transport = paramiko.Transport((host, port))
        transport.connect(
            username=distributor.login,
            password=distributor.password
        )

        sftp = paramiko.SFTPClient.from_transport(transport)

        # Get CatInfoEB.zip
        cat_info_tmp_file = "CatInfo.zip"
        cat_info_remote_location = "./%s" % cat_info_tmp_file
        cat_info_f = open(cat_info_tmp_file, 'w')
        sftp.get(cat_info_remote_location, cat_info_tmp_file)
        unzip(cat_info_tmp_file, ".")

        # Get ItemAvailEB.zip
        item_avail_tmp_file = "ItemAvailEB.zip"
        item_avail_remote_location = "./%s" % item_avail_tmp_file
        item_avail_f = open(item_avail_tmp_file, 'w')
        sftp.get(item_avail_remote_location, item_avail_tmp_file)
        unzip(item_avail_tmp_file, ".")

        # Close files
        cat_info_f.close()
        item_avail_f.close()
        sftp.close()
        transport.close()

        # Open CatInfo.txt
        cat_file = "CatInfo.txt"
        counter = 0
        counter_upc_missing = 0
        data = {}
        log.log("Parsing downloaded file %s" % cat_file)

        with open(cat_file, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                if counter <= 1:
                    counter += 1
                    continue
                is_upc = True
                item_no = row[0].upper().strip()
                try:
                    upc = re.sub('[\D]', '', str(to_unicode(row[18], 'utf-8'))).strip()
                except:
                    # No UPC provided. Use Item #
                    upc = item_no
                    is_upc = False
                    counter_upc_missing += 1

                upc = upc.strip()
                if upc:
                    pic_url = item_no + '.jpg'
                    abs_url = "https://images.ffldesign.com/images/%d/%s" % (distributor.pk, pic_url)
                    data_entry = {
                        'upc_id': 0,
                        'item_number': item_no,
                        'upc': upc,
                        'is_upc': is_upc,
                        'gun_type': row[3].strip(),
                        'manufacturer': '', # MFGNo only
                        'model': to_unicode(row[6], 'utf-8').strip(),
                        'description': to_unicode(row[7], 'utf-8').strip(),
                        'caliber': '',
                        'action': '',
                        'price': '0.00',
                        'qty': '0',
                        'capacity': '',
                        'finish': '',
                        'stock': '',
                        'sights': '',
                        'length_barrel': '',
                        'length_overall': '',
                        'weight': '',
                        'picture': abs_url
                    }

                    # Attribute fields
                    for ind in range(0,4):
                        attr_ind = (ind + 4) * 2
                        attr = row[attr_ind].strip()
                        if attr:
                            if attr == 'Caliber':
                                data_entry['caliber'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Action':
                                data_entry['action'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Capacity':
                                data_entry['capacity'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Finish':
                                data_entry['finish'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Stock':
                                data_entry['stock'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Sights':
                                data_entry['sights'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Barrel Length':
                                data_entry['length_barrel'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Overall Length':
                                data_entry['length_overall'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                            elif attr == 'Weight':
                                data_entry['weight'] = to_unicode(row[attr_ind + 1], 'utf-8').strip()
                    data[item_no] = data_entry
                    counter += 1

        # Open ItemAvailEB.zip
        item_file = "ItemAvailEB.txt"
        log.log("Parsing downloaded file %s" % item_file)

        with open(item_file, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                if counter == 0:
                    counter += 1
                    continue
                item_no = row[0].upper().strip()
                if data.get(item_no):
                    price = row[3].strip()
                    if price:
                        try:
                            float(price)
                        except ValueError:
                            price = '0.00'
                    else:
                        price = '0.00'
                    qty = row[4].strip()
                    data_entry = data.get(item_no)
                    data_entry['qty'] = qty
                    data_entry['price'] = price
                    data[item_no] = data_entry

        # Remove existing Products before inserting new ones
        log.log("Deleting existing products from distributor %s." % distributor.name)
        to_delete = Product.objects.filter(distributor=distributor)
        to_delete_count = to_delete.count()
        to_delete.delete()
        log.log("Deleted %d products from distributor %s." % (to_delete_count, distributor.name))

        log.log("Processing UPC codes.")
        count_new_upc = 0
        for key in data:
            upc_obj = None
            if data[key]['upc'] not in list_upc:
                upc_data = data[key]['upc']
                upc_obj = UPC.objects.create(
                    upc=upc_data,
                    is_upc=1 if upc_data else 0,
                    category=data[key].get('category'),
                    gun_type=data[key].get('gun_type'),
                    manufacturer=data[key].get('manufacturer'),
                    model=data[key].get('model'),
                    caliber=data[key].get('caliber'),
                    action=data[key].get('action'),
                    description=data[key].get('description'),
                    capacity=data[key].get('capacity'),
                    finish=data[key].get('finish'),
                    stock=data[key].get('stock'),
                    sights=data[key].get('sights'),
                    length_barrel=data[key].get('length_barrel'),
                    length_overall=data[key].get('length_overall'),
                    weight=data[key].get('weight'),
                    picture=data[key].get('picture'),
                )
                log.log("Added new UPC code %s from distributor %s" % (upc_data, distributor.name))
                count_new_upc += 1
            else:
                upc_obj = UPC.objects.filter(upc=data[key]['upc'])[0]
                upc_obj.category = upc_obj.category or data[key].get('category')
                upc_obj.gun_type = upc_obj.gun_type or data[key].get('gun_type')
                upc_obj.manufacturer = upc_obj.manufacturer or data[key].get('manufacturer')
                upc_obj.model = upc_obj.model or data[key].get('model')
                upc_obj.caliber = upc_obj.caliber or data[key].get('caliber')
                upc_obj.action = upc_obj.action or data[key].get('action')
                upc_obj.description = upc_obj.description or data[key].get('description')
                upc_obj.capacity = upc_obj.capacity or data[key].get('capacity')
                upc_obj.finish = upc_obj.finish or data[key].get('finish')
                upc_obj.stock = upc_obj.stock or data[key].get('stock')
                upc_obj.sights = upc_obj.sights or data[key].get('sights')
                upc_obj.length_barrel = upc_obj.length_barrel or data[key].get('length_barrel')
                upc_obj.length_overall = upc_obj.length_overall or data[key].get('length_overall')
                upc_obj.weight = upc_obj.weight or data[key].get('weight')
                upc_obj.picture = data[key].get('picture')
                upc_obj.save()

            try:
                Product.objects.create(
                    upc=upc_obj,
                    distributor=distributor,
                    item_number=data[key].get('item_number'),
                    qty=data[key].get('qty'),
                    price=data[key].get('price'),
                )
            except Exception, e:
                log.log("Error adding product to database: %s" % e,
                        result='error',
                        time_finished=timezone.now(),
                        distributor=distributor)
                import_result = 'error'
                break

        #---------------------------------------#
        # Update import results for distributor #
        #---------------------------------------#
        log.log("Parsing finished.", result='success',
                time_finished=timezone.now(),
                distributor=distributor)

        #--------------------#
        # Delete temp files. #
        #--------------------#
        os.remove(cat_info_tmp_file)
        os.remove(cat_file)
        os.remove(item_avail_tmp_file)
        os.remove(item_file)

        # All done! Finalize logging. #
        #-----------------------------#
        if import_result == 'in progress':
            import_result = 'success'
        if import_result == 'success':
            distributor.update_date = date.today()
            distributor.save()
        log.log("Import process complete. Result: %s" % import_result)
