import string
import random
import requests
import re
import csv
import os
import xml.etree.ElementTree as ET
from datetime import datetime
from cryptography.fernet import Fernet
from kitchen.text.converters import to_bytes, to_unicode
from ftplib import FTP
from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils import timezone

from products.models import UPC, Product
from import_stats.models import ImportStat, Log
from accounts.models import Distributor, UserProfile
from saas.fernet import FERNET_KEY

def generate_random_name(length=20, chars=string.ascii_lowercase):
        return ''.join(random.choice(chars) for _ in range(length))


class Logger:
    def __init__(self, time_started):
        self.time_started = time_started

    def log(self, message, result='in progress', time_finished=None, distributor=None):
        print '%s - %s' %(datetime.utcnow(), message)
        import_stat = ImportStat.objects.create(
            time_start=self.time_started,
            time_finish=time_finished,
            result=result
        )
        Log.objects.create(
            import_stat=import_stat,
            log_time=timezone.now(),
            log_text=message
        )

        if result != 'in progress':
            # need to update distributor's main log
            distributor.update_result = result
            distributor.update_msg = message
            distributor.last_updated = timezone.now()
            distributor.save()



class Command(BaseCommand):
    args = ""
    help = "Imports products and UPCs from distributors."

    def handle(self,*args,**options):
        log = Logger(timezone.now())
        import_result = 'in progress'
        # First, get a list of all existing UPCs and distributors
        list_upc = set()
        upcs = UPC.objects.all()
        for u in upcs:
            list_upc.add(u.upc)
        log.log('Found %s unique UPC values in master database.' % upcs.count())
        distributor = Distributor.objects.get(id=2)
        login = distributor.login
        password = distributor.password
        
        # ValidateDealer
        schema_url = 'http://www.w3.org/2001/XMLSchema'
        namespace = 'http://www.lipseys.com/webservice/validatedealer.asmx'
        wsdl = 'http://www.lipseys.com/webservice/validatedealer.asmx?wsdl'
        imp = Import(schema_url)
        imp.filter.add(namespace)
        doctor = ImportDoctor(imp)
        client = Client(wsdl, doctor=doctor)
        
        validated_dealer = 'N'
        try:
            response = client.service.ValidateDealer(
                Credentials={
                    'EmailAddress':login,
                    'Password':password
                }
            )
            validated_dealer = response.Success
        except Exception,e:
            log.log("Error in ValidateDealer: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
            import_result = 'error'
        
        if validated_dealer == 'Y':
            #-----------------------------------------------------#
            # First, download all relevant data from distributor  #
            #-----------------------------------------------------#

            # Create temp file to store data from each distributor
            tmp_file = '%s.%s' % (generate_random_name(), distributor.file_type)
            tmp_file_price = 'price_%s' % tmp_file
            f = open(tmp_file, 'w')
            f_price = open(tmp_file_price, 'w')
            log.log('Starting import from distributor %s' % distributor.name)
            log.log('Downloading price list from distributor %s' % distributor.name)
            http_server = distributor.url
            http_server = http_server.replace('{login}', login)
            http_server = http_server.replace('{password}', password)
            log.log("Downloading data to local file %s" % tmp_file)
            try:
                # connect to server and get data
                r = requests.get(http_server)
                f.write(r.content)
                log.log("Product list from distributor %s downloaded" % distributor.name)
            except Exception, e:
                log.log("Error downloading file via HTTP request: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
                import_result = 'error'

            if len(distributor.price_url) > 0:
                # Gotta get the price info seperately
                http_server = distributor.price_url
                http_server = http_server.replace('{login}', login)
                http_server = http_server.replace('{password}', password)
                log.log("Downloading price information to local file %s" % tmp_file_price)
                try:
                    r = requests.get(http_server)
                    f_price.write(r.content)
                    log.log("Price information from distributor %s downloaded" % distributor.name)
                except Exception, e:
                    log.log("Error downloading file via HTTP request: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
                    import_result = 'error'

            f.close()
            f_price.close()

            #----------------------------------------------------------------#
            # Next, parse downloaded price list and update db with temp data #
            #----------------------------------------------------------------#
            if import_result != 'error':
                log.log("Parsing downloaded file %s" % tmp_file)
                counter = 0
                counter_upc_missing = 0
                data = []
                tree = ET.parse(tmp_file)
                root = tree.getroot()
                for item in root.findall('Item'):
                    is_upc = True
                    upc = item.find('UPC').text
                    if upc is None:
                        # No UPC provided. Use SKU
                        upc = re.sub('[\D]', '', str(to_unicode(item.find('ItemNo').text, 'utf-8'))).strip()
                        is_upc = False
                        counter_upc_missing += 1
                    else:
                        upc = upc.strip()

                    item_number = item.find('ItemNo').text
                    if item_number is None:
                        item_number = ''
                    kind = item.find('Type').text
                    if kind is None:
                        kind = ''
                    mfg = item.find('MFG').text
                    if mfg is None:
                        mfg = ''
                    model = item.find('Model').text
                    if model is None:
                        model = ''
                    caliber = item.find('Caliber').text
                    if caliber is None:
                        caliber = ''
                    action = item.find('Action').text
                    if action is None:
                        action = ''
                    capacity = item.find('Capacity').text
                    if capacity is None:
                        capacity = ''
                    finish = item.find('Finish').text
                    if finish is None:
                        finish = ''
                    stock = item.find('StockFrame').text
                    if stock is None:
                        stock = ''
                    sights = item.find('Sights').text
                    if sights is None:
                        sights = ''
                    length_barrel = item.find('Barrel').text
                    if length_barrel is None:
                        length_barrel = ''
                    length_overall = item.find('Length').text
                    if length_overall is None:
                        length_overall = ''
                    weight = item.find('Weight').text
                    if weight is None:
                        weight = ''
                    picture = item.find('Image').text
                    if picture is None:
                        picture = ''

                    if upc:
                        # abs_url = "https://images.ffldesign.com/images/%d/%s" % (distributor.pk, picture)
                        abs_url = 'http://www.lipseys.net/images/%s' % picture
                        data_entry = {
                            'upc_id': 0,
                            'upc': upc,
                            'item_number': item_number,
                            'is_upc': is_upc,
                            'gun_type': kind,
                            'manufacturer': mfg,
                            'model': model,
                            'caliber': caliber,
                            'action': action,
                            'price': '',
                            'qty': '',
                            'capacity': capacity,
                            'finish': finish,
                            'stock': stock,
                            'sights': sights,
                            'length_barrel': length_barrel,
                            'length_overall': length_overall,
                            'weight': weight,
                            'picture': abs_url
                        }
                        data.append(data_entry)

                        counter += 1

                log.log("Parsing quantity and price information.")

                price_data = []
                tree = ET.parse(tmp_file_price)
                root = tree.getroot()
                for item in root.findall('Item'):
                    upc = item.find('UPC').text
                    price_data.append({
                        'upc': upc,
                        'qty': item.find('QtyOnHand').text,
                        'price': re.sub('/[^0-9.]/', '', item.find('Price').text)
                    })

                log.log("Adding quantity and price to price list.")
                counter_price_missing = 0
                for d in data:
                    upc = d['upc']
                    if upc in [p.get('upc', '') for p in price_data]:
                        item = [p for p in price_data if p['upc'] == upc][0]
                        if item['price'] is None or item['price'] == '' or item['price'] == ' ':
                            log.log("Item %s missing qty and/or price" % str(upc))
                            counter_price_missing += 1
                            continue
                        d['qty'] = item['qty']
                        price = item['price']
                        if price[0] == '$':
                            price = item['price'][1:]
                        d['price'] = price
                    else:
                        log.log("Item %s missing qty and/or price" % str(upc))
                        counter_price_missing += 1

                if counter_price_missing > 0:
                    log.log("Missing qty/price entries: %s" % counter_price_missing, result='warning', time_finished=timezone.now(), distributor=distributor)
                    import_result = 'warning'

                if counter_upc_missing > 0:
                    log.log('Parsed %s rows. Missing UPC entries: %s' % (counter, counter_upc_missing), result='warning', time_finished=timezone.now(), distributor=distributor)
                    import_result = 'warning'
                    
                # Remove existing Products before inserting new ones
                log.log("Deleting existing products from distributor %s." % distributor.name)
                to_delete = Product.objects.filter(distributor=distributor)
                to_delete_count = to_delete.count()
                to_delete.delete()
                log.log("Deleted %d products from distributor %s." % (to_delete_count, distributor.name))

                #------------------------------------------------------------#
                # Compare UPC codes of downloaded list to existing UPC codes #
                #------------------------------------------------------------#
                log.log("Processing UPC codes.")
                count_new_upc = 0
                for data_ent in data:
                    data_upc = data_ent['upc']
                    upc_obj = None
                    if data_upc != '' and data_upc is not None:
                        if data_upc[0] == '#':
                            data_upc = data_upc[1:]
                        if data_upc[-1] == '#':
                            data_upc = data_upc[:-1]

                    if data_upc in list_upc:
                        upc_obj = UPC.objects.filter(upc=data_upc)[0]
                        upc_obj.category = upc_obj.category or data_ent.get('category')
                        upc_obj.gun_type = upc_obj.gun_type or data_ent.get('gun_type')
                        upc_obj.manufacturer = upc_obj.manufacturer or data_ent.get('manufacturer')
                        upc_obj.model = upc_obj.model or data_ent.get('model')
                        upc_obj.caliber = upc_obj.caliber or data_ent.get('caliber')
                        upc_obj.action = upc_obj.action or data_ent.get('action')
                        upc_obj.description = upc_obj.description or data_ent.get('description')
                        upc_obj.capacity = upc_obj.capacity or data_ent.get('capacity')
                        upc_obj.finish = upc_obj.finish or data_ent.get('finish')
                        upc_obj.stock = upc_obj.stock or data_ent.get('stock')
                        upc_obj.sights = upc_obj.sights or data_ent.get('sights')
                        upc_obj.length_barrel = upc_obj.length_barrel or data_ent.get('length_barrel')
                        upc_obj.length_overall = upc_obj.length_overall or data_ent.get('length_overall')
                        upc_obj.weight = upc_obj.weight or data_ent.get('weight')
                        upc_obj.picture = data_ent.get('picture')
                        upc_obj.save()
                    else:
                        upc_obj = UPC.objects.create(
                            upc=data_ent.get('upc'),
                            is_upc=data_ent.get('is_upc'),
                            category=data_ent.get('category'),
                            gun_type=data_ent.get('gun_type'),
                            manufacturer=data_ent.get('manufacturer'),
                            model=data_ent.get('model'),
                            caliber=data_ent.get('caliber'),
                            action=data_ent.get('action'),
                            description=data_ent.get('description'),
                            capacity=data_ent.get('capacity'),
                            finish=data_ent.get('finish'),
                            stock=data_ent.get('stock'),
                            sights=data_ent.get('sights'),
                            length_barrel=data_ent.get('length_barrel'),
                            length_overall=data_ent.get('length_overall'),
                            weight=data_ent.get('weight'),
                            picture=data_ent.get('picture'),
                        )
                    if upc_obj:
                        price = None
                        try:
                            price = data_ent.get('price')
                            price = float(price)
                        except ValueError:
                            pass
                        if price:
                            try:
                                Product.objects.create(
                                    upc=upc_obj,
                                    distributor=distributor,
                                    item_number=data_ent.get('item_number'),
                                    price=data_ent.get('price'),
                                    qty=data_ent.get('qty')
                                )
                            except Exception, e:
                                log.log("Error adding product to database: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
                                import_result = 'error'
                                break
                            log.log("Added UPC code %s from distributor %s" % (data_upc, distributor.name))
                            count_new_upc += 1
                log.log("UPC codes processed. Added %s new UPC codes." % count_new_upc)                

            #---------------------------------------#
            # Update import results for distributor #
            #---------------------------------------#
            log.log("Parsing finished.", result='success', time_finished=timezone.now(), distributor=distributor)

            #--------------------#
            # Delete temp files. #
            #--------------------#
            os.remove(tmp_file)
            os.remove(tmp_file_price)

            log.log("Import from distributor %s finished" % distributor.name)

            #-----------------------------#
            # All done! Finalize logging. #
            #-----------------------------#
            if import_result == 'in progress':
                import_result = 'success'
            log.log("Import process complete. Result: %s" % import_result)
        else:
            log.log("User not Validated.", result='error', time_finished=timezone.now(), distributor=distributor)
