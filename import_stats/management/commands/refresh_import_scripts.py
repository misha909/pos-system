from django.core.management.base import BaseCommand

from accounts.models import Distributor
from products.models import Product


class Command(BaseCommand):
    args = ""
    help = "Refresh Import Scripts. Set update_date of distributors to None"

    def handle(self,*args,**options):
        # Set update_date of distributors to None
        Distributor.objects.all().update(update_date=None)
