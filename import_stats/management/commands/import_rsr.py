from django.core.management.base import BaseCommand

import string
import random
import requests
import re
import csv
import os
import xml.etree.ElementTree as ET
from datetime import datetime
from django.utils import timezone
from ftplib import FTP
from cryptography.fernet import Fernet
from kitchen.text.converters import to_bytes, to_unicode

from products.models import UPC, Product
from import_stats.models import ImportStat, Log
from accounts.models import Distributor, UserProfile
from saas.fernet import FERNET_KEY


def generate_random_name(length=20, chars=string.ascii_lowercase):
        return ''.join(random.choice(chars) for _ in range(length))


class Logger:
    def __init__(self, time_started):
        self.time_started = time_started

    def log(self, message, result='in progress', time_finished=None, distributor=None):
        print '%s - %s' %(datetime.utcnow(), message)
        import_stat = ImportStat.objects.create(
            time_start=self.time_started,
            time_finish=time_finished,
            result=result
        )
        Log.objects.create(
            import_stat=import_stat,
            log_time=timezone.now(),
            log_text=message
        )

        if result != 'in progress':
            # need to update distributor's main log
            distributor.update_result = result
            distributor.update_msg = message
            distributor.last_updated = timezone.now()
            distributor.save()



class Command(BaseCommand):
    args = ""
    help = "Imports products and UPCs from distributors."

    def handle(self,*args,**options):
        log = Logger(timezone.now())
        import_result = 'in progress'
        # First, get a list of all existing UPCs and distributors
        list_upc = set()
        upcs = UPC.objects.all()
        for u in upcs:
            list_upc.add(u.upc)
        log.log('Found %s unique UPC values in master database.' % upcs.count())
        distributor = Distributor.objects.get(id=4)
        tmp_file = '%s.%s' % (generate_random_name(), distributor.file_type)
        f = open(tmp_file, 'w')
        log.log('Starting import from distributor %s' % distributor.name)

        ftp_server = distributor.url
        ftp_user_name = distributor.login
        ftp_user_pass = distributor.password

        # Connect to server
        log.log('Establishing FTP connection to %s' % ftp_server)
        ftp = FTP(ftp_server)
        if ftp:
            log.log("FTP connection established")
            log.log("Logging in...")
            try:
                login_result = ftp.login(ftp_user_name, ftp_user_pass)
                log.log("Logged in as %s" % ftp_user_name)
                # In Python 2.1 and later, passive mode is on by default
                # ftp.set_pasv()

                # Change FTP directory, if necessary
                if distributor.file_dir != '':
                    log.log("Changing directory to \"%s\"" % distributor.file_dir)
                    ftp_dir = ftp.cwd(distributor.file_dir)
                else:
                    ftp_dir = True

                if ftp_dir:
                    try:
                        log.log("Current FTP directory is %s" % ftp.pwd())
                        log.log("Downloading data to local file %s" % tmp_file)
                        ftp.retrbinary('RETR %s' % distributor.file_name, f.write)
                        log.log("Product list from distributor %s downloaded" % distributor.name)
                    except Exception, e:
                        log.log("Error downloading file from FTP: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
                        import_result = 'error'
                else:
                    log.log("Error change FTP directory to %s" % distributor.file_dir, result='error', time_finished=timezone.now(), distributor=distributor)
                    import_result = 'error'

            except Exception, e:
                log.log("Error logging in to FTP server %s: %s" % (ftp_server, e), result='error', time_finished=timezone.now(), distributor=distributor)
                import_result = 'error'
        else:
            log.log("Error connecting to FTP server %s" % ftp_server, result='error', time_finished=timezone.now(), distributor=distributor)
            import_result = 'error'
        ftp.close()
        f.close()

        #----------------------------------------------------------------#
        # Next, parse downloaded price list and update db with temp data #
        #----------------------------------------------------------------#
        if import_result != 'error':
            log.log("Parsing downloaded file %s" % tmp_file)
            counter = 0
            counter_upc_missing = 0
            data = []
            with open(tmp_file, 'rb') as csvfile:
                reader = csv.reader(csvfile, delimiter=';')
                for row in reader:
#                    if len(row) < 76:
#                        continue

                    is_upc = True
                    try:
                        upc = re.sub('[\D]', '', str(to_unicode(row[1], 'utf-8'))).strip()
                    except:
                        # No UPC provided. Use column A
                        upc = row[0].strip()
                        is_upc = False
                        counter_upc_missing += 1
                        
                    if upc:
                        pic_url = row[14].strip()
                        abs_url = "https://images.ffldesign.com/images/%d/%s" % (distributor.pk, pic_url)
                        data_entry = {
                            'upc_id': 0,
                            'upc': upc,
                            'item_number': row[0].strip(),
                            'is_upc': is_upc,
                            'gun_type': row[9].strip(),
                            'manufacturer': row[10].strip(),
                            'model': to_unicode(row[2], 'utf-8').strip(),
                            'caliber': '',
                            'action': '',
                            'price': re.sub('/[^0-9.]/', '', row[6]),
                            'qty': row[8].strip(),
                            'capacity': '',
                            'finish': row[15].strip(),
                            'stock': '',
                            'sights': '',
                            'length_barrel': '',
                            'length_overall': '',
                            'weight': row[7].strip(),
                            'picture': abs_url
                        }
                        data.append(data_entry)

                        counter += 1
            print len(data)
            if counter_upc_missing > 0:
                log.log('Parsed %s rows. Missing UPC entries: %s' % (counter, counter_upc_missing), result='warning', time_finished=timezone.now(), distributor=distributor)
                import_result = 'warning'

            # Remove existing Products before inserting new ones
            log.log("Deleting existing products from distributor %s." % distributor.name)
            to_delete = Product.objects.filter(distributor=distributor)
            to_delete_count = to_delete.count()
            to_delete.delete()
            log.log("Deleted %d products from distributor %s." % (to_delete_count, distributor.name))

            #------------------------------------------------------------#
            # Compare UPC codes of downloaded list to existing UPC codes #
            #------------------------------------------------------------#
            log.log("Processing UPC codes.")
            count_new_upc = 0
            for data_ent in data:
                data_upc = data_ent['upc']
                upc_obj = None
                if data_upc != '' and data_upc is not None:
                    if data_upc[0] == '#':
                        data_upc = data_upc[1:]
                    if data_upc[-1] == '#':
                        data_upc = data_upc[:-1]

                if data_upc in list_upc:
                    upc_obj = UPC.objects.filter(upc=data_upc)[0]
                    upc_obj.category = upc_obj.category or data_ent.get('category')
                    upc_obj.gun_type = upc_obj.gun_type or data_ent.get('gun_type')
                    upc_obj.manufacturer = upc_obj.manufacturer or data_ent.get('manufacturer')
                    upc_obj.model = upc_obj.model or data_ent.get('model')
                    upc_obj.caliber = upc_obj.caliber or data_ent.get('caliber')
                    upc_obj.action = upc_obj.action or data_ent.get('action')
                    upc_obj.description = upc_obj.description or data_ent.get('description')
                    upc_obj.capacity = upc_obj.capacity or data_ent.get('capacity')
                    upc_obj.finish = upc_obj.finish or data_ent.get('finish')
                    upc_obj.stock = upc_obj.stock or data_ent.get('stock')
                    upc_obj.sights = upc_obj.sights or data_ent.get('sights')
                    upc_obj.length_barrel = upc_obj.length_barrel or data_ent.get('length_barrel')
                    upc_obj.length_overall = upc_obj.length_overall or data_ent.get('length_overall')
                    upc_obj.weight = upc_obj.weight or data_ent.get('weight')
                    upc_obj.picture = data_ent.get('picture')
                    upc_obj.save()
                else:
                    upc_obj = UPC.objects.create(
                        upc=data_ent.get('upc'),
                        is_upc=data_ent.get('is_upc'),
                        category=data_ent.get('category'),
                        gun_type=data_ent.get('gun_type'),
                        manufacturer=data_ent.get('manufacturer'),
                        model=data_ent.get('model'),
                        caliber=data_ent.get('caliber'),
                        action=data_ent.get('action'),
                        description=data_ent.get('description'),
                        capacity=data_ent.get('capacity'),
                        finish=data_ent.get('finish'),
                        stock=data_ent.get('stock'),
                        sights=data_ent.get('sights'),
                        length_barrel=data_ent.get('length_barrel'),
                        length_overall=data_ent.get('length_overall'),
                        weight=data_ent.get('weight'),
                        picture=data_ent.get('picture'),
                    )

                if upc_obj:
                    try:
                        Product.objects.create(
                            upc=upc_obj,
                            distributor=distributor,
                            item_number=data_ent.get('item_number'),
                            price=data_ent.get('price'),
                            qty=data_ent.get('qty')
                        )
                    except Exception, e:
                        log.log("Error adding product to database: %s" % e, result='error', time_finished=timezone.now(), distributor=distributor)
                        import_result = 'error'
                        break
                    log.log("Added UPC code %s from distributor %s" % (data_upc, distributor.name))
                    count_new_upc += 1
            log.log("UPC codes processed. Added %s new UPC codes." % count_new_upc)

        #---------------------------------------#
        # Update import results for distributor #
        #---------------------------------------#
        log.log("Parsing finished.", result='success', time_finished=timezone.now(), distributor=distributor)

        #--------------------#
        # Delete temp files. #
        #--------------------#
        os.remove(tmp_file)

        log.log("Import from distributor %s finished" % distributor.name)
        print import_result

        #-----------------------------#
        # All done! Finalize logging. #
        #-----------------------------#
        if import_result == 'in progress':
            import_result = 'success'
        log.log("Import process complete. Result: %s" % import_result)
