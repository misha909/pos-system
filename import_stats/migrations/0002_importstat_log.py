# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('import_stats', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImportStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_start', models.DateTimeField()),
                ('time_finish', models.DateTimeField(null=True, blank=True)),
                ('result', models.CharField(max_length=15, choices=[(b'success', b'success'), (b'warning', b'warning'), (b'error', b'error'), (b'in progress', b'in progress')])),
            ],
            options={
                'db_table': 'import_stats',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('log_time', models.DateTimeField()),
                ('log_text', models.TextField()),
                ('import_stat', models.ForeignKey(to='import_stats.ImportStat', db_column=b'import_id')),
            ],
            options={
                'db_table': 'logs',
            },
            bases=(models.Model,),
        ),
    ]
