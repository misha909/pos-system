var r_attachListeners = null;
r_attachListeners = function(){
    var update = document.getElementsByClassName('up')[0];
    var edit_button = document.getElementsByClassName('edit')[0];

    var address = $('#id_street_address');
    var city = $('#id_city');
    var state = $('#id_state');
    var zip_no = $('#id_zip');
    var VALID = 4;

    var edit_inputs = [
        address,
        city,
        state,
        zip_no
    ];
    var edit_form = document.getElementById('edit_address');

    function validate_zip(zip) {
        var re = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
        return re.test(zip);
    }

    function validate_city(a) {
        var re = /[a-zA-Z]/;
        return re.test(a);
    }

    var color_borders = function(a){
        var i;
        for (i=0; i<edit_inputs.length; i++){
            if(i !== a) {
                $(edit_inputs[i]).css('border', 'thin silver solid');
            }
            else {
                $(edit_inputs[i]).css('border', 'thin red solid');
            }
        }
    };


    var check = function(){
        var a = "0123456789";
        var d=4;
        var i,j;

        var input_address = $(address).val();
        var input_city = $(city).val();
        var input_zip = $(zip_no).val();

        if(validate_zip(input_zip) === false){
            d=3;
        }

        if($(state).val() === ""){
            d=2;
        }

        if(validate_city(input_city) === false){
            d=1;
        }

        if(input_address === ""){
            d=0;
        }

        return d;
    };

    function isEditFormVisible() {
        return edit_form.style.cssText === "display: block;";
    }

    function setEditFormVisible(v) {
        if(v){
            edit_form.style.cssText = "display: block;";
        }else{
            edit_form.style.cssText = "display: none;";
        }
    }

    function toggleEditForm() {
        setEditFormVisible(!isEditFormVisible());
    }

    edit_button.onclick = function(){
        toggleEditForm();
    };

    $('.btn-hide').click(function() {
        $(this).parent().addClass('hidden');
    })

    var hide_alerts = function() {
        $('.alert').addClass('hidden');
    }

    $('.up').click(function(){
        //TODO: make this more elegant
        var check_result = check();
        color_borders(check_result);
        if(check_result === VALID){
            hide_alerts();
            $('#edit_address .loading').toggleClass('hidden');
            $('#edit_address').ajaxSubmit({
                success: function(data) {
                    $('.address').load(document.URL + ' .address', function(){
                        r_attachListeners();
                        $('#edit_address .loading').toggleClass('hidden');
                        $('#alert-success').removeClass('hidden');
                        $('#alert-success .message').html('Edited billing address successfully.');
                    });
                    
                },
                error: function(data) {
                    $('#alert-error').removeClass('hidden');
                    $('#alert-error .message').html('Failed to edit billing address. Please try again.');
                    console.log('error');
                },
            });
        }
    });
};
$(document).ready(function() {
    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function sameOrigin(url) {
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            !(/^(\/\/|http:|https:).*/.test(url));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $('#edit_address').ajaxForm();
    r_attachListeners();
});