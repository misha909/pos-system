$(document).ready( function() {
    /* Requires jQuery 1.5.1 or higher, jQuery Cookie plugin */
    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('.add_card_form').ajaxForm();
    $('.add_ac_form').ajaxForm();

    var toc_ac = document.getElementsByClassName('toc_ac')[0];
    var expand = document.getElementsByClassName('expandable')[0].getElementsByTagName('img')[0];

    var add_card_form = document.getElementsByClassName('add_card_form')[0];
    var add_ac_form = document.getElementsByClassName('add_ac_form')[0];

    var add_card_button = document.getElementsByClassName('show_form')[0];
    var add_ac_button = document.getElementsByClassName('show_form')[1];

    var submit_card = document.getElementsByClassName('add_card')[0];
    var submit_ac = document.getElementsByClassName('add_card')[1];

    var cvc_no = document.getElementsByClassName('cvv')[0];
    var zip_no = document.getElementsByClassName('zip')[0];
    var nm = document.getElementsByClassName('nm')[0];

    var cc = document.getElementsByClassName('cc')[0];
    var card_inputs = $($('.add_payment')[0]).find('input[type="text"]');

    var expiration_month = $(add_card_form).find('select')[0];
    var expiration_year = $(add_card_form).find('select')[1];
    var selected_method = document.getElementsByName('selected_method');

    var ac_inputs = document.getElementsByClassName('add_ac')[0].getElementsByTagName('input');
    var ac_name = ac_inputs[0];
    var routing_number = ac_inputs[1];
    var checking_ac = ac_inputs[2];
    var checking_ac_2 = ac_inputs[3];
    var has_cards = document.getElementsByClassName('has-cards');
    var has_ac = document.getElementsByClassName('has-ac');
    var proceed_btn = document.getElementsByClassName('continue-btn')[0];

    /*console.log(add_card_form);
    console.log(add_ac_form);*/
    if(has_cards.length > 0){
        has_cards[has_cards.length-1].checked = true; // default select the last added card.
    }

    else if (has_ac.length > 0){
        has_ac[has_ac.length-1].checked = true;
    }

    var i,j;

    if(selected_method.length == 0){ //if user has no payment methods
        $(".no-payment").show();
        proceed_btn.href = "javascript:";
        proceed_btn.style.cssText = "background-color: #777; border: thin #777 solid; cursor: default;";
    }

    if(selected_method.length > 0){
        for(i=0; i<selected_method.length; i++){
            if(selected_method[i].checked == true) {
                $(".selected_method").eq(i).parents().eq(2).css({
                    'border':'thin #A2CCE9 solid',
                    'background-color':'#D2EBFC'
                });
            }
        }
    }

    function validate_zip(zip) {
        var re = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
        return re.test(zip);
    }

    function validate_cvc(a) {
        var re = /^[0-9]+$/;
        return re.test(a);
    }

    var color_borders1 = function(a){  //makes all other inputs normal except for incorrect input
        var i;
        for (i=0; i<card_inputs.length; i++){
            if(i==a){
            }

            else {
                card_inputs[i].style.cssText = "border: thin silver solid;";
            }
        }
    };

    var color_borders2 = function(a){ //makes all other inputs normal except for incorrect input
        var i;
        for (i=0; i<ac_inputs.length; i++){
            if(i==a){
            }

            else {
                ac_inputs[i].style.cssText = "border: thin silver solid;";
            }
        }
    };

    $(document).on('change', '.cards .card_row', function() {
        $(this).each(function() {
            $('.cards .card_row').css({
                'border':'thin transparent solid',
                'background-color':'transparent'
            });
            $(this).closest('.card_row').css({
                'border':'thin #A2CCE9 solid',
                'background-color':'#D2EBFC'
            });
        });
    });

    /*$(document).on('load', '#payment .form', function() {

    })*/

    expand.onclick = function(){        //expand t&cs box
        this.style.cssText = "background-color: #e2e2e2;";
        toc_ac.style.cssText = "height: 145px";
    };

    add_card_button.onclick = function(){
        $(add_card_form).css('display', 'block');
    };

    add_ac_button.onclick = function(){
        $(add_ac_form).css('display', 'block');
    };

    var check_card = function(){

        var d=0;

        var input_cvc = cvc_no.value;
        var input_zip = zip_no.value;
        var name_input = nm.value;


        if(name_input.length == 0){
            d=4;
        }

        if(validate_zip(input_zip) == false){
            d=3;
            //alert(input_zip.length);
        }

        if(validate_cvc(input_cvc) == false){
            d=2;
        }

        var result = $('.cc').validateCreditCard();
        if(result.valid == false){
            d=1;
        }

        return d;
    };

    var check_ac = function(){
        var d=0;
        var a = checking_ac.value.replace(/ /g,'');
        var b = checking_ac_2.value.replace(/ /g,'');
        $('.help_text').css('display', 'none');

        if((ac_name.value).length == 0){
            d=5;
        }

        else if((routing_number.value).length == 0){
            d=4;
        }

        else if((checking_ac.value).length == 0 || !validate_cvc(checking_ac.value)){
            $('#ac_help_text').css('display', 'block');
            d=3;
        }

        else if((checking_ac_2.value).length == 0 || !validate_cvc(checking_ac_2.value)){
            $('#ac2_help_text').css('display', 'block');
            d=2;
        }

        else if(a !== b){
            d=1;
        }

        return d;
    };

    var hide_alerts = function() {
        $('.alert').addClass('hidden');
    };

    $('.btn-hide').click(function() {
        $(this).parent().addClass('hidden');
    });

    $(submit_card).click( function(){
        if(check_card() == 0){
            color_borders1(4);
            $('.add_card_form .loading').removeClass('hidden');
            hide_alerts();
            $('.add_card_form').ajaxSubmit({
                success: function(data) {
                    $('#payment .form').load(document.URL + ' #payment .form', function(){
                        $('.add_card_form .loading').addClass('hidden');
                        if(data['status']) {
                            $('#alert-success').removeClass('hidden');
                            $('#alert-success .message').html('Added payment method successfully.');
                            $('.add_card_form').clearForm();
                            $(".no-payment").hide();
                            proceed_btn.href = "#processing-gif";
                            proceed_btn.style.cssText = "background-color: #f35600; border: thin #f35600 solid; cursor: pointer;";
                            $(".selected_method").eq(0).prop({
                                checked: true
                            });
                            if (data['sales_rep_wallet_id']) {
                                $('input.selected_method').val(data['sales_rep_wallet_id']);
                                $('a.continue-btn').click();
                                $('.proceed').click();
                            }
                        }
                        else {
                            $('#alert-error').removeClass('hidden');
                            $('#alert-error .message').html(data['message']);
                        }
                    });
                },
                error: function(data) {
                    $('.add_card_form .loading').toggleClass('hidden');
                    $('#alert-error').removeClass('hidden');
                }
            });
        }

        if(check_card() == 1){
            $(cc).css('border', 'thin red solid');
            color_borders1(1);
        }

        if(check_card() == 2) {
            cvc_no.style.cssText = "border: thin red solid;";
            color_borders1(3);
        }

        if(check_card() == 3) {
            zip_no.style.cssText = "border: thin red solid;";
            color_borders1(2);
        }

        if(check_card() == 4) {
            nm.style.cssText = "border: thin red solid;";
            color_borders1(0);
        }
    });

    $(submit_ac).click(function() {
        if(check_ac() == 0){
            color_borders2(4);
            $('.add_ac_form .loading').removeClass('hidden');
            hide_alerts();
            $('.add_ac_form').ajaxSubmit({
                success: function(data) {
                    $('#payment .form').load(document.URL + ' #payment .form', function(){
                        $('.add_ac_form .loading').addClass('hidden');
                        if(data['status']) {
                            $('#alert-success').removeClass('hidden');
                            $('#alert-success .message').html('Added checking account successfully.');
                            $('.add_ac_form').clearForm();
                            $(".no-payment").hide();
                            proceed_btn.href = "#processing-gif";
                            proceed_btn.style.cssText = "background-color: #f35600; border: thin #f35600 solid; cursor: pointer;";
                            $(".selected_method").eq(0).prop({
                                checked: true
                            });
                            if (data['sales_rep_wallet_id']) {
                                $('input.selected_method').val(data['sales_rep_wallet_id']);
                                $('a.continue-btn').click();
                                $('.proceed').click();
                            }
                        }
                        else {
                            $('#alert-error').removeClass('hidden');
                            $('#alert-error .message').html(data['message']);
                        }
                    });
                },
                error: function(data) {
                    $('.add_ac_form .loading').addClass('hidden');
                    $('#alert-error').removeClass('hidden');
                }
            });
        }

        if(check_ac() == 1){
            ac_inputs[2].style.cssText = "border: thin red solid;";
            ac_inputs[3].style.cssText = "border: thin red solid;";
            ac_inputs[0].style.cssText = "border: thin silver solid;";
            ac_inputs[1].style.cssText = "border: thin silver solid;";
        }

        if(check_ac() == 2){
            ac_inputs[3].style.cssText = "border: thin red solid;";
            color_borders2(3);
        }

        if(check_ac() == 3) {
            ac_inputs[2].style.cssText = "border: thin red solid;";
            color_borders2(2);
        }

        if(check_ac() == 4) {
            ac_inputs[1].style.cssText = "border: thin red solid;";
            color_borders2(1);
        }

        if(check_ac() == 5) {
            ac_inputs[0].style.cssText = "border: thin red solid;";
            color_borders2(0);
        }
    });

    var submit_handler = function() {
        hide_alerts();
        $('.proceed button').css({
            'background': 'rgba(243, 86, 0, 0.50) none repeat scroll 0% 0%',
            'border': 'thin solid rgba(243, 86, 0, 0.50)'
        });
        $(document).off('click', ".proceed", submit_handler);

        $('#payment-method-form').ajaxForm();
        $('#payment-method-form').ajaxSubmit({
            success: function(data) {
                if(data['status']) {
                    window.location.href = "/review/";
                }
                else {
                    $.fancybox.close();
                    $('#alert-error').removeClass('hidden');
                    $('#alert-error .message').html(data['message']);
                }
            },
            error: function(data) {
                $.fancybox.close();
                console.log(data);
            },
            complete: function() {
                 $('.proceed button').css({
                    'background': '#F35600 none repeat scroll 0% 0%',
                    'border': 'thin solid #F35600'
                });
                $(document).on('click', ".proceed", submit_handler);
            }
        });
    };

    $("#show-add-card").click(function() {
        var a = $("#show_form");
        $('html, body').animate({
            scrollTop: a.offset().top
        }, 200);
        a.click();
    });

    $("#show-add-ac").click(function() {
        var a = $("#show-ac-form");
        $('html, body').animate({
            scrollTop: a.offset().top
        }, 200);
        a.click();
    });

    $(document).on('click', ".proceed", submit_handler);
    $("a.continue-btn").fancybox({
        padding: 0,
        modal : true,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(58, 42, 45, 0.1)'
                }
            }
        },
        css : {
            'background' : 'rgba(58, 42, 45, 0.2)','opacity':'1'
        },
        afterShow: function () {
            $('.fancybox-skin').css({'box-shadow' :'none','background':'rgba(255,255,255,0.5)'});
            $('.processing').css({'background' : 'rgba(204,204,204, 0.7)','opacity' : '1'});
        }
    });

    $(document).on("click", "a.remove", function() {
        var id = $(this).data('id');
        $("#confirm-btn").data('id', id);
    });

    $("#confirm-btn").click(function() {
        $.ajax('/remove-wallet/', {
            method: 'POST',
            data: {'id': $(this).data('id')},
            success: function(data) {
                if(data['status']) {
                    $('#payment .form').load(document.URL + ' #payment .form');
                }
                else {
                    console.log(data);
                }
            },
            complete: function() {
                $.fancybox.close();
            }
        })
    });

    $("#cancel-btn").click(function() {
        $.fancybox.close();
    });

    $("a.remove").fancybox({
        padding: 0,
        modal : true,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(58, 42, 45, 0.1)'
                }
            }
        },
        css : {
            'background' : 'rgba(58, 42, 45, 0.2)','opacity':'1'
        },
        afterShow: function () {
            $('.fancybox-skin').css({'box-shadow' :'none','background':'rgba(255,255,255,0.5)'});
            $('.processing').css({'background' : 'rgba(204,204,204, 0.7)','opacity' : '1'});
        }
    });
   //window.alert = function() {}; // block script.js alerts.

});
