var distributors = [];
var user_id = 0;

function init_distributors(dists){
    if (dists.hasOwnProperty('davidson')) {
        distributors.push('davidson');
    }
    if (dists.hasOwnProperty('rsr')) {
        distributors.push('rsr');
    }
    if (dists.hasOwnProperty('lipseys')) {
        distributors.push('lipseys')
    }
    if (dists.hasOwnProperty('green')) {
        distributors.push('green');
    }
    if (dists.hasOwnProperty('zanders')) {
        distributors.push('zanders');
    }
    if (dists.hasOwnProperty('camfour')) {
        distributors.push('camfour');
    }
    if (dists.hasOwnProperty('sports')) {
        distributors.push('sports');
    }
    if (dists.hasOwnProperty('jerrys')) {
        distributors.push('jerrys');
    }
    if (dists.hasOwnProperty('ellet')) {
        distributors.push('ellet');
    }
    if (dists.hasOwnProperty('id')) {
        user_id = dists.id;
    }
    $('#'+distributors[0]+'_login').toggleClass('login-close');
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?

            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function skip(distributor){
    if (distributor === 'none'){
        $('#step3_button').attr('disabled', true);
    } else {
        $('#'+distributor+'_button').attr('disabled', true);
    }
    var index = distributors.indexOf(distributor);
    if (index === distributors.length-1){
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            type: "POST",
            url: "/start_trial/",
            headers: {"X-CSRFToken": csrftoken},
            data: {
                id: user_id
            },
            returnType: 'text',
            success: function(data){
                var path = $(location).attr('pathname');
                var url = $(location).attr('href');
                var index = url.indexOf(path);
                var new_url = url.substring(0,index) + '/product-search/';
                window.location = new_url;
            },
            error: function(data) {
                console.log(data);
            }
        });
    } else {
        $('#'+distributor+'_login').toggleClass('login-close');
    }
    $('#'+distributors[index+1]+'_login').toggleClass('login-close');
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



$( "input" ).keyup(function() {
    var value = $( this ).attr('id');
    $( "#"+value+'_alert' ).html('');
  })
  .keyup();

$( "input" ).blur(function(){
    var value = $( this ).val();
    var id = $( this ).attr('id');
    if (id == 'email'){
        var valid = validateEmail(value);
        if (valid){
            good_alert(id);
        }
        else if (value.length > 0){
            bad_alert(id);
        }
    }
    else if (id =='fname' || id =='lname') {
        if (value.length > 2){
            good_alert(id);
        }
        else if (value.length > 0){
            bad_alert(id);
        }
    }
});

$('#get_started_btn').click(function(e){
    var submit=true;
    if($('#fname').val() == 'First Name'){
        submit=false;
        bad_alert('fname');
    }
    if($('#lname').val() == 'Last Name'){
        submit=false;
        bad_alert('lname');
    }
    if($('#email').val() == 'Email'){
        submit=false;
        bad_alert('email');
    }

    if($('#password').val() == 'Password'){
        submit=false;
        bad_alert('password');
    }

    if(submit){
        $('#signup1').submit();
    }

});

$('#fflVerificationSubmit').click(function(e){
    $('#fflVerificationSubmit').attr("disabled", true);
    $('#fflVerification').submit();
});

$("#davidson_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#davidson_button').attr("disabled", true);
    $("#davidson_error").html('');
    davidson_uname = $("#davidson_uname").val();
    davidson_pwd = $("#davidson_pwd").val();
    davidson_id = user_id;

    $.ajax({
        type: "POST",
        url: "/davidson/",
        data: { uname: davidson_uname, password: davidson_pwd, id: davidson_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hide');
                $('#davidson_button').attr("disabled", false);
                $("#davidson_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#davidson_button').attr("disabled", false);
                $("#davidson_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('davidson');
            }
        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#davidson_button').attr("disabled", false);
            $('#davidson_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#lipseys_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#lipseys_button').attr("disabled", true);
    $("#lipseys_error").html('');
    lipseys_uname = $("#lipseys_uname").val();
    lipseys_pwd = $("#lipseys_pwd").val();
    lipseys_id = user_id;

    $.ajax({
        type: "POST",
        url: "/lipseys/",
        data: { uname: lipseys_uname, password: lipseys_pwd, id: lipseys_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hide');
                $('#lipseys_button').attr("disabled", false);
                $("#lipseys_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#lipseys_button').attr("disabled", false);
                $("#lipseys_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('lipseys');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#lipseys_button').attr("disabled", false);
            $('#lipseys_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#green_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#green_button').attr("disabled", true);
    $("#green_error").html('');
    green_uname = $("#green_uname").val();
    green_pwd = $("#green_pwd").val();
    green_id = user_id;

    $.ajax({
        type: "POST",
        url: "/greens/",
        data: { uname: green_uname, password: green_pwd, id: green_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hide');
                $('#green_button').attr("disabled", false);
                $("#green_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#green_button').attr("disabled", false);
                $("#green_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('green');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#green_button').attr("disabled", false);
            $('#green_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#rsr_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#rsr_button').attr("disabled", true);
    $("#rsr_error").html('');
    rsr_uname = $("#rsr_uname").val();
    rsr_pwd = $("#rsr_pwd").val();
    rsr_id = user_id;

    $.ajax({
        type: "POST",
        url: "/rsr/",
        data: { uname: rsr_uname, password: rsr_pwd, id: rsr_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hidden');
                $('#rsr_button').attr("disabled", false);
                $("#rsr_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#rsr_button').attr("disabled", false);
                $("#rsr_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('rsr');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#rsr_button').attr("disabled", false);
            $('#rsr_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#zanders_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#zanders_button').attr("disabled", true);
    $("#zanders_error").html('');
    zanders_uname = $("#zanders_uname").val();
    zanders_pwd = $("#zanders_pwd").val();
    zanders_id = user_id;

    $.ajax({
        type: "POST",
        url: "/zanders/",
        data: { uname: zanders_uname, password: zanders_pwd, id: zanders_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hidden');
                $('#zanders_button').attr("disabled", false);
                $("#zanders_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#zanders_button').attr("disabled", false);
                $("#zanders_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('zanders');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#zanders_button').attr("disabled", false);
            $('#zanders_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#camfour_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#camfour_button').attr("disabled", true);
    $("#camfour_error").html('');
    camfour_uname = $("#camfour_uname").val();
    camfour_pwd = $("#camfour_pwd").val();
    camfour_id = user_id;

    $.ajax({
        type: "POST",
        url: "/camfour/",
        data: { uname: camfour_uname, password: camfour_pwd, id: camfour_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hidden');
                $('#camfour_button').attr("disabled", false);
                $("#camfour_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#camfour_button').attr("disabled", false);
                $("#camfour_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('camfour');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#camfour_button').attr("disabled", false);
            $('#camfour_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#sports_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#sports_button').attr("disabled", true);
    $("#sports_error").html('');
    sports_uname = $("#sports_uname").val();
    sports_pwd = $("#sports_pwd").val();
    sports_id = user_id;

    $.ajax({
        type: "POST",
        url: "/sports/",
        data: { uname: sports_uname, password: sports_pwd, id: sports_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hidden');
                $('#sports_button').attr("disabled", false);
                $("#sports_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#sports_button').attr("disabled", false);
                $("#sports_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('sports');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#sports_button').attr("disabled", false);
            $('#sports_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#jerrys_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#jerrys_button').attr("disabled", true);
    $("#jerrys_error").html('');
    jerrys_uname = $("#jerrys_uname").val();
    jerrys_pwd = $("#jerrys_pwd").val();
    jerrys_id = user_id;

    $.ajax({
        type: "POST",
        url: "/jerrys/",
        data: { uname: jerrys_uname, password: jerrys_pwd, id: jerrys_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hidden');
                $('#jerrys_button').attr("disabled", false);
                $("#jerrys_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#jerrys_button').attr("disabled", false);
                $("#jerrys_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('jerrys');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#sports_button').attr("disabled", false);
            $('#sports_error').html('There was an error. Please try again or skip.');
        }
    });
});

$("#ellet_button").on("click", function () {
    // $('.popup').toggleClass("hide");
    $('#ellet_button').attr("disabled", true);
    $("#ellet_error").html('');
    ellet_uname = $("#ellet_uname").val();
    ellet_pwd = $("#ellet_pwd").val();
    ellet_id = user_id;

    $.ajax({
        type: "POST",
        url: "/ellet/",
        data: { uname: ellet_uname, password: ellet_pwd, id: ellet_id},
        returnType: 'text',
        success: function (data) {
            console.log("success");
            console.log(data);
            if(data == 1){
                // $(".popup").toggleClass('hidden');
                $('#ellet_button').attr("disabled", false);
                $("#ellet_error").html('Please Enter Both User Name and Password');

            }
            else if(data == 2){
                // $(".popup").toggleClass('hide');
                $('#ellet_button').attr("disabled", false);
                $("#ellet_error").html('Please enter the correct User Name or Password')
            }
            else
            {
                // $(".checked").toggleClass('hidden');
                skip('ellet');
            }

        },
        error: function (data) {
            console.log("error");
            console.log(data);
            $('#ellet_button').attr("disabled", false);
            $('#ellet_error').html('There was an error. Please try again or skip.');
        }
    });
});
