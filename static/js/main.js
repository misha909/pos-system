$(document).ready(function() {
	$("#content.checks input[type='checkbox']").crfi();
	$('[placeholder]').each(function() {
		var input = $(this);
		if (input.attr("type") == "password") {
			input.focus(function(){
				if (input.val() == input.attr('placeholder')) {
					input.val('').attr('type', "password").removeClass("placeholder");
				}
			});

			input.blur(function(){
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.attr('type', "text").val(input.attr('placeholder')).addClass("placeholder");
				}
			});
		} else {

			input.focus(function(){
				if (input.val() == input.attr('placeholder')) {
					input.val('').removeClass("placeholder");
				}
			});

			input.blur(function(){
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.val(input.attr('placeholder')).addClass("placeholder");
				}
			});
		};
	}).blur();

	 $('#form').validate(
	 {

  highlight: function(element) {
    $(element).closest('fieldset').removeClass('success').addClass('error');
  },
	 	success: function(element) {
		    element.addClass('valid')
		    .closest('fieldset').removeClass('error').addClass('success');
	  },
	 	invalid: function(element) {
		    element.addClass('error')
		    .closest('fieldset').removeClass('success').addClass('error');
	  }
	 });
});
