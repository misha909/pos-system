$(document).ready( function(){

    $(document).on('click', '.wrapper.detail_modal .wrapper #img-slider-next', function() {
        var j, k=0;
        var img_carousel = $(this).closest('.wrapper.detail_modal .img_slider').find('.img_carousel');
        var thumb = $(this).closest('.wrapper.detail_modal .img_slider').find('.thumbnail_carousel');
        var thumbnails = $(this).closest('.wrapper.detail_modal .img_slider').find('.thumbnails .img_div');

        function thumbnail_clear_highlight() {
            for(j=0; j<thumbnails.length; j++) {
                $(thumbnails[j]).css('border', 'thin #E2E2E2 solid');
            }
        }

        function current_image() {
            var current = $(img_carousel).data('current');
            if(current === undefined) {
                $(img_carousel).data('current', 0);
                current = 0;
            }
            return current;
        }

        var ind = current_image();

        if(ind<thumbnails.length-1) {
            $(img_carousel).data('current', ind+1);
            img_carousel.animate({
                marginLeft: "-=460px"
            }, 800, "easeInOutExpo", function() {
                // Animation complete.
                thumbnail_clear_highlight();
                $(thumbnails[ind+1]).css('border', 'thin #044A7A solid');
                if(ind < thumbnails.length && ind%3 === 2) {
                    $(thumb).animate({
                        marginLeft: "-=219px"
                    }, 300, "easeInOutExpo");
                }
            });
        }
    });

    $(document).on('click', '.detail_modal .wrapper #img-slider-back', function() {
        var j, k=0;
        var img_carousel = $(this).closest('.detail_modal .img_slider').find('.img_carousel');
        var thumb = $(this).closest('.detail_modal .img_slider').find('.thumbnail_carousel');
        var thumbnails = $(this).closest('.detail_modal .img_slider').find('.thumbnails .img_div');

        function thumbnail_clear_highlight() {
            for(j=0; j<thumbnails.length; j++) {
                $(thumbnails[j]).css('border', 'thin #E2E2E2 solid');
            }
        }

        function current_image() {
            var current = $(img_carousel).data('current');
            if(current === undefined) {
                $(img_carousel).data('current', 0);
                current = 0;
            }
            return current;
        }

        var ind = current_image();

        if(ind>0) {
            $(img_carousel).data('current', ind-1);
            img_carousel.animate({
                marginLeft: "+=460px"
            }, 800, "easeInOutExpo", function() {
                // Animation complete.
                thumbnail_clear_highlight();
                $(thumbnails[ind-1]).css('border', 'thin #044A7A solid');
                if(ind > 0 && ind%3 === 0) {
                    $(thumb).animate({
                        marginLeft: "+=219px"
                    }, 300, "easeInOutExpo");
                }
            });
        }
    });

    $(document).on('click', '.detail_modal .wrapper .add-btn', function() {
        var re = /^[1-9][0-9]*$/;
        var product_id = $(this).attr('id');
        var user_quantity = $(this).siblings('.npq').find('.qty input').val();
        var available_quantity = $(this).siblings('.npq').find('.qty span').data('qty');

        if(re.test(user_quantity) && parseInt(user_quantity) <= parseInt(available_quantity)) {
            $(this).siblings('.npq').find('.qty input').css('border', 'thin solid silver');
            $.ajax({
                type: "GET",
                url: "/cart-add-product/",
                data: {
                    product_id: product_id,
                    user_quantity: user_quantity,
                },
                returnType: 'text',
                success: function(data) {
                    $('#shopping-cart-header').load(document.URL + ' #shopping-cart-link');
                    $.fancybox.close();
                },
                error: function(data) {
                    alert("Something went wrong!");
                }
            });
        }
        else {
            $(this).siblings('.npq').find('.qty input').css('border', 'thin solid red');
        }
    });
});