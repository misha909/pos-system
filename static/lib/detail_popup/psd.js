function has_representative_email(add_button, supplier) {
    if (supplier == "Davidson's") {
        email = add_button.data('davidson');
    }
    else if (supplier == "Lipseys") {
        email = add_button.data('lipseys');
    }
    else if (supplier == "Green Supply") {
        email = add_button.data('green');
    }
    else if (supplier == "RSR Group") {
        email = add_button.data('rsr');
    }
    else if (supplier == "Gzanders") {
        email = add_button.data('zanders');
    }
    else if (supplier == "Camfour") {
        email = add_button.data('camfour');
    }
    else if (supplier == "Sport's South") {
        email = add_button.data('sports');
    }
    else if (supplier == "Jerry's Sports Center") {
        email = add_button.data('jerrys');
    }
    else if (supplier == "Ellet") {
        email = add_button.data('ellet');
    }
    return email != ""
}

function reset_search_params() {
    console.log('reset search params');
    $('#drpOrderByPrice').val("DESC");
    $('#drpDistributors').val("");
    $('#drpManufactures').val("");
    $('#drpCategories').val("");
    $('#drpCaliber').val("");
    $('#instock').prop("checked", false);
}

function prepare_search_results() {
    $(".detail_modal").each(function() {
        $(".exit", this).click(function(){
            $.fancybox.close();
        });

        $(".pos", this).click(function(){
            var product = $(this).data("product");
            var view = $(this).data("view");
            if (view == 'list'){
                var counter = $("#counter_" + product).text();
            } else {
                var counter = $("#grid_counter_" + product).text();
            }
            var a = parseInt(counter);
            console.log("Counter:", counter);
            console.log("Product:", product);
            if (a >= 1) {
                $("#counter_" + product).text('' + (a + 1));
            }
        });

        $(".neg", this).click(function(){
            var product = $(this).data("product");
            var counter = $("#counter_" + product).text();
            var a = parseInt(counter);
            console.log("Counter:", counter);
            console.log("Product:", product);
            if (a > 1) {
                $("#counter_" + product).text('' + (a - 1));
            }
        });

        $(".upc_link", this).click(function(event){
            event.preventDefault();

            var product = $(this).data("product");
            var upc = $(this).data("upc");
            //$("#search_name").val(upc);
            //$('#search_button').click();
            //$.fancybox.close();
            
            // $.ajax({
            //     type: "POST",
            //     url: "/advance-search/",
            //     data: {
            //         q: search_term,
            //         order_by: OrderBy,
            //         distributors: Distributors,
            //         manufacturers: Manufactures,
            //         category: Category,
            //         caliber: Caliber,
            //         in_stock: InStock,
            //         page: page,
            //         is_customer_view: customerView,
            //     },

            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "/advance-search/");

            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("name", "q");
            hiddenField.setAttribute("value", upc);
            hiddenField.setAttribute("type", "hidden");
            form.appendChild(hiddenField);

            document.body.appendChild(form);

            form.submit();
        });

        $(".qty_2", this).click(function(){
            var product = $(this).data("product");
            var stock = $(this).data("qty");
            var counter = parseInt($("#counter_" + product).text());
            var valid = true;

            if (stock == "None" || stock == "ALLOCATED") {
                stock = "0";
            }
            //Validations
            if (stock == "0") {
                alert('Please select product which are available..See QTY Field');
                valid = false;
            }
            else if (stock != "99+") {
                var stockInt = parseInt(stock);
                if (counter > stockInt) {
                    alert('Number of Quantities you want to order are not available in the database');
                    valid = false;
                }
            }

            if(valid) {
                //Proceed with adding to cart
                $.ajax({
                    type: "GET",
                    url: "/cart-add-product/",
                    data: { product_id : product, user_quantity : counter},
                    returnType: 'text',
                    success: function (data) {
                        $('#shopping-cart-header').load(document.URL + ' #shopping-cart-link');
                        $('#shopping-cart').load(document.URL + ' #shopping-cart');
                        $.fancybox.close();
                    },
                    error: function (data) {
                        $.fancybox.close();
                    }
                });
            }
        });
    });
}

$(document).ready(function () {
    prepare_search_results();
    $(".fancybox").fancybox();
});
