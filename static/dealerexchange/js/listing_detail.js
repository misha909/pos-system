window.onload=function() {
    var listing_type_select = document.getElementById("id_listing_type");
    var ammo_fields = document.getElementById("ammo-fields");
    var firearm_fields = document.getElementById("firearm-fields");
    var optics_fields = document.getElementById("optics-fields");
    var apparel_fields = document.getElementById("apparel-fields");
    var blade_fields = document.getElementById("blade-fields");

    var primary_buttons = document.getElementsByClassName('photo-primary-button');
    var change_image_btns = document.getElementsByClassName('change-photo');
    var cost = document.getElementById('id_price');
    var price = document.getElementById('id_list_price');
    var list_price = document.getElementById('list_price_value');
    var tag_input = document.getElementsByClassName('tag-input')[0];
    var tags_list = document.getElementById('tag-list');
    var tags_left = document.getElementsByClassName('left')[0];

    for(var i=0; i<change_image_btns.length; i++){
        var btn = change_image_btns[i];
        btn.addEventListener('change', function(event){
            if(event.target.files && event.target.files[0]){
                var photo_id = event.target.name;
                var img = document.getElementById(photo_id);
                var reader = new FileReader();
                reader.onload = function(e){
                    img.src = e.target.result;
                    img.style.width = '145px';
                    img.style.height = '105px';
                }
                reader.readAsDataURL(event.target.files[0])
            }
        });
    }

    for(var i=0; i<primary_buttons.length; i++){
        var btn = primary_buttons[i];
        btn.addEventListener('change', function(e){
            if(e.target.checked){
                for(var s=0; s<primary_buttons.length; s++){
                    primary_buttons[s].checked = false;
                }
                e.target.checked = true;
            }
        });
    }


    listing_type_select.addEventListener("change", function () {
        ammo_fields.style.display = "";
        firearm_fields.style.display = "";
        optics_fields.style.display = "";
        apparel_fields.style.display = "";

        var chosenoption = listing_type_select.options[listing_type_select.selectedIndex];
        if (chosenoption.value === "Ammo") {
            ammo_fields.style.display = "block";
        } else if (chosenoption.value === "Firearm") {
            firearm_fields.style.display = "block";
        } else if (chosenoption.value === "Optics") {
            optics_fields.style.display = "block";
        } else if (chosenoption.value === "Apparel") {
            apparel_fields.style.display = "block";
        } else if (chosenoption.value === "Blades") {
            blade_fields.style.display = "block";
        }
    });

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    costHandler = function(){
        var value = (1.135 * cost.value.replace(',','')).toFixed(2); // replace commas in cost value before calculation
        list_price.innerHTML = numberWithCommas(value);
        price.value = value;
    };

    cost.oninput = costHandler;

    cost.onblur = function(){
      this.value = numberWithCommas(this.value);
    };

    costHandler();

    function tag_counter(){
        var tags_array = tag_input.value.split(",");
        var array_length = tags_array.length;
        if (array_length > 10) {
            tag_input.value = tag_input.value.substring(0, tag_input.value.lastIndexOf(','));
            tags_array = tags_array.splice(0, 10);
            array_length -= 1;
        }
        if (tags_array[array_length - 1].trim() === "") {
            array_length -= 1;
        }
        tags_left.innerHTML = 10 - array_length + " left";
        var a = "";
        for (i=0; i<array_length; i++) {
            if (tags_array[i] !== "") {
                a += "<span><button type='button' class='tag-name'>" + tags_array[i] + "</button></span>";
            }
        }
        tags_list.innerHTML = a;
    }
    tag_counter();

    tag_input.oninput = function(){
        tag_counter();
    };
};
