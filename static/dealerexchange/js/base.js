jQuery(function() {

    var logo = document.getElementById('logo');

    var arrow_menu = document.getElementsByClassName('arrow-menu');
    var nav_bar = document.getElementsByClassName('top-menu-full-width')[0];
    var header_right = document.getElementsByClassName('header_right')[0];

    var dropdown_menu_button = document.getElementsByClassName('dropdown-menu-button')[0];
    var dashboard_header_dropdown = document.getElementsByClassName('dashboard-header-dropdown')[0];

    if(arrow_menu.length == 1){
        nav_bar.style.cssText="display: none;";
        header_right.style.cssText="display: none;";
    }

    /*function setCookie(cname,cvalue) {
        var d = new Date();
        d.setTime(d.getTime() + (30*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    function checkCookie() {
        var beta_cookie = getCookie("close_note");
        if (beta_cookie == "") {
            beta_note.style.cssText="display: block;";
        } else {
            beta_note.style.cssText="display: none;";
        }
    }*/

    /*function delete_cookie( name ) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }*/


    logo.onclick = function () {
        window.location = "/dealerexchange/dashboard/";
    };

    /*if(document.getElementById('top_menu')){
        var dropdown_menu_button = document.getElementsByClassName('dropdown-menu-button')[0];
        var dashboard_header_dropdown = document.getElementsByClassName('dashboard-header-dropdown')[0];

        dropdown_menu_button.onmouseover = function(){
            dashboard_header_dropdown.style.cssText = "display:block";
            dropdown_menu_button.getElementsByTagName('img')[0].src = "/static/account/images/hamburger-active.png";
        };

        dashboard_header_dropdown.onmouseleave = function(){
            this.style.cssText = "display: none;";
            dropdown_menu_button.getElementsByTagName('img')[0].src = "/static/account/images/hamburger.png";
        };
    }*/
    function show_menu(){

        if(getComputedStyle(dashboard_header_dropdown,null).display == "none"){
            //dashboard_header_dropdown.style.cssText = "display:block";
            //dropdown_menu_button.style.cssText = "background: url(/static/images/hamburger-active.png) no-repeat;";
            //alert(0);
            $( ".dashboard-header-dropdown" ).animate({
                display: "block",
                height: "toggle"
            }, 150);
            dropdown_menu_button.innerHTML = "&#215";
            dropdown_menu_button.style.cssText = "background: transparent; margin-top: -14px;";

        }
        else {
            //dashboard_header_dropdown.style.cssText = "display:none";
            $( ".dashboard-header-dropdown" ).animate({
                display: "none",
                height: "toggle"
            }, 150);
            dropdown_menu_button.style.cssText = "background: url(/static/dealerexchange/images/hamburger.png) no-repeat;";
            dropdown_menu_button.innerHTML = "";
            //alert(1);
        }
    }

    $('html').click(function() {
        $('.dashboard-header-dropdown').css('display', 'none');
        dropdown_menu_button.style.cssText = "background: url(/static/dealerexchange/images/hamburger.png) no-repeat;";
        dropdown_menu_button.innerHTML = "";
    });

    $('.menu-dd').click(function(event){
        event.stopPropagation();
    });

    dropdown_menu_button.addEventListener("click", show_menu, false);

});


