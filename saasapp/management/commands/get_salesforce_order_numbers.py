from django.core.management.base import BaseCommand
from django.conf import settings

import pyforce

from saasapp.models import Order
from saasapp.salesforce import get_opportunity


class Command(BaseCommand):
    args = ""
    help = "Get missing Salesforce Order Numbers"

    def handle(self, *args, **options):
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            '%s%s' % (settings.SALESFORCE_LOGIN_PASSWORD,
                      settings.SALESFORCE_SECURITY_TOKEN)
        )
        qs = Order.objects.filter(
            order_number__isnull=True,
            salesforce_id__isnull=False
        ).order_by('-date')
        for order in qs:
            res = get_opportunity(order, svc=svc)
            if res:
                order.order_number = res['Order_Number__c']
                order.save()
