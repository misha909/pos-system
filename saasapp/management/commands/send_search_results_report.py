from django.core.management.base import BaseCommand

from accounts.utils import send_search_result_report
from saasapp.models import SearchHistory


class Command(BaseCommand):
    args = "<email1>, <email2>, ..."
    help = "Create html report for search results and send it via email."

    def handle(self,*args,**options):
        to_email = args
        if to_email:
            send_search_result_report(to_email=to_email)
