import requests
import ssl
import xml.etree.ElementTree as ET
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager

from django.conf import settings
from django.template.loader import render_to_string

from .constants import (
    CREDIT_CARD,
    BANK_ACCOUNT
)


class MyAdapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections,
                                       maxsize=maxsize,
                                       block=block,
                                       ssl_version=ssl.PROTOCOL_TLSv1)


def get_transaction_key():
    from .models import TsysLog # For circular imports
    
    xml_string = render_to_string( 'saas_app/tsys/generate_key.xml', {
        'merchant_id': settings.MERCHANT_ID,
        'user_id': settings.TA,
        'password': settings.TSYS_PASSWORD,
        'developer_id': settings.DEVELOPER_ID
    })

    print 'Transaction Key Request:\n', xml_string

    req = requests.Session()
    req.mount('https://', MyAdapter())
    response = req.post("%s/%s" % (
            settings.TSYS_DOMAIN,
            settings.TSYS_API_URL
        ),
        headers={'Content-Type': 'text/xml'},
        data=xml_string
    )

    print "Transaction Key Response:\n", response.text

    transaction_key = None

    root = ET.fromstring(response.text)
    if root.find('status').text == "PASS":
        transaction_key = root.find('transactionKey').text
    
    # Log Card Verification
    TsysLog.objects.create(
        name='Generate Key',
        request=xml_string,
        response=response.text,
        is_error=not transaction_key
    )
    
    return transaction_key


def verify_card(card_holder_name=None,
                card_number=None,
                cvv=None,
                expiration_month=None,
                expiration_year=None):
    from .models import TsysLog # For circular imports
    
    transaction_key = get_transaction_key()
    
    if expiration_month:
        expiration_month = int(expiration_month)

    xml_string = render_to_string( 'saas_app/tsys/verify_card.xml', {
        'merchant_id': settings.MERCHANT_ID,
        'device_id': settings.DEVICE_ID,
        'transaction_key': transaction_key,
        'user_id': settings.TA,
        'password': settings.TSYS_PASSWORD,
        'card_number': card_number,
        'card_holder_name': card_holder_name,
        'cvv': cvv,
        'expiration_month': expiration_month,
        'expiration_year': expiration_year
    })

    print 'Card Verification Request:\n', xml_string

    req = requests.Session()
    req.mount('https://', MyAdapter())
    response = req.post("%s/%s" % (
            settings.TSYS_DOMAIN,
            settings.TSYS_API_URL
        ),
        headers={'Content-Type': 'text/xml'},
        data=xml_string
    )

    print "Card Verification Response:\n", response.text

    token = None

    root = ET.fromstring(response.text)
    if root.find('status').text == "PASS":
        cvvVerificationCode = root.find('cvvVerificationCode')
        card_type = root.find('cardType').text
        # Case for Visa or MasterCard
        if card_type in ('V', 'M'):
            if cvvVerificationCode.text != 'M':
                return None
        # Case for Discover
        elif card_type == 'R':
            if cvvVerificationCode is None:
                return None
            elif cvvVerificationCode.text != 'M':
                return None
        token = root.find('token').text
    
    # Log Card Verification
    TsysLog.objects.create(
        name='Card Verification',
        request=xml_string,
        response=response.text,
        is_error=not token
    )
    
    return token
