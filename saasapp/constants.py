SUPPLIERS = [
  'davidson',
  'lipseys',
  'green',
  'rsr',
  'zanders',
  'camfour',
  'sports',
  'jerrys',
  'ellet',
]

SUPPLIER_CHOICES = [
  ('davidson','Davidson\'s'),
  ('lipseys','Lipsey\'s'),
  ('green','Green Supply'),
  ('rsr','RSR Group'),
  ('sports','Sport\'s South'),
]

FFL_MONTHS = {
    1:'01', 2:'02', 3:'03', 4:'04',
    5:'05', 6:'06', 7:'07', 8:'08',
    9:'09', 10:'10', 11:'11', 12:'12'
}

STATE_CHOICES = [
("","State"),
("Alabama","Alabama"),
("Alaska","Alaska"),
("Arizona","Arizona"),
("Arkansas","Arkansas"),
("California","California"),
("Colorado","Colorado"),
("Connecticut","Connecticut"),
("Delaware","Delaware"),
("District Of Columbia","District Of Columbia"),
("Florida","Florida"),
("Georgia","Georgia"),
("Hawaii","Hawaii"),
("Idaho","Idaho"),
("Illinois","Illinois"),
("Indiana","Indiana"),
("Iowa","Iowa"),
("Kansas","Kansas"),
("Kentucky","Kentucky"),
("Louisiana","Louisiana"),
("Maine","Maine"),
("Maryland","Maryland"),
("Massachusetts","Massachusetts"),
("Michigan","Michigan"),
("Minnesota","Minnesota"),
("Mississippi","Mississippi"),
("Missouri","Missouri"),
("Montana","Montana"),
("Nebraska","Nebraska"),
("Nevada","Nevada"),
("New Hampshire","New Hampshire"),
("New Jersey","New Jersey"),
("New Mexico","New Mexico"),
("New York","New York"),
("North Carolina","North Carolina"),
("North Dakota","North Dakota"),
("Ohio","Ohio"),
("Oklahoma","Oklahoma"),
("Oregon","Oregon"),
("Pennsylvania","Pennsylvania"),
("Rhode Island","Rhode Island"),
("South Carolina","South Carolina"),
("South Dakota","South Dakota"),
("Tennessee","Tennessee"),
("Texas","Texas"),
("Utah","Utah"),
("Vermont","Vermont"),
("Virginia","Virginia"),
("Washington","Washington"),
("West Virginia","West Virginia"),
("Wisconsin","Wisconsin"),
("Wyoming","Wyoming"),
]

PAYMENT_TYPE_CHOICES = [
  ('Credit Card', 'Credit Card'),
  ('ACH', 'ACH')
]

SHIPPING_STATUS_CHOICES = [
  ('In Transit', 'In Transit'),
  ('Delivered', 'Delivered'),
  ('Picked up', 'Picked up')
]

FEDEX_GROUND = 'FedEx Ground'
FEDEX_1_DAY = 'FedEx Standard Overnight'
FEDEX_2_DAY = 'FedEx 2Day'
UPS_GROUND = 'UPS Ground'
UPS_1_DAY = 'UPS Next Day Air Saver'
UPS_2_DAY = 'UPS 2nd Day Air'

GROUND = (FEDEX_GROUND, UPS_GROUND)
ONE_DAY = (FEDEX_1_DAY, UPS_1_DAY)
TWO_DAY = (FEDEX_2_DAY, UPS_2_DAY)

# Tsys Constants
CREDIT_CARD = 'credit card'
BANK_ACCOUNT = 'bank account'
WALLET_TYPE_CHOICES = (
    (CREDIT_CARD, CREDIT_CARD),
    (BANK_ACCOUNT, BANK_ACCOUNT)
)
SALES_REPS = [
'Andy Birch',
'Nate Goodfellow',
'Brian Tooley'
]

REGION_1 =  [
'FL','GA','SC','NC','VA','WV','OH','MD',
'DE','PA','NY','NJ','CT','MA','VT','NH',
'ME','RI']

REGION_2 = [
'ND','SD','MN','IA','AR','MI','LA','MO',
'AL','TN','KY','IN','IL','WI','MS']

REGION_3 = [
'CA','WA','OR','NV','AZ','NM','UT','ID',
'MT','WY','CO','TX','OK','KS','NE','HI','AK']

REGIONS = [
REGION_1,
REGION_2,
REGION_3
]