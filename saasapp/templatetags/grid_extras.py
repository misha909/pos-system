from django import template

register = template.Library()


@register.filter
def endslice(value, arg):
    """
    Returns a slice of the string from arg to the end.
    """
    return value[arg:]


@register.filter
def trunc(value, arg):
    """
    Returns a slice of the string from the beginning to arg.
    """
    return value[:arg]


@register.filter
def get_at_index(list, index):
    return list[index]
