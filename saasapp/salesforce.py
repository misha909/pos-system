import time
import re
from base64 import b64decode
from io import BytesIO

from pyforce import pyforce
from PIL import Image

from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.base import ContentFile, File

from accounts.utils import send_new_account_email, rep_finder
from accounts.models import FFL, UserProfile, Wallet
from saasapp.utils import get_image_urls

from .constants import (
    CREDIT_CARD,
    BANK_ACCOUNT,
    GROUND,
    ONE_DAY,
    TWO_DAY
)

from .models import ShippingFee


def activate_product(product):
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
        settings.SALESFORCE_LOGIN_EMAIL,
        "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
    )
    res = svc.query(
            "SELECT Approval_Status__c FROM Product2 WHERE Id='" + product.salesforce_id + "' LIMIT 1"
        )
    if res and res[0]:
        if res[0].get('Approval_Status__c', False) == 'Approved':
            product.is_active = not product.is_active
            prod = {
                    'type': 'Product2',
                    'IsActive': product.is_active,
                    'Id': product.salesforce_id,
                    }
            svc.update(prod)
            product.save()
            return 'success'
        else:
            return 'not approved'
    else:
        return 'no product'


def get_opportunity(order, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            '%s%s' % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    res = svc.query(
        "SELECT Id, Order_Number__c FROM Opportunity WHERE Id = '%s' LIMIT 1" % (
            order.salesforce_id
        )
    )
    if res:
        return res[0]
    return None


def create_opportunities(order, coupon=None, opportunity_stage='Prospecting', nfa_product=False):
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
        settings.SALESFORCE_LOGIN_EMAIL,
        "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
    )
    import pdb
    pdb.set_trace()
    # print 'type'
    # print type(order.manufacturer)
    is_dealer_exchange = order.user.userprofile.has_de_profile()
    if is_dealer_exchange:
        manufacturer_account = get_account_id(user=order.user)
    else:
        manufacturer_account = get_account_id_from_name(name=order.manufacturer, svc=svc)
    
    print "Manufacturer Name:", order.manufacturer
    print "Manufacturer Account:", manufacturer_account
    print order
    if manufacturer_account == None:
        return None
    # Set Buyer Account Id base on wallet
    buyer_account_id = order.user.userprofile.account_id
    wallet = order.wallet
    if wallet.represented_by_sales_rep and wallet.salesforce_account_email:
        buyer_account_id = get_account_id_by_email(email=wallet.salesforce_account_email, svc=svc)
        if not buyer_account_id:
            raise ValueError('No Salesforce Account for email %d' % (wallet.salesforce_account_email))
    print "Buyer Account Id:", buyer_account_id
    
    # Create Opportunity
    opportunity = {
        'type': 'Opportunity',
        'Type': 'vArmory',
        'Payment_Type__c': order.payment_type,
        'Opportunity_Number__c': order.opportunity_number,
        'StageName': opportunity_stage,
        'Order_Date__c': order.date,
        'CloseDate': order.date,
        'Name': order.opportunity_number,
        'Masked_Account_Number__c': order.account_number,
        'Buyer_s_Account__c': buyer_account_id,
        'AccountId': buyer_account_id,
        'Seller_Account__c': manufacturer_account,
        'NFA_Product__c': nfa_product,
        'SOT_Approved__c': order.user.userprofile.sot_approved,
    }
    if order.fed_ex_master_track_number:
        opportunity['FedEx_Master_Track_Number__c'] = order.fed_ex_master_track_number
    if order.fed_ex_product_track_number:
        opportunity['FedEx_Product_Track_Number__c'] = order.fed_ex_product_track_number
    if order.salesforce_id:
        opportunity['Id'] = order.salesforce_id
        # Set Shipment_Provider__c and Mail_Service__c to let Salesforce handle shipment object creation
        # Only set if there is a salesforce_id in ShippingFee.
        if opportunity_stage != 'Prospecting':
            # Get selected ShippingFee
#            if ShippingFee.objects.filter(order=order, salesforce_id__isnull=False, selected=True).exists():
            if ShippingFee.objects.filter(order=order, selected=True).exists():
                shipping_fee = ShippingFee.objects.get(order=order, selected=True)
                create_shipping_opp_line(opportunity_id=opportunity['Id'], cost=shipping_fee.cost, svc=svc)
                purchase_history = order.purchasehistory_set.all()[0]
                purchase_history.total_price = purchase_history.total_price + float(shipping_fee.cost)
                purchase_history.shipment_provider = shipping_fee.shipment_provider
                purchase_history.mail_service = shipping_fee.mail_service
                purchase_history.save()
                opportunity['Shipment_Provider__c'] = shipping_fee.shipment_provider
                opportunity['Mail_Service__c'] = 'Domestic: %s' % re.sub(r'([^\s\w]|_)+', '', shipping_fee.mail_service)
                print 'SHIPMENT PROVIDER AND MAIL SERVICE HAVE BEEN ADDED TO THE OPPORTUNITY IN SF', opportunity['Mail_Service__c']
                if order.payment_type == 'ACH':
                    create_ach_file(order=order)
                    print 'CREATED ACH IN SALESFORCE.PY'
        res = svc.update(opportunity)
    else:
        res = svc.create(opportunity)
    print "Opportunity Response:", res
    if not res[0]['errors']:
        opportunity_id = res[0]['id']
        opp_res = svc.query(
            "SELECT Id, Order_Number__c FROM Opportunity WHERE Id = '%s' LIMIT 1" % (
                opportunity_id
            )
        )
        order_number = opp_res[0]['Order_Number__c']
    else:
        raise Exception('Opportunity creation failed {0}'.format(res[0]['errors']))

    order.salesforce_id = opportunity_id
    order.order_number = order_number
    order.save()
    # Create Opportunity Line Items
    if opportunity_stage == 'Prospecting' or coupon:
        for purchase_history in order.purchasehistory_set.all():
            if coupon is None or (coupon.manufacturer is not None and
               coupon.manufacturer.company_name != order.manufacturer):
                create_opportunity_products(
                    purchase_history=purchase_history,
                    opportunity_id=opportunity_id,
                    svc=svc
                )
            else:
                create_opportunity_products(
                    purchase_history=purchase_history,
                    opportunity_id=opportunity_id,
                    coupon=coupon,
                    svc=svc
                )

        get_shipment_calculations(order=order, svc=svc)

    return opportunity_id


def create_opportunity_products(purchase_history=None, opportunity_id=None, coupon=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    price_book_entry_id = create_price_book_entry(purchase_history=purchase_history, svc=svc)
    opportunity_product = {
        'type': 'OpportunityLineItem',
        'OpportunityId': opportunity_id,
        'PriceBookEntryId': price_book_entry_id,
        'UPC__c': purchase_history.upc,
        'Model__c': purchase_history.model,
        'Quantity': purchase_history.qty,
        'Supplier__c': purchase_history.supplier,
        'Manufacture__c': purchase_history.manufacture,
        'UnitPrice': purchase_history.unit_price,
        'Confirmed__c': purchase_history.confirmed,
        'ServiceDate': purchase_history.date
    }
    print "Opportunity Product:", opportunity_product
    opportunity_product_id = purchase_history.salesforce_id
    if purchase_history.salesforce_id:
        del opportunity_product['OpportunityId']
        del opportunity_product['PriceBookEntryId']
        opportunity_product['Id'] = purchase_history.salesforce_id
        if coupon:
            opportunity_product['Discount'] = coupon.percent_discount
        res = svc.update(opportunity_product)
        print "Opportunity Product Response", res
    else:
        res = svc.create(opportunity_product)
        print "Opportunity Product Response:", res
        if not res[0]['errors']:
            opportunity_product_id = res[0]['id']
            purchase_history.salesforce_id = opportunity_product_id
            purchase_history.save()
        else:
            raise Exception('Opportunity Product creation failed {0}'.format(res[0]['errors']))
    return opportunity_product_id


def create_product_2(purchase_history=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    product = {
        'type': 'Product2',
        'Name': purchase_history.model,
        'IsActive': True
    }
    res = svc.create(product)
    product_id = None
    print "Product2 Response:", res
    if not res[0]['errors']:
        product_id = res[0]['id']
    else:
        raise Exception('Product2 creation failed {0}'.format(res[0]['errors']))
    return product_id


def create_product_2_de(listing=None, user=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
        
    url_1,url_2,url_3,url_4,url_5 = get_image_urls(listing)
    
    try:
        account_id = svc.query(
        "SELECT Id FROM Account WHERE Primary_Contact_Email__c='"+user.email+"' LIMIT 1"
        )
        if account_id == []:
            account_id = ['']

    except Exception as e:
        account_id = ['']        
        print 'ACCOUNT_QUERY_ERROR___: ',e  
    product = {
        'type': 'Product2',
        'Name': listing.upc.model,
        'Listing_Type__c': listing.upc.listing_type, # Add field to Salesforce org
        'UPC__c': listing.upc.upc,
        'Category__c': listing.upc.listing_category,
        'Weight_Pounds__c': listing.upc.weight_pounds, # Add field to Salesforce org
        'Weight_Ounces__c': listing.upc.weight_ounces, # Add field to Salesforce org
        'Price__c': str(listing.price), # Add field to Salesforce org
        'bsg_Quantity_On_Hand__c': listing.qty,
        'Description': listing.upc.description,
        'Capacity__c': listing.upc.capacity,
        'Barrel_Length__c': listing.upc.length_barrel,
        'Overall_Length__c': listing.upc.length_overall,
        'Finish__c': listing.upc.finish,
        'Rounds_per_box__c': listing.upc.rounds,
        'Weight__c': listing.upc.weight,
        'Muzzle_Velocity__c': listing.upc.muzzle_velocity,
        'Length__c': listing.upc.length,
        'Ammo_Type__c': listing.upc.ammo_type, # Add field to Salesforce org
        'Image_URL__c': url_1,
        'Image_URL_2__c': url_2,
        'Image_URL_3__c': url_3,
        'Image_URL_4__c': url_4,
        'Image_URL_5__c': url_5,
        'Listing_Account__c': account_id[0].Id,
        'Listing_Account_State__c': '',  # listing.company_profile.address_state
        'Cost__c': str(listing.price),
        'Package_Weight_lbs__c': listing.upc.shipping_weight,
        'Package_Height_inches__c': listing.upc.shipping_height,
        'Package_Length__c': listing.upc.shipping_length,
        'Package_Width_inches__c': listing.upc.shipping_width
    }
    if listing.salesforce_id:
        product['Id'] = listing.salesforce_id
        res = svc.update(product)
    else:
        product['IsActive'] = False
        res = svc.create(product)
    product_id = None
    print "Product2 Response:", res
    if not res[0]['errors']:
        product_id = res[0]['id']
    else:
        raise Exception('Product2 creation failed {0}'.format(res[0]['errors']))
    listing.salesforce_id = product_id
    listing.save()
    return product_id


def delete_product(product=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    res = svc.delete(product.salesforce_id)
    print "Product2 Response:", res


def get_standard_price_book(svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Get standard pricebook from contact
    res = svc.query(
        "SELECT Id FROM Pricebook2 WHERE IsStandard = True limit 1"
    )
    print "Pricebook2 Response:", res
    if res and res[0]:
        print "Standard Price Book Id:", res[0].Id
        return res[0].Id
    return None


def create_price_book_2(purchase_history=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    price_book = {
        'type': 'Pricebook2',
        'Name': purchase_history.model,
        'IsActive': True
    }
    res = svc.create(price_book)
    price_book_id = None
    print "Pricebook2 Response:", res
    if not res[0]['errors']:
        price_book_id = res[0]['id']
    else:
        raise Exception('Pricebook2 creation failed {0}'.format(res[0]['errors']))
    return price_book_id


def create_price_book_entry(purchase_history=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    #product_id = create_product_2(purchase_history=purchase_history, svc=svc)
    product_id = purchase_history.product.salesforce_id
    print 'PRODUCT ID == ', product_id
    price_book_id = get_standard_price_book(svc=svc)
    print 'PRICE BOOK ID == ', price_book_id
    price_book_entry = {
        'type': 'PricebookEntry',
        'Pricebook2Id': price_book_id,
        'Product2Id': product_id,
        'UnitPrice': purchase_history.unit_price,
        'UseStandardPrice': False,
        'IsActive': True
    }

    res = svc.query("SELECT Id From PricebookEntry WHERE Pricebook2Id = '%s' AND Product2Id = '%s'" % (price_book_id, product_id))
    print "Price Book Entry Response:", res
    if res and res[0]:
        return res[0].Id
    
    res = svc.create(price_book_entry)
    price_book_entry_id = None
    print "Price Book Entry Response:", res
    if not res[0]['errors']:
        price_book_entry_id = res[0]['id']
    else:
        raise Exception('Price Book Entry creation failed {0}'.format(res[0]['errors']))
    return price_book_entry_id


def update_shipping_info():
    """
    For all users with salesforce id.
    Update the Shipping fields required
    """
    sf = pyforce._tPartnerNS
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
        settings.SALESFORCE_LOGIN_EMAIL,
        "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
    )
    
    userprofiles = UserProfile.objects.filter(account_id__isnull=False)
    for userprofile in userprofiles:
        # Get account from contact
        res = svc.query(
            "SELECT Id, Which_shipping_carriers_do_you_use__c FROM Account WHERE Id = '%s' LIMIT 1" % (
                userprofile.account_id
            )
        )
        
        if res and res[0]:
            if not res[0].get('Which_shipping_carriers_do_you_use__c'):
                update_shipping_info_from_account(userprofile=userprofile, svc=svc)


def create_ach_file(order=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    ach_file = {
        'type': 'ACH_File__c',
        'Account_number__c' : order.wallet.account_number,
        'Account_Type__c' : 'Check',
        'Amount__c' : -order.transaction_amount,
        'Created_Date__c': order.date,
        'Invoice_Name__c' : "%s %s" % (order.wallet.user.first_name, order.wallet.user.last_name),
        'Routing_Number__c' : order.wallet.routing_number
    }
    
    res = svc.create(ach_file)
    print "Create ACH File REsponse:", res


def update_shipping_info_from_account(userprofile=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    if userprofile:
        userprofile_dict = {
            'type': 'Account',
            'Id': userprofile.account_id,
            'Which_shipping_carriers_do_you_use__c': userprofile.shipping_carrier,
            'Regularly_scheduled_pickups__c': userprofile.scheduled_pickups
        }
        res = svc.update(userprofile_dict)
        print "Update SHipping Info Response:", res


def update_billing_address(userprofile=None, street=None, city=None, state=None, postal=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
                settings.SALESFORCE_LOGIN_EMAIL,
                "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    if userprofile:
        userprofile_dict = {
            'type': 'Account',
            'Id': userprofile.account_id,
            'BillingCountry': 'USA',
        }
        if street is not None:
            userprofile_dict['BillingStreet'] = street
        if city is not None:
            userprofile_dict['BillingCity'] = city
        if state is not None:
            userprofile_dict['BillingState'] = state
        if postal is not None:
            userprofile_dict['BillingPostalCode'] = postal

        res = svc.update(userprofile_dict)
        print "Update SHipping Info Response:", res


def create_users_from_accounts():
    """
    Get all Account objects in salesforce and create users if the user does not exist
    in the database.
    """
    sf = pyforce._tPartnerNS
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
        settings.SALESFORCE_LOGIN_EMAIL,
        "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
    )
    results = svc.query("SELECT Id, Name, Primary_Contact_Email__c, Contact__r.FirstName, Contact__r.LastName From Account")
    
    if results and results[0]:
        # First query
        for result in results['records']:
            create_user_from_result(result)
        done = results['done']
        # Query More
        if not done:
            results = svc.queryMore(results['queryLocator'])
            while True:
                for result in results['records']:
                    create_user_from_result(result)
                done = results['done']
                if done:
                    break
                else:
                    results = svc.queryMore(results['queryLocator'])
            
    
def create_user_from_result(result):
    if result["Primary_Contact_Email__c"] and not User.objects.filter(email=result["Primary_Contact_Email__c"]).exists():
        if result["Primary_Contact_Email__c"] == 'jason@gsadirect.com':
            password = User.objects.make_random_password()
            
            contact = result.get("Contact__r", None)
            first_name = None
            last_name = None
            
            if contact:
                first_name = contact.get("FirstName", None)
                last_name = contact.get("LastName", None)
            
            userData = User.objects.create_user(
                result["Primary_Contact_Email__c"],
                result["Primary_Contact_Email__c"],
                password
            )
            
            # Some account first and last names have a title like General Manager, etc.
            # So the length of the names are long.
            if first_name and len(first_name) <= 30:
                userData.first_name = first_name
            if last_name  and len(last_name) <= 30:
                userData.last_name = last_name
            userData.is_active = True
            userData.save()
            
            userprofile = userData.userprofile
            userprofile.company_name = result["Name"]
            userprofile.from_salesforce = True
            userprofile.save()
            
            send_new_account_email(
                to_email=[userData.email],
                user=userData,
                password=password
            )
            
            print "Created user %s" % userData.email


def create_lead(last_name=None,
                first_name=None,
                email=None,
                company = None,
                davidson=False,
                lipseys=False,
                rsr=False,
                green=False,
                camfour=False,
                sports=False,
                zanders=False):
    svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
    svc.login(
        settings.SALESFORCE_LOGIN_EMAIL,
        "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
    )
    
    contact = {
        'type': 'Lead',
        'LastName': last_name,
        'FirstName': first_name,
        'User_Name__c': email,
        'Email': email,
        'Company': company,
        'davidson__c': davidson,
        'lipseys__c': lipseys,
        'rsr__c': rsr,
        'greens_sipply__c': green,
        'camfour__c': camfour,
        'sport__c': sports,
        'zanders__c': zanders,
        'LeadSource': 'vArmory IS'
    }
    
    # Check for Lead duplicates
    duplicate_lead = False
    duplicate_lead_id = None
    print "Checking Lead duplicates..."
    # Check for same company and email
    print "Checking for same company and email..."
    res = svc.query("SELECT Id FROM Lead WHERE Company='%s' AND Email='%s'" % (company.replace("'", "\\'"), email.replace("'", "\\'")))
    if res and res[0]:
        print "Duplicate for same company and email"
        duplicate_lead = True
        duplicate_lead_id = res[0].Id
    # Check for same fullname and company
    print "Checking for same fullname and company..."
    res = svc.query("SELECT Id FROM Lead WHERE Company='%s' AND FirstName='%s' AND LastName='%s'" % (company.replace("'", "\\'"), first_name.replace("'", "\\'"), last_name.replace("'", "\\'")))
    if res and res[0]:
        print "Duplicate for same fullname and company"
        duplicate_lead = True
        duplicate_lead_id = res[0].Id
        
    if duplicate_lead:
        contact['Id'] = duplicate_lead_id
        print "Contact:", contact
        res = svc.update(contact)
    else:
        res = svc.create(contact)
    
    print "Res for create update:", res
    if not res[0]['errors']:
        contact_id = res[0]['id']
    else:
        raise Exception('Contact creation failed {0}'.format(res[0]['errors']))


def create_contact(user=None, svc=None, account=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Check for duplicate Contact
    res = svc.query(
        "SELECT Id FROM Contact WHERE Email = '%s'" % (
            user.email
        )
    )
    duplicate_contact = False
    contact_id = None
    if res and res[0]:
        duplicate_contact = True
        contact_id = res[0].Id
    contact = {
        'type': 'Contact',
        'FirstName': user.first_name,
        'LastName': user.last_name,
        'Email': user.email
    }
    if account:
        contact['AccountId'] = account
    if duplicate_contact:
        contact['Id'] = contact_id
        res = svc.update(contact)
    else:
        res = svc.create(contact)
    if not res[0]['errors']:
        contact_id = res[0]['id']
    else:
        raise Exception('Contact creation failed {0}'.format(res[0]['errors']))
    return contact_id


def get_record_type_id(sobject_type=None, name=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    res = svc.query(
        "SELECT Id FROM RecordType WHERE SobjectType='%s' AND Name='%s'" % (
            sobject_type,
            name
        )
    )
    print "Record Type Res:", res
    if res and res[0]:
        return res[0].Id
    return None


def get_account_id(user=None, svc=None):
    # Check if userprofile.account_id exists
    if user.userprofile.account_id:
        return user.userprofile.account_id
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Get account from contact
    res = svc.query(
        "SELECT Id, AccountId FROM Contact WHERE Email = '%s'" % (
            user.email
        )
    )
    if res and res[0]:
        user_profile = user.userprofile
        user_profile.account_id = res[0].AccountId
        user_profile.save()
        return res[0].AccountId
    return None


def get_account_id_by_email(email=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Get account from contact
    res = svc.query(
        "SELECT Id FROM Account WHERE Primary_Contact_Email__c = '%s'" % (
            email
        )
    )
    print "Account Res = ", res
    if res and res[0]:
        return res[0].Id
    return None


def get_account_approval_by_email(email=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            '%s%s' % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    res = svc.query(
        "SELECT vArmory_Account_Approved__c FROM Account " +
        "WHERE Primary_Contact_Email__c = '%s'" % email
    )
    print 'Account Res = ', res
    if res and res[0]:
        return res[0].vArmory_Account_Approved__c
    return None


def get_account_id_from_name(name=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Get account from contact
    for attempt in range(3):
        time.sleep(1)
        print 'name'
        print name
        try:
            name = name.company_name
            # if not name.is_dealer_direct:
                # name = 'Dealer Exchange'
        except:
            pass
        res = svc.query(
            "SELECT Id FROM Account WHERE Name = '%s'" % (
                name.replace("'", "\\'")
            )
        )
        print 'GET_ACCOUNT_ID_FROM_NAME() RESPONSE: ', res
        if res and res[0]:
            return res[0].Id
        else:
            pass
    else:
        return None


def get_account_number(user=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    res = svc.query(
        "SELECT Id, Account_Number__c FROM Account WHERE Primary_Contact_Email__c = '%s'" % (
            user.email
        )
    )
    if res and res[0]:
        return (res[0].Id, res[0].Account_Number__c)
    return None



def get_shipment_calculations(order=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    if order.salesforce_id:
        # Wait for 3 seconds per attempt for a maximum of 5 attempts
        # This is in order to wait for the Salesforce Trigger to finish executing.
        print "Order Salesforce Id:", order.salesforce_id
        total = order.transaction_amount
        histories = order.purchasehistory_set.all()
        product_total = 0
        for product in histories:
            product_total = product_total + product.qty
                
        if product_total == 1:            
            for attempt in range(3):
                time.sleep(3)
                print "Attempt ", attempt
                res = svc.query(
                    "SELECT Id, Shipment_Provider__c, Mail_Service__c, Cost__c FROM Shipment_Calculation__c WHERE Opportunity__c = '%s'" % (
                        order.salesforce_id
                    )
                )
    
                print "Res:", res
                if res and res[0]:
                    # Delete previous ShippingFee objects to avoid conflict
                    shipping_fee = order.shippingfee_set.all()
                    shipping_fee.delete()
                    print 'RECEIVED VALID SF RESPONSE'
                    for shipment in res:
                        service = shipment.Mail_Service__c
                        if not service[-1].isalpha():
                            service = service[:-2]
                        cost = shipment.Cost__c                         
                        if 'ground' in shipment.Mail_Service__c.lower() and total >= 500:
                            cost = 0
                        print 'COST == ', service , cost
                        ShippingFee.objects.create(
                            order=order,
                            salesforce_id=shipment.Id,
                            shipment_provider=shipment.Shipment_Provider__c,
                            mail_service=shipment.Mail_Service__c,
                            cost=cost
                        )
                        print 'Shipping object created', shipment.Shipment_Provider__c, cost
                    break

            else:
                print 'CREATING SHIPPING OPTIONS BECUASE SF DID NOT RESPOND'
                print 'Creating shipping options without SF'
                carriers = ['FedEx','UPS']
#                options = [ONE_DAY[1],TWO_DAY[1],GROUND[1]]
                for carrier in range(2):
                    for choice in range(3):
                        if total >= 1000 and choice == 0:
                            cost = (product_total * 23.12)
                        elif total >= 1000 and choice == 1: 
                            cost = (product_total * 18.37)
                        elif total >= 500 and total < 1000 and choice == 0:
                            cost = (product_total * 27.62)
                        elif total >= 500 and total < 1000 and choice == 1:
                            cost = (product_total * 19.36)
                        elif total >= 500 and choice == 2:
                            cost = 0
                        elif total < 500 and choice == 0:
                            cost = (product_total * 37.33)
                        elif total < 500 and choice == 1:
                            cost = (product_total * 24.32)
                        elif total < 500 and choice == 2:
                            cost = (product_total * 5.36)
                        print 'COST == ', cost
                        options = [ONE_DAY[carrier],TWO_DAY[carrier],GROUND[carrier]]
                        ShippingFee.objects.create(
                            order=order,
                            shipment_provider=carriers[carrier],
                            mail_service=options[choice],
                            cost=cost
                        )  
                        print 'Shipping object created', carrier, cost
            
        elif product_total > 1:
            print 'Creating shipping options because product total == ', product_total
            print 'Creating shipping options without SF'
            carriers = ['FedEx','UPS']
#            options = [ONE_DAY[1],TWO_DAY[1],GROUND[1]]
            for carrier in range(2):
                for choice in range(3):
                    if total >= 1000 and choice == 0:
                        cost = (product_total * 23.12)
                    elif total >= 1000 and choice == 1: 
                        cost = (product_total * 18.37)
                    elif total >= 500 and total < 1000 and choice == 0:
                        cost = (product_total * 27.62)
                    elif total >= 500 and total < 1000 and choice == 1:
                        cost = (product_total * 19.36)
                    elif total >= 500 and choice == 2:
                        cost = 0
                    elif total < 500 and choice == 0:
                        cost = (product_total * 37.33)
                    elif total < 500 and choice == 1:
                        cost = (product_total * 24.32)
                    elif total < 500 and choice == 2:
                        cost = (product_total * 5.36)
                    print 'COST == ', cost
                    options = [ONE_DAY[carrier],TWO_DAY[carrier],GROUND[carrier]]
                    ShippingFee.objects.create(
                        order=order,
                        shipment_provider=carriers[carrier],
                        mail_service=options[choice],
                        cost=cost
                    )  

                    print 'Shipping object created', carrier, cost

    else:
        print 'Order did not have a salesforce Id'
        

def create_account(user=None, svc=None, update=True):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )

    # Check for duplicate contact
    res = svc.query(
        "SELECT Id, AccountId, Dealer_Direct_User__c FROM Contact WHERE Email = '%s'" % (
            user.email
        )
    )

    accounts_receivable = svc.query(
        "SELECT Id FROM c2g__codaGeneralLedgerAccount__c WHERE Name = 'Accounts Receivable'" 
    )

    accounts_payable = svc.query(
        "SELECT Id FROM c2g__codaGeneralLedgerAccount__c WHERE Name = 'Accounts Payable'" 
        )

    cogs = svc.query(
        "SELECT Id FROM c2g__codaGeneralLedgerAccount__c WHERE Name = 'COGS - Dealer Direct'" 
        )

    tax_code = svc.query(
        "SELECT Id FROM c2g__codaTaxCode__c WHERE Name = 'Sales Tax Exempt'" 
        )

    duplicate_contact = False
    duplicate_account_id = None
    contact_id = None
    if res and res[0]:
        duplicate_contact = True
        duplicate_account_id = res[0].AccountId
        if duplicate_account_id == '' and res[0].Dealer_Direct_User__c:
            print 'Contact.Dealer_Direct_User__c == True'
            print 'Contact Response_____: ', res[0]
            duplicate_contact = False
        contact_id = res[0].Id
        
    sales_rep = rep_finder(state=user.userprofile.address_state)
    # Only create account if there is no duplicate contact and Dealer_Direct_User__c == False
    account = {
        'type': 'Account',
        'Name': user.userprofile.company_name,
        'Type__c': "FFL Design",
        'Account_Assigned_To__c': sales_rep,
        'FFL_Number_First_Three__c': user.userprofile.ffl_number_first_three,
        'FFL_Number_Last_Five__c': user.userprofile.ffl_number_last_five,
        'FFL_Expiration_Date__c': user.userprofile.ffl_expiration_date,
        'ShippingStreet': user.userprofile.address_street,
        'ShippingCity': user.userprofile.address_city,
        'ShippingState': user.userprofile.address_state,
        'ShippingPostalCode': user.userprofile.address_zip,
        'ShippingCountry': 'USA',
        'company__c': user.userprofile.company_name,
        'margin__c': user.userprofile.margin,
        'camfour__c': user.userprofile.camfour,
        'davidson__c': user.userprofile.davidson,
        'greens_supply__c': user.userprofile.green,
        'lipseys__c': user.userprofile.lipseys,
        'rsr__c': user.userprofile.rsr,
        'sport__c': user.userprofile.sports,
        'zanders__c': user.userprofile.zanders,
        'Dealer_Direct_User__c': user.userprofile.is_dealer_direct,
        'vArmory_User__c': user.userprofile.is_varmory,
        'Dealer_Exchange_User__c': user.userprofile.dealer_exchange,
        'Which_shipping_carriers_do_you_use__c': user.userprofile.shipping_carrier,
        'Regularly_scheduled_pickups__c': user.userprofile.scheduled_pickups,
        'Carrier_package_dropoff__c': user.userprofile.carrier_dropoff,
        'FFL_Received_In_File__c': user.userprofile.ffl_is_uploaded,
        'FFL_URL__c': user.userprofile.ffl_url,
        'c2g__CODAAccountsReceivableControl__c': accounts_receivable[0].Id,
        'c2g__CODATaxCode1__c': tax_code[0].Id,
        'c2g__CODABaseDate1__c': 'Invoice Date',
        'c2g__CODAAccountsPayableControl__c': accounts_payable[0].Id,
        'c2g__CODADefaultExpenseAccount__c': cogs[0].Id

    }

    # Check for Wallet
    if Wallet.objects.filter(user=user, wallet_type=BANK_ACCOUNT).exists():
        wallet = Wallet.objects.filter(user=user, wallet_type=BANK_ACCOUNT).first()
        account['c2g__CODABankAccountNumber__c'] = wallet.account_number
        account['c2g__CODABankAccountName__c'] = wallet.account_name

    # Split address
    if user.userprofile.ffl_premise_address:
        address = user.userprofile.ffl_premise_address.split(" - ")
        postal_code = address[1]
        left_address = address[0].split(" ")
        state = left_address[len(left_address) - 1]
        city = left_address[len(left_address) - 2]
        street = " ".join(left_address[0:len(left_address) - 2])
        sales_rep = rep_finder(state=state)
        account.update({
            'Account_Assigned_To__c': sales_rep,
            'ShippingStreet': street,
            'ShippingCity': city,
            'ShippingState': state,
            'ShippingPostalCode': postal_code,
            'ShippingCountry': 'USA'
        })
    if user.userprofile.ffl_mailing_address:
        address = user.userprofile.ffl_mailing_address.split(" - ")
        postal_code = address[1]
        left_address = address[0].split(" ")
        state = left_address[len(left_address) - 1]
        city = left_address[len(left_address) - 2]
        street = " ".join(left_address[0:len(left_address) - 2])
        sales_rep = rep_finder(state=state)
        account.update({
            'Account_Assigned_To__c': sales_rep,
            'BillingStreet': street,
            'BillingCity': city,
            'BillingState': state,
            'BillingPostalCode': postal_code,
            'BillingCountry': 'USA',
        })

    # Add Phone
    ffl_number = "%s%s" % (
        user.userprofile.ffl_number_first_three,
        user.userprofile.ffl_number_last_five
    )
    if FFL.objects.filter(ffl_number=ffl_number).exists():
        ffl = FFL.objects.get(ffl_number=ffl_number)
        account['Phone'] = ffl.phone

    if not duplicate_contact:
        res = svc.create(account)
        if not res[0]['errors']:
            account_id = res[0]['id']
        else:
            raise Exception('Account creation failed {0}'.format(res[0]['errors']))

        # Create contact and attach account to it
        contact_id = create_contact(user=user, svc=svc, account=account_id)

        # Update Contact of Account
        account['Id'] = account_id
        account['Contact__c'] = contact_id
        res = svc.update(account)
        if not res[0]['errors']:
            account_id = res[0]['id']
        else:
            raise Exception('Account update failed {0}'.format(res[0]['errors']))

        userprofile = user.userprofile
        userprofile.account_id = account_id
        userprofile.save()

        return account_id
    else:
        account['Id'] = duplicate_account_id
        account['Contact__c'] = contact_id
        res = svc.update(account)
        if not res[0]['errors']:
            account_id = res[0]['id']
        else:
            raise Exception('Account update failed {0}'.format(res[0]['errors']))

        userprofile = user.userprofile
        userprofile.account_id = account_id
        userprofile.save()

        return account_id
    return None


def create_payment_detail(wallet=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )

    # Check for duplicate wallet
    res = svc.query(
        "SELECT Id FROM Payment_Detail__c WHERE Wallet_Id__c = %d" % (
            wallet.pk
        )
    )
    duplicate_object = False
    duplicate_object_id = None
    if res and res[0]:
        duplicate_object = True
        duplicate_object_id = res[0].Id

    # Get record type id
    record_type_id = None
    if wallet.wallet_type == CREDIT_CARD:
        record_type_id = get_record_type_id(sobject_type='Payment_Detail__c', name='Credit Card', svc=svc)
    elif wallet.wallet_type == BANK_ACCOUNT:
        record_type_id = get_record_type_id(sobject_type='Payment_Detail__c', name='Electronic Check', svc=svc)

    # Only save for BANK ACCOUNT as per requirement for varmory
    if wallet.wallet_type == BANK_ACCOUNT:
        account_id = get_account_id(user=wallet.user, svc=svc)
        if not account_id:
            account_id = create_account(user=wallet.user, svc=svc, update=False)
        payment_detail = {
            'type': 'Payment_Detail__c',
            'Account__c': account_id,
            'Wallet_Id__c': wallet.pk,
            'Name': "Wallet ID %d for %s %s" % (wallet.pk, wallet.user.first_name, wallet.user.last_name),
            'Routing_Number__c': wallet.routing_number,
            'Electronic_Check__c': wallet.account_number,
            'RecordTypeId': record_type_id
        }
        if not duplicate_object:
            res = svc.create(payment_detail)
            if not res[0]['errors']:
                duplicate_object_id = res[0]['id']
            else:
                raise Exception('Payment Detail creation failed {0}'.format(res[0]['errors']))
        else:
            payment_detail['Id'] = duplicate_object_id
            res = svc.update(payment_detail)
        return duplicate_object_id
    return None


def create_search_history(search_history=None, svc=None, account_id=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Check for duplicate search_history
    res = svc.query(
        "SELECT Id FROM Search_History__c WHERE Search_Id__c = %d" % (
            search_history.pk
        )
    )
    duplicate_search_history = False
    search_history_id = None
    if res and res[0]:
        duplicate_search_history = True
        search_history_id = res[0].Id
    if not account_id:
        account_id = get_account_id(search_history.user)
    if account_id:
        search_history_data = {
            'type': 'Search_History__c',
            'Account__c': account_id,
            'Date__c': search_history.date,
            'Searched_Term__c': search_history.searched_term,
            'Search_Id__c': search_history.pk,
            'Name': "%d: %s" % (search_history.pk, search_history.searched_term)
        }
        if not duplicate_search_history:
            res = svc.create(search_history_data)
            if not res[0]['errors']:
                search_history_id = res[0]['id']
            else:
                raise Exception('Search History creation failed {0}'.format(res[0]['errors']))
        else:
            search_history_data['Id'] = search_history_id
            res = svc.update(search_history_data)
        return search_history_id
    return None


def get_order_id(order=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    res = svc.query(
        "SELECT Id FROM Order__c WHERE Order_Id__c = %d" % (
            order.pk
        )
    )
    if res and res[0]:
        return res[0].Id
    return None


def create_order(order=None, svc=None, account_id=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Check for duplicate search_history
    res = svc.query(
        "SELECT Id FROM Order__c WHERE Order_Id__c = %d" % (
            order.pk
        )
    )
    duplicate_object = False
    duplicate_object_id = None
    if res and res[0]:
        duplicate_object = True
        duplicate_object_id = res[0].Id
    
    if not account_id:
        account_id = get_account_id(order.user)
    if account_id:
        order_data = {
            'type': 'Order__c',
            'Account__c': account_id,
            'Date__c': order.date,
            'Order_Id__c': order.pk,
            'Name': "%d: %s %s" % (order.pk, order.user.first_name, order.user.last_name)
        }
        if not duplicate_object:
            res = svc.create(order_data)
            if not res[0]['errors']:
                order_id = res[0]['id']
            else:
                raise Exception('Order creation failed {0}'.format(res[0]['errors']))
            return order_id
        else:
            order_data['Id'] = duplicate_object_id
            res = svc.update(order_data)
            if not res[0]['errors']:
                order_id = res[0]['id']
            else:
                raise Exception('Order update failed {0}'.format(res[0]['errors']))
            return order_id
    return None


def create_purchase_history(purchase_history=None, svc=None, order_id=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    # Check for duplicate search_history
    res = svc.query(
        "SELECT Id FROM Purchase_History__c WHERE Purchase_History_Id__c = %d" % (
            purchase_history.pk
        )
    )
    duplicate_object = False
    duplicate_object_id = None
    if res and res[0]:
        duplicate_object = True
        duplicate_object_id = res[0].Id
    
    if not order_id:
        order_id = get_order_id(purchase_history.order)
    if order_id:
        purchase_history_data = {
            'type': 'Purchase_History__c',
            'Confirmed__c': purchase_history.confirmed,
            'Date__c': purchase_history.date,
            'Manufacture__c': purchase_history.manufacture,
            'Model__c': purchase_history.model,
            'Order__c': order_id,
            'Purchase_History_Id__c': purchase_history.pk,
            'Quantity__c': purchase_history.qty,
            'Supplier__c': purchase_history.supplier,
            'Total_Price__c': purchase_history.total_price,
            'Unit_Price__c': purchase_history.unit_price,
            'UPC__c': purchase_history.upc,
            'Name': "%d: %s %s" % (
                purchase_history.pk,
                purchase_history.order.user.first_name,
                purchase_history.order.user.last_name
            )
        }
        if not duplicate_object:
            try:
                res = svc.create(purchase_history_data)
            except Exception, e:
                print "Exception:", e
                raise e
            if not res[0]['errors']:
                order_id = res[0]['id']
            else:
                raise Exception('Order creation failed {0}'.format(res[0]['errors']))
            return order_id
        else:
            purchase_history_data['Id'] = duplicate_object_id
            res = svc.update(purchase_history_data)
            if not res[0]['errors']:
                order_id = res[0]['id']
            else:
                raise Exception('Order update failed {0}'.format(res[0]['errors']))
            return order_id
    return None

def create_shipping_opp_line(opportunity_id=None, cost=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    
    shipping = svc.query(
        "SELECT Id,UnitPrice,Pricebook2Id,Product2Id,Product2.Name,Pricebook2.Name FROM PriceBookEntry WHERE Product2.Name='Shipping' AND Pricebook2.Name='Standard Price Book' LIMIT 1" 
    )
    
    opportunity_product = {
        'type': 'OpportunityLineItem',
        'OpportunityId': opportunity_id,
        'PriceBookEntryId': shipping[0].Id,
        'Quantity': '1',
        'UnitPrice': str(cost)
    }
    print "Opportunity Shipping Product:", opportunity_product
    res = svc.create(opportunity_product)
    opportunity_product_id = None
    print "Opportunity Product Response:", res
    if not res[0]['errors']:
        opportunity_product_id = res[0]['id']
    else:
        raise Exception('Opportunity Product creation failed {0}'.format(res[0]['errors']))
    return opportunity_product_id

def download_shipping_label(order=None, svc=None):
    if not svc:
        svc = pyforce.Client(serverUrl=settings.SALESFORCE_SERVER_URL)
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
    if not order.salesforce_id:
        return None
    # Get UPS Shipment Object
    res = svc.query(
        "SELECT Id FROM zkups__UPSShipment__c WHERE zkups__RecipientOpportunity__c = '%s' OR Opportunity__c = '%s' LIMIT 1" % (
            order.salesforce_id,
            order.salesforce_id
        )
    )
    ups_shipment_id = None
    if res and res[0]:
        ups_shipment_id = res[0].Id
    if not ups_shipment_id:
        return None
    
    # Get Attachment Object
    res = svc.query(
        "SELECT Name, Body, ContentType FROM Attachment WHERE ParentId = '%s' AND ContentType = 'GIF' LIMIT 1" % (
            ups_shipment_id
        )
    )
    if res and res[0]:
        image_data = b64decode(res[0].Body)
        order.shipping_label.save(
            "%s.gif" % order.salesforce_id,
            ContentFile(image_data),
            save=True
        )
        order.save()
        # Save to pdf
        image = Image.open(order.shipping_label.path)
        pdf_file = BytesIO()
        image.save(
            pdf_file,
            format="PDF",
            resolution=100.0
        )
        order.shipping_label_pdf.save(
            "%s.pdf" % order.salesforce_id,
            File(pdf_file),
            save=True
        )
        order.save()
    return None
    
    
