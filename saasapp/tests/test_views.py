import json
from decimal import Decimal

from django.contrib.auth.models import User
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.test import TestCase, Client, RequestFactory

from carton.cart import Cart

from accounts.models import Wallet
from products.models import Product
from saasapp.constants import CREDIT_CARD
from saasapp.views import add_product, ReviewView


def setUpModule():
    call_command('clear_index', interactive=False)
    call_command('loaddata', 'distributors')
    call_command('loaddata', 'categories')
    call_command('add_gsa_products')


class CartViewsTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            'username',
            password='varmoryffldesign'
        )
        self.client = Client()
        self.client.login(
            username=self.user.username,
            password='varmoryffldesign'
        )
        self.factory = RequestFactory()

    def test_add_product_cv_off(self):
        url = reverse('cart_add_product')
        product = Product.objects.first()
        response = self.client.get(url, {
            'product_id': product.id,
            'user_quantity': 1,
        })
        self.assertEqual(response.status_code, 200)
        cart = Cart(self.client.session)
        self.assertEqual(len(cart.items), 1)
        self.assertEqual(cart.items[0].price, product.price)

    def test_add_product_cv_on(self):
        url = reverse('cart_add_product')
        product = Product.objects.first()
        request = self.factory.get(url, {
            'product_id': product.id,
            'user_quantity': 1
        })
        request.user = self.user
        request.session = self.client.session
        request.session['cv'] = True
        setattr(request, 'modified', False)
        response = add_product(request)
        self.assertEqual(response.status_code, 200)
        cart = Cart(request.session)
        self.assertEqual(len(cart.items), 1)
        margin = self.user.userprofile.margin
        final_price = product.price + (product.price * margin / 100.0)
        self.assertAlmostEqual(
            cart.items[0].price,
            Decimal(final_price),
            places=2
        )

    def test_update_product_qty(self):
        url = reverse('cart_add_product')
        product = Product.objects.first()
        self.client.get(url, {
            'product_id': product.id,
            'user_quantity': 1,
        })

        url = reverse('cart_update_product')
        response = self.client.get(url, {
            'product_id': product.id,
            'new_qty': 2
        })
        self.assertEqual(response.content, 'true')
        cart = Cart(self.client.session)
        self.assertEqual(len(cart.items), 1)
        self.assertEqual(cart.items[0].quantity, 2)

    def test_remove_product(self):
        url = reverse('cart_add_product')
        product = Product.objects.first()
        self.client.get(url, {
            'product_id': product.id,
            'user_quantity': 1,
        })

        url = reverse('cart_remove_product')
        response = self.client.get(url, {
            'product_id': product.id
        })
        self.assertEqual(response.status_code, 200)


class POSViewsTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            'username',
            password='varmoryffldesign',
            first_name='First',
            last_name='Last'
        )
        self.client = Client()
        self.client.login(
            username=self.user.username,
            password='varmoryffldesign'
        )
        self.factory = RequestFactory()

        url = reverse('cart_add_product')
        product1 = Product.objects.filter(
            upc__manufacturer='MILTAC Industries'
        ).first()
        product1.upc.model = 'DummyModelName'
        product1.upc.save()
        product2 = Product.objects.filter(
            upc__manufacturer='SRM Arms'
        ).first()
        self.client.get(url, {
            'product_id': product1.id,
            'user_quantity': 1,
        })
        self.client.get(url, {
            'product_id': product2.id,
            'user_quantity': 1,
        })

        card_data = {
            'wallet_type': CREDIT_CARD,
            'expiration_month': 12,
            'expiration_year': 2020
        }
        wallet = Wallet.objects.create(
            user=self.user,
            cvv='999',
            **card_data
        )
        wallet.create_token('4012881888818888')
        wallet = Wallet.objects.create(
            user=self.user,
            cvv='999',
            **card_data
        )
        wallet.create_token('4012000098765439')
        wallet = Wallet.objects.create(
            user=self.user,
            cvv='998',
            **card_data
        )
        wallet.create_token('5499740000000057')

    def test_cart_view(self):
        url = reverse('view_cart')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        item_list = response.context_data.get('item_list')
        self.assertEqual(len(item_list), 2)
        self.assertEqual(item_list[0].get('name'), 'MILTAC Industries')
        self.assertEqual(item_list[1].get('name'), 'SRM Arms')

    def test_payment_view(self):
        url = reverse('view_payment')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        card_wallets = response.context_data['card_wallets']
        self.assertEqual(len(card_wallets), 3)

        response = self.client.post(
            url,
            {'selected_method': card_wallets[2].id},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertTrue(content['status'])

    def test_review_view(self):
        url = reverse('view_review')
        request = self.factory.get(url)
        request.user = self.user
        request.session = self.client.session
        request.session['wallet_id'] = Wallet.objects.last().id
        request.modified = False
        response = ReviewView.as_view()(request)
        self.assertEqual(response.status_code, 200)

        request = self.factory.post(
            url,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        request.user = self.user
        request.session = self.client.session
        request.session['wallet_id'] = Wallet.objects.last().id
        request.modified = False
        response = ReviewView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        cart = Cart(request.session)
        self.assertEqual(len(cart.items), 0)
