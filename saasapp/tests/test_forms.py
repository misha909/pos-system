import datetime

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core import mail
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.test import TestCase, RequestFactory
from django.utils import timezone

from api.constants import GSA_DIRECT, DEALER_DIRECT
from accounts.models import UserProfile
from products.models import Category, Product
from saasapp.constants import SUPPLIERS
from saasapp.forms import (
    AddCardForm,
    AddSupplierForm,
    ChangeBillingAddressForm,
    ChangeShippingAddressForm,
    ChangeFFLForm,
    ChangePasswordForm,
    ForgotPasswordForm,
    SearchForm,
    UpdateMarginForm,
    UserInfoChangeForm,
)


def setUpModule():
    """
    Populate test database with fixtures.
    Use a separate haystack index for the test database.
    haystack.signals.RealtimeSignalProcessor will take care of the index.
    """
    call_command('clear_index', interactive=False)
    call_command('loaddata', 'categories')
    call_command('loaddata', 'distributors')
    call_command('loaddata', 'upcs')
    call_command('add_gsa_products')
    call_command('loaddata', 'products')


class SearchFormTestCase(TestCase):

    def setUp(self):
        self.data = {
            'q': '',
            'order_by': 'DESC',
            'distributors': '',
            'manufacturers': '',
            'price_filter': 0,
            'in_stock': 'false',
            'is_customer_view': 'true',
            'page': '1'
        }
        user = User.objects.create(
            first_name='Kevin',
            last_name='Lorenzana',
            username='user',
            password=make_password('password'),
        )
        self.userprofile = user.userprofile
        self.userprofile.davidson_verified = True
        self.userprofile.lipseys_verified = True
        self.userprofile.rsr_verified = True
        self.userprofile.green_verified = True
        self.userprofile.zanders_verified = True
        self.userprofile.camfour_verified = True
        self.userprofile.sports_verified = True
        self.userprofile.jerrys_verified = True
        self.userprofile.ellet_verified = True
        self.userprofile.margin = 30.0
        self.userprofile.save()

    def test_check_if_category(self):
        form = SearchForm()

        q = Category.objects.first()
        self.assertTrue(form.checkIfCategory(q.name))
        q = 'Apparel - Clothing'
        self.assertTrue(form.checkIfCategory(q))
        q = 'slingshot'
        self.assertFalse(form.checkIfCategory(q))

    def test_featured_first(self):
        featured_count = Product.objects.filter(
            distributor__name__in=(GSA_DIRECT, DEALER_DIRECT)
        ).count()
        form = SearchForm(self.data)
        self.assertTrue(form.is_valid())
        sqs = form.search(user_profile=self.userprofile)
        i = 0
        while i < sqs.count():
            if sqs[i].is_featured or sqs[i].is_dealer_direct:
                i += 1
            else:
                break
        self.assertEqual(i, featured_count)

    def test_active(self):
        count = Product.objects.filter(is_active=True, price__gt=0).count()
        form = SearchForm(self.data)
        self.assertTrue(form.is_valid())
        sqs = form.search(user_profile=self.userprofile)
        self.assertEqual(sqs.count(), count)

        product = Product.objects.get(id=sqs[0].pk)
        product.is_active = False
        product.save()

        sqs = form.search(user_profile=self.userprofile)
        self.assertEqual(sqs.count(), count-1)
        # reset inactive product
        product.is_active = True
        product.save()

    def test_price_filter(self):
        qs = Product.objects.filter(is_active=True, price__gt=0)
        q1 = Q(price__lte=50)
        q2 = Q(price__gte=50) & Q(price__lte=200)
        q3 = Q(price__gte=200) & Q(price__lte=500)
        q4 = Q(price__gte=500)

        i = 1
        while i <= 15:
            self.data['price_filter'] = i
            form = SearchForm(self.data)
            form.is_valid()
            sqs = form.search(user_profile=self.userprofile, record_search=False)
            final_q = Q(price__lt=-999999)
            if i % 2 == 1:
                final_q = final_q | q1
            if i / 2 % 2 == 1:
                final_q = final_q | q2
            if i / 4 % 2 == 1:
                final_q = final_q | q3
            if i / 8 % 2 == 1:
                final_q = final_q | q4
            final_count = qs.filter(final_q).count()
            self.assertEquals(sqs.count(), final_count)
            i += 1

    def test_distributor_filter_single(self):
        data = {
            'distributors': "Davidson's"
        }
        form = SearchForm(data)
        self.assertTrue(form.is_valid())
        sqs = form.search(user_profile=self.userprofile, record_search=False)
        for result in sqs:
            self.assertEqual(result.distributor_name, "Davidson's")

    def test_distributor_filter_multiple(self):
        data = {
            'distributors': "Davidson's|Lipseys"
        }
        form = SearchForm(data)
        self.assertTrue(form.is_valid())
        sqs = form.search(user_profile=self.userprofile, record_search=False)
        for result in sqs:
            self.assertIn(result.distributor_name, ["Davidson's", 'Lipseys'])

    def test_featured(self):
        data = {
            'featured': 'true'
        }
        form = SearchForm(data)
        self.assertTrue(form.is_valid())
        sqs = form.search(user_profile=self.userprofile, record_search=False)
        for result in sqs:
            self.assertEqual(result.distributor_name, GSA_DIRECT)

    def test_in_stock(self):
        data = {
            'in_stock': 'true'
        }
        form = SearchForm(data)
        self.assertTrue(form.is_valid())
        sqs = form.search(user_profile=self.userprofile, record_search=False)
        for result in sqs:
            self.assertTrue(result.is_available)


class AddCardFormTestCase(TestCase):
    def setUp(self):
        curr_year = timezone.now().year
        data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **data)
        self.card_data = {
            'name_on_card': '%s %s' % (
                self.user.first_name, self.user.last_name
            ),
            'card_number': '4012000098765439',
            'zip_code': '83642',
            'cvv': '999',
            'expiration_month': 12,
            'expiration_year': curr_year+2
        }

    def test_success(self):
        form = AddCardForm(self.card_data)
        self.assertTrue(form.is_valid())
        wallet = form.create_card_wallet(self.user)
        self.assertIsNotNone(wallet)

    def test_invalid_data_fail(self):
        self.card_data['cvv'] = '123'
        form = AddCardForm(self.card_data)
        self.assertFalse(form.is_valid())
        error = form.non_field_errors()[0]
        self.assertEqual(error, 'Invalid card data.')


class AddSupplierFormTestCase(TestCase):
    def setUp(self):
        data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **data)

    def test_success(self):
        for supplier in SUPPLIERS:
            data = {
                'supplier': supplier,
                'username': 'username',
                'password': 'password'
            }
            form = AddSupplierForm(data)
            self.assertTrue(form.is_valid())
            self.assertTrue(form.action(user=self.user))
            profile = UserProfile.objects.get(user__username='username')
            self.assertTrue(getattr(profile, supplier+'_verified'))
            self.assertEqual(getattr(profile, supplier+'_login'), 'username')
            self.assertEqual(getattr(profile, supplier+'_password'), 'password')


class ChangeAddressFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **self.data)

    def test_change_billing_success(self):
        data = {
            'street_address': 'Address',
            'city': 'City',
            'state': 'Alabama',
            'zip': '83642'
        }
        form = ChangeBillingAddressForm(data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.action(user=self.user))
        profile = UserProfile.objects.get(user=self.user)
        self.assertEqual(form.get_address(), profile.ffl_mailing_address)

    def test_change_shipping_success(self):
        data = {
            'street_address': 'Address',
            'city': 'City',
            'state': 'Wyoming',
            'zip': '83642'
        }
        form = ChangeShippingAddressForm(data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.action(user=self.user))
        profile = UserProfile.objects.get(user=self.user)
        self.assertEqual(form.get_address(), profile.ffl_premise_address)


class ChangeFFLFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **self.data)

    def test_success(self):
        data = {
            'first_three_digits': '982',
            'last_five_digits': '01710',
            'date_month': '01',
            'date_day': '1',
            'date_year': timezone.now().date().year+2
        }
        form = ChangeFFLForm(data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.action(user=self.user))
        profile = UserProfile.objects.get(user=self.user)
        self.assertEqual(
            profile.ffl_number_first_three,
            data['first_three_digits']
        )
        self.assertEqual(
            profile.ffl_number_last_five,
            data['last_five_digits']
        )
        self.assertEqual(
            profile.ffl_expiration_date,
            datetime.date(data['date_year'], 1, 1)
        )

    def test_invalid_ffl_fail(self):
        data = {
            'first_three_digits': '982',
            'last_five_digits': '01719',
            'date_month': '01',
            'date_day': '1',
            'date_year': timezone.now().date().year+2
        }
        form = ChangeFFLForm(data)
        self.assertFalse(form.is_valid())
        errors = form.non_field_errors()
        invalid_error = errors[0]
        self.assertEqual(invalid_error, 'Invalid FFL Number')


class ChangePasswordFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **self.data)

    def test_success(self):
        data = {
            'old_password': self.data['password'],
            'new_password': 'sh0rtpa55w0rd',
            'new_password_repeat': 'sh0rtpa55w0rd'
        }
        form = ChangePasswordForm(data, user=self.user)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.action(user=self.user))

        user = User.objects.first()
        self.assertTrue(user.check_password('sh0rtpa55w0rd'))

    def test_clean_old_password_fail(self):
        data = {
            'old_password': 'wrongpas55w0rd',
            'new_password': 'd0esntmatt3r',
            'new_password_repeat': 'd0esntmatt3r'
        }
        form = ChangePasswordForm(data, user=self.user)
        self.assertFalse(form.is_valid())
        errors = form.errors.as_data()
        password_error = errors.get('old_password')[0]
        self.assertIsInstance(password_error, ValidationError)
        self.assertEqual(password_error.message, 'Invalid password')

    def test_clean_password_mismatch_fail(self):
        data = {
            'old_password': self.data['password'],
            'new_password': 'pa55w0rd1',
            'new_password_repeat': 'pa55w0rd2'
        }
        form = ChangePasswordForm(data, user=self.user)
        self.assertFalse(form.is_valid())
        errors = form.non_field_errors()
        mismatch_error = errors[0]
        self.assertEqual(mismatch_error, 'The passwords do not match')


class ForgotPasswordFormTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **self.data)

    def test_clean_email_fail(self):
        form = ForgotPasswordForm({'email': 'non@existent.com'})
        self.assertFalse(form.is_valid())
        error = form.errors.as_data().get('email')[0]
        self.assertEqual(error.message, 'User does not exist.')

    def test_generate_token(self):
        form = ForgotPasswordForm({'email': self.data.get('email')})
        self.assertTrue(form.is_valid())
        token = form.generate_token()
        self.assertTrue(token.valid)
        print token.token

    def test_send_reset_password(self):
        request = self.factory.post(reverse('forgot-password'), {
            'password': self.data.get('password')
        })
        form = ForgotPasswordForm({'email': self.data.get('email')})
        self.assertTrue(form.is_valid())
        token = form.generate_token()
        form.send_reset_password(request, token)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.subject, 'Reset Your FFLDesign.com password')


class UpdateMarginFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **self.data)
        self.user.userprofile.margin = 10.0
        self.user.userprofile.save()

    def test_success(self):
        data = {
            'margin': 45.0
        }
        form = UpdateMarginForm(data)
        self.assertTrue(form.is_valid())

        user = User.objects.first()
        self.assertTrue(form.action(user=self.user))
        profile = user.userprofile
        self.assertEqual(profile.margin, data['margin'])

    def test_clean_margin_fail(self):
        data = {
            'margin': -1.0
        }
        form = UpdateMarginForm(data)
        self.assertFalse(form.is_valid())
        errors = form.errors.as_data()
        margin_error = errors.get('margin')[0]
        self.assertIsInstance(margin_error, ValidationError)
        self.assertEqual(margin_error.message, 'Invalid margin')


class UserInfoChangeFormTestCase(TestCase):
    def setUp(self):
        data = {
            'email': 'email@address.com',
            'password': 'longpa55word123',
            'first_name': 'John',
            'last_name': 'Doe'
        }
        self.user = User.objects.create_user('username', **data)

    def test_success(self):
        data = {
            'first_name': 'John',
            'last_name': 'McClane',
            'email_address': 'sarahconnor@skynet.com'
        }
        form = UserInfoChangeForm(data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.action(user=self.user))
        user = User.objects.first()
        self.assertEqual(user.email, data['email_address'])
        self.assertEqual(user.first_name, data['first_name'])
        self.assertEqual(user.last_name, data['last_name'])
