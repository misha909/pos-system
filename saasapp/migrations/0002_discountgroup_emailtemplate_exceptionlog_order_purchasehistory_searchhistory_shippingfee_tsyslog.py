# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import saasapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20160321_1807'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_auto_20160321_1807'),
        ('saasapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiscountGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount_discount', models.FloatField()),
                ('buyer', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EmailTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.TextField(help_text=b'Email content subject when user order products.', null=True, verbose_name=b'Email content subject', blank=True)),
                ('message', models.TextField(help_text=b'Email content message when user order products.', null=True, verbose_name=b'Email content message', blank=True)),
                ('footer', models.TextField(help_text=b'Email content footer when user order products.', null=True, verbose_name=b'Email content footer', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Email Template Message',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExceptionLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('exception', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('manufacturer', models.CharField(max_length=50)),
                ('order_number', models.CharField(max_length=50, null=True, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('payment_type', models.CharField(blank=True, max_length=12, null=True, choices=[(b'Credit Card', b'Credit Card'), (b'ACH', b'ACH')])),
                ('fed_ex_master_track_number', models.CharField(max_length=30, null=True, blank=True)),
                ('fed_ex_product_track_number', models.CharField(max_length=30, null=True, blank=True)),
                ('shipping_cost', models.FloatField(default=0)),
                ('account_number', models.CharField(max_length=4, null=True, blank=True)),
                ('shipping_status', models.CharField(blank=True, max_length=12, null=True, choices=[(b'In Transit', b'In Transit'), (b'Delivered', b'Delivered'), (b'Picked up', b'Picked up')])),
                ('shipping_label', models.ImageField(null=True, upload_to=saasapp.models.get_label_filename, blank=True)),
                ('shipping_label_pdf', models.FileField(null=True, upload_to=saasapp.models.get_label_pdf_filename, blank=True)),
                ('tracking_number', models.CharField(max_length=50, null=True, blank=True)),
                ('invoice_url', models.CharField(max_length=255, null=True, blank=True)),
                ('completed', models.BooleanField(default=False)),
                ('salesforce_id', models.CharField(max_length=50, null=True, blank=True)),
                ('sot_needed', models.BooleanField(default=False)),
                ('group', models.ForeignKey(related_name='orders', blank=True, to='saasapp.DiscountGroup', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('wallet', models.ForeignKey(blank=True, to='accounts.Wallet', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PurchaseHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('upc', models.CharField(max_length=50, null=True, blank=True)),
                ('model', models.CharField(max_length=50, null=True, blank=True)),
                ('qty', models.IntegerField(default=0)),
                ('supplier', models.CharField(max_length=50, null=True, blank=True)),
                ('manufacture', models.CharField(max_length=50, null=True, blank=True)),
                ('unit_price', models.FloatField(default=0)),
                ('total_price', models.FloatField(default=0)),
                ('confirmed', models.BooleanField(default=False)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('shipment_provider', models.CharField(max_length=50, null=True, blank=True)),
                ('mail_service', models.CharField(max_length=50, null=True, blank=True)),
                ('salesforce_id', models.CharField(max_length=50, null=True, blank=True)),
                ('order', models.ForeignKey(blank=True, to='saasapp.Order', null=True)),
                ('product', models.ForeignKey(blank=True, to='products.Product', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SearchHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('searched_term', models.CharField(max_length=500, null=True, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShippingFee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('salesforce_id', models.CharField(max_length=50, null=True, blank=True)),
                ('shipment_provider', models.CharField(max_length=50, null=True, blank=True)),
                ('mail_service', models.CharField(max_length=50, null=True, blank=True)),
                ('cost', models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True)),
                ('selected', models.BooleanField(default=False)),
                ('order', models.ForeignKey(blank=True, to='saasapp.Order', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TsysLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('request', models.TextField()),
                ('response', models.TextField()),
                ('is_error', models.BooleanField(default=False)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
