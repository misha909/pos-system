from django.conf import settings
from django.contrib.sites.models import Site

from products.models import Product, Category
from saasapp.models import DiscountGroup
from accounts.utils import get_state_from_zip

from functools import reduce
from decimal import Decimal

import csv


DISTRIBUTOR_SITE_DICT = {
        'Davidson\'s'     : 'http://www11.davidsonsinc.com/Login/Login.aspx',
        'Sport\'s South'  : 'http://www.theshootingwarehouse.com/',
        'RSR Group'           : 'https://www.rsrgroup.com/catalog/login',
        'Lipseys'       : 'https://www.lipseys.com/login.aspx',
        'Green Supply'        : 'https://www.dealerease.net/catalog/cart/login/?ret_id=140067',
        }


FEATURED_MANUFACTURERS = [
    u'SRM Arms',
    u'MILTAC Industries',
    u'Crossfire Shooting Gear',
    u'Founding Fathers Armory',
    u'Odin Works',
    u'2A Armament',
    u'WM Tactical',
]


def get_search_criteria_results(category, optional_manu_list=None):
    results = []
    if not category in ('price', 'qty'):
        category = 'upc__%s' % category
    products = Product.objects.order_by(category).values(category).distinct()
    for product in products:
        if category == 'upc__category':
            if product.get(category) is None:
                continue
            cat = Category.objects.get(pk=product.get(category))
            to_add = cat.name
            while cat.parent is not None:
                to_add = cat.parent.name + ' - ' + to_add
                cat = cat.parent
            results.append(to_add)
            continue
        if product.get(category):
            results.append(product.get(category))

    if category == 'upc__manufacturer':
        featured_manufacturers = FEATURED_MANUFACTURERS
        if optional_manu_list:
            for manu in optional_manu_list:
                featured_manufacturers.append(manu.company_name)
        featured_manufacturers.sort(reverse=True)  # Reverse and insert them into list backwards

        counter = 0
        # Pull Featured Manufacturers from the results list and add them to the top
        for featured_manu in featured_manufacturers:
            if featured_manu in results:
                results.remove(featured_manu)
                results.insert(0, featured_manu)
                counter += 1

        results.insert(counter, u'')       # Insert a space between featured manufacturers and normal manufacturers
        results.insert(counter, u'--------------------------------')  # Insert a separator
        results.insert(0, u'FEATURED MANUFACTURERS')
        results.insert(0, u'')


    return results


def get_image_urls(product):
    urls = {1: '', 2: '', 3: '', 4: '', 5: ''}
    product_images = product.productlistingimage_set.all()
    i = 1

    for img in product_images:
        urls[i] = 'http://' + Site.objects.get_current().domain + '/' + img.image.url
        i += 1

    return urls[1], urls[2], urls[3], urls[4], urls[5]


def verify_routing_number(number):
    verified = False
    bank_name = 'Invalid'
    file_path = settings.ROUTING_NUMBER_CSV_FILE

    with open(file_path, 'r') as csvfile:
        file_reader = csv.reader(csvfile)
        for row in file_reader:
            if row[2] == str(number):
                bank_name = row[0]
                verified = True

        return {
                'verified': verified,
                'bank_name': bank_name
                }


def string_cleaner(entry):
    return ''.join(e for e in entry if e.isalnum())


def get_item_list_from_cart(cart):
    manufacturer_list = {}
    item_list = []
    for item in cart.items:
        manufacturer = item.product.upc.manufacturer
        if manufacturer not in manufacturer_list:
            manufacturer_list[manufacturer] = len(item_list)
            item_list.append({
                'name': manufacturer,
                'items': [],
                'subtotal': 0,
                'product': item.product
            })
        index = manufacturer_list[manufacturer]
        item_list[index]['items'].append((item))
        item_list[index]['subtotal'] += item.subtotal
    return item_list


def apply_shipping_discounts(formset, cost, userprofile):
    zip_code = userprofile.premise_zip_code or userprofile.address_zip
    state = get_state_from_zip(zip_code)
    if state in ('AK', 'PR', 'GU', 'HI'):
        if cost >= 2500:
            for form in formset:
                try:
                    for sf in form.fields['shipping_fee'].queryset:
                        sf.cost = 0
                except:
                    pass


def get_discount_total(orders=[], coupon=None):
    discount = 0.0
    if not coupon:
        return Decimal(discount)
    if coupon.amount_discount > 0.0:
        if coupon.manufacturer is None:
            discount = coupon.amount_discount
        else:
            for order in orders:
                if coupon.manufacturer.company_name == order.manufacturer:
                    discount += min(order.transaction_amount, coupon.amount_discount)
    else:
        for order in orders:
            if coupon.manufacturer is None or \
               coupon.manufacturer.company_name == order.manufacturer:
                discount += order.transaction_amount * (coupon.percent_discount/100.0)
    return Decimal(discount)


def apply_coupon_discounts(orders=[], user=None, coupon=None):
    if coupon:
        if coupon.duration == 'Once':
            coupon.redeemed_by.add(user)
        if coupon.amount_discount > 0.0:
            group = DiscountGroup.objects.create(
                buyer=user,
                amount_discount=get_discount_total(
                    orders=orders, coupon=coupon
                )
            )
            for order in orders:
                order.group = group
                order.save()
