import decimal

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template import Context, RequestContext, Template
from django.template.loader import render_to_string

from .salesforce import get_account_number


class EmailPurchaseMixin(object):
    """
    Constains methods that are used by both the API version
    and web version of order purchase.
    
    Note:
    - Both versions accept different input parameters.
      The API version is a simplified web version.
      The web version has complicated code in the javascript part
      (which can be simplified) and it will take time to simplify
      thee existing code.
      The methods in this mixin is compatible to both version
      regardless of the difference in input parameters.
    """
    
    order = None
    davidson_list = []
    lipseys_list = []
    green_list = []
    rsr_list = []
    zanders_list = []
    camfour_list = []
    sports_list = []
    gsa_direct_list = []
    jerrys_list = []
    ellet_list = []
    
    def append_order(self, purchase_record):
        if purchase_record.supplier == "Davidson's":
            self.davidson_list.append(purchase_record)
        elif purchase_record.supplier == "Lipseys":
            self.lipseys_list.append(purchase_record)
        elif purchase_record.supplier == "Green Supply":
            self.green_list.append(purchase_record)
        elif purchase_record.supplier == "RSR Group":
            self.rsr_list.append(purchase_record)
        elif purchase_record.supplier == "Gzanders":
            self.zanders_list.append(purchase_record)
        elif purchase_record.supplier == "Camfour":
            self.camfour_list.append(purchase_record)
        elif purchase_record.supplier == "Sport's South":
            self.sports_list.append(purchase_record)
        elif purchase_record.supplier == "GSA Direct":
            self.gsa_direct_list.append(purchase_record)
        elif purchase_record.supplier == "Jerry's Sports Center":
            self.jerrys_list.append(purchase_record)
        elif purchase_record.supplier == "Ellet":
            self.ellet_list.append(purchase_record)
    
    def handle_purchase(self, purchase_data, email, email_template, supplier):
        mail_message = None
        mail_footer = None
        mail_subject = None
        
        if email_template:
            mail_message_template = Template(email_template.message)
            account_id = None
            account_name = None
            if supplier == "Davidson's":
                account_name = self.request.user.userprofile.davidson_account_name
            elif supplier == "Lipseys":
                account_name = self.request.user.userprofile.lipseys_account_name
            elif supplier == "Green Supply":
                account_name = self.request.user.userprofile.green_account_name
            elif supplier == "RSR Group":
                account_name = self.request.user.userprofile.rsr_account_name
            elif supplier == "Gzanders":
                account_name = self.request.user.userprofile.zanders_account_name
            elif supplier == "Camfour":
                account_name = self.request.user.userprofile.camfour_account_name
            elif supplier == "Sport's South":
                account_name = self.request.user.userprofile.sports_account_name
            elif supplier == "GSA Direct":
                (account_id, account_name) = get_account_number(self.request.user)
            elif supplier == "Jerry's Sports Center":
                account_name = self.request.user.userprofile.jerrys_account_name
            elif supplier == "Ellet":
                account_name = self.request.user.userprofile.ellet_account_name
            mail_message_context = Context({
                "account_name": account_name
            })
            mail_message = mail_message_template.render(mail_message_context)
            
            mail_footer_template = Template(email_template.footer)
            mail_footer_context = Context({
                "name": "%s %s" % (self.request.user.first_name, self.request.user.last_name)
            })
            mail_footer = mail_footer_template.render(mail_footer_context)
            
            subject_template = Template(email_template.subject)
            subject_context = Context({
                "order": self.order.pk
            })
            subject = subject_template.render(subject_context)
        
        # Compute total price
        total_price = 0.0
        for data in purchase_data:
            total_price = total_price + float(data.total_price)
        
        request_context = RequestContext(self.request)
        text_content = render_to_string(
            'saas_app/email/email.txt', {
                'order_info': purchase_data,
                'mail_message':mail_message,
                'total_price':total_price,
                'mail_footer':mail_footer
            },
            request_context
        )
        
        html_content = render_to_string(
            'saas_app/email/email.html', {
                'order_info': purchase_data,
                'mail_message': mail_message,
                'total_price': total_price,
                'mail_footer': mail_footer,
                'confirm_button_url': self.request.build_absolute_uri(
                    reverse(
                        'confirm-order',
                        kwargs={
                            'user_id': self.request.user.pk,
                            'order_id': self.order.pk,
                            'supplier': supplier
                        }
                    )
                ),
                'account_id': account_id,
                'account_name': account_name
            },
            request_context
        )
        
        html_content_user = render_to_string(
            'saas_app/email/email.html', {
                'order_info': purchase_data,
                'mail_message': mail_message,
                'total_price': total_price,
                'mail_footer': mail_footer
            },
            request_context
        )
        
        if not isinstance(email, list):
            email = [email]
        
        # Send email to supplier representative
        print "Email:", email
        msg = EmailMultiAlternatives(
                subject,
                text_content,
                'FFL Design <%s>' % settings.EMAIL_HOST_USER,
                email
            )
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        
        # Send email to user
        msg_user = EmailMultiAlternatives(
                subject,
                text_content,
                'FFL Design <%s>' % settings.EMAIL_HOST_USER,
                [self.request.user.email]
            )
        msg_user.attach_alternative(html_content_user, "text/html")
        msg_user.send()
        
    
    def send_email(self, email_template):
        if self.davidson_list and self.request.user.userprofile.davidson_mail:
            self.handle_purchase(
                self.davidson_list,
                self.request.user.userprofile.davidson_mail,
                email_template,
                "Davidson's"
            )
            self.davidson_list = []

        if self.lipseys_list and self.request.user.userprofile.lipseys_mail:
            self.handle_purchase(
                self.lipseys_list,
                self.request.user.userprofile.lipseys_mail,
                email_template,
                "Lipseys"
            )
            self.lipseys_list = []

        if self.green_list and self.request.user.userprofile.green_mail:
            self.handle_purchase(
                self.green_list,
                self.request.user.userprofile.green_mail,
                email_template,
                "Green Supply"
            )
            self.green_list = []
            
        if self.rsr_list and self.request.user.userprofile.rsr_mail:
            self.handle_purchase(
                self.rsr_list,
                self.request.user.userprofile.rsr_mail,
                email_template,
                "RSR Group"
            )
            self.rsr_list = []
        
        if self.zanders_list and self.request.user.userprofile.zanders_mail:
            self.handle_purchase(
                self.zanders_list,
                self.request.user.userprofile.zanders_mail,
                email_template,
                "Gzanders"
            )
        
        if self.camfour_list and self.request.user.userprofile.camfour_mail:
            self.handle_purchase(
                self.camfour_list,
                self.request.user.userprofile.camfour_mail,
                email_template,
                "Camfour"
            )
        
        if self.sports_list and self.request.user.userprofile.sports_mail:
            self.handle_purchase(
                self.sports_list,
                self.request.user.userprofile.sports_mail,
                email_template,
                "Sport's South"
            )
        
        if self.gsa_direct_list:
            self.handle_purchase(
                self.gsa_direct_list,
                settings.GSA_DIRECT_EMAILS,
                email_template,
                "GSA Direct"
            )
        
        if self.jerrys_list and self.request.user.userprofile.jerrys_mail:
            self.handle_purchase(
                self.jerrys_list,
                self.request.user.userprofile.jerrys_mail,
                email_template,
                "Jerry's Sports Center"
            )
        
        if self.ellet_list and self.request.user.userprofile.ellet_mail:
            self.handle_purchase(
                self.ellet_list,
                self.request.user.userprofile.ellet_mail,
                email_template,
                "Ellet"
            )

    def clear_list(self):
        self.order = None
        self.davidson_list = []
        self.lipseys_list = []
        self.green_list = []
        self.rsr_list = []
        self.zanders_list = []
        self.camfour_list = []
        self.sports_list = []
        self.gsa_direct_list = []
        self.jerrys_list = []
        self.ellet_list = []
