import json
import requests

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import (
        HttpResponse,
        HttpResponseRedirect,
        HttpResponseNotAllowed,
)
from django.forms import modelformset_factory
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View, FormView, TemplateView, ListView


from braces.views import JSONResponseMixin, LoginRequiredMixin
from carton.cart import Cart
from haystack.query import SearchQuerySet

from accounts.decorators import approval_required
from accounts.mixins import ApprovalRequiredMixin
from accounts.models import Wallet, FFL, ResetPasswordToken, UserProfile
from accounts.utils import send_order_notification_email
from dealerexchange.models import ShippingInfo
from products.constants import HANDGUNS
from products.models import Product, ProductCoupon, ProductListingImage
from .constants import CREDIT_CARD, BANK_ACCOUNT
from .models import *
from .forms import (
    PaginatedSearchForm,
    ForgotPasswordForm,
    ResetPasswordForm,
    ChangePasswordForm,
    UpdateMarginForm,
    ChangeShippingCarrierForm,
    ChangeBankInfoForm,
    AddSupplierForm,
    ChangeFFLForm,
    ChangeBillingAddressForm,
    UserInfoChangeForm,
    ShippingOptionsForm,
    UploadfflForm,
    UploadSOTForm,
    AddCardForm,
    AddBankAccountForm
)
from .salesforce import (
    create_account,
    create_contact,
    create_order,
    create_purchase_history,
    get_order_id,
    create_opportunities,
    create_ach_file,
    create_product_2_de,
    get_account_approval_by_email,
)
from .utils import (
    get_search_criteria_results,
    DISTRIBUTOR_SITE_DICT,
    string_cleaner,
    get_item_list_from_cart,
    apply_shipping_discounts,
    get_discount_total,
    apply_coupon_discounts,
)


def box(request):
    return render(request, '004_inventory_varmory_004_v009.html')


class SearchHistoryView(ApprovalRequiredMixin, ListView):
    template_name = 'search-result.html'
    context_object_name = 'history'

    def get_queryset(self, **kwargs):
        return SearchHistory.objects.filter(user=self.request.user
                                            ).order_by('-date')


class OrderHistoryView(ApprovalRequiredMixin, ListView):
    template_name = 'order_search_history.html'
    context_object_name = 'orders'

    def get_queryset(self, **kwargs):
        return Order.objects.filter(user=self.request.user, completed=True
                                    ).order_by('-date')


class SettingsView(ApprovalRequiredMixin, TemplateView):
    template_name = 'settings.html'

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        selected_form = None
        post = self.request.POST
        ffl_number = "%s%s" % (
            self.request.user.userprofile.ffl_number_first_three,
            self.request.user.userprofile.ffl_number_last_five
        )
        phone_number = ''
        try:
            ffl = FFL.objects.get(ffl_number=ffl_number)
            phone_number = ffl.phone
        except Exception:
            pass

        try:
            selected_form = kwargs.pop('selected_form')
        except KeyError:
            pass
        password_form = ChangePasswordForm(
            user=self.request.user, prefix='password',
        )
        margin_form = UpdateMarginForm(
            prefix='margin',
            initial={'margin': self.request.user.userprofile.margin}
        )
        wallet = Wallet.objects.filter(user=self.request.user).first()
        routing_number = "*******************"
        account_number = "*******************"
        if wallet:
            routing_number = wallet.routing_number
            account_number = wallet.account_number
            if account_number:
                account_number = "********" + account_number[-4:]
        bank_form = ChangeBankInfoForm(
            user=self.request.user, 
            prefix='de_bank',
            initial={
                'routing_number': routing_number,
                'account_number': account_number,
                }
        )
        fedex = False
        ups = False
        usps = False 
        fedex_drop = False
        ups_drop = False
        usps_drop = False
        shipping_info = ShippingInfo.objects.\
                filter(user_profile=self.request.user.userprofile).first()
        if shipping_info:
            shipping_info = self.request.user.userprofile.shipping_info
            fedex = shipping_info.FedEx
            ups = shipping_info.UPS
            usps = shipping_info.USPS
            fedex_drop = shipping_info.FedEx_drop
            ups_drop = shipping_info.UPS_drop
            usps_drop = shipping_info.USPS_drop
        carrier_form = ChangeShippingCarrierForm(
            user=self.request.user,
            prefix='de_carrier',
            initial={
                'fedex': fedex,
                'ups': ups,
                'usps': usps,
                'fedex_drop': fedex_drop,
                'ups_drop': ups_drop,
                'usps_drop': usps_drop,
                }
        )

        supplier_form = AddSupplierForm(prefix='supplier')
        ffl_form = ChangeFFLForm(prefix='ffl')
        # shipping_form = ChangeShippingAddressForm(prefix='shipping')
        billing_form = ChangeBillingAddressForm(prefix='billing')
        info_form = UserInfoChangeForm(
            prefix='user_info',
            initial={
                'email_address': self.request.user.email,
                'phone_number': phone_number
            }
        )

        if selected_form == 'password':
            password_form = ChangePasswordForm(
                post, user=self.request.user, prefix='password',
            )
        if selected_form == 'de_carrier':
            carrier_form = ChangeShippingCarrierForm(
                post, user=self.request.user, prefix='de_carrier'
            )
        if selected_form == 'de_bank':
            bank_form = ChangeBankInfoForm(
                post, user=self.request.user, prefix='de_bank'
            )
        if selected_form == 'user_info':
            info_form = UserInfoChangeForm(post, prefix='user_info')
        # if selected_form == 'shipping':
        #     shipping_form = ChangeShippingAddressForm(post, prefix='shipping')
        if selected_form == 'billing':
            billing_form = ChangeBillingAddressForm(post, prefix='billing')
        if selected_form == 'ffl':
            ffl_form = ChangeFFLForm(post, prefix='ffl')
        if selected_form == 'add_supplier':
            supplier_form = AddSupplierForm(post, prefix='supplier')

        context['change_password_form'] = password_form
        context['margin_form'] = margin_form
        context['add_supplier_form'] = supplier_form
        context['change_ffl_form'] = ffl_form
        context['carrier_form'] = carrier_form
        context['bank_form'] = bank_form
        # context['change_shipping_address_form'] = shipping_form
        context['change_billing_address_form'] = billing_form
        context['user_info_change_form'] = info_form
        context['phone_number'] = phone_number
        return context

    def post(self, request, *args, **kwargs):
        selected_form = request.POST.get('selected-form')
        profile = request.user.userprofile
        form = None
        if selected_form == 'password':
            form = ChangePasswordForm(
                request.POST,
                user=request.user,
                prefix='password'
            )
        elif selected_form == 'user_info':
            form = UserInfoChangeForm(request.POST, prefix='user_info')
        # elif selected_form == 'shipping':
        #     form = ChangeShippingAddressForm(request.POST, prefix='shipping')
        elif selected_form == 'billing':
            form = ChangeBillingAddressForm(request.POST, prefix="billing")
        elif selected_form == 'ffl':
            form = ChangeFFLForm(request.POST, prefix='ffl')
        elif selected_form == 'delete_supplier':
            if 'supplier' in request.POST:
                supplier = request.POST['supplier']
                if supplier in SUPPLIERS:
                    setattr(profile, supplier+'_verified', False)
                    setattr(profile, supplier+'_login', '')
                    setattr(profile, supplier+'_password', '')
                    profile.save()
                    messages.success(request, 'Supplier has been removed')
        elif selected_form == 'add_supplier':
            form = AddSupplierForm(request.POST, prefix='supplier')
        elif selected_form == 'margin':
            form = UpdateMarginForm(request.POST, prefix='margin')
        elif selected_form == 'de_carrier':
            form = ChangeShippingCarrierForm(
                    request.POST,
                    user=self.request.user,
                    prefix='de_carrier')
        elif selected_form == 'de_bank':
            form = ChangeBankInfoForm(
                    request.POST,
                    user=self.request.user,
                    prefix='de_bank')
        if form is not None and form.is_valid():
            message = form.action(user=request.user)
            messages.success(request, message)
            create_account(user=User.objects.get(id=request.user.id))
            create_contact(user=User.objects.get(id=request.user.id))
        elif form is not None:
            kwargs['selected_form'] = selected_form
            messages.error(request, 'Fix form errors.')
        return render(request, 'settings.html', self.get_context_data(**kwargs))


def add_product(request):
    cart = Cart(request.session)
    product = Product.objects.get(id=request.GET.get('product_id'))
    qty = int(request.GET.get('user_quantity', 1))
    price = product.price
    if product.bulk_tier_prices:
        bulk_tier_prices = json.loads(product.bulk_tier_prices)
        for tier in bulk_tier_prices:
            if int(tier['start']) <= qty <= int(tier['finish']):
                price = float(tier['cost'])
                break
    if request.session.get('cv', False):
        try:
            margin = UserProfile.objects.get(user=request.user).margin
        except:
            margin = 30.0

        price_increased = (price * margin) / 100.00
        price += price_increased
        cart.add(product, price=price, quantity=qty)
    else:
        cart.add(product, price=price, quantity=qty)
    return HttpResponse("Added")


@csrf_exempt
def remove_product(request):
    cart = Cart(request.session)
    product = Product.objects.get(id=request.GET.get('product_id'))
    cart.remove(product)
    return HttpResponse("Removed")


def update_product(request):
    cart = Cart(request.session)
    product = Product.objects.get(id=request.GET.get('product_id'))
    new_qty = int(request.GET.get('new_qty'))
    try:
        qty = int(product.qty)
    except Exception:
        qty = 0
    if new_qty <= qty:
        cart.set_quantity(product, quantity=request.GET.get('new_qty'))
        return HttpResponse('true')
    return HttpResponse('false')


@login_required
def set_customer_view(request):
    if request.is_ajax():
        cv = request.POST.get('cv')
        if cv == 'true':
            request.session['cv'] = True
        else:
            request.session['cv'] = False
        return HttpResponse('ok')
    return HttpResponseNotAllowed(['POST'])


@login_required
@approval_required
@csrf_exempt
def Product_Search(request):
    profile_data = User.objects.get(id=request.user.id)
    user_profile = UserProfile.objects.get(user = profile_data)

    # Caliber Results
    # caliber_results = get_search_criteria_results('caliber')

    # Category Results
    cat_results = get_search_criteria_results('category')

    # Featured Manufacturers
    featured_manufacturers = list()
    ids = Product.objects.filter(dealer_exchange=False).values('user_profile_id')
    for profile in UserProfile.objects.filter(user_id__in=ids):
        if Product.objects.filter(user_profile_id=profile.user_id,
                is_active=True,
                ).count() > 0:
            if profile.company_name == 'SRM Arms':
                featured_manufacturers.insert(0, profile)
            else:
                featured_manufacturers.append(profile)

    # Manufacturer Results
    manu_results = get_search_criteria_results('manufacturer', featured_manufacturers)

    context = {
        'profiledata': profile_data,
        'manu_results': manu_results,
        'featured_manufacturers': featured_manufacturers,
        'cat_results': cat_results,
        'userprofile': user_profile,
        'current_page': 1
    }

    response = render(request, 'search-items-list.html', context)
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    return response


""" Function to call when user performs search with any criteria"""
@csrf_exempt
def AdvanceSearch(request):
    profile_data = User.objects.get(id=request.user.id)
    user_profile = UserProfile.objects.get(user=profile_data)

    if request.POST:
        q = request.POST['q']
        # print request.POST
        form = PaginatedSearchForm(request.POST)
        try:
            if form.is_valid():

                # Non-ajax request happens during SearchHistory
                if not request.is_ajax():
                    (results, num_pages) = form.paginated_search(user_profile, record_search=False)
                    # Caliber Results
                    caliber_results = get_search_criteria_results('caliber')

                    # Category Results
                    cat_results = get_search_criteria_results('category')

                    # Manufacturer Results
                    manu_results = get_search_criteria_results('manufacturer')

                    # Pagination variables
                    current_page = form.cleaned_data.get('page', 1)
                    if current_page is None:
                        current_page = 1
                    pagination_range = range(current_page, min(num_pages, current_page + 10) + 1)

                    # Work-around to display either Jerry's or Ellet since they have the same products
                    show_ellet = False
                    if form.cleaned_data.get('distributors') == 'Ellet':
                        show_ellet = True
                    elif not user_profile.jerrys_verified and user_profile.ellet_verified:
                        show_ellet = True

                    grid_html = render_to_string(
                        'saas_app/product-search-result-grid.html', RequestContext(request, {
                            'results': results,
                            'is_customer_view': form.cleaned_data.get('is_customer_view', False),
                            'user_profile': user_profile,
                            'num_pages': num_pages,
                            'current_page': current_page,
                            'pagination_range': pagination_range,
                            'show_ellet': show_ellet
                        })
                    )

                    return render(
                        request,
                        'search-items-list.html',
                        {
                            'profiledata': profile_data,
                            'userprofile': user_profile,
                            'searched_term': form.cleaned_data.get('q'),
                            'history_record': True,
                            'results': results,
                            'grid_html': grid_html,
                            'manu_results': manu_results,
                            'caliber_results': caliber_results,
                            'cat_results': cat_results,
                            'num_pages': num_pages,
                            'current_page': current_page,
                            'pagination_range': pagination_range,
                            'show_ellet': show_ellet
                        }
                    )
                else:
                    (results, num_pages) = form.paginated_search(user_profile)
                    print 'found %d results' % len(results)
                    list_results = []
                    for result in results:
                        qty = result.qty
                        if qty is None or qty == 'ALLOCATED':
                            qty = 0
                        listing_detail_urls = []
                        listing_thumbnail_urls = []
                        product = result.object
                        if (result.is_dealer_direct or result.is_dealer_exchange) and \
                            ProductListingImage.objects.filter(product__id=result.product_id).exists(): 
                            images = ProductListingImage.objects.filter(
                                product__id=result.product_id
                            )
    #                        result.category_name = product.upc.listing_category
                            try:
                                result.picture = images.first().image.url
                                result.thumb_grid_image_url = images.first().image.url
                                result.thumb_list_image_url = images.first().image_thumbnail.url
                            except:
                                pass
                            for image in images:
                                listing_detail_urls.append(image.image.name)
                                listing_thumbnail_urls.append(image.image_thumbnail.name)

                        if result.is_dealer_direct:
                            is_dealer_direct = result.is_dealer_direct
                            company_name = result.manufacturer
                            distributor = result.distributor
                            distributor_name = result.distributor_name
                        else:
                            distributor = product.distributor
                            distributor_name = distributor.name
                            is_dealer_direct = False
                            company_name = result.manufacturer
                        try:
                            result.bulk_tier_prices = json.loads(result.object.bulk_tier_prices)
                        except:
                            result.bulk_tier_prices = []
                        
                        list_results.append({
                            'pk': result.pk,
                            'is_dealer_direct': is_dealer_direct,
                            'picture': result.picture,
                            'is_full_path_image_url': result.is_full_path_image_url,
                            'model_value': result.model_value,
                            'caliber': result.caliber,
                            'item_number': result.item_number,
                            'manufacturer': company_name,
                            'distributor': distributor,
                            'distributor_name': distributor_name,
                            'distributor_site_url': DISTRIBUTOR_SITE_DICT.get(distributor_name),
                            'qty': qty,
                            'price': result.price,
                            'bulk_tier_prices': result.bulk_tier_prices,
                            'upc_value': result.upc_value,
                            'stock': result.stock,
                            'capacity': result.capacity,
                            'finish': result.finish,
                            'weight': result.weight,
                            'length_barrel': result.length_barrel,
                            'length_overall': result.length_overall,
                            'description': result.description,
                            'short_description': result.short_description,
                            'category_name': result.category_name,
                            'listing_type': product.upc.listing_type,
                            'listing_category': product.upc.listing_category,
                            'product': product,
                            'is_featured': result.is_featured,
                            'is_dealer_exchange': result.is_dealer_exchange,
                            'thumb_grid_url': result.thumb_grid_image_url,
                            'thumb_list_url': result.thumb_list_image_url,
                            'detail_popup_url': result.detail_popup_image_url,
                            'detail_popup_urls': listing_detail_urls,
                            'detail_thumb_urls': listing_thumbnail_urls,
                            'gun_type': result.gun_type,
                            'sights': result.sights,
                            'action': result.action,
                            'video_url': product.video_url,

                        })
                        print 'RESULT VIDEO URL == ', product.video_url
                    # print list_results
                    # Pagination variables
                    current_page = form.cleaned_data.get('page', 1)
                    if current_page is None:
                        current_page = 1
                    pagination_range = range(current_page, min(num_pages, current_page + 10) + 1)

                    # Work-around to display either Jerry's or Ellet since they have the same products
                    show_ellet = False
                    if form.cleaned_data.get('distributors') == 'Ellet':
                        show_ellet = True
                    elif not user_profile.jerrys_verified and user_profile.ellet_verified:
                        show_ellet = True

                    list_rendered_results = render_to_string(
                        'saas_app/product-search-result.html', RequestContext(request, {
                            'results': list_results,
                            'is_customer_view': form.cleaned_data.get('is_customer_view', False),
                            'user_profile': user_profile,
                            'num_pages': num_pages,
                            'current_page': current_page,
                            'pagination_range': pagination_range,
                            'show_ellet': show_ellet
                        })
                    )
                    context = RequestContext(request, {
                            'results': list_results,
                            'is_customer_view': form.cleaned_data.get('is_customer_view', False),
                            'user_profile': user_profile,
                            'num_pages': num_pages,
                            'current_page': current_page,
                            'pagination_range': pagination_range,
                            'show_ellet': show_ellet
                        })
                    grid_rendered_results = render_to_string(
                        'saas_app/product-search-result-grid.html', RequestContext(request, {
                            'results': list_results,
                            'is_customer_view': form.cleaned_data.get('is_customer_view', False),
                            'user_profile': user_profile,
                            'num_pages': num_pages,
                            'current_page': current_page,
                            'pagination_range': pagination_range,
                            'show_ellet': show_ellet
                        })
                    )
                    pagination_html = render_to_string(
                        'saas_app/pagination.html', RequestContext(request, {
                            'num_pages': num_pages,
                            'current_page': current_page,
                            'pagination_range': pagination_range
                        })
                    )

                    data = {
                        'list_html': list_rendered_results,
                        'grid_html': grid_rendered_results,
                        'pagination_html': pagination_html,
                        'num_pages': num_pages,
                        'current_page': current_page,
                        'results_found': len(results) != 0
                    }
                    return HttpResponse(json.dumps(data),content_type="application/json")
            else:
                print "Form Errors:", form.errors
        except Exception, e:
            print e

    return HttpResponseRedirect(reverse('product-search'))


@login_required
def start_trial(request):
    # new user already logged in at start of registration
    try:
        userprofile = UserProfile.objects.get(user__id=request.user.id)

        return render(request, 'search-items-list.html', {
            'profiledata': request.user,
            'caliber_results': None,
            'manu_results': None,
            'cat_results': None,
            'userprofile': userprofile
        })
    except Exception, e:
        print e
        return HttpResponseRedirect('/index/')


def RespEmail(request):
    """
    TODO:
    - Refactor code to use forms.
    """
    davidson_resp = request.POST.get('davidon_resp','')
    lipseys_resp = request.POST.get('lipseys_resp','')
    green_resp = request.POST.get('green_resp','')
    rsr_resp = request.POST.get('rsr_resp','')
    zanders_resp = request.POST.get('zanders_resp', '')
    camfour_resp = request.POST.get('camfour_resp', '')
    sports_resp = request.POST.get('sports_resp', '')
    jerrys_resp = request.POST.get('jerrys_resp', '')
    ellet_resp = request.POST.get('ellet_resp', '')

    profile_data = User.objects.get(id=request.user.id)
    User_Profile = UserProfile.objects.get(user=request.user)

    if davidson_resp:
        User_Profile.davidson_mail = davidson_resp
    if lipseys_resp:
        User_Profile.lipseys_mail = lipseys_resp
    if green_resp:
        User_Profile.green_mail = green_resp
    if rsr_resp:
        User_Profile.rsr_mail = rsr_resp
    if zanders_resp:
        User_Profile.zanders_mail = zanders_resp
    if camfour_resp:
        User_Profile.camfour_mail = camfour_resp
    if sports_resp:
        User_Profile.sports_mail = sports_resp
    if jerrys_resp:
        User_Profile.jerrys_mail = jerrys_resp
    if ellet_resp:
        User_Profile.ellet_mail = ellet_resp

    User_Profile.save()
    return render(request, 'settings.html', {'profiledata': profile_data, 'userprofile': User_Profile})


class ForgotPassword(View):
    def post(self, request, *args, **kwargs):
        form = ForgotPasswordForm(request.POST)
        if form.is_valid():
            if form.exists:
                token = form.generate_token()
                form.send_reset_password(request, token)
            else:
                form.send_reset_password(request, None)
            response = {
                'success': True
            }
            return HttpResponse(json.dumps(response), content_type="application_json")
        response = {
            'success': False,
            'errors': form.errors
        }
        return HttpResponse(json.dumps(response), content_type="application_json")


class ResetPassword(FormView):
    form_class = ResetPasswordForm
    success_url = '/product-search/'
    template_name = 'saas_app/reset_password.html'

    def get(self, request, *args, **kwargs):
        form = ResetPasswordForm(initial={
            'token': request.GET.get('token', None)
        })
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        token = form.cleaned_data.get('token', None)
        password = form.cleaned_data.get('password', None)
        if token and password:
            token = ResetPasswordToken.objects.get(token=token)
            user = token.user
            user.set_password(password)
            user.save()
            user = authenticate(username=user.username, password=password)
            login(self.request, user)
            # Invalidate token
            token.valid = False
            token.save()

        return super(ResetPassword, self).form_valid(form)


class UserTemplateView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(UserTemplateView, self).get_context_data(**kwargs)
        profile_data = User.objects.get(id=self.request.user.id)
        user_profile = UserProfile.objects.get(user=profile_data)
        context['profiledata'] = profile_data
        context['userprofile'] = user_profile
        return context


class CartView(ApprovalRequiredMixin, JSONResponseMixin, UserTemplateView):
    template_name = "saas_app/cart.html"

    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        cart = Cart(self.request.session)
        context['item_list'] = get_item_list_from_cart(cart)
        return context


class ShippingView(LoginRequiredMixin, UserTemplateView):
    template_name = "saas_app/shipping.html"

    def get_context_data(self, **kwargs):
        context = super(ShippingView, self).get_context_data(**kwargs)
        context['change_address_form'] = ChangeBillingAddressForm()
        return context


class UploadfflView(LoginRequiredMixin, JSONResponseMixin, FormView):
    form_class = UploadfflForm
    template_name = 'saas_app/upload_ffl.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            profile = request.user.userprofile
            if profile.varmory_account_approved:
                return HttpResponseRedirect(reverse_lazy('product-search'))
            elif profile.varmory_account_approved and not profile.varmory_account_approved:
                return HttpResponseRedirect(reverse_lazy('ffl_thank'))
        return super(UploadfflView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = self.request.user
        ffl = self.request.FILES.get('ffl_file')
        if ffl:            
            user.userprofile.ffl_file = ffl
            user.userprofile.ffl_is_uploaded = True
            user.userprofile.save()
            url = 'https://' + Site.objects.get_current().domain + user.userprofile.ffl_file.url
            user.userprofile.ffl_url = url
            user.userprofile.save()
            create_account(user=user)
        response = {'status': True, 'url': reverse('ffl_thank')}
        return self.render_json_response(response)

    def form_invalid(self, form):
        return self.render_json_response({
            'status': False,
            'message': form.errors['ffl_file'][0]
        })

class UploadfflThankView(LoginRequiredMixin,TemplateView):
    template_name = 'saas_app/upload_ffl_thank.html'
    
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            profile = self.request.user.userprofile
            if profile.ffl_is_uploaded and profile.varmory_account_approved:
                return HttpResponseRedirect(reverse_lazy('product-search'))
        return super(UploadfflThankView, self).dispatch(request, *args, **kwargs)  
                
    def get_context_data(self, **kwargs):
        logout(self.request)
        return super(UploadfflThankView, self).get_context_data(**kwargs)


class UploadSOTView(LoginRequiredMixin, JSONResponseMixin, FormView):
    form_class = UploadSOTForm
    template_name = 'saas_app/upload_sot.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            if request.user.userprofile.sot_is_uploaded:
                return HttpResponseRedirect(reverse_lazy('view_thank'))
        return super(UploadSOTView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = self.request.user
        sot = self.request.FILES.get('sot_file')
        if sot:            
            user.userprofile.sot_file = sot
            user.userprofile.sot_is_uploaded = True
            user.userprofile.save()
            url = 'https://' + Site.objects.get_current().domain + user.userprofile.sot_file.url
            user.userprofile.sot_url = url
            user.userprofile.save()
            create_account(user=user)
        response = {'status': True, 'url': reverse('view_thank')}
        return self.render_json_response(response)

    def form_invalid(self, form):
        return self.render_json_response({
            'status': False,
            'message': form.errors['sot_file'][0]
        })


class PaymentView(ApprovalRequiredMixin, JSONResponseMixin, UserTemplateView):
    template_name = "saas_app/payment.html"
    error_message = 'Transaction Error.'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated() and \
           not request.user.userprofile.varmory_account_approved:
            return HttpResponseRedirect(reverse('upload_ffl'))
        cart = Cart(request.session)
        if not cart.count:
            return HttpResponseRedirect(reverse('view_cart'))
        return super(PaymentView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PaymentView, self).get_context_data(**kwargs)
        wallets = Wallet.objects.filter(user=self.request.user)
        context['card_form'] = AddCardForm()
        context['bankacct_form'] = AddBankAccountForm()
        context['change_address_form'] = ChangeBillingAddressForm()
        context['card_wallets'] = wallets.filter(
            wallet_type=CREDIT_CARD,
            represented_by_sales_rep=False)
        context['bank_wallets'] = wallets.filter(
            wallet_type=BANK_ACCOUNT,
            represented_by_sales_rep=False)
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            transaction_error = False
            order_numbers = []
            wallet_id = request.POST.get('selected_method')
            request.session['wallet_id'] = wallet_id
            print "Wallet Id Payment View:", wallet_id

            cart = Cart(request.session)
            item_list = get_item_list_from_cart(cart)
            wallet = Wallet.objects.get(id=wallet_id)

            for manufacturer in item_list:
                manufacturer_name = manufacturer.get('name')
                if not manufacturer_name:
                    manufacturer_name = 'None'
                if manufacturer.get('product').dealer_exchange:
                    manufacturer_name = manufacturer.get('product').\
                            user_profile.user_id
                print "Manufacturer Name:", manufacturer_name
                import pdb
                pdb.set_trace()
                sot_needed = False
                order = Order.objects.create(
                    user=request.user,
                    manufacturer=manufacturer_name,
                    payment_type=wallet.wallet_type,
                    account_number=wallet.masked_card_number,
                    wallet=wallet
                )

                for item in manufacturer.get('items'):
                    if request.user.userprofile.sot_is_uploaded:
                        if item.product.nfa_product:
                            sot_needed = True
                    product_name = string_cleaner(item.product.upc.model)
                    PurchaseHistory.objects.create(
                        user=request.user,
                        order=order,
                        product=item.product,
                        upc=item.product.upc.upc,
                        model=product_name,
                        qty=item.quantity,
                        supplier=item.product.distributor,
                        manufacture=manufacturer_name,
                        unit_price=item.price,
                        total_price=item.subtotal,
                        confirmed=True,
                    )
                order.sot_needed = sot_needed
                order.save()
                try:
                    try:
                        if not settings.DISABLE_SALESFORCE:
                            print create_opportunities(order=order, nfa_product=sot_needed)
                    except Exception, e:
                        ExceptionLog.objects.create(
                            name='Sale saasapp/views.PaymentView.post 1',
                            exception=str(e)
                        )
                        transaction_error = True
                        break
                    if order.shippingfee_set.count() == 0:
                        transaction_error = True
                        self.error_message = 'Shipping options timed out.'
                        break
                    order_numbers.append(order.id)
                except Exception, e:
                    ExceptionLog.objects.create(
                        name='Sale saasapp/views.PaymentView.post 2',
                        exception=str(e)
                    )
                    transaction_error = True
                    break

            if transaction_error:
                return self.render_json_response({
                    'status': False,
                    'message': self.error_message
                })
            else:
                request.session['order_numbers'] = order_numbers
                return self.render_json_response({'status': True})
            
            return HttpResponseRedirect(reverse('view_review'))


class AddWalletView(View, JSONResponseMixin):

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            status = False
            message = ''
            sales_rep_wallet_id = None
            if request.POST.get('card_number'):
                card_form = AddCardForm(request.POST)
                if card_form.is_valid():
                    status = True
                    wallet = card_form.create_card_wallet(request.user)
                    if wallet.represented_by_sales_rep:
                        sales_rep_wallet_id = wallet.pk
                else:
                    message = card_form.non_field_errors()
            else:
                bankacct_form = AddBankAccountForm(request.POST)
                if bankacct_form.is_valid():
                    status = True
                    wallet = bankacct_form.create_bank_wallet(request.user)
                    if wallet.represented_by_sales_rep:
                        sales_rep_wallet_id = wallet.pk
                else:
                    print "Errors:", bankacct_form.errors
                    message = bankacct_form.non_field_errors()
            print "Wallet Id AddWalletView:", sales_rep_wallet_id
            return self.render_json_response({
                'status': status,
                'message': message,
                'sales_rep_wallet_id': sales_rep_wallet_id
            })


class RemoveWalletView(JSONResponseMixin, View):
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            id = request.POST.get('id')
            try:
                wallet = Wallet.objects.get(id=id)
                wallet.delete()
                return self.render_json_response({
                    'status': True
                })
            except Wallet.DoesNotExist:
                return self.render_json_response({
                    'status': False,
                    'id': id
                })


class ReviewView(ApprovalRequiredMixin, JSONResponseMixin, UserTemplateView):
    template_name = "saas_app/review.html"

    def dispatch(self, request, *args, **kwargs):
        if not self.request.session.get('wallet_id'):
            return HttpResponseRedirect(reverse('view_payment'))
        if not self.request.user.userprofile.varmory_account_approved:
            return HttpResponseRedirect(reverse('upload_ffl'))
        return super(ReviewView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        try:
            context = super(ReviewView, self).get_context_data(**kwargs)
            wallet_id = self.request.session.get('wallet_id')
            coupon_id = self.request.session.get('coupon')
            order_numbers = self.request.session.get('order_numbers', list())
            coupon = ProductCoupon.objects.filter(id=coupon_id).first()
            wallet = Wallet.objects.get(id=wallet_id)
            # order_numbers = self.request.session['order_numbers']

            OrderFormSet = modelformset_factory(
                Order, fields=['shipping_fee'],
                form=ShippingOptionsForm
            )
            queryset = Order.objects.filter(
                id__in=order_numbers,
            ).order_by('manufacturer')
            if self.request.method == 'POST':
                formset = OrderFormSet(self.request.POST, queryset=queryset)
            else:
                formset = OrderFormSet(queryset=queryset)

            cart = Cart(self.request.session)
            context['wallet'] = wallet
            context['item_list'] = get_item_list_from_cart(cart)
            # new_formset = list(copy(formset))
            apply_shipping_discounts(formset, cart.total, self.request.user.userprofile)
            orders = Order.objects.filter(id__in=order_numbers)
            discount = get_discount_total(orders, coupon=coupon)
            for form in formset:
                handgun = False
                try:
                    manufacturer = form.instance.manufacturer
                    item_mfr = filter(lambda x: x['name'] == manufacturer, item_list)[0]
                    items = item_mfr['items']
                    for item in items:
                        if item.product.upc.listing_category in HANDGUNS:
                            handgun = True
                    if handgun:
                        form.fields['shipping_fee'].queryset = form.fields['shipping_fee'].queryset[:2]
                except:
                    pass
            context['formset'] = formset
            context['discount'] = discount
            context['total'] = max(0.0, cart.total - discount)
            return context
        except Exception, e:
            print e

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        formset = context['formset']
        if formset.is_valid():
            cart = Cart(request.session)
            sot_needed = False
            for item in cart.items:
                try:
                    new_qty = int(item.product.qty) - item.quantity
                    product = Product.objects.get(id=item.product.id)
                    if product.nfa_product:
                        sot_needed = True
                    try:
                        product.qty = str(new_qty)
                    except Exception, e:
                        ExceptionLog.objects.create(
                            name='Review saasapp/views.py - line 885',
                            exception=str(e)
                        )
                    product.save()
                    api_url = settings.DEALER_DIRECT_URL + \
                        'api/update-listing-sid/%s/' % \
                        item.product.salesforce_id
                    # headers = {
                        # 'Authorization': 'Token ' +
                        # request.user.userprofile.dealer_direct_token
                    # }
                    api_data = {'quantity': new_qty}
                    # Salesforce update will happen in Dealer Direct
                    requests.post(url=api_url, data=api_data)
                except Exception, e:
                    ExceptionLog.objects.create(
                        name='Review saasapp/views.py - line 881',
                        exception=str(e)
                    )
            cart.clear()
            for form in formset:
                shipping_fee = form.cleaned_data.get('shipping_fee', None)
                if shipping_fee:
                    print "Shipping Fee:", shipping_fee
                    shipping_fee.selected = True
                    shipping_fee.save()
            
            # Handle tsys payments
            wallet_id = self.request.session.get('wallet_id')
            wallet = Wallet.objects.get(id=wallet_id)
            order_numbers = self.request.session['order_numbers']
            orders = Order.objects.filter(
                id__in=order_numbers,
            )
            try:
                for order in orders:
                    if wallet.wallet_type == CREDIT_CARD:
                        wallet.sale(order=order)
            except Exception:
                return self.render_json_response({
                    'status': False,
                    'message': 'Card not approved.'
                })

            return self.render_json_response({'status': True, 'sot_needed': sot_needed})
        else:
            return self.render_json_response({
                'status': False,
                'message': 'Shipping option required.'
            })


class RedeemCouponView(ApprovalRequiredMixin, JSONResponseMixin, View):
    def post(self, request, *args, **kwargs):
        if request.is_ajax() and not request.session.get('coupon'):
            coupon_code = request.POST.get('coupon_code')
            try:
                coupon = ProductCoupon.objects.get(coupon_code=coupon_code)
            except ProductCoupon.DoesNotExist:
                return self.render_json_response({'status': False})
            if coupon.duration == 'Once' and \
               coupon.redeemed_by.filter(id=request.user.id).exists():
                return self.render_json_response({'status': False})

            request.session['coupon'] = coupon.id
        return self.render_json_response({'status': True})


class ThankView(ApprovalRequiredMixin, UserTemplateView):
    template_name = 'saas_app/thank.html'

    def get_context_data(self, **kwargs):
        context = super(ThankView, self).get_context_data(**kwargs)
        context['order_numbers'] = self.request.session.get('order_numbers')
        context['sot_needed'] = False
        coupon_id = self.request.session.get('coupon')
        coupon = ProductCoupon.objects.filter(id=coupon_id).first()
        if self.request.session.get('order_numbers'):
            # Set Completed to True
            order_list = []
            for order_number in self.request.session.get('order_numbers'):
                order = Order.objects.get(pk=order_number)
                if order.sot_needed and not self.request.user.userprofile.sot_approved:
                    context['sot_needed'] = True
                order_list.append(order)
                order.completed = True
                order.save()

                # account = UserProfile.objects.get(
                    # company_name=order.manufacturer
                # ).user
                account = self.request.user
                send_order_notification_email(
                    to_email=[account.email],
                    context={
                        'orders': [order],
                        'company': self.request.user.userprofile,
                        'buyer': False
                    }
                )

                for history in order.purchasehistory_set.all():
                    product = Product.objects.get(pk=history.product.id)
                    product.qty_sold += history.qty
                    product.save()
                if not settings.DISABLE_SALESFORCE:
                    create_opportunities(order=order, coupon=coupon, opportunity_stage='Ready to Bill', 
                            nfa_product=order.sot_needed)

            send_order_notification_email(
                to_email=[self.request.user.email],
                context={
                    'orders': order_list,
                    'buyer': True
                }
            )
            if coupon:
                apply_coupon_discounts(
                    orders=order_list,
                    coupon=coupon,
                    user=self.request.user
                )
                del self.request.session['coupon']
            del self.request.session['order_numbers']
        return context


class NoImageProductReport(ListView):
    paginate_by = 100
    template_name = 'saas_app/product-report.html'

    def get_queryset(self):
        queryset = SearchQuerySet().models(Product).filter(thumb_grid_image_url='noimage404')

        return queryset


class TestFormView(LoginRequiredMixin, TemplateView):
    template_name = 'testform.html'

    def post(self, request, *args, **kwargs):
        checkboxes = [
            'login_check',
            'tab_check',
            'featured_products_check',
            'customer_view_check',
            'grid_list_check',
            'in_stock_check',
            'price_filter_check',
            'supplier_filter_check',
            'manufacturer_filter_check',
            'product_popup_check',
        ]
        suppliers = ['davidson', 'green', 'lipseys', 'rsr']
        fields = ['upc', 'supplier_cost',
                  'supplier_qty', 'varmory_cost', 'varmory_qty'
                  ]
        data = {}
        for checkbox in checkboxes:
            if request.POST.get(checkbox):
                data[checkbox] = request.POST.get(checkbox) or 'Not done'
        for supplier in suppliers:
            li = []
            for i in range(3):
                li2 = []
                for field in fields:
                    key = "%s-form-%d-%s" % (supplier, i, field)
                    if request.POST.get(key):
                        write = "Product %d %s: %s" % (
                            i+1, field, request.POST[key]
                        )
                        li2.append(write)
                if len(li2) > 0:
                    li.append(li2)
            data[supplier] = li
        message = render_to_string('saas_app/email/testform_report.txt', data)
        send_mail(
            'vArmory TestForm Report',
            message,
            'kevinz@bixly.com',
            [user.email],
        )
        return HttpResponseRedirect(reverse('testform'))
