import json
import requests

from django import forms
from django.forms.extras.widgets import SelectDateWidget
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password, make_password
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.utils import timezone

from haystack.inputs import AutoQuery, Clean
from haystack.query import SearchQuerySet, SQ

from accounts.models import UserProfile, ResetPasswordToken, Wallet, FFL
from accounts.forms import PasswordField
from accounts.utils import (
    send_reset_password_email,
    send_reset_password_email_missing
)
from products.models import Product, Category
from products.utils import verify_ffl
from .models import SearchHistory, Order
from .salesforce import (
    create_search_history,
    create_payment_detail,
    create_account,
    update_billing_address
)
from .tsys import verify_card
from .utils import verify_routing_number
from .constants import (
    SUPPLIER_CHOICES,
    FFL_MONTHS,
    STATE_CHOICES,
    CREDIT_CARD,
    BANK_ACCOUNT
)

curr_year = timezone.now().year
EXPIRATION_MONTHS = (
    (i+1, FFL_MONTHS[i+1]) for i in range(12)
)
EXPIRATION_YEARS = (
    (curr_year+i, str(curr_year+i)) for i in range(11)
)


class ResetPasswordForm(forms.Form):
    token = forms.CharField(widget=forms.HiddenInput())
    password = forms.CharField(widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Password'
            }
        )
    )

    def clean_token(self):
        token = self.cleaned_data.get('token', None)
        if not ResetPasswordToken.objects.filter(token=token).exists():
            raise forms.ValidationError("Invalid token.")
        reset_token = ResetPasswordToken.objects.get(token=token)
        if not reset_token.valid:
            raise forms.ValidationError("Invalid token.")
        return token


class ForgotPasswordForm(forms.Form):
    exists = True
    email = forms.EmailField(
        error_messages={
            'required': "Email Field is required."
        }
    )

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not User.objects.filter(email=email).exists():
            self.exists = False
        return email

    def generate_token(self):
        email = self.cleaned_data.get('email')
        user = User.objects.get(email=email)
        (token, created) = ResetPasswordToken.objects.get_or_create(
            user=user
        )
        token.valid = True
        token.save()
        token.generate_token()
        return token

    def send_reset_password(self, request, token):
        email = self.cleaned_data.get('email', None)
        if email and self.exists:
            reset_password_url = "%s?token=%s" % (
                request.build_absolute_uri(
                    reverse(
                        'reset-password'
                    )
                ),
                token.token
            )

            send_reset_password_email(
                to_email=[email],
                user=User.objects.get(email=email),
                reset_password_url=reset_password_url
            )
        else:
            url = request.build_absolute_uri(reverse('index'))
            send_reset_password_email_missing(
                to_email=[email],
                url=url
            )


class SearchForm(forms.Form):
    mfr_id = forms.IntegerField(required=False)
    q = forms.CharField(max_length=100, required=False)
    distributors = forms.CharField(max_length=100, required=False)
    manufacturers = forms.CharField(max_length=100, required=False)
    category = forms.CharField(max_length=100, required=False)
    caliber = forms.CharField(max_length=100, required=False)
    in_stock = forms.CharField(max_length=100, required=False)
    order_by = forms.CharField(max_length=100, required=False)
    featured = forms.BooleanField(required=False)
    is_customer_view = forms.BooleanField(required=False)
    is_dealer_direct = forms.BooleanField(required=False)
    user = forms.IntegerField(required=False)
    price_filter = forms.IntegerField(required=False)

    def checkIfCategory(self, q):
        try:
            cat = Category.objects.get(name=q)
            return True
        except:
            try:
                cat_arr = q.split(' - ')
                cat = Category.objects.filter(name=cat_arr[0])
                if cat.count() > 0:
                    return True
                else:
                    return False
            except:
                return False

    def pre_postprocessing(self, sqs):
        return sqs

    def search(self, user_profile=None, record_search=True):
        if self.cleaned_data:
            sqs = SearchQuerySet().models(Product).filter(is_active=True)
            if self.cleaned_data.get('q'):
                if not self.cleaned_data.get('mfr_id'):
                    q = self.cleaned_data['q']
                    is_category = self.checkIfCategory(q)
                    if not is_category:
                        sqs = sqs.filter(content=AutoQuery(q))
                    else:
                        sqs = sqs.filter(category_name=q)
            if self.cleaned_data.get('mfr_id'):
                sqs = sqs.filter(user_profile_id=self.cleaned_data['mfr_id'])
            if self.cleaned_data.get('featured'):
                featured = self.cleaned_data['featured']
                if featured != 'false':
                    sqs = sqs.filter(distributor_name='GSA Direct')
            if self.cleaned_data.get('is_dealer_direct'):
                is_dealer_direct = self.cleaned_data['is_dealer_direct']
                if is_dealer_direct != 'false':
                    sqs = sqs.filter(is_dealer_direct=True)
            if self.cleaned_data.get('distributors'):
                distributors = self.cleaned_data.get('distributors').split("|")
                if "Ellet" in distributors:
                    distributors.remove("Ellet")
                    distributors.append("Jerry's Sports Center")
                sqs = sqs.filter(distributor_name__in=distributors)
            if self.cleaned_data.get('manufacturers'):
                sqs = sqs.filter(manufacturer=self.cleaned_data['manufacturers'])
            if self.cleaned_data.get('category'):
                sqs = sqs.filter(category_name=self.cleaned_data['category'])
            if self.cleaned_data.get('caliber'):
                sqs = sqs.filter(caliber=self.cleaned_data['caliber'])
            if self.cleaned_data.get('in_stock'):
                in_stock = self.cleaned_data['in_stock']
                if in_stock != 'false':
                    sqs = sqs.filter(is_available=True)

            # Filter products with $0.00 price
            final_query = SQ(price__gt=0)
            if self.cleaned_data.get('price_filter'):
                if self.cleaned_data.get('is_customer_view'):
                    try:
                        margin = user_profile.margin
                    except:
                        margin = 30.0
                else:
                    margin = 0.0
                price_filter_flags = self.cleaned_data.get('price_filter')
                if price_filter_flags > 0:
                    temp_query = SQ(price__lt=-10000) #placeholder empty query
                    if price_filter_flags%2 == 1:
                        temp_query = temp_query | (SQ(price__lte=49))
                    if (price_filter_flags/2)%2 == 1:
                        temp_query = temp_query | (SQ(price__gte=50) & SQ(price__lt=200))
                    if (price_filter_flags/4)%2 == 1:
                        temp_query = temp_query | (SQ(price__gte=200) & SQ(price__lt=500))
                    if (price_filter_flags/8)%2 == 1:
                        temp_query = temp_query | (SQ(price__gte=500) & SQ(price__lt=1000))
                    if (price_filter_flags/16)%2 == 1:
                        temp_query = temp_query | (SQ(price__gte=1000))
                    final_query = final_query & temp_query

            sqs = sqs.filter(final_query)

            # Ordering
            if self.cleaned_data.get('order_by'):
                order_by = self.cleaned_data.get('order_by')
                if order_by == 'ASC':
                    sqs = sqs.order_by('-is_featured', '-is_dealer_direct', 'price')
                else:
                    sqs = sqs.order_by('-is_featured', '-is_dealer_direct', '-price')
            else:
                sqs = sqs.order_by('-is_featured', '-is_dealer_direct', '-price')
            #Code Block to display only products of suppliers user is logged in with..
            if not user_profile and self.cleaned_data.get('user', None):
                if User.objects.filter(pk=self.cleaned_data.get('user')).exists():
                    user = User.objects.get(pk=self.cleaned_data.get('user'))
                    if user.userprofile:
                        user_profile = user.userprofile
            if user_profile:
                if not user_profile.davidson_verified:
                    sqs = sqs.exclude(distributor_name="Davidson's")
                if not user_profile.lipseys_verified:
                    sqs = sqs.exclude(distributor_name="Lipseys")
                if not user_profile.rsr_verified:
                    sqs = sqs.exclude(distributor_name="RSR Group")
                if not user_profile.green_verified:
                    sqs = sqs.exclude(distributor_name="Green Supply")
                if not user_profile.zanders_verified:
                    sqs = sqs.exclude(distributor_name="Zanders")
                if not user_profile.camfour_verified:
                    sqs = sqs.exclude(distributor_name="Camfour")
                if not user_profile.sports_verified:
                    sqs = sqs.exclude(distributor_name="Sport's South")
                if not user_profile.jerrys_verified and not user_profile.ellet_verified:
                    # The products of jerrys and ellet are the same.
                    # The prodcts registered for ellet are stored in jerrys.
                    sqs = sqs.exclude(distributor_name="Jerry's Sports Center")

            if self.cleaned_data.get('is_customer_view'):
                # going to assume that there is a user_profile as login will be required : per skype discussion - brie

                paged = self.pre_postprocessing(sqs)
                results = [s for s in paged]
                try:
                    margin = user_profile.margin
                except:
                    margin = 30.0
                for r in results:
                    price_increased = (r.price * margin) / 100.00
                    price = float("{0:.2f}".format(r.price + price_increased))
                    r.price = price

            # Create Search History
            if record_search:
                search_history = SearchHistory.objects.create(
                    user=user_profile.user,
                    searched_term=self.cleaned_data.get('q')
                )
                if search_history.searched_term != '' and search_history.searched_term != ' ' and self.cleaned_data.get('q'):
                    if not settings.DISABLE_SALESFORCE:
                        create_search_history(
                            search_history=search_history
                        )
            return sqs

        return None


class PaginatedSearchForm(SearchForm):
    page = forms.IntegerField(required=False)

    def paginated_search(self, user_profile=None, record_search=True):
        """
        Returns tuple of current_page results and number of pages.
        """
        sqs = self.search(user_profile=user_profile, record_search=record_search)
        page = self.cleaned_data.get('page', 1)
        search_pages = Paginator(sqs, settings.HAYSTACK_SEARCH_RESULTS_PER_PAGE)
        try:
            current_page = search_pages.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            current_page = search_pages.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            current_page = search_pages.page(search_pages.num_pages)
        return (current_page, search_pages.num_pages)
    
    def pre_postprocessing(self, sqs):
        page = self.cleaned_data.get('page', 1)
        search_pages = Paginator(sqs, settings.HAYSTACK_SEARCH_RESULTS_PER_PAGE)
        try:
            current_page = search_pages.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            current_page = search_pages.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            current_page = search_pages.page(search_pages.num_pages)
        return current_page.object_list


def get_margin_value(orig, margin):
    return orig - (orig*margin)/100.0


class UpdateMarginForm(forms.Form):
    margin = forms.FloatField(
        widget=forms.NumberInput(
            attrs={
                'name': 'margin',
                'class': 'form-control',
                'aria-describedby': 'basic-addon2'
            }
        )
    )

    def clean_margin(self):
        data = self.cleaned_data['margin']
        if data < 0.0 or data > 100.0:
            raise forms.ValidationError('Invalid margin')
        return data

    def action(self, user):
        if user:
            data = self.cleaned_data
            profile = user.userprofile
            profile.margin = data.get('margin')
            profile.save()
            return 'Margin has been updated.'
        return False


class AddSupplierForm(forms.Form):
    supplier = forms.ChoiceField(choices=SUPPLIER_CHOICES, required=True)
    username = forms.CharField(required=True, widget=forms.TextInput(
            attrs={
                'placeholder': 'Username'
            }
        )
    )
    password = forms.CharField(required=True, widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Password'
            }
        )
    )

    def action(self, user=None):
        if user:
            profile = user.userprofile
            data = self.cleaned_data
            supplier = data.get('supplier')
            setattr(profile, supplier+'_verified', True)
            setattr(profile, supplier+'_login', data.get('username'))
            setattr(profile, supplier+'_password', data.get('password'))
            profile.save()
            return 'Supplier has been added.'
        return False


class ChangeFFLForm(forms.Form):
    first_three_digits = forms.CharField(required=True, max_length=3, widget=forms.TextInput(
            attrs={
                'placeholder': 'First 3 digits'
            }
        )
    )
    last_five_digits = forms.CharField(required=True, max_length=5, widget=forms.TextInput(
            attrs={
                'placeholder': 'Last 5 digits'
            }
        )
    )
    date = forms.DateField(widget=SelectDateWidget(months=FFL_MONTHS), required=True)

    def clean(self):
        super(ChangeFFLForm, self).clean()

        data = self.cleaned_data
        if 'first_three_digits' in data and 'last_five_digits' in data:
            first = data['first_three_digits'][0]
            second = data['first_three_digits'][1:]
            last = data['last_five_digits']

            if UserProfile.objects.filter(
                ffl_number_first_three=data['first_three_digits'],
                ffl_number_last_five=last
            ).exists():
                raise forms.ValidationError(
                    'FFL number already registered with vArmory.'
                )
            try:
                result = verify_ffl(first, second, last)
                if not result.get('verified'):
                    raise forms.ValidationError("Invalid FFL Number")
            except Exception, e:
                print e
                raise forms.ValidationError("Invalid FFL Number")
        return data

    def action(self, user=None):
        if user:
            data = self.cleaned_data
            profile = user.userprofile
            profile.ffl_number_first_three = data.get('first_three_digits')
            profile.ffl_number_last_five = data.get('last_five_digits')
            profile.ffl_expiration_date = data.get('date')
            profile.save()
            return 'FFL has been updated.'
        return False


class AddCardForm(forms.Form):
    name_on_card = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'nm',
                'placeholder': 'Name on card',
            }
        )
    )
    card_number = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'cc',
                'placeholder': 'Card number',
            }
        )
    )
    zip_code = forms.CharField(
        max_length=5,
        widget=forms.TextInput(
            attrs={
                'class': 'zip',
                'placeholder': 'Zip code',
            }
        )
    )
    cvv = forms.CharField(
        max_length=4,
        widget=forms.TextInput(
            attrs={
                'class': 'cvv',
                'placeholder': 'CVV',
            }
        )
    )
    expiration_month = forms.ChoiceField(
        choices=EXPIRATION_MONTHS,
    )
    expiration_year = forms.ChoiceField(
        choices=EXPIRATION_YEARS,
    )
    represented_by_sales_rep = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'sales_rep'
            }
        )
    )
    salesforce_account_email = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'account_email',
                'placeholder': 'Salesforce Account Email'
            }
        )
    )

    def create_card_wallet(self, user):
        data = self.cleaned_data
        wallet = Wallet(
            user=user,
            wallet_type=CREDIT_CARD,
            cvv=data.get('cvv'),
            expiration_month=data.get('expiration_month'),
            expiration_year=data.get('expiration_year'),
            token=data.get('token'),
            masked_card_number=data.get('masked_card_number'),
            account_name=data.get('name_on_card'),
            represented_by_sales_rep=data.get('represented_by_sales_rep', False),
            salesforce_account_email=data.get('salesforce_account_email', None)
        )
        wallet.save()
        return wallet

    def clean(self):
        super(AddCardForm, self).clean()
        data = self.cleaned_data

        # Check if card is valid
        token = verify_card(
            card_holder_name=data.get('name_on_card'),
            card_number=data.get('card_number'),
            cvv=data.get('cvv'),
            expiration_month=data.get('expiration_month'),
            expiration_year=data.get('expiration_year')
        )
        if token is None:
            self.add_error(
                None,
                ValidationError(
                    'Invalid card data.',
                    code='mismatch'
                )
            )
        else:
            data.update({
                'token': token,
                'masked_card_number': data.get('card_number')[-4:]
            })
        # Check if salesforce_account_email when represented_by_sales_rep is True
        if data.get('represented_by_sales_rep') and not data.get('salesforce_account_email'):
            self.add_error(
                None,
                ValidationError(
                    'Salesforce Account Email Missing.',
                    code='invalid'
                )
            )
        return data


class AddBankAccountForm(forms.Form):
    name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'ac_name',
                'placeholder': 'Name'
            }
        )
    )
    routing_num = forms.CharField(
        max_length=10,
        widget=forms.TextInput(
            attrs={
                'class': 'routing',
                'placeholder': 'Bank Routing Number'
            }
        )
    )
    account_num = forms.CharField(
        max_length=12,
        help_text='This is not a valid Account Number',
        widget=forms.TextInput(
            attrs={
                'class': 'ac_num',
                'placeholder': 'Checking Account number',
            }
        )
    )
    account_num2 = forms.CharField(
        max_length=12,
        help_text='This is not a valid Account Number',
        widget=forms.TextInput(
            attrs={
                'class': 'ac_num',
                'placeholder': 'Re-enter Checking Account number',
            }
        )
    )
    represented_by_sales_rep = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'sales_rep'
            }
        )
    )
    salesforce_account_email = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'account_email',
                'placeholder': 'Salesforce Account Email'
            }
        )
    )

    def clean(self):
        data = self.cleaned_data
        account_num = data.get('account_num')
        account_num2 = data.get('account_num2')
        routing = data.get('routing_num').replace(' ', '')
        result = verify_routing_number(routing)
        if account_num != account_num2:
            self.add_error(
                None,
                ValidationError(
                    'Checking account numbers do not match.',
                    code='mismatch'
                )
            )
        if result['bank_name'] == 'Invalid':
            self.add_error(
                None,
                ValidationError(
                    'Please enter a valid routing number',
                    code='invalid'
                )
            )
        # Check if salesforce_account_email when represented_by_sales_rep is True
        if data.get('represented_by_sales_rep') and not data.get('salesforce_account_email'):
            self.add_error(
                None,
                ValidationError(
                    'Salesforce Account Email Missing.',
                    code='invalid'
                )
            )

    def create_bank_wallet(self, user):
        data = self.cleaned_data
        wallet = Wallet(
            user=user,
            wallet_type=BANK_ACCOUNT,
            routing_number=data.get('routing_num'),
            account_number=data.get('account_num'),
            account_name=data.get('name'),
            represented_by_sales_rep=data.get('represented_by_sales_rep', False),
            salesforce_account_email=data.get('salesforce_account_email', None)
        )
        wallet.save()
        try:
            create_payment_detail(wallet=wallet)
        except Exception:
            pass
        return wallet


class ChangeAddressForm(forms.Form):
    street_address = forms.CharField(required=True, widget=forms.TextInput(
            attrs={
                'placeholder': 'Street Address'
            }
        )
    )
    city = forms.CharField(required=True, widget=forms.TextInput(
            attrs={
                'placeholder': 'City'
            }
        )
    )
    state = forms.ChoiceField(choices=STATE_CHOICES, required=True)
    zip = forms.CharField(required=True, widget=forms.TextInput(
            attrs={
                'placeholder': 'Zip'
            }
        )
    )

    def get_address(self):
        # assumes valid form
        data = self.cleaned_data
        address = "%s\n %s, %s - %s" % (
            data['street_address'],
            data['city'],
            data['state'],
            data['zip']
        )
        return address

    def action(self, user):
        user.userprofile.ffl_premise_address = self.get_address()
        user.userprofile.save()


class ChangeShippingCarrierForm(forms.Form):
    ups = forms.BooleanField(required=False, label='UPS')
    usps = forms.BooleanField(required=False, label='USPS')
    fedex = forms.BooleanField(required=False, label='FedEx')
    ups_drop = forms.BooleanField(required=False, label='UPS')
    usps_drop = forms.BooleanField(required=False, label='USPS')
    fedex_drop = forms.BooleanField(required=False, label='FedEx')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super(ChangeShippingCarrierForm, self).__init__(*args, **kwargs)

    def action(self, user=None):
        if user:
            data = self.cleaned_data
            shipping_info = user.userprofile.shipping_info
            shipping_info.FedEx=data['fedex']
            shipping_info.USPS=data['usps']
            shipping_info.UPS=data['ups']
            shipping_info.FedEx_drop=data['fedex_drop']
            shipping_info.USPS_drop=data['usps_drop']
            shipping_info.UPS_drop=data['ups_drop']
            shipping_info.save()
            return 'Shipping Info updated'
        return False


class ChangeBankInfoForm(forms.Form):
    routing_number = forms.CharField(label='Routing Number')
    routing_check = forms.CharField(label='Verify Routing Number')
    account_number = forms.CharField(label='Account Number')
    account_check = forms.CharField(label='Verify Account Number')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super(ChangeBankInfoForm, self).__init__(*args, **kwargs)

    def clean(self):
        super(ChangeBankInfoForm, self).clean()
        data = self.cleaned_data

        if data['routing_number'] != data['routing_check']:
            raise forms.ValidationError("Routing numbers do not match")
        if data['account_number'] != data['account_check']:
            raise forms.ValidationError("Account numbers do not match")
        return data

    def action(self, user=None):
        if user:
            data = self.cleaned_data
            wallet = Wallet.objects.get(user=user)
            wallet.routing_number = data['routing_number']
            wallet.account_number = data['account_number']
            wallet.save()
            return 'Billing info updated'
        return False


class ChangeBillingAddressForm(ChangeAddressForm):
    def action(self, user=None):
        if user:
            data = self.cleaned_data
            user.userprofile.ffl_mailing_address = self.get_address()
            user.userprofile.save()
            update_billing_address(user.userprofile, street=data['street_address'], city=data['city'], state=data['state'], postal=data['zip'])
            return 'Billing Address has been changed.'
        return False


class ChangeShippingAddressForm(ChangeAddressForm):
    def action(self, user=None):
        if user:
            super(ChangeShippingAddressForm, self).action(user)
            return 'Shipping Address has been changed.'
        return False


class UserInfoChangeForm(forms.Form):
    email_address = forms.CharField(required=True, widget=forms.EmailInput(
            attrs={
                'placeholder': 'Email Address'
            }
        )
    )
    phone_number = forms.CharField(required=True, widget=forms.TextInput(
            attrs={
                'placeholder': 'Phone number'
            }
        )
    )

    def action(self, user=None):
        if user:
            data = self.cleaned_data
            user.email = data.get('email_address')
            user.username = data.get('email_address')

            api_url = settings.DEALER_DIRECT_URL + 'api/update-user-info/'
            headers = {
                'Authorization': 'Token ' + user.userprofile.dealer_direct_token
            }
            api_data = {'email': user.email}
            requests.patch(url=api_url, headers=headers, data=api_data)

            ffl_number = "%s%s" % (
                user.userprofile.ffl_number_first_three,
                user.userprofile.ffl_number_last_five
            )
            if FFL.objects.filter(ffl_number=ffl_number).exists():
                ffl = FFL.objects.get(ffl_number=ffl_number)
                ffl.phone = data.get('phone_number')
                ffl.save()
            create_account(user=user)
            user.save()
            return 'User Info has been updated.'
        return False


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(required=True, widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Old Password'
            }
        )
    )
    new_password = PasswordField(required=True, widget=forms.PasswordInput(
            attrs={
                'placeholder': 'New Password'
            }
        )
    )
    new_password_repeat = PasswordField(required=True, widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Re-type New Password'
            }
        )
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_old_password(self):
        data = self.cleaned_data['old_password']
        if not self.user.check_password(data):
            raise forms.ValidationError("Invalid password")
        return data

    def clean(self):
        super(ChangePasswordForm, self).clean()

        data = self.cleaned_data
        if 'new_password' in data and 'new_password_repeat' in data and data['new_password'] != data['new_password_repeat']:
            raise forms.ValidationError("The passwords do not match")
        return data

    def action(self, user=None):
        user_obj = self.user or user
        if user_obj:
            data = self.cleaned_data
            user_obj.set_password(data.get('new_password'))
            user_obj.save()
            return 'Password has been changed.'
        return False


class TsysLogAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TsysLogAdminForm, self).__init__(*args, **kwargs)
        self.fields['request'].widget = admin.widgets.AdminTextareaWidget()
        self.fields['response'].widget = admin.widgets.AdminTextareaWidget()


class ShippingOptionsForm(forms.ModelForm):
    shipping_fee = forms.ModelChoiceField(
        queryset=None,
        widget=forms.RadioSelect()
    )

    def __init__(self, *args, **kwargs):
        super(ShippingOptionsForm, self).__init__(*args, **kwargs)
        user = getattr(self.instance, 'user', None)
        if user:
            # seller = UserProfile.objects.get(
                # company_name=self.instance.manufacturer
            # )
            seller = user.userprofile
            options = self.instance.shippingfee_set.all()
            self.fields['shipping_fee'].queryset = options.filter(
                shipment_provider=seller.shipping_carrier,
            )
                
        self.fields['shipping_fee'].empty_label = None
        
    class Meta:
        model = Order
        fields = ['shipping_fee']


class UploadfflForm(forms.Form):

    ffl_file = forms.FileField(widget=forms.FileInput())

    def clean(self):
        if type(self.cleaned_data.get('ffl_file')) == 'NoneType':
            self.add_error('ffl_file', 'You must upload a FFL file to continue with your purchase')
            return
        return self.cleaned_data


class UploadSOTForm(forms.Form):

    sot_file = forms.FileField(widget=forms.FileInput())

    def clean(self):
        if type(self.cleaned_data.get('sot_file')) == 'NoneType':
            self.add_error('sot_file', 'You must upload a SOT file to continue with your purchase')
            return
        return self.cleaned_data
