from django.contrib import admin

from .models import (
    EmailTemplate,
    DiscountGroup,
    Order,
    PurchaseHistory,
    TsysLog,
    ExceptionLog
)


class MessageAdmin(admin.ModelAdmin):
    pass


class PurchaseHistoryInline(admin.TabularInline):
    model = PurchaseHistory
    readonly_fields = (
        'user',
        'upc',
        'model',
        'qty',
        'supplier',
        'manufacture',
        'unit_price',
        'total_price',
        'confirmed'
    )
    extra = 0
    

class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ('date',)
    inlines = (PurchaseHistoryInline, )


class OrderInline(admin.TabularInline):
    model = Order
    readonly_fields = ('manufacturer', 'salesforce_id')
    extra = 0


class DiscountGroupAdmin(admin.ModelAdmin):
    inlines = [OrderInline]


class TsysLogAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name', 'date', 'is_error')
    list_filter = ('name','date', 'is_error')


admin.site.register(EmailTemplate, MessageAdmin)
admin.site.register(DiscountGroup, DiscountGroupAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(PurchaseHistory)
admin.site.register(TsysLog, TsysLogAdmin)
admin.site.register(ExceptionLog)
