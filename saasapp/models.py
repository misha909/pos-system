import os

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.db import models

from products.models import Product

from accounts.models import Wallet
from .constants import (PAYMENT_TYPE_CHOICES, SHIPPING_STATUS_CHOICES,
                        GROUND, ONE_DAY, TWO_DAY)


SHIPPING_MEDIA_ROOT = 'shipping_label_gif_media/'
SHIPPING_PDF_MEDIA_ROOT = 'shipping_label_pdf_media/'


def get_label_filename(instance, filename):
    name = instance.salesforce_id
    ext = filename.split('.')[-1]
    if not name:
        name = instance.pk
    filename = "shipping-label-%s.%s" % (name, ext)
    return os.path.join(SHIPPING_MEDIA_ROOT, filename)

def get_label_pdf_filename(instance, filename):
    name = instance.salesforce_id
    ext = filename.split('.')[-1]
    if not name:
        name = instance.pk
    filename = "shipping-label-%s.%s" % (name, ext)
    return os.path.join(SHIPPING_PDF_MEDIA_ROOT, filename)


class SearchHistory(models.Model):
    user = models.ForeignKey(User)
    searched_term = models.CharField(max_length=500,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True,)

    def __str__(self):
        return "%s" % self.searched_term


class EmailTemplate(models.Model):
    subject = models.TextField(('Email content subject'),null=True,blank=True,
        help_text=('Email content subject when user order products.'))
    message = models.TextField(('Email content message'),null=True,blank=True,
        help_text=('Email content message when user order products.'))

    footer = models.TextField(('Email content footer'),null=True,blank=True,
        help_text=('Email content footer when user order products.'))


    def __unicode__(self):
        return u"%s" % (self.message)

    class Meta:
        verbose_name_plural = "Email Template Message"


class DiscountGroup(models.Model):
    buyer = models.ForeignKey(User)
    amount_discount = models.FloatField()


class Order(models.Model):
    user = models.ForeignKey(User)
    group = models.ForeignKey(DiscountGroup, null=True, blank=True,
                              related_name='orders')
    manufacturer = models.CharField(max_length=50)
    # Order_Number__c in Salesforce
    order_number = models.CharField(
        max_length=50, null=True, blank=True
    )
    # Order_Date__c in Salesforce
    date = models.DateTimeField(auto_now_add=True,)
    # Payment_Type__c in Salesforce
    payment_type = models.CharField(
        max_length=12,
        choices=PAYMENT_TYPE_CHOICES,
        null=True,
        blank=True
    )
    # FedEx_Master_Track_Number__c in Salesforce
    fed_ex_master_track_number = models.CharField(
        max_length=30,
        null=True,
        blank=True
    )
    # FedEx_Product_Track_Number__c in Salesforce
    fed_ex_product_track_number = models.CharField(
        max_length=30,
        null=True,
        blank=True
    )
    # Shipping_Charges__c in Salesforce
    shipping_cost = models.FloatField(default=0)
    # Masked_Account_Number__c in Salesforce
    account_number = models.CharField(
        max_length=4,
        null=True,
        blank=True
    )
    shipping_status = models.CharField(
        max_length=12,
        choices=SHIPPING_STATUS_CHOICES,
        null=True,
        blank=True
    )
    shipping_label = models.ImageField(
        upload_to=get_label_filename,
        null=True,
        blank=True
    )
    shipping_label_pdf = models.FileField(
        upload_to=get_label_pdf_filename,
        null=True,
        blank=True
    )
    tracking_number = models.CharField(max_length=50, null=True, blank=True)
    invoice_url = models.CharField(max_length=255, null=True, blank=True)
    wallet = models.ForeignKey(Wallet, blank=True, null=True)
    completed = models.BooleanField(default=False)
    salesforce_id = models.CharField(max_length=50, blank=True, null=True)
    sot_needed = models.BooleanField(default=False)

    def __unicode__(self):
        return u"Order %d" % (self.pk)

    @property
    def shipping_label_pdf_url(self):
        if self.shipping_label_pdf:
            return "https://%s%s" % (Site.objects.get_current().domain, self.shipping_label_pdf.url)
        else:
            if self.shipping_label_pdf:
                from .salesforce import download_shipping_label
                download_shipping_label(self)
                return "https://%s%s" % (Site.objects.get_current().domain, self.shipping_label_pdf.url)
            return ''

    # Opportunity_Number__c in Salesforce
    @property
    def opportunity_number(self):
        return "%s-%s-%s" % (self.account_number, self.date.strftime("%d%m%y"), self.pk)

    @property
    def order_number_c(self):
        from saasapp.salesforce import get_opportunity
        if self.order_number is None and self.salesforce_id is not None:
            res = get_opportunity(self)
            if res:
                self.order_number = res['Order_Number__c']
                self.save()
        return self.order_number

    @property
    def transaction_amount(self):
        total_price = 0
        for purchase_history in self.purchasehistory_set.all():
            total_price = total_price + purchase_history.total_price
        return total_price

    @property
    def transaction_amount_cents(self):
        return self.transaction_amount * 100


class PurchaseHistory(models.Model):
    user = models.ForeignKey(User)
    order = models.ForeignKey(Order, blank=True, null=True)
    product = models.ForeignKey(Product, blank=True, null=True)
    # UPC__c in Salesforce
    upc = models.CharField(max_length=50,blank=True,null=True)
    # Model__c in Salesforce
    model = models.CharField(max_length=50,blank=True,null=True)
    # Quantity in Salesforce
    qty = models.IntegerField(default=0)
    # Supplier__c in Salesforce
    supplier = models.CharField(max_length=50,blank=True,null=True)
    # Manufacture__c in Salesforce
    manufacture = models.CharField(max_length=50,blank=True,null=True)
    # UnitPrice in Salesforce
    unit_price = models.FloatField(default=0)
    # TotalPrice in Salesforce
    total_price = models.FloatField(default=0)
    # Confirmed__c in Salesforce
    confirmed = models.BooleanField(default=False)
    # ServiceDate in Salesforce
    date = models.DateTimeField(auto_now_add=True,)
    shipment_provider = models.CharField(max_length=50,blank=True,null=True)
    mail_service = models.CharField(max_length=50,blank=True,null=True)
    salesforce_id = models.CharField(max_length=50, blank=True, null=True)
    def __str__(self):
        return "%s" % self.date

    @property
    def unit_price_cents(self):
        return self.unit_price * 100

    @property
    def total_price_cents(self):
        return self.total_price * 100


class ShippingFee(models.Model):
    """
    Shipping Fee Calculations coming from Salesforce
    """
    order = models.ForeignKey(Order, blank=True, null=True)
    salesforce_id = models.CharField(max_length=50, blank=True, null=True)
    shipment_provider = models.CharField(max_length=50, blank=True, null=True)
    mail_service = models.CharField(max_length=50, blank=True, null=True)
    cost = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    selected = models.BooleanField(default=False)

    def __unicode__(self):
        string = self.mail_service
        if not string[-1].isalpha():
            string = string[:-1]
        if string in ONE_DAY:
            return 'Overnight'
        elif string in TWO_DAY:
            return '2nd Day'
        elif string in GROUND:
            return 'Ground'
        return "%s - %s" % (self.shipment_provider, self.mail_service)

    @property
    def official(self):
        return self.__unicode__() in ('Ground','Overnight','2nd Day')


class TsysLog(models.Model):
    name = models.CharField(max_length=50)
    request = models.TextField()
    response = models.TextField()
    is_error = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return "%s - %s" % (self.name, self.date)


class ExceptionLog(models.Model):
    name = models.CharField(max_length=50)
    exception = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return "%s - %s" % (self.name, self.date)
