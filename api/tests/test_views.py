import os
import mock
from datetime import timedelta

from django.contrib.auth.models import User
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone

from rest_framework.test import APIRequestFactory, APIClient

from accounts.models import Wallet, Distributor, UserProfile
from products.models import UPC, Product
from saasapp.models import Order, PurchaseHistory
from saasapp.constants import CREDIT_CARD
from saasapp.salesforce import create_product_2
from api.constants import (
    ONE_DAY,
    SEVEN_DAYS,
    THIRTY_DAYS,
    YTD,
    DEALER_DIRECT,
)
from api.tests import recipes


def setUpModule():
    call_command('clear_index', interactive=False)
    call_command('loaddata', 'distributors')
    call_command('loaddata', 'blacklisted_passwords')
    call_command('loaddata', 'categories')
    call_command('add_gsa_products')


class RegisterAPITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'user@email.com',
            'password': 'goodenoughpa55word',
        }
        self.ffl_data = {
            'first': '982',
            'last': '01710',
        }
        self.product_data = {
            'upc': '1234567890',
            'price': 12345.0,
            'qty': 10,
            'tags': [
                {'name': 'tag1'},
                {'name': 'tag2'}
            ]
        }

    def set_auth_token(self):
        self.client.credentials()
        response = self.client.post(reverse('auth_token'), self.data)
        token = response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_register(self):
        response = self.client.post(reverse('registration_register'), self.data)
        self.assertEqual(response.status_code, 200)
        token = response.data.get('token')

        self.ffl_data.update({'token': token})
        response = self.client.post(reverse('registration_ffl'), self.ffl_data)
        self.assertEqual(response.status_code, 200)
        query = User.objects.filter(email=self.data.get('email'))
        self.assertTrue(query.exists())
        self.assertIsNotNone(response.data.get('token'))


class APIStatsTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.manufacturer = 'Egyptian slaves'
        self.data = {
            'email': 'user@email.com',
            'password': 'goodenoughpa55word',
        }
        self.user = User.objects.create_user(
            self.data.get('email'),
            password=self.data.get('password'),
            email=self.data.get('email'),
        )
        self.order = Order.objects.create(
            user=self.user,
            payment_type=CREDIT_CARD,
            completed=True,
            manufacturer=self.manufacturer
        )

        distributor = Distributor.objects.get(name=DEALER_DIRECT)
        upc = UPC.objects.create(upc='TestUPC', manufacturer='TestM')
        base = timezone.now()
        today = base.replace(hour=0, minute=0, second=0, microsecond=0)
        week = today-timedelta(days=6)
        month = today-timedelta(days=29)
        year = today.replace(month=1, day=1)

        self.times = [today, week, month, year]
        self.products = [p for p in Product.objects.all()[:4]]
        self.models = [p.upc.model for p in Product.objects.all()[:4]]
        self.prices = [5.0, 34.75, 1450.00, 64]
        self.intervals = [ONE_DAY, SEVEN_DAYS, THIRTY_DAYS, YTD]

        for i in range(4):
            p = Product.objects.create(
                upc=upc,
                distributor=distributor,
                price=1.0,
                qty='1'
            )
            p.is_active = (i % 2 == 0)
            p.save()

            with mock.patch('django.utils.timezone.now') as now:
                now.return_value = self.times[i]
                PurchaseHistory.objects.create(
                    user=self.user,
                    order=self.order,
                    product=self.products[i],
                    model=self.models[i],
                    manufacture=self.manufacturer,
                    supplier=DEALER_DIRECT,
                    unit_price=self.prices[i],
                    total_price=self.prices[i],
                    qty=1
                )

    def set_auth_token(self):
        self.client.credentials()
        response = self.client.post(reverse('auth_token'), self.data)
        token = response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_order_total(self):
        url = reverse('order_total')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()

        response = self.client.post(url, {
            'manufacturer': self.manufacturer,
            'period': YTD
        })
        content = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(content.get('count'), Order.objects.count())

    def test_sales_total(self):
        url = reverse('sales_total')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()

        responses = []
        for period in self.intervals:
            responses.append(self.client.post(url, {
                'manufacturer': self.manufacturer,
                'period': period
            }))
        for i in range(4):
            response = responses[i]
            content = response.json()
            data = content.get('data')
            self.assertEqual(response.status_code, 200)
            print data[0].get('y')
            # self.assertEqual(data[0].get('y'), self.prices[i])

    def test_products_total(self):
        url = reverse('products_total')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()

        response = self.client.post(url, {
                'manufacturer': 'TestM',
            }
        )
        content = response.json()
        self.assertTrue(response.status_code, 200)
        self.assertEqual(content.get('count'), 4)
        self.assertEqual(content.get('active_products'), 2)
        self.assertEqual(content.get('total_products'), 4)

    def test_products_top(self):
        url = reverse('products_top')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        period = YTD
        if self.times[2] < self.times[3]:
            period = THIRTY_DAYS
        response = self.client.post(url, {
            'manufacturer': self.manufacturer,
            'period': period
        })
        content = response.json()
        data = content.get('data')
        self.assertEqual(response.status_code, 200)
        sorted_descending = [
            self.models[2],
            self.models[3],
            self.models[1],
            self.models[0]
        ]
        for i in range(4):
            self.assertEqual(data[i].get('model'), sorted_descending[i])


class APITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'user@email.com',
            'password': 'goodenoughpa55word',
        }
        self.userprofile_data = {
            'margin': 50.0,
            'company': 'Wayne Enterprises Inc.',
            'supplier_emails': [
                {
                  'supplier': "Davidson's",
                  'rep_email': 'davidsonemail@address.com'
                },
                {
                  'supplier': 'Lipseys',
                  'rep_email': 'lipseysemail@address.com'
                },
                {
                  'supplier': 'Green Supply',
                  'rep_email': 'greenemail@address.com'
                },
                {
                  'supplier': 'RSR Group',
                  'rep_email': 'rsremail@address.com'
                },
                {
                  'supplier': "Sport's South",
                  'rep_email': 'sportemail@address.com'
                }
            ]
        }
        self.card_data = {
            'name_on_card': 'John Doe',
            'card_number': '5499740000000057',
            'cvv': '998',
            'expiration_month': 12,
            'expiration_year': 2020,
            'zip_code': '83642'
        }
        self.product_data = {
            'salesforce_id': 'abcd123987',
            'upc': '1234567890',
            'price': 12345.0,
            'qty': 10,
            'tags': [
                {'name': 'tag1'},
                {'name': 'tag2'}
            ],
            'title': 'Title1',
            'category': '',
            'listing_category': 'Ammo-Test',
            'description': 'Description1',
            'manufacturer': 'Manufacturer1',
            'msrp': '1.0',
            'map': '1.0',
            'estimated_ship_days': '10',
        }
        self.product_data_2 = {
            'price': 54321.0,
            'qty': 1,
            'title': 'UpdatedTitle',
            'tags': [
                {'name': 'newtag'},
            ],
            'category': '',
            'listing_category': 'Ammo-Test2',
            'description': 'Description2',
            'manufacturer': 'Manufacturer2',
            'msrp': '2.0',
            'map': '2.0',
            'estimated_ship_days': '5',
        }
        self.user = User.objects.create_user(
            self.data.get('email'),
            password=self.data.get('password'),
            email=self.data.get('email'),
        )
        self.user.first_name = self.data.get('first_name')
        self.user.last_name = self.data.get('last_name')
        self.user.save()
        self.userprofile = self.user.userprofile
        self.userprofile.company_name = 'Queen Consolidated'
        self.userprofile.margin = 30.0
        self.userprofile.davidson_verified = True
        self.userprofile.lipseys_verified = True
        self.userprofile.rsr_verified = True
        self.userprofile.green_verified = True
        self.userprofile.zanders_verified = True
        self.userprofile.camfour_verified = True
        self.userprofile.sports_verified = True
        self.userprofile.jerrys_verified = True
        self.userprofile.ellet_verified = True
        self.userprofile.account_id = 'account_id'
        self.userprofile.save()
        self.order = Order.objects.create(
            user=self.user,
            payment_type=CREDIT_CARD,
            completed=True,
        )
        self.product = Product.objects.create(
            distributor=Distributor.objects.get(name='Dealer Direct'),
            upc=UPC.objects.first(),
            price=12345.0,
            qty=5,
            is_active=False
        )
        self.upc = UPC.objects.filter(manufacturer='SRM Arms').first()
        PurchaseHistory.objects.create(
            user=self.user,
            order=self.order,
            upc='892756002373',
            model='GSG AK-47',
            supplier='Dealer Direct',
            manufacture='Mfr',
            qty=1
        )
        self.wallet1 = Wallet.objects.create(
            user=self.user,
            wallet_type=CREDIT_CARD,
            cvv='999',
            token='A5AtLGyHVZVh5439',
            masked_card_number='5439',
            expiration_month=12,
            expiration_year=2020,
        )
        self.wallet2 = Wallet.objects.create(
            user=self.user,
            wallet_type=CREDIT_CARD,
            cvv='996',
            token='AdYrWsUVih0200',
            masked_card_number='0200',
            expiration_month=12,
            expiration_year=2020,
        )

    def set_auth_token(self):
        self.client.credentials()
        response = self.client.post(reverse('auth_token'), self.data)
        token = response.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_auth_token(self):
        response = self.client.post(reverse('auth_token'), self.data)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get('token'))
        data = response.data
        self.assertEqual(data.get('first_name'), self.data.get('first_name'))
        self.assertEqual(data.get('last_name'), self.data.get('last_name'))

    def test_user_info(self):
        url = reverse('user_info')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        content = response.json()
        self.assertEqual(response.status_code, 200)
        user_data = content.get('user')
        profile = UserProfile.objects.get(user=self.user)
        self.assertEqual(content.get('margin'), 30.0)
        self.assertEqual(user_data['first_name'], self.data['first_name'])
        self.assertEqual(user_data['last_name'], self.data['last_name'])
        self.assertEqual(user_data['company'], profile.company_name)

    def test_update_user_info(self):
        url = reverse('update_user_info')
        response = self.client.put(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.put(url, self.userprofile_data, format='json')
        self.assertEqual(response.status_code, 200)
        profile = UserProfile.objects.get(user=self.user)
        self.assertEqual(profile.company_name, self.userprofile_data.get('company'))
        self.assertEqual(profile.margin, self.userprofile_data.get('margin'))
        self.assertEqual(profile.davidson_mail, 'davidsonemail@address.com')
        self.assertEqual(profile.lipseys_mail, 'lipseysemail@address.com')
        self.assertEqual(profile.green_mail, 'greenemail@address.com')
        self.assertEqual(profile.rsr_mail, 'rsremail@address.com')
        self.assertEqual(profile.sports_mail, 'sportemail@address.com')

    def test_activate_account(self):
        url = reverse('activate_varmory_account')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)
        self.user.is_staff = True
        self.user.save()
        self.assertFalse(self.user.userprofile.varmory_account_approved)
        response = self.client.post(url, {'salesforce_id': 'account_id'})
        self.assertEqual(response.status_code, 200)
        profile = UserProfile.objects.get(user=self.user)
        self.assertTrue(profile.varmory_account_approved)

    def test_add_supplier(self):
        self.set_auth_token()
        response = self.client.post(
            reverse('add_supplier'),
            {
                'supplier': 1,  # Davidson's
                'username': 'scott@gsadirect.com',
                'password': 'gsadirect1'
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_change_password(self):
        url = reverse('change_password')
        response = self.client.patch(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.patch(url)
        self.assertEqual(response.status_code, 400)
        response = self.client.patch(
            url,
            {
                'old_password': self.data.get('password'),
                'new_password': 'passwordnew123',
            }
        )
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(email=self.data.get('email'))
        self.assertTrue(user.check_password('passwordnew123'))

    def test_verify_ffl(self):
        self.set_auth_token()
        response = self.client.get(
            reverse('verify_ffl'),
            {
                'first': '9',
                'second': '82',
                'last': '01710',
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data.get('verified'))
        self.assertEqual(response.data.get('company'), 'GSA DIRECT LLC')

    def test_suppliers(self):
        response = self.client.get(reverse('suppliers'))
        self.assertEqual(response.status_code, 200)

    def test_order_histories(self):
        url = '/api/order-histories/'
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_categories(self):
        url = reverse('category')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url, {'category': 3})
        self.assertEqual(response.status_code, 200)

    def test_manufacturers(self):
        url = reverse('manufacturers')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_calibers(self):
        url = reverse('calibers')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(reverse('calibers'))
        self.assertEqual(response.status_code, 200)

    def test_product_detail(self):
        product = Product.objects.order_by('?').first()
        url = reverse('product_detail', kwargs={'pk': product.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_product_image(self):
        url = reverse('product_image', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_featured_manufacturers(self):
        url = reverse('featured_manufacturers')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('MILTAC Industries', response.content)
        self.assertIn('SRM Arms', response.content)

    def test_featured_products(self):
        url = reverse('featured_products')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_order(self):
        url = reverse('orders')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_admin_order_list(self):
        url = reverse('admin_orders')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)
        self.user.is_staff = True
        self.user.save()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_admin_order_detail(self):
        order = Order.objects.all().first()
        url = reverse('admin_order_detail', kwargs={'pk': order.pk})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)
        self.user.is_staff = True
        self.user.save()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_dealer_direct_order_list(self):
        url = reverse('dealer_direct_orders')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)
        self.user.userprofile.is_dealer_direct = True
        self.user.userprofile.save()
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 400)
        response = self.client.post(url, {'manufacturer': 'Mfr'})
        self.assertEqual(response.status_code, 200)

    def test_dealer_direct_order_detail(self):
        order = Order.objects.all().first()
        url = reverse('dealer_direct_order_detail', kwargs={'pk': order.pk})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)
        self.user.userprofile.is_dealer_direct = True
        self.user.userprofile.save()
        response = self.client.post(url, {'manufacturer': 'Mfr'})
        content = response.json()
        purchase = content.get('purchases')[0]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(purchase.get('supplier'), 'Dealer Direct')
        self.assertEqual(purchase.get('manufacture'), 'Mfr')

    def test_add_listing(self):
        url = reverse('add_listing')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url, self.product_data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_add_listing_image(self):
        url = reverse('add_listing_image')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        product = self.product
        product.salesforce_id = create_product_2(purchase_history=self.upc)
        product.save()
        exts = ['.png', '.jpg']
        image_url = 'http://mockurl.com'

        # Test image upload with product_id and salesforce_id
        for ext in exts:
            path = os.path.join(os.path.dirname(__file__), 'item-img'+ext)
            with open(path, 'r') as image:
                response1 = self.client.post(
                    url,
                    {'product_id': product.id, 'image': image}
                )
            with open(path, 'r') as image:
                response2 = self.client.post(
                    url,
                    {'salesforce_id': product.salesforce_id, 'image': image}
                )

            self.assertEqual(response1.status_code, 201)
            self.assertEqual(response2.status_code, 201)

        self.assertNotEqual(product.get_thumb_list_image_url(), 'noimage404')
        self.assertNotEqual(product.get_thumb_grid_image_url(), 'noimage404')
        self.assertEqual(product.productlistingimage_set.all().count(), 4)

        # Test url retrieve with product_id and salesforce_id
        path = os.path.join(os.path.dirname(__file__), 'item-img.png')
        with mock.patch('urllib.urlretrieve') as retriever:
            retriever.return_value = [path]

            response = self.client.post(
                url,
                {'product_id': product.id, 'image_url': image_url}
            )
            self.assertEqual(response.status_code, 201)

            response = self.client.post(
                url,
                {
                    'salesforce_id': product.salesforce_id,
                    'image_url': image_url
                }
            )
            self.assertEqual(response.status_code, 201)
        self.assertEqual(product.productlistingimage_set.all().count(), 6)

    def test_update_listing(self):
        url = reverse(
            'update_listing',
            kwargs={'sid': self.product_data['salesforce_id']}
        )
        response = self.client.patch(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        self.client.post(
            reverse('add_listing'),
            self.product_data,
            format='json'
        )
        response = self.client.patch(url, self.product_data_2, format='json')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['price'], self.product_data_2['price'])
        self.assertEqual(int(data['qty']), self.product_data_2['qty'])
        self.assertEqual(data['title'], self.product_data_2['title'])
        self.assertEqual(len(data['tags']), 1)
        self.assertEqual(data['tags'][0]['name'], 'newtag')
        upc = Product.objects.get(
            salesforce_id=self.product_data['salesforce_id']).upc
        self.assertEqual(
            upc.listing_category,
            self.product_data_2['listing_category']
        )
        self.assertEqual(upc.description, self.product_data_2['description'])
        self.assertEqual(upc.manufacturer, self.product_data_2['manufacturer'])
        self.assertEqual(upc.msrp, self.product_data_2['msrp'])
        self.assertEqual(upc.map, self.product_data_2['map'])
        self.assertEqual(
            upc.estimated_ship_days,
            self.product_data_2['estimated_ship_days']
        )

    def test_activate_product(self):
        url = reverse('activate_product')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403)
        self.user.is_staff = True
        self.user.save()
        self.assertFalse(self.product.is_active)
        response = self.client.post(url, {'product_id': self.product.id})
        self.assertEqual(response.status_code, 200)
        prod = Product.objects.get(id=self.product.id)
        self.assertTrue(prod.is_active)

    def test_listing_tags(self):
        url = reverse('listing_tags')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_credit_cards(self):
        url = reverse('credit_cards')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url)
        content = response.json()
        self.assertEqual(response.status_code, 200)
        card1 = content[0]
        card2 = content[1]
        self.assertEqual(card1['token'], self.wallet1.token)
        self.assertEqual(card1['cvv'], self.wallet1.cvv)
        self.assertEqual(
            card1['masked_card_number'],
            self.wallet1.masked_card_number
        )
        self.assertEqual(card2['token'], self.wallet2.token)
        self.assertEqual(card2['cvv'], self.wallet2.cvv)
        self.assertEqual(
            card2['masked_card_number'],
            self.wallet2.masked_card_number
        )

    def test_add_credit_card(self):
        url = reverse('add_credit_card')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        response = self.client.post(url, self.card_data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Wallet.objects.all().exists())

    def test_sale(self):
        url = reverse('sale')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.set_auth_token()
        if not Wallet.objects.exists():
            self.client.post(reverse('add_credit_card'), self.card_data)
        card = Wallet.objects.all().last()
        srm = Product.objects.filter(upc__manufacturer='SRM Arms').first()
        srm.salesforce_id = create_product_2(purchase_history=self.upc)
        srm.save()
        response = self.client.post(
            url,
            {
                'wallet': card.pk,
                'orders': [
                    {
                        'manufacturer': 'SRM Arms',
                        'items': [
                            {
                                'id': srm.id,
                                'upc': srm.upc.upc,
                                'distributor': srm.distributor.name,
                                'model': srm.upc.model,
                                'price': srm.price,
                                'qty': srm.qty
                            }
                        ]
                    }
                ]
            },
            format='json'
        )
        self.assertEquals(response.status_code, 200)
