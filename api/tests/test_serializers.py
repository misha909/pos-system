import mock
import os

from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.core.files import File
from django.core.management import call_command
from django.forms.models import model_to_dict
from django.test import TestCase
from django.utils.translation import ugettext_lazy as _

from rest_framework import exceptions
from model_mommy import mommy

from api.serializers import (
    CartItemSerializer,
    ChangePasswordSerializer,
    FFLRegistrationSerializer,
    ListingSerializer,
    ListingImageSerializer,
    ProductLookupSerializer,
    RegistrationSerializer,
)
from accounts.constants import FFL_VERIFICATION
from products.models import Product, ProductImage, ProductListingImage
from . import recipes


def setUpModule():
    call_command('loaddata', 'blacklisted_passwords')
    call_command('loaddata', 'categories')
    call_command('loaddata', 'distributors')


class CartItemSerializerTestCase(TestCase):
    def setUp(self):
        self.product = recipes.PRODUCT.make()
        self.data = model_to_dict(self.product)
        self.data['model'] = 'model'

    def test_success(self):
        serializer = CartItemSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())

    def test_validate_id_fail(self):
        self.data.update({'id': 500})
        serializer = CartItemSerializer(data=self.data)
        self.assertRaises(
            exceptions.ValidationError,
            serializer.validate_id,
            self.data['id']
        )

    def test_validate_qty_fail(self):
        self.data.update({'qty': -1})
        serializer = CartItemSerializer(data=self.data)
        self.assertRaises(
            exceptions.ValidationError,
            serializer.validate_qty,
            self.data['qty']
        )

    def test_qty_less_instock_fail(self):
        self.data.update({'qty': 6})
        serializer = CartItemSerializer(data=self.data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors['non_field_errors'][0]
        self.assertEqual(
            error,
            'Order quantity must not exceed in-stock quantity.'
        )


class ChangePasswordSerializerTestCase(TestCase):
    def setUp(self):
        self.password = 'longpa55word123'
        recipe = recipes.USER.extend(password=make_password(self.password))
        self.user = recipe.make()

    def test_success(self):
        data = {
            'old_password': self.password,
            'new_password': 'equallyval1dpa55w0rd'
        }
        serializer = ChangePasswordSerializer(user=self.user, data=data)
        self.assertTrue(serializer.is_valid())

    def test_invalid_old_password_fail(self):
        data = {
            'old_password': 'wr0ngpa55w0rd',
            'new_password': 'equallyval1dpa55w0rd'
        }
        serializer = ChangePasswordSerializer(user=self.user, data=data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('old_password')[0]
        self.assertEqual(
            error,
            _('Old Password does not match for user %s.' % self.user.username)
        )

    def test_common_new_password_fail(self):
        data = {
            'old_password': self.password,
            'new_password': 'password'
        }
        serializer = ChangePasswordSerializer(user=self.user, data=data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('new_password')[0]
        self.assertEqual(
            error,
            _('Password is too common.')
        )


class FFLRegistrationSerializerTestCase(TestCase):
    def setUp(self):
        self.user = recipes.USER.extend(is_active=False).make()
        self.reg_info = mommy.make('accounts.RegistrationInfo',
                                   user=self.user, step=FFL_VERIFICATION)

    def test_validate_success(self):
        data = {
            'token': self.user.auth_token.key,
            'first': '982',
            'last': '01710'
        }
        serializer = FFLRegistrationSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_validate_token_wrong_step_fail(self):
        data = {
            'token': self.user.auth_token.key,
            'first': '982',
            'last': '01710'
        }
        self.reg_info.move_to_next_step()

        serializer = FFLRegistrationSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('token')[0]
        self.assertEqual(error, _('User cannot access this step.'))

        self.reg_info.step = FFL_VERIFICATION
        self.reg_info.save()

    def test_invalid_token_fail(self):
        data = {
            'token': 'randomfaketoken',
            'first': '982',
            'last': '01710'
        }
        serializer = FFLRegistrationSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('token')[0]
        self.assertEqual(error, _('Invalid token.'))


class ListingSerializerTestCase(TestCase):
    def setUp(self):
        user = recipes.USER.make()
        tags = [{'name': 'tag1'}, {'name': 'tag2'}]
        upcs = recipes.UPC.prepare(_quantity=2)
        products = recipes.PRODUCT.prepare(_quantity=2)

        self.data = model_to_dict(upcs[0])
        self.alt_data = model_to_dict(upcs[1])
        self.data.update(model_to_dict(products[0]))
        self.alt_data.update(model_to_dict(products[1]))
        self.data.update({
            'category': 'Airsoft', 'user_id': user.id, 'tags': tags,
            'barrel_length': self.data['length_barrel'],
            'overall_length': self.data['length_overall']})
        self.alt_data.update({
            'category': 'Ammunition', 'user_id': user.id, 'tags': tags,
            'barrel_length': self.data['length_barrel'],
            'overall_length': self.data['length_overall']})

    def test_validate_category(self):
        serializer = ListingSerializer()
        function = serializer.validate_category

        category = self.data['category']
        self.assertEqual(category, function(category))

        category = 'nonexistent category'
        self.assertRaises(exceptions.ValidationError, function, category)

    def test_populate_upc(self):
        serializer = ListingSerializer(data=self.data)
        function = serializer.populate_upc
        self.assertTrue(serializer.is_valid())

        upc_obj = function(self.data['upc'], serializer.data)
        self.assertIsNotNone(upc_obj)
        self.assertEqual(upc_obj.model, self.data['title'])
        self.assertEqual(upc_obj.category.name, self.data['category'])
        self.assertEqual(upc_obj.description, self.data['description'])
        self.assertEqual(upc_obj.caliber, self.data['caliber'])
        self.assertEqual(upc_obj.capacity, self.data['capacity'])
        self.assertEqual(upc_obj.finish, self.data['finish'])
        self.assertEqual(upc_obj.weight, self.data['weight'])
        self.assertEqual(upc_obj.length_barrel, self.data['barrel_length'])
        self.assertEqual(upc_obj.length_overall, self.data['overall_length'])
        self.assertEqual(upc_obj.manufacturer, self.data['manufacturer'])

    def test_create(self):
        serializer = ListingSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())

        serializer.save()
        product = Product.objects.first()
        self.assertIsNotNone(product)
        self.assertFalse(product.is_active)
        self.assertEqual(product.price, self.data['price'])
        self.assertEqual(product.qty, self.data['qty'])
        self.assertEqual(product.salesforce_id, self.data['salesforce_id'])

    def test_create_without_upc(self):
        self.data['upc'] = ''
        serializer = ListingSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())

        product = serializer.save()
        product = Product.objects.get(id=product.id)
        self.assertIsNotNone(product)
        self.assertFalse(product.is_active)
        self.assertEqual(product.price, self.data['price'])
        self.assertEqual(product.qty, self.data['qty'])
        self.assertEqual(product.salesforce_id, self.data['salesforce_id'])
        self.assertEqual(product.upc.upc, self.data['salesforce_id'])

    def test_create_without_upc_and_sid(self):
        self.data['upc'] = ''
        self.data['salesforce_id'] = ''
        serializer = ListingSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())

        product = serializer.save()
        product = Product.objects.get(id=product.id)
        self.assertIsNotNone(product)
        self.assertFalse(product.is_active)
        self.assertEqual(product.price, self.data['price'])
        self.assertEqual(product.qty, self.data['qty'])
        self.assertEqual(product.upc.upc, 'DealerDirect'+str(product.id))

    def test_update(self):
        serializer = ListingSerializer(data=self.data)
        serializer.is_valid()
        product = serializer.save()

        serializer = ListingSerializer(
            product,
            data=self.alt_data,
            partial=True
        )
        serializer.is_valid()
        serializer.save()
        product = Product.objects.first()
        upc = product.upc

        self.assertFalse(product.is_active)
        self.assertEqual(product.title, self.alt_data['title'])
        self.assertEqual(upc.category.name, self.alt_data['category'])
        self.assertEqual(upc.description, self.alt_data['description'])
        self.assertEqual(upc.caliber, self.alt_data['caliber'])
        self.assertEqual(upc.capacity, self.alt_data['capacity'])
        self.assertEqual(upc.finish, self.alt_data['finish'])
        self.assertEqual(upc.weight, self.alt_data['weight'])
        self.assertEqual(upc.length_barrel, self.alt_data['barrel_length'])
        self.assertEqual(upc.length_overall, self.alt_data['overall_length'])
        self.assertEqual(upc.manufacturer, self.alt_data['manufacturer'])

        self.assertEqual(product.price, self.data['price'])
        self.assertEqual(product.qty, self.data['qty'])
        self.assertEqual(product.salesforce_id, self.data['salesforce_id'])


class ListingImageSerializerTestCase(TestCase):
    def setUp(self):
        self.product = recipes.PRODUCT.make()

    def tearDown(self):
        qs = ProductListingImage.objects.all()
        for obj in qs:
            obj.image.delete()
            obj.image_thumbnail.delete()
        qs = ProductImage.objects.all()
        for obj in qs:
            obj.image.delete()
            obj.image_thumb_grid.delete()
            obj.image_thumb_list.delete()
            obj.image_detail_popup.delete()

    def test_validate_image_url_success(self):
        path = os.path.join(os.path.dirname(__file__), 'item-img.png')
        data = {
            'product_id': self.product.id,
            'image_url': 'http://dummyurl.com',
        }
        with mock.patch('urllib.urlretrieve') as retriever:
            retriever.return_value = [path]
            serializer = ListingImageSerializer(data=data)
            self.assertTrue(serializer.is_valid())

    def test_validate_image_url_nonimage_fail(self):
        path = __file__
        data = {
            'product_id': self.product.id,
            'image_url': 'http://dummyurl.com',
        }
        with mock.patch('urllib.urlretrieve') as retriever:
            retriever.return_value = [path]
            serializer = ListingImageSerializer(data=data)
            self.assertFalse(serializer.is_valid())
            self.assertEqual(
                serializer.errors.get('image_url')[0],
                'Image could not be retrieved from url %s' % data['image_url']
            )

    def test_validate_missing_parameter_fail(self):
        data = {
            'product_id': self.product.id
        }
        serializer = ListingImageSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors['non_field_errors'][0]
        self.assertEqual(
            error,
            "Requires either 'image' or 'image_url' parameter."
        )

    def test_create_success(self):
        path = os.path.join(os.path.dirname(__file__), 'item-img.png')
        with File(open(path, 'r')) as image:
            data = {
                'product_id': self.product.id,
                'image': image
            }
            serializer = ListingImageSerializer(data=data)
            serializer.is_valid()
            self.assertIsInstance(serializer.save(), ProductListingImage)


class ProductLookupSerializerTestCase(TestCase):
    def setUp(self):
        self.product = recipes.PRODUCT.make()

    def test_product_id_success(self):
        data = {
            'product_id': self.product.id
        }
        serializer = ProductLookupSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsNotNone(getattr(serializer, 'product', None))

    def test_salesforce_id_success(self):
        data = {
            'salesforce_id': self.product.salesforce_id
        }
        serializer = ProductLookupSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsNotNone(getattr(serializer, 'product', None))

    def test_both_product_salesforce_id_success(self):
        data = {
            'product_id': self.product.id,
            'salesforce_id': self.product.salesforce_id
        }
        serializer = ProductLookupSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsNotNone(getattr(serializer, 'product', None))

    def test_product_id_fail(self):
        serializer = ProductLookupSerializer(data={'product_id': 3})
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('non_field_errors')[0]
        self.assertEqual(
            error,
            'Product with id 3 does not exist.'
        )

    def test_salesforce_id_fail(self):
        serializer = ProductLookupSerializer(data={'salesforce_id': 'yolo'})
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('non_field_errors')[0]
        self.assertEqual(
            error,
            'Product with Salesforce id yolo does not exist.'
        )


class RegistrationSerializerTestCase(TestCase):
    def setUp(self):
        user = recipes.USER.make()
        self.data = model_to_dict(user)

    def test_validate_email_fail(self):
        data = {
            'email': self.data.get('email'),
            'password': self.data.get('password')
        }
        serializer = RegistrationSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        error = serializer.errors.get('email')[0]
        self.assertEqual(error, _('User account exists.'))

    def test_validate_password_fail(self):
        data = {
            'email': 'have@thee.com',
            'password': 'password'
        }
        serializer = RegistrationSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        error = serializer.errors.get('password')[0]
        self.assertEqual(error, _('Password is too common.'))

    def test_create_user_non_dealerdirect(self):
        data = {
            'email': 'have@thee.com',
            'password': 'longpa55word123',
        }
        serializer = RegistrationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsNotNone(serializer.save_user())
        user = User.objects.get(email=data.get('email'))
        self.assertFalse(user.is_active)
        self.assertFalse(user.userprofile.is_dealer_direct)

    def test_create_user_dealerdirect(self):
        data = {
            'email': 'have@thee.com',
            'password': 'longpa55word123',
            'is_dealer_direct': True
        }
        serializer = RegistrationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsNotNone(serializer.save_user())
        user = User.objects.get(email=data.get('email'))
        self.assertTrue(user.is_active)
        self.assertTrue(user.userprofile.is_dealer_direct)
