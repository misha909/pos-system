from django.contrib.auth.models import User

from model_mommy.recipe import Recipe, foreign_key

USER = Recipe(
    User,
    email='email@address.com',
    _fill_optional=True)

DISTRIBUTOR = Recipe(
    'accounts.Distributor',
    name='Name',
    _fill_optional=True)

CATEGORY = Recipe(
    'products.Category',
    name='Airsoft')

UPC = Recipe(
    'products.UPC',
    upc='UPC',
    category=None,
    shirt_size='XS',
    listing_category='Ammo-Rifle',
    listing_condition='New',
    condition_detail='New',
    shipping_zip_code='83642',
    _fill_optional=True)

PRODUCT = Recipe(
    'Product',
    distributor=foreign_key(DISTRIBUTOR),
    upc=foreign_key(UPC),
    price=12345.0,
    qty='5',
    salesforce_id='abcd123987',
    _fill_optional=True)

INACTIVE_PRODUCT = PRODUCT.extend(is_active=False)

ORDER = Recipe(
    'saasapp.Order',
    user=foreign_key(USER))

PURCHASE_HISTORY = Recipe(
    'saasapp.PurchaseHistory',
    user=foreign_key(USER),
    order=foreign_key(ORDER),
    _fill_optional=True)
