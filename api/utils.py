import StringIO
from PIL import Image

from django.core.files import File

from products.models import ProductImage


def resize_image(img, w, h):
    (orig_width, orig_height) = img.size
    if w <= 0 or h <= 0:
        return img
    factor = 1.0
    if orig_width > orig_height:
        factor = float(w)/orig_width
    else:
        factor = float(h)/orig_height
    return img.resize((int(orig_width*factor), int(orig_height*factor)), Image.ANTIALIAS)


def create_image(raw_image, w, h):
    image_obj = Image.open(raw_image)
    new_image = resize_image(image_obj, w, h)
    new_image_file = StringIO.StringIO()
    new_image.save(new_image_file, format=image_obj.format)
    return File(new_image_file)


def create_search_result_images(raw_image, product):
    base_image_file = raw_image
    upc = product.upc
    try:
        base_image = Image.open(base_image_file)
        image = ProductImage.objects.create(
            upc=upc,
            is_davidson=False
        )
        image.image.save(
            "%s.%s" % (upc.upc, base_image.format),
            File(base_image_file)
        )

        thumb_grid = resize_image(base_image, 230, 230)
        thumb_grid_file = StringIO.StringIO()
        thumb_grid.save(thumb_grid_file, format=thumb_grid.format)
        image.image_thumb_grid.save(
            "%s.%s" % (upc.upc, thumb_grid.format),
            File(thumb_grid_file)
        )

        thumb_list = resize_image(base_image, 63, 63)
        thumb_list_file = StringIO.StringIO()
        thumb_list.save(thumb_list_file, format=thumb_list.format)
        image.image_thumb_list.save(
            "%s.%s" % (upc.upc, thumb_list.format),
            File(thumb_list_file)
        )

        image.save()
    except Exception, e:
        # probably not an image file.
        print e


def delete_images(product):
    for image in product.upc.productimage_set.all():
        image.image.delete()
        image.image_thumb_grid.delete()
        image.image_thumb_list.delete()
        image.image_detail_popup.delete()
    for image in product.productlistingimage_set.all():
        image.image.delete()
        image.image_thumbnail.delete()
