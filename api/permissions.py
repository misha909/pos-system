from rest_framework import permissions


class DealerDirectPermission(permissions.BasePermission):
    """
    Permission for Dealer Direct users
    """
    def has_permission(self, request, view):
        return request.user.userprofile.is_dealer_direct
