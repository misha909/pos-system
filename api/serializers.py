import urllib
import requests
from PIL import Image

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from rest_framework import exceptions, serializers, permissions
from rest_framework.authtoken.models import Token
from rest_framework.pagination import (
    PaginationSerializer,
    NextPageField,
    PreviousPageField
)

from accounts.models import (
    FFL,
    UserProfile,
    Distributor,
    RegistrationInfo,
    BlacklistedPassword,
)
from accounts.utils import (
    verify_davidson,
    verify_lipseys,
    verify_green,
    verify_rsr,
    verify_sports,
    verify_camfour,
    verify_zanders,
    verify_jerrys,
    verify_ellet
)
from accounts.constants import (
    REGISTRATION_STEP,
    FFL_VERIFICATION
)
from products.models import (
    UPC,
    Product,
    ProductListingImage,
    ProductTag,
    Category
)
from accounts.models import Wallet
from products.utils import is_integer, verify_ffl
from saasapp.constants import (
    STATE_CHOICES,
    WALLET_TYPE_CHOICES,
    CREDIT_CARD,
    BANK_ACCOUNT
)
from saasapp.models import PurchaseHistory, Order, ShippingFee
from saasapp.tsys import verify_card
from saasapp.salesforce import (
    create_ach_file,
    create_opportunities,
    create_account
)
from .constants import PERIOD_CHOICES, DEALER_DIRECT
from .permissions import DealerDirectPermission
from .utils import create_image, create_search_result_images


class AuthTokenSerializer(serializers.Serializer):
    """
    Uses email instead of username for getting the token.
    """
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            username = None
            if User.objects.filter(email=email).exists():
                username = User.objects.get(email=email).username
            user = authenticate(username=username, password=password)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise exceptions.ValidationError(msg)
            else:
                msg = _('Unable to log in with provided credentials.')
                raise exceptions.ValidationError(msg)
        else:
            msg = _('Must include "email" and "password"')
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs


class UserSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)

    def validate_email(self, value):
        if User.objects.filter(username=value).exists():
            raise exceptions.ValidationError(
                _('This field must be unique in vArmory.')
            )
        return value


class EditSupplierSerializer(serializers.Serializer):
    supplier = serializers.CharField()
    rep_email = serializers.EmailField()


class UserInfoSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    suppliers = serializers.SerializerMethodField()
    supplier_names = {
        'davidson': "Davidson's",
        'lipseys': 'Lipseys',
        'green': 'Green Supply',
        'rsr': 'RSR Group',
        'sports': "Sport's South",
    }

    def get_suppliers(self, obj):
        supplier_data = []
        for key in self.supplier_names:
            if getattr(obj, key+'_verified'):
                distributor = Distributor.objects.get(
                    name=self.supplier_names[key])
                supplier_data.append({
                    'supplier_id': distributor.id,
                    'supplier': distributor.name,
                    'rep_email': getattr(obj, key+'_mail')
                })
        return supplier_data

    def get_user(self, obj):
        return {
            'first_name': obj.user.first_name,
            'last_name': obj.user.last_name,
            'company': obj.company_name,
            'email': obj.user.email
        }

    class Meta:
        model = UserProfile
        fields = [
            'user', 'suppliers',
            'margin',
            'dealer_direct_token',
            'is_dealer_direct',
            'logo_url',
            'address_street',
            'address_city',
            'address_state',
            'address_zip',
            'shipping_carrier',
            'carrier_dropoff',
            'scheduled_pickups',
            'ffl_number_first_three',
            'ffl_number_last_five',
        ]


class UserUpdateSerializer(serializers.ModelSerializer):
    company = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=False)
    supplier_emails = EditSupplierSerializer(many=True)
    user = UserSerializer(required=False)

    supplier_fields = {
        "Davidson's": 'davidson_mail',
        'Lipseys': 'lipseys_mail',
        'Green Supply': 'green_mail',
        'RSR Group': 'rsr_mail',
        "Sport's South": 'sports_mail'
    }

    class Meta:
        model = UserProfile
        fields = (
            'user',
            'company',
            'phone_number',
            'supplier_emails',
            'margin',
            'dealer_direct_token',
            'is_dealer_direct',
            'logo_url',
            'address_street',
            'address_city',
            'address_state',
            'address_zip',
            'shipping_carrier',
            'carrier_dropoff',
            'scheduled_pickups',
        )

    def update(self, instance, validated_data):
        for field in self.Meta.fields:
            if field == 'user':
                new_user_info = validated_data.get(field, {})
                user = instance.user
                user.first_name = new_user_info.get(
                    'first_name', user.first_name)
                user.last_name = new_user_info.get('last_name', user.last_name)
                user.email = new_user_info.get('email', user.email)
                user.username = new_user_info.get('email', user.username)
                if new_user_info.get('email', None):
                    api_url = settings.DEALER_DIRECT_URL + 'api/update-user-info/'
                    headers = {'Authorization': 'Token ' + user_profile.dealer_direct_token}
                    api_data = {'email': user.email}
                    requests.patch(url=api_url, headers=headers, data=api_data)
                user.save()
            elif field == 'company':
                instance.company_name = validated_data.get(
                    field, instance.company_name
                )
            elif field == 'phone_number':
                if FFL.objects.filter(ffl_number=instance.ffl_number).exists():
                    ffl = FFL.objects.get(ffl_number=instance.ffl_number)
                    ffl.phone = validated_data.get('phone_number', ffl.phone)
                    ffl.save()
            elif field == 'supplier_emails':
                for dic in validated_data.get(field):
                    try:
                        edit_field = self.supplier_fields[dic['supplier']]
                        setattr(instance, edit_field, dic['rep_email'])
                    except Exception:
                        pass
            else:
                setattr(instance, field, validated_data.get(
                    field, getattr(instance, field))
                )
        instance.save()
        if instance.company_name:
            create_account(user=instance.user)
        return instance


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.RegexField(
        regex=r'^[a-zA-Z0-9]+$',
        min_length=8,
        error_messages={
            'min_length': _('Ensure password has a minimum of 8 characters.'),
            'invalid': _('Ensure password consists of letters or numbers.'),
        }
    )

    def __init__(self, user, *args, **kwargs):
        super(ChangePasswordSerializer, self).__init__(*args, **kwargs)
        self.user = user

    def validate_old_password(self, value):
        # Check if old password is valid
        user = authenticate(
            username=self.user.username,
            password=value
        )
        if not user:
            msg = _('Old Password does not match for user %s.' % self.user.username)
            raise exceptions.ValidationError(msg)
        return value

    def validate_new_password(self, value):
        if BlacklistedPassword.objects.filter(text=value).exists():
            msg = _('Password is too common.')
            raise serializers.ValidationError(msg)
        return value


class AddSupplierSerializer(serializers.Serializer):
    supplier = serializers.IntegerField()
    username = serializers.CharField()
    password = serializers.CharField()

    def __init__(self, user, *args, **kwargs):
        super(AddSupplierSerializer, self).__init__(*args, **kwargs)
        self.user = user

    def validate(self, data):
        # Check if supplier exists
        if not Distributor.objects.filter(pk=data.get('supplier')).exists():
            raise exceptions.ValidationError("Supplier does not exist.")
        supplier = Distributor.objects.get(pk=data.get('supplier'))
        options = {
            "Davidson's": verify_davidson,
            'Lipseys': verify_lipseys,
            'Green Supply': verify_green,
            'RSR Group': verify_rsr,
            "Sport's South": verify_sports,
            'Camfour': verify_camfour,
            'Zanders': verify_zanders,
            "Jerry's Sports Center": verify_jerrys,
            'Ellet': verify_ellet
        }
        function = options.get(supplier.name)
        if function:
            username = data.get('username')
            password = data.get('password')
            if not function(self.user, username, password):
                raise exceptions.ValidationError(
                    "Can't login to %s" % supplier.name
                )
        else:
            raise exceptions.ValidationError('Supplier does not exist.')
        return data


class DistributorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Distributor
        fields = (
            'id',
            'name'
        )


class PurchaseHistorySerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        try:
            domain = Site.objects.get_current().domain
            qs = obj.product.productlistingimage_set.first()
            return domain + qs.image.url
        except Exception:
            return ''

    class Meta:
        model = PurchaseHistory
        exclude = []


class PurchaseHistoryNextPageField(NextPageField):
    """
    Field that returns a link to the next page in Purchase History
    paginated results.
    """
    def to_representation(self, value):
        if not value.has_next():
            return None
        page = value.next_page_number()
        request = self.context.get('request')
        relative_url = reverse(
            'order_histories',
            kwargs={
                'page': page
            }
        )
        url = request.build_absolute_uri(relative_url) or ''
        return url


class PurchaseHistoryPreviousPageField(PreviousPageField):
    """
    Field that returns a link to the previous page in Purchase History
    paginated results.
    """
    def to_representation(self, value):
        if not value.has_previous():
            return None
        page = value.previous_page_number()
        request = self.context.get('request')
        relative_url = reverse(
            'order_histories',
            kwargs={
                'page': page
            }
        )
        url = request.build_absolute_uri(relative_url) or ''
        return url


class PaginatedPurchaseHistorySerializer(PaginationSerializer):
    next = PurchaseHistoryNextPageField(source='*')
    previous = PurchaseHistoryPreviousPageField(source='*')


class PurchaseOrderSerializer(serializers.Serializer):
    """
    product - Id of the product
    quantity - Quantity of the item purchased
    """
    product = serializers.IntegerField()
    quantity = serializers.IntegerField()

    def validate_product(self, value):
        if not Product.objects.filter(pk=value).exists():
            raise exceptions.ValidationError("Product does not exist.")
        return value

    def validate_quantity(self, value):
        if value <= 0:
            raise exceptions.ValidationError("Quantity must be positive.")
        return value

    def validate(self, attrs):
        """
        Additional Validations:
        - Check if there is an available product.
        """
        product_id = attrs.get('product')
        product = Product.objects.get(pk=product_id)
        if not product.is_product_available():
            raise exceptions.ValidationError("No available product.")
        # Check if there is enough quantity
        if not product.qty == '99+':
            quantity = attrs.get('quantity')
            if is_integer(product.qty):
                remaining_quantity = int(product.qty)
                if remaining_quantity < quantity:
                    raise exceptions.ValidationError("No available product.")
        return attrs


class VerifyFFLSerializer(serializers.Serializer):
    """
    Patterned after https://www.atfonline.gov/fflezcheck/

    first accepts a single digit number.
    second accepts a two-digit number with leading zero allowed.
    last accepts a five digit number with leading zeroes allowed.
    """
    first = serializers.CharField()
    second = serializers.CharField()
    last = serializers.CharField()

    def validate_first(self, value):
        if not is_integer(value):
            msg = _('First set of digits is not an integer.')
            raise exceptions.ValidationError(msg)
        if not len(value) == 1:
            msg = _('First set of digits must be only a single digit integer.')
            raise exceptions.ValidationError(msg)
        return value

    def validate_second(self, value):
        if not is_integer(value):
            msg = _('Second set of digits is not an integer.')
            raise exceptions.ValidationError(msg)
        if not len(value) == 2:
            msg = _('Second set of digits must be only a two digit integer with leading zeroes allowed.')
            raise exceptions.ValidationError(msg)
        return value

    def validate_last(self, value):
        if not is_integer(value):
            msg = _('Last set of digits is not an integer.')
            raise exceptions.ValidationError(msg)
        int_value = int(value)
        if not len(value) == 5:
            msg = _('Last set of digits must be only a five digit integer with leading zeroes allowed.')
            raise exceptions.ValidationError(msg)
        return value


class ProductIDSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()

    def validate_product_id(self, value):
        if not Product.objects.filter(id=value).exists():
            raise exceptions.ValidationError(
                'Product with id %d does not exist.' % value
            )
        self.product = Product.objects.get(id=value)
        return value


class ProductLookupSerializer(serializers.Serializer):
    product_id = serializers.IntegerField(required=False)
    salesforce_id = serializers.CharField(
        required=False,
        allow_blank=True,
        default=None
    )

    def validate(self, data):
        product_id = data.get('product_id')
        salesforce_id = data.get('salesforce_id')
        if not product_id and not salesforce_id:
            raise exceptions.ValidationError(
                'Product id or Salesforce id required.'
            )
        if product_id and Product.objects.filter(id=product_id).exists():
            self.product = Product.objects.get(id=product_id)
        elif salesforce_id and Product.objects.filter(
            salesforce_id=salesforce_id
        ).exists():
            self.product = Product.objects.get(salesforce_id=salesforce_id)
        else:
            mode = 'id' if product_id else 'Salesforce id'
            id = product_id or salesforce_id
            raise exceptions.ValidationError(
                'Product with %s %s does not exist.' % (mode, id)
            )
        return data


class AccountActivationSerializer(serializers.Serializer):
    salesforce_id = serializers.CharField()

    def validate(self, data):
        account_id = data.get('salesforce_id')
        if UserProfile.objects.filter(account_id=account_id).exists():
            self.profile = UserProfile.objects.get(account_id=account_id)
        else:
            raise exceptions.ValidationError(
                'Account with Salesforce id = %s does not exist'
            )
        return data


class ProductTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductTag
        fields = ['name']


class ProductSerializer(serializers.ModelSerializer):
    upc = serializers.CharField(source='upc.upc')
    distributor = serializers.CharField(source='distributor.name')
    gun_type = serializers.CharField(source='upc.gun_type')
    manufacturer = serializers.CharField(source='upc.manufacturer')
    model = serializers.CharField(source='upc.model')
    caliber = serializers.CharField(source='upc.caliber')
    action = serializers.CharField(source='upc.action')
    description = serializers.CharField(source='upc.description')
    capacity = serializers.CharField(source='upc.capacity')
    finish = serializers.CharField(source='upc.finish')
    stock = serializers.CharField(source='upc.stock')
    sights = serializers.CharField(source='upc.sights')
    length_barrel = serializers.CharField(source='upc.length_barrel')
    length_overall = serializers.CharField(source='upc.length_overall')
    weight = serializers.CharField(source='upc.weight')
    tags = ProductTagSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        extra_kwargs = {'is_active': {'write_only': True}}


class CategorySerializer(serializers.ModelSerializer):
    isCategory = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'isCategory'
        )

    def get_isCategory(self, obj):
        if len(obj.children.all()) > 0:
            return True
        return False

    def get_name(self, obj):
        to_add = obj.name
        while obj.parent is not None:
            to_add = obj.parent.name + ' - ' + to_add
            obj = obj.parent
        return to_add


class ProductImageSerializer(serializers.Serializer):
    thumb_grid_image_url = serializers.URLField(
        source='get_thumb_grid_image_url'
    )
    thumb_list_image_url = serializers.URLField(
        source='get_thumb_list_image_url'
    )
    detail_popup_image_url = serializers.SerializerMethodField()

    def get_detail_popup_image_url(self, obj):
        if obj.distributor.name == DEALER_DIRECT:
            images = obj.productlistingimage_set.all()
            return [img.image.url for img in images]
        else:
            return obj.get_detail_popup_image_url()


class ProductSearchResultSerializer(serializers.Serializer):
    id = serializers.IntegerField(source='pk')
    upc = serializers.CharField(source='upc_value')
    distributor = serializers.CharField(source='distributor_name')
    category = serializers.CharField(source='category_name')
    gun_type = serializers.CharField()
    manufacturer = serializers.CharField()
    model = serializers.CharField(source='model_value')
    caliber = serializers.CharField()
    description = serializers.CharField()
    price = serializers.FloatField()
    qty = serializers.CharField()
    stock = serializers.CharField()
    thumb_grid_image_url = serializers.URLField()
    thumb_list_image_url = serializers.URLField()


class RegistrationInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegistrationInfo


class RegistrationSerializer(serializers.Serializer):
    first_name = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True
    )
    last_name = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True
    )
    email = serializers.EmailField()
    password = serializers.RegexField(
        regex=r'^[A-Za-z0-9@!()\/\\<>\.\-\?\_{}[\]\';|#$*%^&+=]',
        min_length=8,
        error_messages={
            'min_length': _('Ensure password has a minimum of 8 characters.'),
            'invalid': _('Ensure password consists of letters or numbers.'),
        }
    )
    is_dealer_direct = serializers.BooleanField(required=False, default=False)
    dealer_direct_token = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True
    )

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            msg = _('User account exists.')
            raise exceptions.ValidationError(msg)
        return value

    def validate_password(self, value):
        if BlacklistedPassword.objects.filter(text=value).exists():
            msg = _('Password is too common.')
            raise serializers.ValidationError(msg)
        return value

    def save_user(self):
        """
        Saves user and creates a RegistrationInfo object for the user.
        """
        first_name = self.validated_data.get('first_name', '')
        last_name = self.validated_data.get('last_name', '')
        email = self.validated_data.get('email')
        password = self.validated_data.get('password')
        is_dealer_direct = self.validated_data.get('is_dealer_direct')

        user = User.objects.create_user(
            email,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        # Make user aleady active if it's a Dealer Direct User
        user.is_active = is_dealer_direct
        user.save()
        if is_dealer_direct:
            token = self.validated_data.get('dealer_direct_token')
            user.userprofile.is_dealer_direct = True
            user.userprofile.dealer_direct_token = token
            user.userprofile.save()
            
        # Create and return registration info
        reg_info = RegistrationInfo.objects.create(
            user=user
        )
        reg_info.move_to_next_step()
        return reg_info


class FFLRegistrationSerializer(serializers.Serializer):
    token = serializers.CharField()
    first = serializers.CharField()
    last = serializers.CharField()
    zip = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True
    )

    def validate_token(self, value):
        """
        Validate that token exists and current step is FFL verification.
        """
        if not Token.objects.filter(key=value).exists():
            msg = _('Invalid token.')
            raise exceptions.ValidationError(msg)
        token = Token.objects.get(key=value)
        user = token.user
        if not RegistrationInfo.objects.filter(user=user).exists():
            msg = _('Registration Info does not exist.')
            raise exceptions.ValidationError(msg)
        reg_info = RegistrationInfo.objects.get(user=user)
        if reg_info.step != FFL_VERIFICATION:
            msg = _('User cannot access this step.')
            raise exceptions.ValidationError(msg)
        return value

    def validate_first(self, value):
        if not is_integer(value):
            msg = _('First set of digits is not an integer.')
            raise exceptions.ValidationError(msg)
        if not len(value) == 3:
            msg = _('First set of digits must be only a three digit integer with leading zeroes allowed.')
            raise exceptions.ValidationError(msg)
        return value

    def validate_last(self, value):
        if not is_integer(value):
            msg = _('Last set of digits is not an integer.')
            raise exceptions.ValidationError(msg)
        int_value = int(value)
        if not len(value) == 5:
            msg = _('Last set of digits must be only a five digit integer with leading zeroes allowed.')
            raise exceptions.ValidationError(msg)
        return value

    def validate(self, data):
        first = data.get('first')
        last = data.get('last')
        if UserProfile.objects.filter(
            ffl_number_first_three=first,
            ffl_number_last_five=last
        ).exists():
            msg = _('FFL number already registered with vArmory')
            raise exceptions.ValidationError(msg)
        return data

    def verify_ffl(self):
        if self.is_valid():
            token = Token.objects.get(key=self.validated_data.get('token'))
            user = token.user
            reg_info = RegistrationInfo.objects.get(user=user)

            ffl_valid = verify_ffl(
                self.validated_data.get('first')[0],
                self.validated_data.get('first')[1:3],
                self.validated_data.get('last')
            )
            if ffl_valid.get('verified', False):
                user.is_active = True
                user.save()
                # Save company to user profile
                user_profile = user.userprofile
                user_profile.company_name = ffl_valid.get('company', None)
                user_profile.ffl_number_first_three = self.validated_data.get('first')
                user_profile.ffl_number_last_five = self.validated_data.get('last')
                user_profile.ffl_expiration_date = ffl_valid.get('expiration_date', None)
                user_profile.ffl_premise_address = ffl_valid.get('premise_address', None)
                user_profile.ffl_mailing_address = ffl_valid.get('mailing_address', None)
                user_profile.save()
                create_account(user=user)
                reg_info.move_to_next_step()
            return (ffl_valid, reg_info)
        return (False, None)


class MassAddSupplierSerializer(serializers.Serializer):
    supplier_id = serializers.IntegerField()

    def validate_supplier_id(self, value):
        if not Distributor.objects.filter(id=value).exists():
            msg = _('Supplier with id = %d does not exist.' % value)
            raise exceptions.ValidationError(msg)
        return value


class DashboardSerializer(serializers.Serializer):
    period = serializers.ChoiceField(PERIOD_CHOICES)
    manufacturer = serializers.CharField()


class ManufacturerSerializer(serializers.Serializer):
    manufacturer = serializers.CharField()


class OrderNextPageField(NextPageField):
    """
    Field that returns a link to the next page in Purchase History
    paginated results.
    """
    def to_representation(self, value):
        if not value.has_next():
            return None
        page = value.next_page_number()
        request = self.context.get('request')
        perms = self.context.get('view').permission_classes
        admin = permissions.IsAdminUser
        url_name = 'orders'
        if admin in perms:
            url_name = 'admin_orders'
        elif DealerDirectPermission in perms:
            url_name = 'dealer_direct_orders'
        relative_url = reverse(
            url_name,
            kwargs={
                'page': page
            }
        )
        url = request.build_absolute_uri(relative_url) or ''
        return url


class OrderPreviousPageField(PreviousPageField):
    """
    Field that returns a link to the previous page in Purchase History
    paginated results.
    """
    def to_representation(self, value):
        if not value.has_previous():
            return None
        page = value.previous_page_number()
        request = self.context.get('request')
        perms = self.context.get('view').permission_classes
        admin = permissions.IsAdminUser
        url_name = 'orders'
        if admin in perms:
            url_name = 'admin_orders'
        elif DealerDirectPermission in perms:
            url_name = 'dealer_direct_orders'
        relative_url = reverse(
            url_name,
            kwargs={
                'page': page
            }
        )
        url = request.build_absolute_uri(relative_url) or ''
        return url


class PaginatedOrderSerializer(PaginationSerializer):
    next = OrderNextPageField(source='*')
    previous = OrderPreviousPageField(source='*')


class ShippingFeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShippingFee
        exclude = ['order']


class BaseOrderSerializer(serializers.ModelSerializer):
    order_number = serializers.ReadOnlyField(source='order_number_c')
    payment_type = serializers.ChoiceField(choices=WALLET_TYPE_CHOICES)
    shipping_options = serializers.SerializerMethodField()
    total = serializers.ReadOnlyField(source='transaction_amount')

    def get_shipping_options(self, obj):
        user = self.context.get('request').user
        fees = obj.shippingfee_set.filter(
            shipment_provider=user.userprofile.shipping_carrier
        )
        official_fees = [fee for fee in fees if fee.official]
        serializer = ShippingFeeSerializer(data=official_fees, many=True)
        return serializer.data

    class Meta:
        model = Order
        read_only_fields = [
            'id', 'user', 'group',
            'date', 'manufacturer',
            'payment_type',
            'account_number',
            'shipping_cost',
            'shipping_status',
            'shipping_label',
            'shipping_label_pdf',
            'wallet'
        ]


class OrderSerializer(BaseOrderSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api_order_detail'
    )


class OrderDetailSerializer(OrderSerializer):
    purchases = PurchaseHistorySerializer(
        source='purchasehistory_set',
        many=True,
    )


class AdminOrderSerializer(BaseOrderSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='admin_order_detail'
    )


class AdminOrderDetailSerializer(AdminOrderSerializer):
    purchases = PurchaseHistorySerializer(
        source='purchasehistory_set',
        many=True,
    )
    shipping_label_pdf_url = serializers.CharField()


class DealerDirectOrderSerializer(BaseOrderSerializer):
    purchases = PurchaseHistorySerializer(
        source='purchasehistory_set',
        many=True,
    )
    url = serializers.HyperlinkedIdentityField(
        view_name='dealer_direct_order_detail'
    )
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        try:
            domain = Site.objects.get_current().domain
            purchase = obj.purchasehistory_set.order_by('-total_price').first()
            qs = purchase.product.productlistingimage_set.first()
            return domain + qs.image.url
        except Exception:
            return ''


class DealerDirectOrderDetailSerializer(DealerDirectOrderSerializer):
    purchases = PurchaseHistorySerializer(
        source='purchasehistory_set',
        many=True,
    )
    company = serializers.CharField(source='user.userprofile.company_name')
    ffl_number_first_three = serializers.CharField(source='user.userprofile.ffl_number_first_three')
    ffl_number_last_five = serializers.CharField(source='user.userprofile.ffl_number_last_five')
    ffl_url = serializers.CharField(source='user.userprofile.ffl_url')
    # shipping_label_pdf_url = serializers.CharField()


class ListingImageSerializer(ProductLookupSerializer):
    image = serializers.ImageField(use_url=False, required=False)
    image_url = serializers.URLField(
        required=False,
        allow_blank=True,
        default=None
    )

    def validate_image_url(self, value):
        if value:
            try:
                result = urllib.urlretrieve(value)
                base_image_file = open(result[0])
                Image.open(base_image_file)
                self.image_from_url = base_image_file
            except IOError:
                raise serializers.ValidationError(
                    'Image could not be retrieved from url %s' % value
                )
        return value

    def validate(self, data):
        super(ListingImageSerializer, self).validate(data)
        image = data.get('image')
        image_url = data.get('image_url')
        if not image and not image_url:
            raise exceptions.ValidationError(
                "Requires either 'image' or 'image_url' parameter."
            )
        return data

    def create(self, validated_data):
        image = validated_data.get('image')
        image_url = validated_data.get('image_url')
        image_from_url = getattr(self, 'image_from_url', None)
        product = self.product

        if not product.upc.productimage_set.exists():
            if image:
                create_search_result_images(image, product)
            elif image_url:
                product.upc.picture = image_url
                product.upc.save()
                create_search_result_images(image_from_url, product)

        raw_image = image or image_from_url
        new_image = ProductListingImage(
            product=product,
        )

        new_image.image.save(
            '%s.jpg' % product.upc.upc,
            create_image(raw_image, 460, 400)
        )

        new_image.image_thumbnail.save(
            '%s.jpg' % product.upc.upc,
            create_image(raw_image, 63, 63)
        )
        new_image.save()
        product.save()
        raw_image.close()

        return new_image


class ListingSerializer(serializers.Serializer):
    upc = serializers.CharField(
        required=False,
        allow_blank=True,
        default=''
    )
    user_id = serializers.IntegerField()
    tags = ProductTagSerializer(required=False, many=True)
    price = serializers.FloatField()
    bulk_tier_prices = serializers.CharField(allow_blank=True, default='')
    qty = serializers.CharField()
    minimum_order = serializers.IntegerField(required=False, default=0)
    listing_type = serializers.ChoiceField(
        required=False,
        choices=UPC.LISTING_CHOICES,
        default=UPC.FIREARM
    )
    salesforce_id = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    msrp = serializers.CharField(required=False, allow_blank=True, default='')
    map = serializers.CharField(required=False, allow_blank=True, default='')
    nfa_product = serializers.BooleanField(required=False, default=False)
    title = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    category = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    listing_category = serializers.CharField(
       required=False,
       allow_blank=True,
       default='',
    )
    short_description = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    description = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    caliber = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    capacity = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    finish = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    weight = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    barrel_length = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    overall_length = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    manufacturer = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    magnification = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    estimated_ship_days = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )

    # Extra Data for Ammo
    ammo_type = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    rounds = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    length = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    muzzle_velocity = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )

    # Extra Data for Optics
    dot_size = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    battery = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    max_elevation_adjustment = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    max_windage_adjustment = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    eye_relief = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    objective_lens = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    reticle = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    field_of_view = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )

    # Extra Data for Blades
    blade_type = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    blade_length = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    blade_total_length = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    blade_steel = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    blade_handle_material = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    blade_edge = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )

    # Extra Data for Apparel
    shirt_size = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )
    shirt_color = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )

    # YouTube Link
    video_url = serializers.CharField(
        required=False,
        allow_blank=True,
        default='',
    )

    def validate_category(self, value):
        choices = Category.objects.all().values_list('name', flat=True)
        if value != '' and value not in choices:
            raise serializers.ValidationError(
                'Category %s not in valid choices' % value
            )
        return value

    def populate_upc(self, upc, data):
        upc_obj, created = UPC.objects.get_or_create(upc=upc)
        cat_name = data.get('category')
        if cat_name:
            upc_obj.category = Category.objects.get(name=cat_name) or upc_obj.category
        listing_category = data.get('listing_category') or data.get('category')
        upc_obj.listing_category = listing_category or upc_obj.listing_category
        if upc_obj.model is None or upc == data.get('salesforce_id'):
            upc_obj.model = data.get('title') or upc_obj.model
        upc_obj.description = data.get('description') or upc_obj.description
        upc_obj.short_description = data.get('short_description') or upc_obj.short_description
        upc_obj.caliber = data.get('caliber') or upc_obj.caliber
        upc_obj.capacity = data.get('capacity') or upc_obj.capacity
        upc_obj.finish = data.get('finish') or upc_obj.finish
        upc_obj.weight = data.get('weight') or upc_obj.weight
        upc_obj.gun_type = data.get('type') or upc_obj.gun_type
        upc_obj.length_barrel = data.get('barrel_length') or upc_obj.length_barrel
        upc_obj.length_overall = data.get('overall_length') or upc_obj.length_barrel
        upc_obj.manufacturer = data.get('manufacturer') or upc_obj.manufacturer
        upc_obj.listing_type = data.get('listing_type') or upc_obj.listing_type
        upc_obj.msrp = data.get('msrp', upc_obj.msrp)
        upc_obj.map = data.get('map', upc_obj.map)
        upc_obj.estimated_ship_days = data.get(
            'estimated_ship_days', upc_obj.estimated_ship_days
        )

        # Extra Data for Ammo
        upc_obj.rounds = data.get('rounds') or upc_obj.rounds
        upc_obj.muzzle_velocity = data.get('muzzle_velocity') or upc_obj.muzzle_velocity
        upc_obj.length = data.get('length') or upc_obj.length
        upc_obj.ammo_type = data.get('ammo_type') or upc_obj.ammo_type

        # Extra Data for Optics
        upc_obj.magnification = data.get('magnification') or upc_obj.magnification
        upc_obj.field_of_view = data.get('field_of_view') or upc_obj.field_of_view
        upc_obj.reticle = data.get('reticle') or upc_obj.reticle
        upc_obj.objective_lens = data.get('objective_lens') or upc_obj.objective_lens
        upc_obj.eye_relief = data.get('eye_relief') or upc_obj.eye_relief
        upc_obj.max_windage_adjustment = data.get('max_windage_adjustment') or upc_obj.max_windage_adjustment
        upc_obj.max_elevation_adjustment = data.get('max_elevation_adjustment') or upc_obj.max_elevation_adjustment
        upc_obj.battery = data.get('battery') or upc_obj.battery
        upc_obj.dot_size = data.get('dot_size') or upc_obj.dot_size

        # Extra Data for Blades
        upc_obj.blade_type = data.get('blade_type') or upc_obj.blade_type
        upc_obj.blade_length = data.get('blade_length') or upc_obj.blade_length
        upc_obj.blade_total_length = data.get('blade_total_length') or upc_obj.blade_total_length
        upc_obj.blade_steel = data.get('blade_steel') or upc_obj.blade_steel
        upc_obj.blade_handle_material = data.get('blade_handle_material') or upc_obj.blade_handle_material
        upc_obj.blade_edge = data.get('blade_edge') or upc_obj.blade_edge

        # Extra Data for Apparel
        upc_obj.shirt_size = data.get('shirt_size') or upc_obj.shirt_size
        upc_obj.shirt_color = data.get('shirt_color') or upc_obj.shirt_color

        upc_obj.save()
        return upc_obj

    def create(self, validated_data):
        upc = validated_data.get('upc') or validated_data.get('salesforce_id')
        upc = upc or 'DefaultUPC'
        dealer_direct = Distributor.objects.get(name=DEALER_DIRECT)
        upc_obj = self.populate_upc(upc, validated_data)
        try:
            user_profile = UserProfile.objects.filter(user_id=validated_data.get('user_id')).first()
        except Exception, e:
            print "Exception: %s" % e

        product = Product.objects.create(
            upc=upc_obj,
            user_profile=user_profile,
            distributor=dealer_direct,
            price=validated_data.get('price'),
            bulk_tier_prices=validated_data.get('bulk_tier_prices'),
            qty=validated_data.get('qty'),
            minimum_order=validated_data.get('minimum_order'),
            video_url=validated_data.get('video_url'),
            salesforce_id=validated_data.get('salesforce_id'),
            is_active=False,
            nfa_product=validated_data.get('nfa_product'),
        )
        product.title = validated_data.get('title')
        product.save()
        if upc == 'DefaultUPC':
            upc_obj.upc = 'DealerDirect'+str(product.id)
            upc_obj.save()

        tags = validated_data.get('tags', [])
        for tag in tags:
            query = ProductTag.objects.filter(name=tag.get('name'))
            if query.exists():
                product.tags.add(query.first())
            else:
                product.tags.create(
                    name=tag.get('name')
                )

        return product

    def update(self, instance, validated_data):
        upc = instance.upc.upc or instance.salesforce_id
        self.populate_upc(upc, validated_data)

        instance.salesforce_id = validated_data.get('salesforce_id') or instance.salesforce_id
        instance.title = validated_data.get('title', instance.title)
        instance.qty = validated_data.get('qty') or instance.qty
        instance.minimum_order = validated_data.get('minimum_order') or 0
        instance.price = validated_data.get('price') or instance.price
        instance.save()

        tags = validated_data.get('tags', None)
        if tags:
            instance.tags.clear()
            for tag in tags:
                query = ProductTag.objects.filter(name=tag.get('name'))
                if query.exists():
                    instance.tags.add(query.first())
                else:
                    instance.tags.create(
                        name=tag.get('name')
                    )
        return Product.objects.get(id=instance.id)


class AddCreditCardSerializer(serializers.Serializer):
    name_on_card = serializers.CharField()
    card_number = serializers.CharField()
    cvv = serializers.CharField()
    expiration_month = serializers.IntegerField()
    expiration_year = serializers.IntegerField()
    zip_code = serializers.CharField()

    def validate(self, data):
        token = verify_card(
            card_holder_name=data.get('name_on_card'),
            card_number=data.get('card_number'),
            cvv=data.get('cvv'),
            expiration_month=data.get('expiration_month'),
            expiration_year=data.get('expiration_year'),
        )
        if not token:
            raise exceptions.ValidationError('Invalid card data')
        data['token'] = token
        data['masked_card_number'] = data.get('card_number')[-4:]
        return data


class CreditCardWalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ['id', 'cvv', 'token', 'masked_card_number']


class BankAccountWalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ['id', 'account_name', 'routing_number', 'account_number']
        extra_kwargs = {
            'id': {'read_only': True},
            'account_name': {'required': True, 'allow_blank': False},
            'routing_number': {'required': True, 'allow_blank': False},
            'account_number': {'required': True, 'allow_blank': False},
        }

    def create(self, data):
        request = self.context.get('request')
        wallet = Wallet.objects.create(
            user=request.user,
            wallet_type=BANK_ACCOUNT,
            **data
        )
        if request.user.userprofile.company_name:
            create_account(user=request.user)
        return wallet


class BillingAddressSerializer(serializers.Serializer):
    street_address = serializers.CharField()
    city = serializers.CharField()
    state = serializers.ChoiceField(STATE_CHOICES)
    zip = serializers.CharField()

    def get_address(self):
        data = self.validated_data
        address = "%s\n %s, %s - %s" % (
            data['street_address'],
            data['city'],
            data['state'],
            data['zip']
        )
        return address


class CartItemSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    qty = serializers.IntegerField()
    upc = serializers.CharField()
    distributor = serializers.CharField()
    model = serializers.CharField()
    price = serializers.FloatField()

    def validate_id(self, value):
        if not Product.objects.filter(id=value).exists():
            raise serializers.ValidationError(
                'Product with id %s does not exist.' % value
            )
        return value

    def validate_qty(self, value):
        if value <= 0:
            raise serializers.ValidationError(
                'Quantity of item must be at least one (1).'
            )
        return value

    def validate(self, data):
        product = Product.objects.get(id=data.get('id'))
        qty = data.get('qty')
        if qty > int(product.qty):
            raise serializers.ValidationError(
                'Order quantity must not exceed in-stock quantity.'
            )
        return data


class ManufacturerOrderSerializer(serializers.Serializer):
    items = CartItemSerializer(many=True)
    manufacturer = serializers.CharField()


class SaleSerializer(serializers.Serializer):
    orders = ManufacturerOrderSerializer(many=True)
    wallet = serializers.IntegerField()

    def validate_wallet(self, value):
        user = self.context.get('request').user
        try:
            self.wallet = Wallet.objects.get(id=value, user=user)
        except Wallet.DoesNotExist:
            raise exceptions.ValidationError('Wallet does not exist.')
        return value

    def sale(self):
        data = self.validated_data
        request = self.context.get('request')
        orders = data.get('orders')
        wallet = self.wallet
        receipts = []
        for order in orders:
            manufacturer = order.get('manufacturer')
            order_obj = Order.objects.create(
                user=request.user,
                manufacturer=manufacturer,
                payment_type=wallet.wallet_type,
                wallet=wallet
            )
            for item in order.get('items'):
                product = Product.objects.get(id=item['id'])
                price = item.get('price')
                qty = item.get('qty')
                PurchaseHistory.objects.create(
                    user=request.user,
                    order=order_obj,
                    product=product,
                    upc=item.get('upc'),
                    model=item.get('model'),
                    qty=qty,
                    supplier=item.get('distributor'),
                    manufacture=manufacturer,
                    unit_price=price,
                    total_price=price*qty,
                    confirmed=True,
                )
            if wallet.wallet_type == CREDIT_CARD:
                wallet.sale(order=order_obj)
            create_opportunities(order=order_obj)
            receipts.append(order_obj)
        return receipts


class ShippingListSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        orders = []
        for item in validated_data:
            order = Order.objects.get(id=item['order'])
            shipping_fee = ShippingFee.objects.get(id=item['shipping_fee'])
            shipping_fee.selected = True
            shipping_fee.save()
            order.completed = True
            order.save()
            orders.append(order)
            if not settings.DISABLE_SALESFORCE:
                create_opportunities(
                    order=order,
                    opportunity_stage='Ready to Bill'
                )
                create_ach_file(order=order)
        return orders


class ShippingSerializer(serializers.Serializer):
    order = serializers.IntegerField()
    shipping_fee = serializers.IntegerField()

    def validate_order(self, value):
        try:
            order = Order.objects.get(id=value)
            if order.completed:
                raise serializers.ValidationError('Order is completed.')
        except Order.DoesNotExist:
            raise serializers.ValidationError('Order does not exist.')
        return value

    def validate_shipping_fee(self, value):
        profile = self.context.get('request').user.userprofile
        try:
            fee = ShippingFee.objects.get(id=value)
            if not (fee.official or
                    profile.shipping_carrier != fee.shipment_provider):
                raise serializers.ValidationError('Invalid shipping fee.')
        except ShippingFee.DoesNotExist:
            raise serializers.ValidationError('Shipping fee does not exist.')
        return value

    def validate(self, data):
        order = Order.objects.get(id=data['order'])
        shipping_fee = ShippingFee.objects.get(id=data['shipping_fee'])
        if shipping_fee.order.id != order.id:
            raise serializers.ValidationError(
                'Shipping fee does not match order.'
            )
        return data

    class Meta:
        list_serializer_class = ShippingListSerializer
