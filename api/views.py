import datetime
import requests
from datetime import timedelta

from pyforce import pyforce

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core import serializers
from django.db.models import Sum, Count
from django.utils import timezone

from haystack.query import EmptySearchQuerySet

from rest_framework import (
    authentication,
    permissions,
    generics,
    mixins,
    status
)
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import UserProfile, Distributor, Wallet, FFL
from accounts.utils import send_intro_email
from products.models import Product, ProductTag, Category
from products.utils import verify_ffl
from products.search_indexes import ProductIndex
from saasapp.models import Order, PurchaseHistory, EmailTemplate, ShippingFee
from saasapp.constants import CREDIT_CARD, BANK_ACCOUNT
from saasapp.forms import PaginatedSearchForm
from saasapp.salesforce import (
    create_lead,
    create_account,
    create_contact,
    create_order,
    create_purchase_history,
    delete_product,
    activate_product,
)
from saasapp.mixins import EmailPurchaseMixin
from saasapp.utils import get_search_criteria_results
from .constants import GSA_DIRECT, DEALER_DIRECT
from .utils import delete_images


from .permissions import DealerDirectPermission
from .serializers import (
    AccountActivationSerializer,
    AddSupplierSerializer,
    AdminOrderDetailSerializer,
    AdminOrderSerializer,
    AuthTokenSerializer,
    RegistrationInfoSerializer,
    RegistrationSerializer,
    FFLRegistrationSerializer,
    UserInfoSerializer,
    UserUpdateSerializer,
    ChangePasswordSerializer,
    MassAddSupplierSerializer,
    DistributorSerializer,
    PurchaseHistorySerializer,
    PaginatedPurchaseHistorySerializer,
    PurchaseOrderSerializer,
    VerifyFFLSerializer,
    ProductSerializer,
    ProductLookupSerializer,
    ProductImageSerializer,
    ProductTagSerializer,
    ProductSearchResultSerializer,
    CategorySerializer,
    DashboardSerializer,
    ManufacturerSerializer,
    OrderSerializer,
    OrderDetailSerializer,
    DealerDirectOrderSerializer,
    DealerDirectOrderDetailSerializer,
    PaginatedOrderSerializer,
    ListingSerializer,
    ListingImageSerializer,
    AddCreditCardSerializer,
    CreditCardWalletSerializer,
    BankAccountWalletSerializer,
    BillingAddressSerializer,
    SaleSerializer,
    ShippingSerializer,
)
from .constants import (
    ONE_DAY,
    SEVEN_DAYS,
    THIRTY_DAYS,
    YTD,
)


class LoginView(ObtainAuthToken):
    """
    Uses the app's AuthTokenSerializer instead of rest_framework's
    serializer to be able to use email instead of username in
    getting the token.
    """
    def post(self, request):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response(
            {
                'token': token.key,
                'first_name': user.first_name,
                'last_name': user.last_name,
            },
            status=status.HTTP_200_OK
        )


class ProductInfoView(APIView):
    """
    Returns product info for user. Used for vArmory store page
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        products = Product.objects.filter(user_profile_id=request.user.id)
        result = {
            p.salesforce_id: {
                'qty_sold': p.qty_sold,
                'is_active': p.is_active,
            } for p in products
        }
        response = Response(
            {'products': result},
            status=status.HTTP_200_OK
        )
        return response


class UserInfoView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    """
    Returns user info from the current user.
    Returns Retail Margin (margin), Suppliers together with Representative Emails.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = UserInfoSerializer

    def get_object(self):
        return self.request.user.userprofile

    def post(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class UserUpdateView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_object(self):
        return self.request.user.userprofile

    def put(self, request, *args, **kwargs):
        serializer = UserUpdateSerializer(
            self.get_object(), data=request.data, partial=True
        )
        if serializer.is_valid():
            profile = serializer.save()
            info_serializer = UserInfoSerializer(profile)
            profile_data = info_serializer.data
            profile_data['supplier_emails'] = profile_data['suppliers']
            profile_data['company'] = profile.company_name
            if request.data.get('phone_number'):
                profile_data.update({
                    'phone_number': request.data.get('phone_number')
                })
            del profile_data['suppliers']
            return Response(profile_data, status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ChangePasswordView(generics.UpdateAPIView):
    """
    Update current user's password.
    """
    model = User
    serializer_class = ChangePasswordSerializer
    queryset = User.objects.all()
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_object(self):
        return self.request.user

    def patch(self, request, *args, **kwargs):
        serializer = ChangePasswordSerializer(user=request.user, data=request.data)
        if serializer.is_valid():
            user = request.user
            user.set_password(serializer.data.get('new_password'))
            user.save()

            return Response(
                "Successful change password.",
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class AddSupplierView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def post(self, request, *args, **kwargs):
        serializer = AddSupplierSerializer(user=request.user, data=request.data)
        if serializer.is_valid():
            return Response(
                "Successfully added Supplier to user.",
                status=status.HTTP_200_OK
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class MassAddSupplierView(APIView):
    """
    Accepts JSON as input.
    Input will be an array of distributor ids.
    [
        {
            "supplier_id": 1
        },
        {
            "supplier_id": 2
        },
        {
            "supplier_id": 3
        },
        {
            "supplier_id": 4
        }
    ]

    Add suppliers to user.
    Does not yet verify suppliers.
    Verification is done individually.  (Use Add Supplier.)
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def post(self, request, *args, **kwargs):
        serializer = MassAddSupplierSerializer(data=request.data, many=True)

        if serializer.is_valid():
            user_profile = request.user.userprofile

            for data in serializer.data:
                supplier_id = data.get('supplier_id')
                supplier = Distributor.objects.get(pk=supplier_id)
                if supplier.name == "Davidson's":
                    user_profile.davidson = True
                elif supplier.name == "Lipseys":
                    user_profile.lipseys = True
                elif supplier.name == "Green Supply":
                    user_profile.green = True
                elif supplier.name == "RSR Group":
                    user_profile.rsr = True
                elif supplier.name == "Gzanders":
                    user_profile.zanders = True
                elif supplier.name == "Camfour":
                    user_profile.camfour = True
                elif supplier.name == "Sport's South":
                    user_profile.sports = True
                elif supplier.name == "Jerry's Sports Center":
                    user_profile.jerrys = True
                elif supplier.name == "Ellet":
                    user_profile.ellet = True

            user_profile.save()

            # Create Salesforce Lead
            create_account(
                user=request.user
            )

            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class FetchSuppliersView(generics.ListAPIView):
    """
    Returns list of Suppliers (Distributor)
    """
    serializer_class = DistributorSerializer
    queryset = Distributor.objects.all()


class FetchOrderHistoryView(mixins.ListModelMixin, generics.GenericAPIView):
    """
    Returns paginated list of Order History
    """
    queryset = PurchaseHistory.objects.all()
    serializer_class = PurchaseHistorySerializer
    pagination_serializer_class = PaginatedPurchaseHistorySerializer
    paginate_by = 20
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self, *args, **kwargs):
        return PurchaseHistory.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PurchaseOrderView(APIView, EmailPurchaseMixin):
    """
    Accepts JSON as input.
    Input will be an array of an order of a specific item.

    Example:
    [
        {
            "product": 286128,
            "quantity": 2
        },
        {
            "product": 286129,
            "quantity": 3
        }
    ]

    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = PurchaseOrderSerializer(data=request.data, many=True)

        if serializer.is_valid():
            self.clear_list()

            svc = pyforce.Client()
            svc.login(
                settings.SALESFORCE_LOGIN_EMAIL,
                "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
            )

            order = Order(user=request.user)
            order.save()
            order_id = create_order(
                order=order,
                svc=svc
            )
            self.order = order

            for data in serializer.data:
                product_id = data.get('product')
                product = Product.objects.get(pk=product_id)
                unit_price = product.price
                total_price = unit_price * data.get('quantity')

                purchase_record = PurchaseHistory(
                    user=request.user,
                    order=order,
                    product=product,
                    upc=product.upc.upc,
                    model=product.model,
                    qty=data.get('quantity'),
                    manufacture=product.manufacturer,
                    supplier=product.distributor.name,
                    unit_price=unit_price,
                    total_price=total_price
                )
                purchase_record.save()
                # Query again to resolve unicode error
                purchase_record = PurchaseHistory.objects.get(pk=purchase_record.pk)
                create_purchase_history(
                    purchase_history=purchase_record,
                    svc=svc,
                    order_id=order_id
                )
                self.append_order(purchase_record)

            email_template = EmailTemplate.objects.first()
            self.send_email(email_template)

            return Response(
                "Successfully sent email to suppliers.",
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class VerifyFFLView(APIView):
    """
    Returns True if given FFL is valid.
    False otherwise.
    """
    def get(self, request):
        serializer = VerifyFFLSerializer(data=request.GET)
        if serializer.is_valid():
            ffl_valid = verify_ffl(
                serializer.data['first'],
                serializer.data['second'],
                serializer.data['last'],
            )
            return Response(
                ffl_valid,
                status=status.HTTP_200_OK
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class SubCategoriesView(generics.ListAPIView):
    """
    Returns given category's children.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = CategorySerializer

    def get_queryset(self, *args, **kwargs):
        request = self.request
        if request.GET.get('category'):
            cat = Category.objects.get(id=request.GET.get('category'))
            return cat.children.all()
        else:
            return Category.objects.filter(level=0)


class SearchProducts(generics.ListAPIView):
    """
    Returns list of products base on search query.
    """
    serializer_class = ProductSearchResultSerializer
    paginate_by = 20

    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self, *args, **kwargs):
        request = self.request
        results = EmptySearchQuerySet()

        form = PaginatedSearchForm(request.GET)
        if form.is_valid():
            results = form.search(user_profile=self.request.user.userprofile)
        return results


class ProductDetailView(generics.RetrieveAPIView):
    """
    Return product info from id.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductImageView(generics.RetrieveAPIView):
    """
    Return image url info.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    queryset = Product.objects.all()
    serializer_class = ProductImageSerializer


class FeaturedProductView(generics.ListAPIView):
    """
    Returns list of featured products
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = ProductSerializer

    def get_queryset(self, *args, **kwargs):
        manufacturer = self.request.GET.get('manufacturer')
        queryset = Product.objects.filter(
            distributor__name__in=(GSA_DIRECT, DEALER_DIRECT)
        )
        if manufacturer:
            queryset = queryset.filter(upc__manufacturer=manufacturer)
        return queryset


class CriteriaResultView(APIView):
    """
    Returns list of criteria from all products.
    """
    criteria = None

    def get(self, request):
        results = get_search_criteria_results(self.criteria)
        return Response(
            results
        )


class FetchCategoryView(CriteriaResultView):
    """
    Returns list of categories.
    Category of the products refers to gun_type in the db.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    criteria = 'gun_type'


class FetchManufacturerView(CriteriaResultView):
    """
    Returns list of manufacturers.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    criteria = 'manufacturer'


class FetchCaliberView(CriteriaResultView):
    """
    Returns list of calibers.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    criteria = 'caliber'


class RegistrationView(APIView):
    """
    First step of registration process.
    Accepts POST request with the following parameters.
    - first_name
    - last_name
    - email
    - password

    Returns authorization token to be used for ffl verification
    step in registration.
    Cannot use returned token yet for login since user is still
    inactive.

    Endpoint: /api/registration/register/
    """
    def post(self, request, format=None):
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            reg_info = serializer.save_user()
            reg_info.save()

            token, created = Token.objects.get_or_create(user=reg_info.user)

            # Create Salesforce Account
            # Update salesforce info
            user = User.objects.get(pk=reg_info.user.pk)
            if user.userprofile.company_name:
                create_account(user=user)

            reg_info_serializer = RegistrationInfoSerializer(instance=reg_info)
            return Response(
                {'token': token.key},
                status=status.HTTP_200_OK
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class FFLRegistrationView(APIView):
    """
    Second step of registration process.
    Accepts POST request with the following parameters.
    - token (Token received from first step of registration.)
    - first (First three digits)
    - last (Last five digits)
    - zip

    Returns a newly generated token to be used for authentication.

    Endpoint: /api/registration/ffl/
    """
    def post(self, request, format=None):
        serializer = FFLRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            (verify, reg_info) = serializer.verify_ffl()
            if verify:
                token, created = Token.objects.get_or_create(user=reg_info.user)
                # Only create a new token if user is not active
                if not reg_info.user.is_active:
                    # Can't replace token.key since it is a primary key.
                    # If we try to replace it, a new Token object will be created.
                    token.delete()
                    token, created = Token.objects.get_or_create(user=reg_info.user)
                data = {
                    'token': token.key,
                }
                # Send intro email
                send_intro_email(to_email=[reg_info.user.email])

                # Create Salesforce Account
                # Update salesforce info
                user = User.objects.get(pk=reg_info.user.pk)
                if user.userprofile.company_name:
                    create_account(user=user)
                create_contact(user=user)
                return Response(
                    data,
                    status=status.HTTP_200_OK
                )
            else:
                data = {
                    'ffl': [
                        'Not valid FFL number.'
                    ]
                }
                return Response(
                    data,
                    status=status.HTTP_400_BAD_REQUEST
                )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class TotalOrdersView(APIView):
    """
    Returns number of orders issued by currently logged-in user.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = DashboardSerializer(data=request.data)
        if serializer.is_valid():
            # period = serializer.data.get('period')
            # mfr = serializer.data.get('manufacturer')
            # items = PurchaseHistory.objects.filter(
                # manufacture=mfr, supplier=DEALER_DIRECT
            # )

            # today = timezone.now()
            # today.replace(hour=0, minute=0, second=0, microsecond=0)
            # if period == YTD:
                # year = today.date().year
                # count = items.filter(date__year=year).count()
            # else:
                # past = today-timedelta(days=period)
                # count = items.filter(date__gte=past).count()
            try:
                mfr = serializer.data.get('manufacturer')
                count = Order.objects.filter(manufacturer=mfr).count()
            except Exception:
                count = 0
            return Response(
                {
                    'count': count
                },
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class TotalProductsView(APIView):
    """
    Returns total quantity of products listed by manufacturer,
    as well as number of active/inactive products.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        data = {
            'manufacturer': request.data.get('manufacturer'),
            'period': YTD
        }
        serializer = DashboardSerializer(data=data)
        if serializer.is_valid():
            mfr = serializer.data.get('manufacturer')
            products = Product.objects.filter(
                upc__manufacturer=mfr, distributor__name=DEALER_DIRECT
            )
            return Response(
                {
                    'count': products.aggregate(count=Sum('qty'))['count'],
                    'active_products': products.filter(is_active=True).count(),
                    'total_products': products.count(),
                },
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class TotalSalesView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = DashboardSerializer(data=request.data)
        if serializer.is_valid():
            period = serializer.data.get('period')
            mfr = serializer.data.get('manufacturer')
            items = PurchaseHistory.objects.filter(
                manufacture=mfr, supplier=DEALER_DIRECT
            )

            data = []
            total = 0
            today = timezone.now()
            start = timezone.now()
            start = start.replace(hour=0, minute=0, second=0, microsecond=0)

            if period == ONE_DAY:
                delta = timedelta(hours=1)
                total = items.filter(
                    date__gte=start
                ).aggregate(t=Sum('total_price'))['t'] or 0
                while start <= today:
                    data.append({
                        'x': '%s-%s' % (
                            start.strftime('%H:%M'),
                            (start+delta).strftime('%H:%M')
                        ),
                        'y': items.filter(
                            date__range=(start, start+delta)
                            ).aggregate(y=Sum('total_price'))['y'] or 0
                    })
                    start += delta
            elif period == SEVEN_DAYS:
                start -= timedelta(days=6)
                delta = timedelta(days=1)
                total = items.filter(
                    date__gte=start
                ).aggregate(t=Sum('total_price'))['t'] or 0
                while start <= today:
                    data.append({
                        'x': start.strftime('%m/%d'),
                        'y': items.filter(
                            date__range=(start, start+delta)
                        ).aggregate(y=Sum('total_price'))['y'] or 0
                    })
                    start += delta
            elif period == THIRTY_DAYS:
                start -= timedelta(days=29)
                delta = timedelta(days=1)
                total = items.filter(
                    date__gte=start
                ).aggregate(t=Sum('total_price'))['t'] or 0
                while start <= today:
                    data.append({
                        'x': start.strftime('%m/%d'),
                        'y': items.filter(
                            date__range=(start, start+delta)
                        ).aggregate(y=Sum('total_price'))['y'] or 0
                    })
                    start += delta
            elif period == YTD:
                start = start.replace(month=1, day=1)
                month = 1
                total = items.filter(
                    date__gte=start
                ).aggregate(t=Sum('total_price'))['t'] or 0
                while month <= today.month:
                    right_interval = start.replace(month=min(12, month+1))
                    if month == 12:
                        right_interval = today
                    data.append({
                        'x': start.strftime('%B %Y'),
                        'y': items.filter(
                            date__range=(start, right_interval)
                        ).aggregate(y=Sum('total_price'))['y'] or 0
                    })
                    month += 1
                    start = start.replace(month=min(12, month))
            return Response(
                {'total': total, 'data': data},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class TopProductsView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = DashboardSerializer(data=request.data)
        if serializer.is_valid():
            period = serializer.data.get('period')
            mfr = serializer.data.get('manufacturer')
            items = PurchaseHistory.objects.filter(
                manufacture=mfr, supplier=DEALER_DIRECT
            )

            today = timezone.now()
            today.replace(hour=0, minute=0, second=0, microsecond=0)
            if period == YTD:
                year = today.date().year
                items = items.filter(
                    date__year=year
                )
            else:
                past = today-timedelta(days=period)
                items = items.filter(
                    date__gte=past
                )
            items = items.values('model', 'product').annotate(
                total=Sum('total_price')
                ).order_by('-total')[:5]

            for i in items:
                prod = Product.objects.get(id=i['product'])
                if prod.productlistingimage_set.count() > 0:
                    domain = Site.objects.get_current().domain
                    url = prod.productlistingimage_set.first().image.url
                    i['image'] = domain + url
                else:
                    i['image'] = ''

            return Response(
                {'data': items},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class FetchOrdersView(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = OrderSerializer
    pagination_serializer_class = PaginatedOrderSerializer
    paginate_by = 20

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user, completed=True)

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class OrderDetailView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = OrderDetailSerializer

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user, completed=True)

    def post(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class AdminOrderListView(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

    serializer_class = AdminOrderSerializer
    queryset = Order.objects.filter(completed=True)
    pagination_serializer_class = PaginatedOrderSerializer
    paginate_by = 20

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AdminOrderDetailView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

    serializer_class = AdminOrderDetailSerializer
    queryset = Order.objects.filter(completed=True)

    def post(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class DealerDirectOrderListView(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, DealerDirectPermission,)

    serializer_class = DealerDirectOrderSerializer
    pagination_serializer_class = PaginatedOrderSerializer
    paginate_by = 20

    def get_queryset(self):
        manufacturer = self.request.data.get('manufacturer')
        ids = PurchaseHistory.objects.filter(
            supplier=DEALER_DIRECT,
            manufacture=manufacturer,
        ).values('order').distinct()
        return Order.objects.filter(id__in=ids, completed=True)

    def post(self, request, *args, **kwargs):
        serializer = ManufacturerSerializer(data=request.data)
        if serializer.is_valid():
            return self.list(request, *args, **kwargs)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )


class DealerDirectOrderDetailView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, DealerDirectPermission,)

    serializer_class = DealerDirectOrderDetailSerializer

    def get_queryset(self):
        manufacturer = self.request.data.get('manufacturer')
        ids = PurchaseHistory.objects.filter(
            supplier=DEALER_DIRECT,
            manufacture=manufacturer,
        ).values('order').distinct()
        orders = Order.objects.filter(id__in=ids, completed=True)
        return orders

    def post(self, request, *args, **kwargs):
        serializer = ManufacturerSerializer(data=request.data)
        if serializer.is_valid():
            return self.retrieve(request, *args, **kwargs)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )


class UpdateOrderView(mixins.UpdateModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    queryset = Order.objects.filter(salesforce_id__isnull=False)
    serializer_class = OrderSerializer
    lookup_field = 'salesforce_id'

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class AddListingView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            data.update({'user_id': request.user.id})
            serializer = ListingSerializer(data=data)
            if serializer.is_valid():
                product_serializer = ProductSerializer(serializer.save())
                return Response(
                    product_serializer.data,
                    status=status.HTTP_201_CREATED
                )
            else:
                return Response(
                    serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
                )
        except Exception, e:
            print "Exception: {}".format(e)


class UpdateListingView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def patch(self, request, *args, **kwargs):
        try:
            queryset = Product.objects.filter(distributor__name=DEALER_DIRECT)
            product = queryset.get(salesforce_id=kwargs['sid'])
        except Product.DoesNotExist:
            return Response(
                {'detail': 'Not found'},
                status=status.HTTP_404_NOT_FOUND
            )
        serializer = ListingSerializer(product, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(product, request.data)
            product_serializer = ProductSerializer(serializer.save())
            product_index = ProductIndex()
            product_index.update_object(product)

            return Response(
                product_serializer.data,
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def post(self, request, *args, **kwargs):
        try:
            queryset = Product.objects.filter(distributor__name=DEALER_DIRECT)
            product = queryset.get(salesforce_id=kwargs['sid'])
        except Product.DoesNotExist:
            return Response(
                    {'detail': 'Not found'},
                    status=status.HTTP_404_NOT_FOUND
            )
        serializer = ListingSerializer(product, data=request.data, partial=True)
        if serializer.is_valid():
            product_serializer = ProductSerializer(serializer.save())
            return Response(
                    product_serializer.data,
                    status=status.HTTP_200_OK
            )
        else:
            return Response(
                    serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
            )


class ListingActivationView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(salesforce_id=kwargs['sid'])
            response = activate_product(product)
        except Product.DoesNotExist:
            return Response(
                {'detail': 'Not found'},
                status=status.HTTP_404_NOT_FOUND
            )
        product.is_active = request.POST.get('is_active')
        return Response({'response': response}, status=status.HTTP_200_OK)


class DeleteListingView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(salesforce_id=kwargs['sid'])
        except Product.DoesNotExist:
            return Response(
                {'detail': 'Not found'},
                status=status.HTTP_404_NOT_FOUND
            )
        delete_product(product=product)
        delete_images(product)
        product.delete()
        return Response(
            status=status.HTTP_204_NO_CONTENT
        )


class ActivateProductView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

    def post(self, request, *args, **kwargs):
        serializer = ProductLookupSerializer(data=request.data)
        if serializer.is_valid():
            serializer.product.is_active = True
            serializer.product.save()
            prod = ProductSerializer(serializer.product)
            return Response(prod.data, status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ActivateVarmoryAccountView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

    def post(self, request, *args, **kwargs):
        serializer = AccountActivationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.profile.varmory_account_approved = True
            serializer.profile.save()
            data = {'account_approved': serializer.profile.varmory_account_approved}
            return Response(data,status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ListingTagListView(generics.ListAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    queryset = ProductTag.objects.all()
    serializer_class = ProductTagSerializer


class AddListingImageView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = ListingImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = serializer.validated_data
            if data.get('product_id'):
                response = {'product_id': serializer.product.id}
            else:
                response = {'salesforce_id': data.get('salesforce_id')}
            if data.get('image'):
                response.update({'image': data.get('image').name})
            else:
                response.update({'image_url': data.get('image_url')})
            return Response(
                response,
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ClearListingImagesView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        serializer = ProductLookupSerializer(data=request.data)
        if serializer.is_valid():
            images = serializer.product.productlistingimage_set.all()
            for image in images:
                image.image.delete(False)
                image.image_thumbnail.delete(False)
                image.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class AddCreditCardView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = AddCreditCardSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data
            Wallet.objects.create(
                user=request.user,
                wallet_type=CREDIT_CARD,
                cvv=data.get('cvv'),
                expiration_month=data.get('expiration_month'),
                expiration_year=data.get('expiration_year'),
                token=data.get('token'),
                masked_card_number=data.get('masked_card_number')
            )
            return Response(
                data,
                status=status.HTTP_201_CREATED,
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class AddBankDetailsView(generics.CreateAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = BankAccountWalletSerializer

    def get_queryset(self):
        return Wallet.objects.filter(
            user=self.request.user,
            wallet_type=BANK_ACCOUNT,
        )


class UpdateBankDetailsView(mixins.UpdateModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = BankAccountWalletSerializer

    def get_object(self):
        return self.request.user.wallet_set.filter(
               wallet_type=BANK_ACCOUNT).first()

    def post(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class CreditCardListView(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = CreditCardWalletSerializer

    def get_queryset(self):
        return Wallet.objects.filter(
            user=self.request.user,
            wallet_type=CREDIT_CARD,
        )

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BankAccountListView(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = BankAccountWalletSerializer

    def get_queryset(self):
        return Wallet.objects.filter(
            user=self.request.user,
            wallet_type=BANK_ACCOUNT,
        )

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ShippingView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        profile = request.user.userprofile
        return Response(
            {
                'shipping_address': profile.ffl_mailing_address,
                'billing_address': profile.ffl_premise_address
            },
            status=status.HTTP_200_OK
        )

    def patch(self, request, *args, **kwargs):
        serializer = BillingAddressSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        address = serializer.get_address()
        profile = request.user.userprofile
        profile.ffl_premise_address = address
        profile.save()
        return Response(
            {
                'shipping_address': profile.ffl_mailing_address,
                'billing_address': address,
            },
            status=status.HTTP_200_OK,
        )


class SaleView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        context = {'request': request}
        serializer = SaleSerializer(data=request.data, context=context)
        if serializer.is_valid():
            receipts = serializer.sale()
            order_serializer = OrderSerializer(
                data=receipts,
                many=True,
                context=context,
            )
            return Response(
                order_serializer.data,
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ReviewView(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = OrderSerializer

    def get_queryset(self):
        completed = ShippingFee.objects.filter(
            selected=True
        ).values_list('order', flat=True)
        return Order.objects.filter(
            user=self.request.user,
            shippingfee__isnull=False,
        ).exclude(id__in=completed).distinct()

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CompleteOrderView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        context = {'request': request}
        serializer = ShippingSerializer(
            data=request.data, context=context, many=True
        )
        if serializer.is_valid():
            orders = serializer.save()
            response = OrderSerializer(orders, context=context, many=True)
            return Response(response.data, status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class FeaturedManufacturersView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        products = Product.objects.filter(
            distributor__name__in=(GSA_DIRECT, DEALER_DIRECT)
        )
        data = products.values_list('upc__manufacturer', flat=True).distinct()
        return Response(
            data,
            status=status.HTTP_200_OK
        )
