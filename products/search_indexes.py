from haystack import indexes

from django.conf import settings

from accounts.models import Distributor

from .models import Product, Category, ProductImage


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    user_profile_id = indexes.IntegerField()
    product_id = indexes.IntegerField()
    upc_value = indexes.CharField()
    distributor = indexes.IntegerField()
    distributor_name = indexes.CharField()
    category = indexes.IntegerField()
    category_name = indexes.CharField()
    item_number = indexes.CharField(model_attr='item_number', null=True)
    manufacturer = indexes.CharField()
    model_value = indexes.CharField() #Search index has built-in model attribute
    caliber = indexes.CharField()
    capacity = indexes.CharField()
    finish = indexes.CharField()
    gun_type = indexes.CharField()
    action = indexes.CharField()
    sights = indexes.CharField()
    weight = indexes.CharField()
    length_barrel = indexes.CharField()
    length_overall = indexes.CharField()
    description = indexes.CharField()
    short_description = indexes.CharField()
    price = indexes.FloatField(model_attr='price')
    qty = indexes.CharField(model_attr='qty')
    stock = indexes.CharField()
    picture = indexes.CharField()
    image_url = indexes.CharField(model_attr='image_url')
    is_full_path_image_url = indexes.BooleanField()
    thumb_grid_image_url = indexes.CharField()
    thumb_list_image_url = indexes.CharField()
    detail_popup_image_url = indexes.CharField()
    is_available = indexes.BooleanField()
    is_featured = indexes.BooleanField()
    is_dealer_direct = indexes.BooleanField()
    is_dealer_exchange = indexes.BooleanField()
    is_active = indexes.BooleanField()

    def get_model(self):
        return Product

    def prepare_gun_type(self, obj):
        if obj.upc and obj.upc.gun_type:
            return obj.upc.gun_type
        return None

    def prepare_action(self, obj):
        if obj.upc and obj.upc.action:
            return obj.upc.action
        return None
    
    def prepare_sights(self, obj):
        if obj.upc and obj.upc.sights:
            return obj.upc.sights
        return None

    def prepare_user_profile_id(self, obj):
        return obj.user_profile_id

    def prepare_distributor(self, obj):
        if obj.distributor:
            return obj.distributor.pk
        return None

    def prepare_distributor_name(self, obj):
        if obj.distributor:
            return obj.distributor.name
        return None

    def prepare_upc_value(self, obj):
        if obj.upc:
            return obj.upc.upc
        return None

    def prepare_category(self, obj):
        if obj.upc and obj.upc.category:
            return obj.upc.category.pk
        return None

    def prepare_manufacturer(self, obj):
        if obj.upc:
            return obj.upc.manufacturer
        return None

    def prepare_model_value(self, obj):
        if obj.upc:
            return obj.upc.model
        return None

    def prepare_caliber(self, obj):
        if obj.upc:
            return obj.upc.caliber
        return None

    def prepare_capacity(self, obj):
        if obj.upc:
            return obj.upc.capacity
        return None

    def prepare_finish(self, obj):
        if obj.upc:
            return obj.upc.finish
        return None

    def prepare_weight(self, obj):
        if obj.upc:
            return obj.upc.weight
        return None

    def prepare_length_barrel(self, obj):
        if obj.upc:
            return obj.upc.length_barrel
        return None

    def prepare_length_overall(self, obj):
        if obj.upc:
            return obj.upc.length_overall
        return None

    def prepare_description(self, obj):
        if obj.upc:
            return obj.upc.description
        return None

    def prepare_short_description(self, obj):
        if obj.upc:
            return obj.upc.short_description
        return None

    def prepare_stock(self, obj):
        if obj.upc:
            return obj.upc.stock
        return None

    def prepare_picture(self, obj):
        if obj.upc:
            davidson = Distributor.objects.get(name="Davidson's")
            if obj.distributor == davidson:
                return obj.upc.davidson_picture
            return obj.upc.picture
        return None

    def prepare_product_id(self, obj):
        return obj.pk

    def prepare_category_name(self, obj):
        if obj.upc and obj.upc.category_name:
            return obj.upc.category_name
        return None

    def prepare_is_available(self, obj):
        return obj.is_product_available()

    def prepare_is_featured(self, obj):
        return obj.distributor.name == 'GSA Direct'

    def prepare_is_dealer_direct(self, obj):
        return obj.distributor.name == 'Dealer Direct'

    def prepare_is_dealer_exchange(self, obj):
        return obj.distributor.name == 'Dealer Exchange'

    def prepare_is_active(self, obj):
        return obj.is_active

    def prepare_is_full_path_image_url(self, obj):
        davidson = Distributor.objects.get(name="Davidson's")
        if obj.distributor == davidson:
            return obj.upc and obj.upc.davidson_picture and obj.upc.davidson_picture.startswith('http')
        return obj.upc and obj.upc.picture and obj.upc.picture.startswith('http')

    def prepare_thumb_grid_image_url(self, obj):
        return obj.get_thumb_grid_image_url()

    def prepare_thumb_list_image_url(self, obj):
        return obj.get_thumb_list_image_url()

    def prepare_detail_popup_image_url(self, obj):
        return obj.get_detail_popup_image_url()
