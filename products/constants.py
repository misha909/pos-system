Ammo_Rifle = 'Ammo-Rifle'
Ammo_Handgun = 'Ammo-Handgun'
Ammo_Shotgun = 'Ammo-Shotgun'
Ammo_Rimfire = 'Ammo-Rimfire' 
Ammo_Promo = 'Ammo-Promo'
Ammo_Slugs = 'Ammo-Slugs'
Ammo_Blanks = 'Ammo-Blanks'
Ammo_Dummy = 'Ammo_Dummy'
Ammo_Black_Powder = 'Ammo-Black Powder'
Apparel_Gloves = 'Apparel_Gloves'
Apparel_Outerwear = 'Apparel_Outerwear'
Apparel_Pants = 'Apparel_Pants'
Apparel_Shirts = 'Apparel_Shirts'
Apparel_Shoes = 'Apparel_Shoes'
Apparel_Other = 'Apparel_Other'
Handguns_Revolvers = 'Handguns-Revolvers'
Handguns_Semi_Automatic = 'Handguns-Semi Automatic'
Handguns_Lever_Action = 'Handguns-Lever Action'
Handguns_Derringer = 'Handguns-Derringer'
Handguns_Single_Shot = 'Handguns-Single Shot'
Handguns_Double_Action = 'Handguns-Double Action'
Handguns_Black_Powder = 'Handguns-Black Powder'
Handguns_AR_Pistol = 'Handguns-AR Pistol'
Handguns_Other = 'Handguns-Other'
Knives_Fixed_Blade = 'Knives-Fixed Blade'
Knives_Folding = 'Knives-Folding'
Knives_Knife_Accessories = 'Knives-Knife Accessories'
Knives_Utility = 'Knives-Utility'
Magazines_High_Capacity = 'Magazines-High Capacity'
Magazines_Standard = 'Magazines-Standard'
Optics_Binoculars = 'Optics-Binoculars'
Optics_Flashlights_Batteries = 'Optics-Flashlights/Batteries'
Optics_Night_Vision = 'Optics-Night Vision'
Optics_Optical_Accessories = 'Optics-Optical Accessories'
Optics_Scope_Mounts = 'Optics-Scope Mounts'
Optics_Scopes = 'Optics-Scopes'
Optics_Sights_Lasers_Lights = 'Optics-Sights/Lasers/Lights'
Optics_Spotting_Scopes = 'Optics-Spotting Scopes'
Optics_Thermal_Optics = 'Optics-Thermal Optics'
Parts_Gear_Accessories = 'Parts&Gear-Accessories'
Parts_Gear_Airguns = 'Parts&Gear-Airguns'
Parts_Gear_Airguns_Choke_Tubes = 'Parts&Gear-Airguns / Choke Tubes'
Parts_Gear_Blinds_Accessories = 'Parts&Gear-Blinds and Accessories'
Parts_Gear_Bolt_Carrier_Group = 'Parts&Gear-Bolt Carrier Group'
Parts_Gear_Books_Media = 'Parts&Gear-Books / Media'
Parts_Gear_Bore_Sighter_Arbors = 'Parts&Gear-Bore Sighters  Arbors'
Parts_Gear_Camping = 'Parts&Gear-Camping'
Parts_Gear_Cleaning_Equipment = 'Parts&Gear-Cleaning Equipment'
Parts_Gear_Clothing = 'Parts&Gear-Clothing'
Parts_Gear_Conversion_Kits = 'Parts&Gear-Conversion Kits'
Parts_Gear_Coolers = 'Parts&Gear-Coolers'
Parts_Gear_Decoys = 'Parts&Gear-Decoys'
Parts_Gear_Electronics = 'Parts&Gear-Electronics'
Parts_Gear_Feeders = 'Parts&Gear-Feeders'
Parts_Gear_Frames = 'Parts&Gear-Frames'
Parts_Gear_Game_Calls = 'Parts&Gear-Game Calls'
Parts_Gear_Grips_Pads_Stocks = 'Parts&Gear-Grips Pads / Stocks'
Parts_Gear_Gun_Rests = 'Parts&Gear-Gun Rests'
Parts_Gear_Hard_Gun_Cases = 'Parts&Gear-Hard Gun Cases'
Parts_Gear_Holsters = 'Parts&Gear-Holsters'
Parts_Gear_Hunting_Scents = 'Parts&Gear-Hunting Scents'
Parts_Gear_Non_Lethal_Defense = 'Parts&Gear-Non-Lethal Defense'
Parts_Gear_Parts = 'Parts&Gear-Parts'
Parts_Gear_Pistol_Cases = 'Parts&Gear-Pistol Cases'
Parts_Gear_Racks = 'Parts&Gear-Racks'
Parts_Gear_Reloading_Equipment = 'Parts&Gear-Reloading Equipment'
Parts_Gear_Safes_Security = 'Parts&Gear-Safes / Security'
Parts_Gear_Safety_Protection = 'Parts&Gear-Safety / Protection'
Parts_Gear_Slings_Swivels = 'Parts&Gear-Slings / Swivels'
Parts_Gear_Soft_Gun_Cases = 'Parts&Gear-Soft Gun Cases'
Parts_Gear_Survival_Supplies = 'Parts&Gear-Survival Supplies'
Parts_Gear_Targets = 'Parts&Gear-Targets'
Parts_Gear_Tools = 'Parts&Gear-Tools'
Parts_Gear_Upper_Receivers = 'Parts&Gear-Upper Receivers'
Parts_Gear_Utility_Boxes = 'Parts&Gear-Utility Boxes'
Rifles_Semi_Automatic = 'Rifles-Semi-Automatic'
Rifles_Bolt_Action = 'Rifles-Bolt Action'
Rifles_Lever_Action = 'Rifles-Lever Action'
Rifles_Pump_Action = 'Rifles-Pump Action'
Rifles_Combos = 'Rifles-Combos'
Rifles_Single_Shot = 'Rifles-Single Shot'
Rifles_Tactical = 'Rifles-Tactical'
Rifles_Lower_Receivers = 'Rifles-Lower Receivers'
Rifles_Black_Powder = 'Rifles-Black Powder'
Rifles_Revolver = 'Rifles-Revolver'
Shotguns_Pump_Action = 'Shotguns-Pump Action'
Shotguns_Lever_Action = 'Shotguns-Lever Action'
Shotguns_Single_Shot = 'Shotguns-Single Shot'
Shotguns_Semi_Automatic = 'Shotguns-Semi-Automatic'
Shotguns_Over_Under = 'Shotguns-Over-Under'
Shotguns_Side_By_Side = 'Shotguns-Side By Side'
Shotguns_Bolt_Action = 'Shotguns-Bolt Action'

condition_new = 'Like New'
condition_very_good = 'Very Good'
condition_good = 'Good'
condition_acceptable = 'Acceptable'
condition_refurbished = 'Refurbished'

conditions = [
    ('Choose Condition', 'Choose Condition'),
    (condition_new, 'Like New'),
    (condition_very_good, 'Very Good'),
    (condition_good, 'Good'),
    (condition_acceptable, 'Acceptable'),
    (condition_refurbished, 'Refurbished'),
]

categories = [
    (None, 'Select a category'),
    (Ammo_Rifle, 'Ammo-Rifle'),
    (Ammo_Handgun, 'Ammo-Handgun'),
    (Ammo_Shotgun, 'Ammo-Shotgun'),
    (Ammo_Rimfire, 'Ammo-Rimfire'),
    (Ammo_Promo, 'Ammo-Promo'),
    (Ammo_Slugs, 'Ammo-Slugs'),
    (Ammo_Blanks, 'Ammo-Blanks'),
    (Ammo_Dummy, 'Ammo-Dummy'),
    (Ammo_Black_Powder, 'Ammo-Black Powder'),
    (Apparel_Gloves, 'Apparel-Gloves'),
    (Apparel_Outerwear, 'Apparel-Outerwear'),
    (Apparel_Pants, 'Apparel-Pants'),
    (Apparel_Shirts, 'Apparel-Shirts'),
    (Apparel_Shoes, 'Apparel-Shoes'),
    (Apparel_Other, 'Apparel-Other'),
    (Handguns_Revolvers, 'Handguns-Revolvers'),
    (Handguns_Semi_Automatic, 'Handguns-Semi Automatic'),
    (Handguns_Lever_Action, 'Handguns-Lever Action'),
    (Handguns_Derringer, 'Handguns-Derringer'),
    (Handguns_Single_Shot, 'Handguns-Single Shot'),
    (Handguns_Double_Action, 'Handguns-Double Action'),
    (Handguns_Black_Powder, 'Handguns-Black Powder'),
    (Handguns_AR_Pistol, 'Handguns-AR Pistol'),
    (Handguns_Other, 'Handguns-Other'),
    (Knives_Fixed_Blade, 'Knives-Fixed Blade'),
    (Knives_Folding, 'Knives-Folding'),
    (Knives_Knife_Accessories, 'Knives-Knife Accessories'),
    (Knives_Utility, 'Knives-Utility'),
    (Magazines_High_Capacity, 'Magazines-High Capacity'),
    (Magazines_Standard, 'Magazines-Standard'),
    (Optics_Binoculars, 'Optics-Binoculars'),
    (Optics_Flashlights_Batteries, 'Optics-Flashlights/Batteries'),
    (Optics_Night_Vision, 'Optics-Night Vision'),
    (Optics_Optical_Accessories, 'Optics-Optical Accessories'),
    (Optics_Scope_Mounts, 'Optics-Scope Mounts'),
    (Optics_Scopes, 'Optics-Scopes'),
    (Optics_Sights_Lasers_Lights, 'Optics-Sights/Lasers/Lights'),
    (Optics_Spotting_Scopes, 'Optics-Spotting Scopes'),
    (Optics_Thermal_Optics, 'Optics-Thermal Optics'),
    (Parts_Gear_Accessories, 'Parts&Gear-Accessories'),
    (Parts_Gear_Airguns, 'Parts&Gear-Airguns'),
    (Parts_Gear_Airguns_Choke_Tubes, 'Parts&Gear-Airguns / Choke Tubes'),
    (Parts_Gear_Blinds_Accessories, 'Parts&Gear-Blinds and Accessories'),
    (Parts_Gear_Bolt_Carrier_Group, 'Parts&Gear-Bolt Carrier Group'),
    (Parts_Gear_Books_Media, 'Parts&Gear-Books / Media'),
    (Parts_Gear_Bore_Sighter_Arbors, 'Parts&Gear-Bore Sighters  Arbors'),
    (Parts_Gear_Camping, 'Parts&Gear-Camping'),
    (Parts_Gear_Cleaning_Equipment, 'Parts&Gear-Cleaning Equipment'),
    (Parts_Gear_Clothing, 'Parts&Gear-Clothing'),
    (Parts_Gear_Conversion_Kits, 'Parts&Gear-Conversion Kits'),
    (Parts_Gear_Coolers, 'Parts&Gear-Coolers'),
    (Parts_Gear_Decoys, 'Parts&Gear-Decoys'),
    (Parts_Gear_Electronics, 'Parts&Gear-Electronics'),
    (Parts_Gear_Feeders, 'Parts&Gear-Feeders'),
    (Parts_Gear_Frames, 'Parts&Gear-Frames'),
    (Parts_Gear_Game_Calls, 'Parts&Gear-Game Calls'),
    (Parts_Gear_Grips_Pads_Stocks, 'Parts&Gear-Grips / Pads / Stocks'),
    (Parts_Gear_Gun_Rests, 'Parts&Gear-Gun Rests'),
    (Parts_Gear_Hard_Gun_Cases, 'Parts&Gear-Hard Gun Cases'),
    (Parts_Gear_Holsters, 'Parts&Gear-Holsters'),
    (Parts_Gear_Hunting_Scents, 'Parts&Gear-Hunting Scents'),
    (Parts_Gear_Non_Lethal_Defense, 'Parts&Gear-Non-Lethal Defense'),
    (Parts_Gear_Parts, 'Parts&Gear-Parts'),
    (Parts_Gear_Pistol_Cases, 'Parts&Gear-Pistol Cases'),
    (Parts_Gear_Racks, 'Parts&Gear-Racks'),
    (Parts_Gear_Reloading_Equipment, 'Parts&Gear-Reloading Equipment'),
    (Parts_Gear_Safes_Security, 'Parts&Gear-Safes / Security'),
    (Parts_Gear_Safety_Protection, 'Parts&Gear-Safety / Protection'),
    (Parts_Gear_Slings_Swivels, 'Parts&Gear-Slings / Swivels'),
    (Parts_Gear_Soft_Gun_Cases, 'Parts&Gear-Soft Gun Cases'),
    (Parts_Gear_Survival_Supplies, 'Parts&Gear-Survival Supplies'),
    (Parts_Gear_Targets, 'Parts&Gear-Targets'),
    (Parts_Gear_Tools, 'Parts&Gear-Tools'),
    (Parts_Gear_Upper_Receivers, 'Parts&Gear-Upper Receivers'),
    (Parts_Gear_Utility_Boxes, 'Parts&Gear-Utility Boxes'),
    (Rifles_Semi_Automatic, 'Rifles-Semi-Automatic'),
    (Rifles_Bolt_Action, 'Rifles-Bolt Action'),
    (Rifles_Lever_Action, 'Rifles-Lever Action'),
    (Rifles_Pump_Action, 'Rifles-Pump Action'),
    (Rifles_Combos, 'Rifles-Combos'),
    (Rifles_Single_Shot, 'Rifles-Single Shot'),
    (Rifles_Tactical, 'Rifles-Tactical'),
    (Rifles_Lower_Receivers, 'Rifles-Lower Receivers'),
    (Rifles_Black_Powder, 'Rifles-Black Powder'),
    (Rifles_Revolver, 'Rifles-Revolver'),
    (Shotguns_Pump_Action, 'Shotguns-Pump Action'),
    (Shotguns_Lever_Action, 'Shotguns-Lever Action'),
    (Shotguns_Single_Shot, 'Shotguns-Single Shot'),
    (Shotguns_Semi_Automatic, 'Shotguns-Semi Automatic'),
    (Shotguns_Over_Under, 'Shotguns-Over-Under'),
    (Shotguns_Side_By_Side, 'Shotguns-Side By Side'),
    (Shotguns_Bolt_Action, 'Shotguns-Bolt Action'),
]

HANDGUNS = [
    Handguns_Revolvers,
    Handguns_Semi_Automatic,
    Handguns_Lever_Action,
    Handguns_Derringer,
    Handguns_Single_Shot,
    Handguns_Double_Action,
    Handguns_Black_Powder,
    Handguns_AR_Pistol,
    Handguns_Other,
]
