import autocomplete_light

from django import forms
from django.contrib import admin

from accounts.models import UserProfile
from .models import UPC, Product, FeaturedProduct, ProductImage, ProductCoupon


class CouponModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.company_name


class UPCAdmin(admin.ModelAdmin):
    search_fields = [
        'upc',
    ]


class ProductCouponAdminForm(forms.ModelForm):
    manufacturer = CouponModelChoiceField(
        queryset=UserProfile.objects.filter(is_dealer_direct=True),
        required=False,
        empty_label='All Manufacturers'
    )

    class Meta:
        model = ProductCoupon
        fields = [
            'coupon_name',
            'coupon_code',
            'percent_discount',
            'amount_discount',
            'duration',
            'manufacturer',
        ]
        labels = {
            'percent_discount': 'Percent Off',
            'amount_discount': 'Amount Off'
        }


class ProductCouponAdmin(admin.ModelAdmin):
    form = ProductCouponAdminForm


# Product Admin
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        '__unicode__',
        'image',
    )

# Product Image Admin
class ProductImageAdminForm(forms.ModelForm):
    class Meta:
        model = ProductImage
        fields = ['image', 'upc']
    
    def __init__(self, *args, **kwargs):
        super(ProductImageAdminForm, self).__init__(
            *args,
            **kwargs
        )
        self.fields['upc'].widget = autocomplete_light.ChoiceWidget(
            'UPCAutoComplete'
        )
            

class ProductImageAdmin(admin.ModelAdmin):
    form = ProductImageAdminForm

admin.site.register(ProductCoupon, ProductCouponAdmin)
admin.site.register(UPC, UPCAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(FeaturedProduct, ProductAdmin)
admin.site.register(ProductImage, ProductImageAdmin)
