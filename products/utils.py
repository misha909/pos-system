import requests
import mechanize

from datetime import date

from pyquery import PyQuery as pq

from django.conf import settings


def verify_ffl(first, second, last):
    """
    Verify an FFL using the form in https://www.atfonline.gov/fflezcheck/fflSearch.do.
    To simulate form submission, use mechanize.
    Then analyze the response using PyQuery.
    It is assumed that an FFL is valid if there is an expiration date.
    This function will return True if there is an expiration date (verified).
    Otherwise return False.
    """
    br = mechanize.Browser()
    br.open(settings.FFL_EZ_CHECK_URL)
    br.select_form(name='fflSearchForm')
    br['licsRegn'] = first
    br['licsDis'] = second
    br['licsSeq'] = last

    response = br.submit()
    content = response.get_data()

    dom = pq(content)
    verified = False
    company = ''
    premise_address = None
    mailing_address = None
    expiration_date = None
    premise_address = None
    mailing_address = None
    for item in dom("table[cellpadding='2']").items('tr'):
        tr_right = item("td[align='right']")
        if tr_right.text().startswith("Expiration Date"):
            tr_left = item("td[align='left']")
            if tr_left.text().strip():
                verified = True
                # Get Expiration Date
                date_str = tr_left.text().strip().split("/")
                expiration_date = date(int(date_str[2]), int(date_str[0]), int(date_str[1]))
            else:
                verified = False
        elif tr_right.text().startswith("License Name"):
            tr_left = item("td[align='left']")
            company = tr_left.text().strip()
        elif tr_right.text().startswith("Premise Address"):
            tr_left = item("td[align='left']")
            premise_address = tr_left.text().strip()
        elif tr_right.text().startswith("Mailing Address"):
            tr_left = item("td[align='left']")
            mailing_address = tr_left.text().strip()
    print verified
    return {
        'verified': verified,
        'company': company,
        'expiration_date': expiration_date,
        'premise_address': premise_address,
        'mailing_address': mailing_address
    }

def is_integer(string_value):
    try:
        int(string_value)
        return True
    except ValueError:
        return False
