import autocomplete_light

from .models import ProductImage, UPC


class UPCAutoComplete(autocomplete_light.AutocompleteModelBase):
    search_fields = [
        'upc'
    ]

autocomplete_light.register(UPC, UPCAutoComplete)
