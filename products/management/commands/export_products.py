import csv

from optparse import make_option

from django.core.management.base import BaseCommand

from products.models import Product, ProductImage, UPC


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--missing-images',
            action='store_true',
            dest='missing_images',
            default=False,
            help='Only filter products with missing images'),
    )
    args = ""
    help = "Creates csv file of all products"

    def handle(self,*args,**options):
        products = Product.objects.all()
        
        product_file = 'products.csv'
        
        if options.get('missing_images'):
            product_file = 'products-missing-images.csv'
            excluded_upc_ids = set(ProductImage.objects.exclude(upc=None).exclude(upc__upc='').values_list('upc__pk', flat=True))
            products = products.exclude(upc__in=excluded_upc_ids)
            
        print "Count:", products.count()
        
        with open(product_file, 'wb') as csvfile:
            writer = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
            writer.writerow([
                'Item Number',
                'UPC',
                'Distributor',
                'Gun Type',
                'Manufacturer',
                'Model',
                'Caliber',
                'Action',
                'Description',
                'Price',
                'Quantity',
                'Capacity',
                'Finish',
                'Stock',
                'Sights',
                'Barrel Length',
                'Overall Length',
                'Weight',
                'Picture',
                'Davidson Picture'
            ])
            for product in products:
                upc = ''
                distributor = ''
                if product.upc:
                    upc = product.upc.upc
                if product.distributor:
                    distributor = product.distributor.name
                
                writer.writerow([
                    encode_to_utf(product.item_number),
                    encode_to_utf(upc),
                    encode_to_utf(distributor),
                    encode_to_utf(product.upc.gun_type),
                    encode_to_utf(product.upc.manufacturer),
                    encode_to_utf(product.upc.model),
                    encode_to_utf(product.upc.caliber),
                    encode_to_utf(product.upc.action),
                    encode_to_utf(product.upc.description),
                    product.price,
                    product.qty,
                    encode_to_utf(product.upc.capacity),
                    encode_to_utf(product.upc.finish),
                    encode_to_utf(product.upc.stock),
                    encode_to_utf(product.upc.sights),
                    encode_to_utf(product.upc.length_barrel),
                    encode_to_utf(product.upc.length_overall),
                    encode_to_utf(product.upc.weight),
                    encode_to_utf(product.upc.picture),
                    encode_to_utf(product.upc.davidson_picture),
                ])


def encode_to_utf(value):
    if value:
        return value.encode("utf-8")
    return None