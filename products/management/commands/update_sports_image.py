import memcache
import os
import xml.etree.ElementTree as ET

from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import
from xml.sax.saxutils import unescape
from xml.etree.ElementTree import ParseError

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Q

from accounts.models import Distributor
from products.models import UPC, Product


class Command(BaseCommand):
    """
    This will be depracated once UPC named image files are
    implemented for product images.
    """
    args = ""
    help = "Updates images of Sports South"

    def handle(self, *args, **options):
        distributor = Distributor.objects.get(name="Sport's South")
        products = Product.objects.select_related('upc').filter(
            Q(distributor=distributor) &
            (
                Q(upc__picture__isnull=True) |
                ~Q(upc__picture__startswith='http')
            )
        )
        print "Processing %d Products" % products.count()

        schema_url = 'http://www.w3.org/2001/XMLSchema'
        namespace = 'http://webservices.theshootingwarehouse.com/smart/Images.asmx'
        wsdl = 'http://webservices.theshootingwarehouse.com/smart/images.asmx?wsdl'
        imp = Import(schema_url)
        imp.filter.add(namespace)
        doctor = ImportDoctor(imp)
        client = Client(wsdl, doctor=doctor)

        tmp_file = 'product_image_url.xml'
        for product in products:
            f = open(tmp_file, 'w')
            response = client.service.GetPictureURLs(
                CustomerNumber=distributor.login,
                UserName=distributor.login,
                Password=distributor.password,
                ItemNumber=int(product.item_number),
                Source=distributor.login
            )
            f.write(response)
            f.close()
            tree = ET.parse(tmp_file)
            root = tree.getroot()
            image = None
            for item in root.findall('Table'):
                image_size = item.find('ImageSize').text
                if image_size is None or image_size != 'large':
                    continue
                image = item.find('Link').text
                print "----------------------------------------------------------------"
                print "Item Number:", product.item_number
                print "Image = ", image
                break
            if image:
                upc = product.upc
                upc.picture = image
                upc.save()
        try:
            os.remove(tmp_file)
        except Exception:
            pass
