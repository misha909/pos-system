import urllib
import os
import StringIO
import traceback

from optparse import make_option
from PIL import Image

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.files import temp as tempfile
from django.core.files import File
from django.db.models import Q

from accounts.models import Distributor
from products.models import UPC, Product, ProductImage


class Command(BaseCommand):
    """
    Converts image files from current products to upc filename format.
    """
    option_list = BaseCommand.option_list + (
        make_option(
            '--upc_id',
            action='store',
            dest='upc_id',
            default=False,
            help='Specific upc id to update'
        ),
    )
    help = "Saves an image in the upc filename format"

    def handle(self, *args, **options):
        upc_id = options.get('upc_id', False)

        # Distributors
        print "----------------- Start Management Command -----------------"
        davidson = Distributor.objects.get(name="Davidson's")
        sports = Distributor.objects.get(name="Sport's South")

        if upc_id:
            upc = UPC.objects.get(pk=upc_id)
            handle_upc(upc)
            return

        # first block
        # handle all non-davidsons
        excluded_upc_ids = set(ProductImage.objects.filter(is_davidson=False).exclude(upc=None).exclude(upc__upc='').values_list('upc__pk', flat=True))
        upcs = UPC.objects.filter(picture__isnull=False).exclude(pk__in=excluded_upc_ids)
        upccount = upcs.count()
        for i, upc in enumerate(upcs):
            print "=============================================="
            print "Processing (Non-Davidson) %d/%d UPC %s" % (i+1, upccount, upc.upc)
            handle_upc(upc)

        # second block
        # handle all davidsons
        excluded_upc_ids = set(ProductImage.objects.filter(is_davidson=True).exclude(upc=None).exclude(upc__upc='').values_list('upc__pk', flat=True))
        upcs = UPC.objects.filter(davidson_picture__isnull=False).exclude(pk__in=excluded_upc_ids)
        upccount = upcs.count()
        for i, upc in enumerate(upcs):
            print "=============================================="
            print "Processing (Davidson) %d/%d UPC %s" % (i+1, upccount, upc.upc)
            handle_upc(upc)


def handle_picture(upc, picture_url, is_davidson=False):
    tempfile.NamedTemporaryFile(prefix='djangotemp', dir='/opt/webapps/varmory/tmp/')
    if picture_url:
        result = None
        print picture_url
        try:
            result = urllib.urlretrieve(picture_url)
        except IOError, e:
            # Image does not exist
            print e
            pass
        if result:
            base_image_file = open(result[0])
            try:
                base_image = Image.open(base_image_file)
                print "Image Loaded"
                image = ProductImage.objects.create(
                    upc=upc,
                    is_davidson=is_davidson
                )
                print "Image Instance Created"
                image.image.save(
                    "%s.jpg" % upc.upc,
                    File(base_image_file)
                )
                print "Image Saved"

                thumb_grid = resize_image(base_image, 230, 230)
                thumb_grid_file = StringIO.StringIO()
                thumb_grid.save(thumb_grid_file, format='JPEG')
                image.image_thumb_grid.save(
                    "%s.jpg" % upc.upc,
                    File(thumb_grid_file)
                )
                print "Image Thumb Grid Saved"

                thumb_list = resize_image(base_image, 63, 63)
                thumb_list_file = StringIO.StringIO()
                thumb_list.save(thumb_list_file, format='JPEG')
                image.image_thumb_list.save(
                    "%s.jpg" % upc.upc,
                    File(thumb_list_file)
                )
                print "Image Thumb List Saved"

                detail_popup = resize_image(base_image, 460, 400)
                detail_popup_file = StringIO.StringIO()
                detail_popup.save(detail_popup_file, format='JPEG')
                image.image_detail_popup.save(
                    "%s.jpg" % upc.upc,
                    File(detail_popup_file)
                )
                print "Image Detail Popup Saved"

                image.save()
                print "Image created for %s" % upc.upc
                base_image_file.close()
            except Exception, e:
                # probably not an image file.
                print traceback.format_exc()
                print e
                base_image_file.close()


def handle_upc(upc):
    if upc:
        if upc.picture:
            handle_picture(upc, upc.picture, is_davidson=False)
        if upc.davidson_picture:
            handle_picture(upc, upc.davidson_picture, is_davidson=True)


def resize_image(img, w, h):
    (orig_width, orig_height) = img.size
    if w <= 0 or h <= 0:
        return img
    factor = 1.0
    if orig_width > orig_height:
        factor = float(w)/orig_width
    else:
        factor = float(h)/orig_height
    return img.resize((int(orig_width*factor), int(orig_height*factor)), Image.ANTIALIAS)
