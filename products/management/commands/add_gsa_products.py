import csv
import re
from kitchen.text.converters import to_unicode
from django.core.management.base import BaseCommand
from django.conf import settings
from products.models import Product, UPC
from accounts.models import Distributor


class Command(BaseCommand):
    args = ""
    help = "Migrates existing categories to new tree system."

    def handle(self, *args, **options):
        """
        Imports products from GSA Direct.
        Columns:
            Distributor ID, UPC Code, Distributor, Manufacturer, Model, Caliber,
            Action, Price, QTY, Capacity, Finish, Barrel Length, Overall Length,
            Weight, Description, Category
        """
        distributor = Distributor.objects.get(pk=9)
        
        # Remove other products from same distributor
        products = Product.objects.filter(distributor=distributor)
        products.delete()
        
        with open(settings.GSA_CSV_FILE, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            count = 0
            for row in reader:
                count += 1
                if count == 1:
                    continue
                upc_code = row[1].strip()
                price = re.sub('/[^0-9.]/', '', row[8]).replace("$", "").strip()
                price = to_unicode(price, 'utf-8').strip()
                price = price.replace(",", "")
                
                qty = to_unicode(row[9], 'utf-8').strip()
                if not qty:
                    qty = "99"
                
                if upc_code != '':
                    (upc, created) = UPC.objects.get_or_create(
                        upc=upc_code,
                        defaults={
                            'is_upc': True
                        }
                    )
                    
                    # Fill-up upc fields if None
                    if not upc.gun_type:
                        upc.gun_type = row[15]
                    if not upc.manufacturer:
                        upc.manufacturer = row[3]
                    if not upc.model:
                        upc.model = to_unicode(row[4], 'utf-8').strip()
                    if not upc.caliber:
                        upc.caliber = to_unicode(row[6], 'utf-8').strip()
                    if not upc.action:
                        upc.action = to_unicode(row[5], 'utf-8').strip()
                    if not upc.description:
                        upc.description = to_unicode(row[14], 'utf-8').strip()
                    if not upc.capacity:
                        upc.capacity = to_unicode(row[10], 'utf-8').strip()
                    if not upc.finish:
                        upc.finish = to_unicode(row[5], 'utf-8').strip()
                    if not upc.length_barrel:
                        upc.length_barrel = to_unicode(row[11], 'utf-8').strip()
                    if not upc.length_overall:
                        upc.length_overall = to_unicode(row[12], 'utf-8').strip()
                    if not upc.weight:
                        upc.weight = weight=to_unicode(row[13], 'utf-8').strip()
                    
                    product = Product.objects.create(
                        upc=upc,
                        distributor=distributor,
                        item_number=to_unicode(row[0], 'utf-8').strip(),
                        price=price,
                        qty=qty
                    )
                    
                    if not upc.picture:
                        # Save images
                        if upc.manufacturer == 'MILTAC Industries':
                            upc.picture = ("https://images.ffldesign.com/images/%d/miltac/%s.jpg" % (distributor.pk, product.item_number)).lower()
                        elif upc.manufacturer == 'SRM Arms':
                            upc.picture = ("https://images.ffldesign.com/images/%d/srm/%s.jpg" % (distributor.pk, product.item_number)).lower()
                    upc.save()
