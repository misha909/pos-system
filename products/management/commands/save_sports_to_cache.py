import memcache

from django.conf import settings
from django.core.management.base import BaseCommand

from accounts.models import Distributor
from products.models import UPC, Product
from import_stats.utils import update_cache

class Command(BaseCommand):
    args = ""
    help = "Saves Sport's South Details to cache."

    def handle(self,*args,**options):
        sports = Distributor.objects.get(name="Sport's South")
        products = Product.objects.filter(distributor=sports)
        # Memcache
        mc = memcache.Client([settings.MEMCACHED_URL], debug=0)
        
        for product in products:
            update_cache(product, mc=mc)