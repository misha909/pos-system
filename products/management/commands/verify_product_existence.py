from django.core.management.base import BaseCommand

from django.utils import timezone
from django.utils.html import strip_tags

from products.models import UPC, Product
from accounts.models import Distributor

import mechanize
import urllib
import urllib2
from pyquery import PyQuery as pq

class Command(BaseCommand):
    args = ""
    help = "Verifies product's existence in the distributor's database."

    def handle(self,*args,**options):
        davidsons_products = Product.objects.filter(distributor__id=1, upc__isnull=False).exclude(upc__upc='')
        lipseys_products = Product.objects.filter(distributor__id=2, upc__isnull=False).exclude(upc__upc='')
        green_products = Product.objects.filter(distributor__id=3, upc__isnull=False).exclude(upc__upc='')
        rsr_products = Product.objects.filter(distributor__id=4, upc__isnull=False).exclude(upc__upc='')
        zanders_products = Product.objects.filter(distributor__id=5, upc__isnull=False).exclude(upc__upc='')
        camfour_products = Product.objects.filter(distributor__id=6, upc__isnull=False).exclude(upc__upc='')

        pks_to_delete = []
        errors_encountered = []

        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [
            ('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36')
        ]
        login = 'scott@gsadirect.com'
        password = 'gsadirect1'

        # First, let's do davidson's products... login:
        url = "http://www11.davidsonsinc.com/Login/Login.aspx"
        br.open(url)
        br.select_form(name="aspnetForm")
        br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$UserName'] = login
        br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$Password'] = password
        req = br.submit()
        new_url = req.geturl()
        find_url = new_url.find('/Dealers/PowerSearch.aspx?')
        if find_url > -1:
            print 'Logged in to Davidsons'
            # We're logged in, check if products are in database:
            for p in davidsons_products:
                try:
                    upc = p.upc.upc
                    if upc == '':
                        continue
                    url = 'http://www11.davidsonsinc.com/Dealers/ItemDetail.aspx?sid=%s&scode=upcID' % upc
                    if br.open(url).geturl() == url:
                        print 'update upc %s' % upc
                        br.open(url)
                        lines = br.response().readlines()
                        for index, line in enumerate(lines):
                            if 'Quantity in Stock' in line:
                                p.qty = strip_tags(lines[index+1]).strip()
                            if 'Your Price' in line:
                                p.price = float(strip_tags(lines[index+1]).strip()[1:])
                        p.save()
                        continue
                    print 'delete upc %s' % upc
                    pks_to_delete.append(p.pk)
                    p.delete()
                except Exception, e:
                    if str(e) not in errors_encountered:
                        errors_encountered.append(str(e))

        # Next, RSR
        login = '89075'
        password = 'Gsadirect1'
        url = 'https://www.rsrgroup.com/cgi-bin/login?validate'
        user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
        values = {
            "LOGIN": login,
            "PASSWORD": password
        }
        headers = {
            'User-Agent' : user_agent
        }
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data, headers)
        response = urllib2.urlopen(req)
        new_url = response.read()
        find_url = new_url.find('dealer/clsddealerhome')
        if find_url > -1:
            print 'Logged into RSR'
            for p in rsr_products:
                try:
                    upc = p.upc.upc
                    if upc == '':
                        continue
                    url = 'http://www.rsrgroup.com/catalog/search?c=search&m=index&category=all&keywords=%s&suggest_value=&Search=Search' % upc
                    if '/catalog/product/' in br.open(url).geturl():
                        print 'keep upc %s' % upc
                        continue
                    print 'delete upc %s' % upc
                    pks_to_delete.append(p.pk)
                    p.delete()
                except Exception, e:
                    if str(e) not in errors_encountered:
                        errors_encountered.append(str(e))

        password = 'ID8327'
        url = 'https://shop2.gzanders.com/customer/account/login/'
        br.open(url)
        br.select_form(nr=2)  # get third form on the page
        br.form['login[username]'] = login
        br.form['login[password]'] = password
        req = br.submit()
        find_url = (req.geturl() == 'https://shop2.gzanders.com/customer/account/')
        if find_url > -1:
            print 'Logged into Zanders'
            for p in zanders_products:
                try:
                    upc = p.upc.upc
                    if upc == '':
                        continue
                    url = 'http://shop2.gzanders.com/catalogsearch/result/?q=%s' % upc
                    if 'Your search returns no results.' not in br.open(url).get_data():
                        print 'keep upc %s' % upc
                        continue
                    print 'delete upc %s' % upc
                    pks_to_delete.append(p.pk)
                    p.delete()
                except Exception, e:
                    if str(e) not in errors_encountered:
                        errors_encountered.append(str(e))


        url = 'https://secure.camfour.com/Account/SignOn/'
        br.open(url)
        br.select_form(nr=1)  # get second form on the page
        br.form['cu'] = login
        br.form['pwd'] = password
        req = br.submit()
        text = req.get_data()
        find_url = 'Signed in as' in text
        if find_url > -1:
            print 'Logged into Camfour'
            for p in camfour_products:
                try:
                    upc = p.upc.upc
                    if upc == '':
                        continue
                    url = 'https://secure.camfour.com/Search/%s/' % upc
                    if 'Your search did not produce any results.' not in br.open(url).get_data():
                        print 'keep upc %s' % upc
                        continue
                    print 'delete upc %s' % upc
                    pks_to_delete.append(p.pk)
                    p.delete()
                except Exception, e:
                    if str(e) not in errors_encountered:
                        errors_encountered.append(str(e))

        url = "https://www.dealerease.net/catalog/cart/login/?ret_id=140067"
        br.open(url)
        br.select_form(name="form1")
        i=0
        for control in br.form.controls:
            if i == 5:
                ts = control._value
                break
            i = i+1
        br.form['txtLogin'] = login
        br.form['txtPass'] = password
        req = br.submit()
        new_url = req.geturl()
        find_url = new_url.find(ts)
        if find_url > -1:
            print 'Logged into Greens'
            for p in green_products:
                try:
                    upc = p.upc.upc
                    if upc == '':
                        continue
                    url = 'https://www.dealerease.net/catalog/search/searchresult.asp?keywords=%s' % upc
                    if 'Your query yielded 0 results....' not in br.open(url).get_data():
                        print 'keep upc %s' % upc
                        continue
                    print 'delete upc %s' % upc
                    pks_to_delete.append(p.pk)
                    p.delete()
                except Exception, e:
                    if str(e) not in errors_encountered:
                        errors_encountered.append(str(e))

        url = "http://www.lipseys.com/login.aspx"
        br.open(url)
        br.select_form(name="aspnetForm")
        br.form['ctl00$bodyContent$email'] = login
        br.form['ctl00$bodyContent$pass'] = password
        req = br.submit()

        content = req.get_data()
        dom = pq(content)
        contact_dom = dom('.dealer-econtact')
        if contact_dom:
            print 'Logged into Lipseys'
            for p in lipseys_products:
                try:
                    upc = p.upc.upc
                    if upc == '':
                        continue
                    url = 'http://www.lipseys.com/itemfinder.aspx?q=%s' % upc
                    if br.open(url).geturl() != url:
                        print 'keep upc %s' % upc
                        continue
                    print 'delete upc %s' % upc
                    pks_to_delete.append(p.pk)
                    p.delete()
                except Exception, e:
                    if str(e) not in errors_encountered:
                        errors_encountered.append(str(e))

        print len(pks_to_delete)
        print errors_encountered
