from django.core.management.base import BaseCommand

from products.models import UPC


class Command(BaseCommand):
    args = ""
    help = "Deletes UPC without an associated Product"

    def handle(self,*args,**options):
        upc = UPC.objects.filter(product__isnull=True)
        upc.delete()
