import csv
from products.models import UPC
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = ""
    help = "Populates upc descriptions"

    def handle(self, *args, **options):
        upcs = UPC.objects.filter(description='')
        print 'found %s upcs with an empty description' % upcs.count()
        updated_count = 0
        with open('saas/imports/upc-descriptions.csv') as csvfile:
            print 'opening upc description file'
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                if upcs.filter(upc=row[0]).exists():
                    upc = upcs.get(upc=row[0])
                    upc.description = unicode(row[1], 'utf-8')
                    upc.save()
                    updated_count += 1
        print 'updated %s product descriptions' % updated_count
