# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import localflavor.us.models
import mptt.fields
import products.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20160321_1807'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='products.Category', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FeaturedProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_number', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('title', models.CharField(max_length=300, null=True, blank=True)),
                ('qty_sold', models.PositiveIntegerField(default=0)),
                ('price', models.FloatField()),
                ('bulk_tier_prices', models.CharField(default=None, max_length=255, null=True, blank=True)),
                ('qty', models.CharField(max_length=100)),
                ('minimum_order', models.PositiveIntegerField(default=0)),
                ('video_url', models.CharField(default=None, max_length=255, null=True, blank=True)),
                ('salesforce_id', models.CharField(max_length=30, null=True, blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('dealer_exchange', models.BooleanField(default=False)),
                ('nfa_product', models.BooleanField(default=False)),
                ('distributor', models.ForeignKey(to='accounts.Distributor')),
            ],
            options={
                'db_table': 'products',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductCoupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('coupon_name', models.CharField(unique=True, max_length=250)),
                ('percent_discount', models.FloatField(null=True, blank=True)),
                ('amount_discount', models.FloatField(null=True, blank=True)),
                ('duration', models.CharField(max_length=250, choices=[(b'Once', b'Once'), (b'Forever', b'Forever')])),
                ('coupon_code', models.CharField(unique=True, max_length=30, blank=True)),
                ('manufacturer', models.ForeignKey(blank=True, to='accounts.UserProfile', null=True)),
                ('redeemed_by', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(null=True, upload_to=products.models.get_upc_filename, blank=True)),
                ('image_thumb_grid', models.ImageField(null=True, upload_to=products.models.get_thumb_grid_filename, blank=True)),
                ('image_thumb_list', models.ImageField(null=True, upload_to=products.models.get_thumb_list_filename, blank=True)),
                ('image_detail_popup', models.ImageField(null=True, upload_to=products.models.get_detail_popup_filename, blank=True)),
                ('is_davidson', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductListingImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=products.models.get_detail_popup_filename, blank=True)),
                ('image_thumbnail', models.ImageField(upload_to=products.models.get_thumb_list_filename, blank=True)),
                ('product', models.ForeignKey(blank=True, to='products.Product', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('product', models.ManyToManyField(related_name='tags', to='products.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UPC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('upc', models.CharField(max_length=50)),
                ('is_upc', models.BooleanField(default=True)),
                ('gun_type', models.CharField(max_length=100, null=True, blank=True)),
                ('manufacturer', models.CharField(max_length=100, null=True, blank=True)),
                ('model', models.CharField(max_length=300, null=True, blank=True)),
                ('caliber', models.CharField(max_length=100, null=True, blank=True)),
                ('action', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('short_description', models.CharField(max_length=100, null=True, blank=True)),
                ('capacity', models.CharField(max_length=100, null=True, blank=True)),
                ('finish', models.CharField(max_length=100, null=True, blank=True)),
                ('stock', models.CharField(max_length=100, null=True, blank=True)),
                ('sights', models.CharField(max_length=100, null=True, blank=True)),
                ('length_barrel', models.CharField(max_length=100, null=True, blank=True)),
                ('length_overall', models.CharField(max_length=100, null=True, blank=True)),
                ('weight', models.CharField(max_length=100, null=True, blank=True)),
                ('picture', models.CharField(max_length=100, null=True, blank=True)),
                ('davidson_picture', models.CharField(max_length=100, null=True, blank=True)),
                ('listing_type', models.CharField(default=b'Firearm', max_length=7, choices=[(b'Firearm', b'Firearm'), (b'Accssry', b'Accessories'), (b'Ammo', b'Ammo'), (b'Optics', b'Optics'), (b'Apparel', b'Apparel'), (b'Blades', b'Blades'), (b'PrtGear', b'Parts & Gear')])),
                ('listing_condition', models.CharField(blank=True, max_length=8, null=True, choices=[(b'New', b'New'), (b'Used', b'Used')])),
                ('condition_detail', models.CharField(blank=True, max_length=20, null=True, choices=[(b'Choose Condition', b'Choose Condition'), (b'Like New', b'Like New'), (b'Very Good', b'Very Good'), (b'Good', b'Good'), (b'Acceptable', b'Acceptable'), (b'Refurbished', b'Refurbished')])),
                ('copy', models.CharField(max_length=255, null=True, blank=True)),
                ('sku', models.CharField(max_length=255, null=True, blank=True)),
                ('msrp', models.CharField(max_length=55, null=True, blank=True)),
                ('map', models.CharField(max_length=55, null=True, blank=True)),
                ('list_price', models.DecimalField(null=True, max_digits=10, decimal_places=2)),
                ('listing_category', models.CharField(blank=True, max_length=255, null=True, choices=[(None, b'Select a category'), (b'Ammo-Rifle', b'Ammo-Rifle'), (b'Ammo-Handgun', b'Ammo-Handgun'), (b'Ammo-Shotgun', b'Ammo-Shotgun'), (b'Ammo-Rimfire', b'Ammo-Rimfire'), (b'Ammo-Promo', b'Ammo-Promo'), (b'Ammo-Slugs', b'Ammo-Slugs'), (b'Ammo-Blanks', b'Ammo-Blanks'), (b'Ammo_Dummy', b'Ammo-Dummy'), (b'Ammo-Black Powder', b'Ammo-Black Powder'), (b'Apparel_Gloves', b'Apparel-Gloves'), (b'Apparel_Outerwear', b'Apparel-Outerwear'), (b'Apparel_Pants', b'Apparel-Pants'), (b'Apparel_Shirts', b'Apparel-Shirts'), (b'Apparel_Shoes', b'Apparel-Shoes'), (b'Apparel_Other', b'Apparel-Other'), (b'Handguns-Revolvers', b'Handguns-Revolvers'), (b'Handguns-Semi Automatic', b'Handguns-Semi Automatic'), (b'Handguns-Lever Action', b'Handguns-Lever Action'), (b'Handguns-Derringer', b'Handguns-Derringer'), (b'Handguns-Single Shot', b'Handguns-Single Shot'), (b'Handguns-Double Action', b'Handguns-Double Action'), (b'Handguns-Black Powder', b'Handguns-Black Powder'), (b'Handguns-AR Pistol', b'Handguns-AR Pistol'), (b'Handguns-Other', b'Handguns-Other'), (b'Knives-Fixed Blade', b'Knives-Fixed Blade'), (b'Knives-Folding', b'Knives-Folding'), (b'Knives-Knife Accessories', b'Knives-Knife Accessories'), (b'Knives-Utility', b'Knives-Utility'), (b'Magazines-High Capacity', b'Magazines-High Capacity'), (b'Magazines-Standard', b'Magazines-Standard'), (b'Optics-Binoculars', b'Optics-Binoculars'), (b'Optics-Flashlights/Batteries', b'Optics-Flashlights/Batteries'), (b'Optics-Night Vision', b'Optics-Night Vision'), (b'Optics-Optical Accessories', b'Optics-Optical Accessories'), (b'Optics-Scope Mounts', b'Optics-Scope Mounts'), (b'Optics-Scopes', b'Optics-Scopes'), (b'Optics-Sights/Lasers/Lights', b'Optics-Sights/Lasers/Lights'), (b'Optics-Spotting Scopes', b'Optics-Spotting Scopes'), (b'Optics-Thermal Optics', b'Optics-Thermal Optics'), (b'Parts&Gear-Accessories', b'Parts&Gear-Accessories'), (b'Parts&Gear-Airguns', b'Parts&Gear-Airguns'), (b'Parts&Gear-Airguns / Choke Tubes', b'Parts&Gear-Airguns / Choke Tubes'), (b'Parts&Gear-Blinds and Accessories', b'Parts&Gear-Blinds and Accessories'), (b'Parts&Gear-Bolt Carrier Group', b'Parts&Gear-Bolt Carrier Group'), (b'Parts&Gear-Books / Media', b'Parts&Gear-Books / Media'), (b'Parts&Gear-Bore Sighters  Arbors', b'Parts&Gear-Bore Sighters  Arbors'), (b'Parts&Gear-Camping', b'Parts&Gear-Camping'), (b'Parts&Gear-Cleaning Equipment', b'Parts&Gear-Cleaning Equipment'), (b'Parts&Gear-Clothing', b'Parts&Gear-Clothing'), (b'Parts&Gear-Conversion Kits', b'Parts&Gear-Conversion Kits'), (b'Parts&Gear-Coolers', b'Parts&Gear-Coolers'), (b'Parts&Gear-Decoys', b'Parts&Gear-Decoys'), (b'Parts&Gear-Electronics', b'Parts&Gear-Electronics'), (b'Parts&Gear-Feeders', b'Parts&Gear-Feeders'), (b'Parts&Gear-Frames', b'Parts&Gear-Frames'), (b'Parts&Gear-Game Calls', b'Parts&Gear-Game Calls'), (b'Parts&Gear-Grips Pads / Stocks', b'Parts&Gear-Grips / Pads / Stocks'), (b'Parts&Gear-Gun Rests', b'Parts&Gear-Gun Rests'), (b'Parts&Gear-Hard Gun Cases', b'Parts&Gear-Hard Gun Cases'), (b'Parts&Gear-Holsters', b'Parts&Gear-Holsters'), (b'Parts&Gear-Hunting Scents', b'Parts&Gear-Hunting Scents'), (b'Parts&Gear-Non-Lethal Defense', b'Parts&Gear-Non-Lethal Defense'), (b'Parts&Gear-Parts', b'Parts&Gear-Parts'), (b'Parts&Gear-Pistol Cases', b'Parts&Gear-Pistol Cases'), (b'Parts&Gear-Racks', b'Parts&Gear-Racks'), (b'Parts&Gear-Reloading Equipment', b'Parts&Gear-Reloading Equipment'), (b'Parts&Gear-Safes / Security', b'Parts&Gear-Safes / Security'), (b'Parts&Gear-Safety / Protection', b'Parts&Gear-Safety / Protection'), (b'Parts&Gear-Slings / Swivels', b'Parts&Gear-Slings / Swivels'), (b'Parts&Gear-Soft Gun Cases', b'Parts&Gear-Soft Gun Cases'), (b'Parts&Gear-Survival Supplies', b'Parts&Gear-Survival Supplies'), (b'Parts&Gear-Targets', b'Parts&Gear-Targets'), (b'Parts&Gear-Tools', b'Parts&Gear-Tools'), (b'Parts&Gear-Upper Receivers', b'Parts&Gear-Upper Receivers'), (b'Parts&Gear-Utility Boxes', b'Parts&Gear-Utility Boxes'), (b'Rifles-Semi-Automatic', b'Rifles-Semi-Automatic'), (b'Rifles-Bolt Action', b'Rifles-Bolt Action'), (b'Rifles-Lever Action', b'Rifles-Lever Action'), (b'Rifles-Pump Action', b'Rifles-Pump Action'), (b'Rifles-Combos', b'Rifles-Combos'), (b'Rifles-Single Shot', b'Rifles-Single Shot'), (b'Rifles-Tactical', b'Rifles-Tactical'), (b'Rifles-Lower Receivers', b'Rifles-Lower Receivers'), (b'Rifles-Black Powder', b'Rifles-Black Powder'), (b'Rifles-Revolver', b'Rifles-Revolver'), (b'Shotguns-Pump Action', b'Shotguns-Pump Action'), (b'Shotguns-Lever Action', b'Shotguns-Lever Action'), (b'Shotguns-Single Shot', b'Shotguns-Single Shot'), (b'Shotguns-Semi-Automatic', b'Shotguns-Semi Automatic'), (b'Shotguns-Over-Under', b'Shotguns-Over-Under'), (b'Shotguns-Side By Side', b'Shotguns-Side By Side'), (b'Shotguns-Bolt Action', b'Shotguns-Bolt Action')])),
                ('estimated_ship_days', models.CharField(default=b'10', max_length=255)),
                ('weight_pounds', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('weight_ounces', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('rounds', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('muzzle_velocity', models.PositiveIntegerField(null=True, blank=True)),
                ('length', models.CharField(max_length=255, null=True, blank=True)),
                ('ammo_type', models.CharField(max_length=255, null=True, blank=True)),
                ('magnification', models.CharField(max_length=55, null=True, blank=True)),
                ('field_of_view', models.CharField(max_length=55, null=True, blank=True)),
                ('reticle', models.CharField(max_length=55, null=True, blank=True)),
                ('objective_lens', models.CharField(max_length=55, null=True, blank=True)),
                ('eye_relief', models.CharField(max_length=55, null=True, blank=True)),
                ('max_elevation_adjustment', models.CharField(max_length=55, null=True, blank=True)),
                ('max_windage_adjustment', models.CharField(max_length=55, null=True, blank=True)),
                ('battery', models.CharField(max_length=55, null=True, blank=True)),
                ('dot_size', models.CharField(max_length=55, null=True, blank=True)),
                ('blade_type', models.CharField(max_length=255, null=True, blank=True)),
                ('blade_length', models.CharField(max_length=255, null=True, blank=True)),
                ('blade_total_length', models.CharField(max_length=255, null=True, blank=True)),
                ('blade_steel', models.CharField(max_length=255, null=True, blank=True)),
                ('blade_handle_material', models.CharField(max_length=255, null=True, blank=True)),
                ('blade_edge', models.CharField(max_length=255, null=True, blank=True)),
                ('shipping_zip_code', localflavor.us.models.USZipCodeField(max_length=10, null=True, blank=True)),
                ('shipping_weight', models.CharField(max_length=100, null=True, blank=True)),
                ('shipping_length', models.CharField(max_length=100, null=True, blank=True)),
                ('shipping_width', models.CharField(max_length=100, null=True, blank=True)),
                ('shipping_height', models.CharField(max_length=100, null=True, blank=True)),
                ('shirt_size', models.CharField(blank=True, max_length=5, null=True, choices=[(None, b'Select a size'), (b'XS', b'XS'), (b'S', b'S'), (b'M', b'M'), (b'L', b'L'), (b'XL', b'XL'), (b'XXL', b'XXL'), (b'XXXL', b'XXXL')])),
                ('shirt_color', models.CharField(max_length=55, null=True, blank=True)),
                ('category', models.ForeignKey(default=None, blank=True, to='products.Category', null=True)),
            ],
            options={
                'db_table': 'upc',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='productimage',
            name='upc',
            field=models.ForeignKey(blank=True, to='products.UPC', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='upc',
            field=models.ForeignKey(default=None, blank=True, to='products.UPC', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='user_profile',
            field=models.ForeignKey(default=None, blank=True, to='accounts.UserProfile', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featuredproduct',
            name='product',
            field=models.ForeignKey(to='products.Product'),
            preserve_default=True,
        ),
    ]
