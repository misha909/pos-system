import os

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from localflavor.us.models import USZipCodeField

from accounts.models import Distributor, UserProfile
from accounts.constants import DEALER_DIRECT
from products.constants import categories as CATEGORIES
from products.constants import conditions
from mptt.models import MPTTModel, TreeForeignKey

PRODUCTS_MEDIA_ROOT = 'products_media/'

def get_base_folder(instance):
    base_folder = ''
    davidson = Distributor.objects.get(name="Davidson's")
    # check first if this is a Davidson product
    if instance.is_davidson:
        base_folder = 'davidson'
    #
    return base_folder

def get_upc_filename(instance, filename):
    base_folder = os.path.join(get_base_folder(instance), 'base')
    root_folder = os.path.join(PRODUCTS_MEDIA_ROOT, base_folder)
    ext = filename.split('.')[-1]
    if instance.upc.upc:
        filename = "%s.%s" % (instance.upc.upc, ext)
    return os.path.join(root_folder, filename)

def get_thumb_grid_filename(instance, filename):
    base_folder = os.path.join(get_base_folder(instance), '230x230')
    root_folder = os.path.join(PRODUCTS_MEDIA_ROOT, base_folder)
    ext = filename.split('.')[-1]
    if instance.upc.upc:
        filename = "%s.%s" % (instance.upc.upc, ext)
    return os.path.join(root_folder, filename)

def get_thumb_list_filename(instance, filename):
    base_folder = os.path.join(get_base_folder(instance), '63x63')
    root_folder = os.path.join(PRODUCTS_MEDIA_ROOT, base_folder)
    ext = filename.split('.')[-1]
    if instance.upc.upc:
        filename = "%s.%s" % (instance.upc.upc, ext)
    return os.path.join(root_folder, filename)

def get_detail_popup_filename(instance, filename):
    base_folder = os.path.join(get_base_folder(instance), '460x400')
    root_folder = os.path.join(PRODUCTS_MEDIA_ROOT, base_folder)
    ext = filename.split('.')[-1]
    if instance.upc.upc:
        filename = "%s.%s" % (instance.upc.upc, ext)
    return os.path.join(root_folder, filename)


class Category(MPTTModel):
    name = models.CharField(max_length=50)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']
        unique_together = (('name', 'parent'),)


class UPC(models.Model):
    class Meta:
        db_table = 'upc'

    upc = models.CharField(max_length=50)
    is_upc = models.BooleanField(default=True)

    # Product details
    category = models.ForeignKey(Category, blank=True, null=True, default=None)
    gun_type = models.CharField(max_length=100, null=True, blank=True)
    manufacturer = models.CharField(max_length=100, null=True, blank=True)
    model = models.CharField(max_length=300, null=True, blank=True)
    caliber = models.CharField(max_length=100, null=True, blank=True)
    action = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    short_description = models.CharField(max_length=100, null=True, blank=True)
    capacity = models.CharField(max_length=100, null=True, blank=True)
    finish = models.CharField(max_length=100, null=True, blank=True)
    stock = models.CharField(max_length=100, null=True, blank=True)
    sights = models.CharField(max_length=100, null=True, blank=True)
    length_barrel = models.CharField(max_length=100, null=True, blank=True)
    length_overall = models.CharField(max_length=100, null=True, blank=True)
    weight = models.CharField(max_length=100, null=True, blank=True)
    picture = models.CharField(max_length=100, null=True, blank=True)
    davidson_picture = models.CharField(max_length=100, null=True, blank=True)

    FIREARM = 'Firearm'
    ACCESSORIES = 'Accssry'
    AMMO = 'Ammo'
    OPTICS = 'Optics'
    APPAREL = 'Apparel'
    BLADES = 'Blades'
    PARTS_GEAR = 'PrtGear'
    LISTING_CHOICES = [
        (FIREARM, 'Firearm'),
        (ACCESSORIES, 'Accessories'),
        (AMMO, 'Ammo'),
        (OPTICS, 'Optics'),
        (APPAREL, 'Apparel'),
        (BLADES, 'Blades'),
        (PARTS_GEAR, 'Parts & Gear'),
    ]

    # Dealer Exchange listing fields
    listing_type = models.CharField(
        max_length=7,
        choices=LISTING_CHOICES,
        default=FIREARM,
    )
    listing_condition = models.CharField(
        max_length=8,
        choices=[('New', 'New'), ('Used', 'Used')],
        blank=True,
        null=True
    )
    condition_detail = models.CharField(
        max_length=20,
        choices=conditions,
        blank=True,
        null=True
    )
    copy = models.CharField(max_length=255, blank=True, null=True)
    sku = models.CharField(max_length=255, blank=True, null=True)
    msrp = models.CharField(max_length=55, blank=True, null=True)
    map = models.CharField(max_length=55, blank=True, null=True)
    list_price = models.DecimalField(
        max_digits=10, decimal_places=2, null=True
    )
    listing_category = models.CharField(
        max_length=255, choices=CATEGORIES, blank=True, null=True
    )
    estimated_ship_days = models.CharField(max_length=255, default='10')
    weight_pounds = models.PositiveSmallIntegerField(blank=True, null=True)
    weight_ounces = models.PositiveSmallIntegerField(blank=True, null=True)

    # Extra Data for Ammo
    rounds = models.PositiveSmallIntegerField(blank=True, null=True)
    muzzle_velocity = models.PositiveIntegerField(blank=True, null=True)
    length = models.CharField(max_length=255, blank=True, null=True)
    ammo_type = models.CharField(max_length=255, blank=True, null=True)

    # Extra Data for Optics
    magnification = models.CharField(max_length=55, blank=True, null=True)
    field_of_view = models.CharField(max_length=55, blank=True, null=True)
    reticle = models.CharField(max_length=55, blank=True, null=True)
    objective_lens = models.CharField(max_length=55, blank=True, null=True)
    eye_relief = models.CharField(max_length=55, blank=True, null=True)
    max_elevation_adjustment = models.CharField(
        max_length=55, blank=True, null=True
    )
    max_windage_adjustment = models.CharField(
        max_length=55, blank=True, null=True
    )
    battery = models.CharField(max_length=55, blank=True, null=True)
    dot_size = models.CharField(max_length=55, blank=True, null=True)

    # Extra Data for Blades
    blade_type = models.CharField(max_length=255, blank=True, null=True)
    blade_length = models.CharField(max_length=255, blank=True, null=True)
    blade_total_length = models.CharField(
        max_length=255, blank=True, null=True
    )
    blade_steel = models.CharField(max_length=255, blank=True, null=True)
    blade_handle_material = models.CharField(
        max_length=255, blank=True, null=True
    )
    blade_edge = models.CharField(max_length=255, blank=True, null=True)

    # Shipping Fields
    shipping_zip_code = USZipCodeField(max_length=255, blank=True, null=True)
    shipping_weight = models.CharField(max_length=100, blank=True, null=True)
    shipping_length = models.CharField(max_length=100, blank=True, null=True)
    shipping_width = models.CharField(max_length=100, blank=True, null=True)
    shipping_height = models.CharField(max_length=100, blank=True, null=True)

    EXTRA_SMALL = 'XS'
    SMALL = 'S'
    MEDIUM = 'M'
    LARGE = 'L'
    EXTRA_LARGE = 'XL'
    DOUBLE_EXTRA_LARGE = 'XXL'
    TRIPLE_EXTRA_LARGE = 'XXXL'

    SHIRT_CHOICES = [
        (None, 'Select a size'),
        (EXTRA_SMALL, 'XS'),
        (SMALL, 'S'),
        (MEDIUM, 'M'),
        (LARGE, 'L'),
        (EXTRA_LARGE, 'XL'),
        (DOUBLE_EXTRA_LARGE, 'XXL'),
        (TRIPLE_EXTRA_LARGE, 'XXXL'),
    ]

    # Extra Data for Apparel
    shirt_size = models.CharField(
        max_length=5, blank=True, choices=SHIRT_CHOICES, null=True
    )
    shirt_color = models.CharField(max_length=55, blank=True, null=True)

    def __unicode__(self):
        return self.upc

    @property
    def category_name(self):
        if self.category:
            cat = Category.objects.get(pk=self.category.pk)
            to_add = cat.name
            while cat.parent is not None:
                to_add = cat.parent.name + ' - ' + to_add
                cat = cat.parent
            return to_add
        if self.listing_category:
            return self.listing_category
        return None


class ProductImage(models.Model):
    image = models.ImageField(
        upload_to=get_upc_filename,
        null=True,
        blank=True
    )
    image_thumb_grid = models.ImageField(
        upload_to=get_thumb_grid_filename,
        null=True,
        blank=True
    )
    image_thumb_list = models.ImageField(
        upload_to=get_thumb_list_filename,
        null=True,
        blank=True
    )
    image_detail_popup = models.ImageField(
        upload_to=get_detail_popup_filename,
        null=True,
        blank=True
    )
    upc = models.ForeignKey(
        UPC,
        null=True,
        blank=True
    )

    is_davidson = models.BooleanField(default=False)

    def __unicode__(self):
        if self.upc:
            return "Image for %s: %s" % (self.upc.upc, self.image.name)
        else:
            return "Image: %s" % self.image.name


class ProductListingImage(models.Model):
    image = models.ImageField(
        upload_to=get_detail_popup_filename,
        blank=True,
    )
    image_thumbnail = models.ImageField(
        upload_to=get_thumb_list_filename,
        blank=True,
    )
    product = models.ForeignKey(
        'Product',
        null=True,
        blank=True,
    )

    def __unicode__(self):
        if self.upc:
            return "Image for %s: %s" % (self.product.upc.upc, self.image.name)
        else:
            return "Image: %s" % self.image.name

    @property
    def is_davidson(self):
        return False

    @property
    def upc(self):
        return self.product.upc


class Product(models.Model):
    class Meta:
        db_table = 'products'
    
    user_profile = models.ForeignKey(UserProfile, null=True, blank=True, default=None)
    upc = models.ForeignKey(UPC, null=True, blank=True, default=None)
    distributor = models.ForeignKey(Distributor)
    item_number = models.CharField(max_length=100, null=True, blank=True, default=None)
    title = models.CharField(max_length=300, null=True, blank=True)
    qty_sold = models.PositiveIntegerField(default = 0)
    price = models.FloatField()
    bulk_tier_prices = models.CharField(max_length=255, null=True, blank=True, default=None)
    qty = models.CharField(max_length=100)
    minimum_order = models.PositiveIntegerField(default=0)
    video_url = models.CharField(max_length=255, null=True, blank=True, default=None)
    salesforce_id = models.CharField(
                        max_length=30,
                        null=True,
                        blank=True
                    )

    is_active = models.BooleanField(default=True)
    dealer_exchange = models.BooleanField(default=False)
    nfa_product = models.BooleanField(default=False)

    @property
    def unique_title(self):
        if self.distributor.name == DEALER_DIRECT:
            if not self.title:
                self.title = self.upc.model
                self.save()
            return self.title
        return self.upc.model
    

    def __unicode__(self):
        return self.upc.model

    def image_url(self):
        davidson = Distributor.objects.get(name="Davidson's")
        if self.distributor == davidson:
           return self.upc.davidson_picture  # return "%s/%s/%s" % (settings.IMAGE_URL, self.distributor.pk, self.upc.davidson_picture)
        return self.upc.picture  # return "%s/%s/%s" % (settings.IMAGE_URL, self.distributor.pk, self.upc.picture)

    @property
    def get_de_image(self):
        image = ProductListingImage.objects.\
                filter(product=self).first()
        return image.image.url
    
    def image(self):
        if self.get_product_image():
            try:
                return "<img src=%s height='50px' width='60px'/>" % self.get_product_image().image.url
            except ValueError:
                return ""
        else:
            return ""
    image.allow_tags = True

    def is_product_available(self):
        """
        Checks if there is an available product.

        A qty with a value of '0' or 'ALLOCATED' will return False.
        Return True otherwise.
        """
        return not (self.qty in ('0', 'ALLOCATED'))

    def get_product_image(self):
        davidson = Distributor.objects.get(name="Davidson's")
        if self.upc:
            if self.distributor == davidson:
                ret = ProductImage.objects.filter(upc=self.upc, is_davidson=True).order_by('-pk').first()
                if not ret:
                    # if davidson has no productimage,
                    # look for others with the same UPC and use that instead
                    ret = ProductImage.objects.filter(upc=self.upc, is_davidson=False).order_by('-pk').first()
                return ret
            else:
                return ProductImage.objects.filter(upc=self.upc, is_davidson=False).order_by('-pk').first()
        return None

    def get_thumb_grid_image_url(self):
        pi = self.get_product_image()
        if pi and pi.image_thumb_grid:
            return pi.image_thumb_grid.url
        return 'noimage404'

    def get_thumb_list_image_url(self):
        pi = self.get_product_image()
        if pi and pi.image_thumb_list:
            return pi.image_thumb_list.url
        return 'noimage404'

    def get_detail_popup_image_url(self):
        pi = self.get_product_image()
        if pi and pi.image_detail_popup:
            return pi.image_detail_popup.url
        return 'noimage404'


class ProductTag(models.Model):
    product = models.ManyToManyField(Product, related_name='tags')
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class FeaturedProduct(models.Model):
    """
    Products that are featured in the carousel.
    """
    product = models.ForeignKey(Product)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.product.model

    def image(self):
        return "<img src=%s/%s/%s height='50px' width='60px'/>" % (settings.IMAGE_URL, self.product.distributor.pk, self.product.picture)
    image.allow_tags = True


class ProductCoupon(models.Model):
    coupon_name = models.CharField(max_length=250, unique=True)
    percent_discount = models.FloatField(null=True, blank=True)
    amount_discount = models.FloatField(null=True, blank=True)
    duration = models.CharField(max_length=250, choices=[('Once', 'Once'), ('Forever', 'Forever')])
    redeemed_by = models.ManyToManyField(settings.AUTH_USER_MODEL)
    manufacturer = models.ForeignKey(UserProfile, null=True, blank=True)
    coupon_code = models.CharField(max_length=30, unique=True, blank=True)

    def __unicode__(self):
        return self.coupon_name


# ProductImage
@receiver(post_save, sender=ProductImage)
def product_image_post_save(sender, instance, signal, *args, **kwargs):
    from haystack import site
    # Get related products
    products = Product.objects.filter(upc=instance.upc)
    for product in products:
        site.get_index(Product).update_object(product)
