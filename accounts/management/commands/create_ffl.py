import csv

from django.core.management.base import BaseCommand
from django.conf import settings

from accounts.models import FFL


class Command(BaseCommand):
    args = ""
    help = "Creates FFL objects from csv file."

    def handle(self,*args,**options):
        with open(settings.FFL_CSV_FILE, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            count = 0
            for row in reader:
                count = count + 1
                if count <= 2:
                    continue
                ffl_number = "%s%s%s" % (
                    row[0].strip(),
                    row[1].strip(),
                    row[5].strip()
                )
                phone = row[16]
                if ffl_number and phone:
                    FFL.objects.update_or_create(
                        ffl_number=ffl_number,
                        defaults={
                            'phone':phone
                        }
                    )
