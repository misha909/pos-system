import csv
import os

from django.core.management.base import BaseCommand
from django.conf import settings

from accounts.models import AddressState, AddressZip


class Command(BaseCommand):
    args = ''
    help = 'Import zip data'

    def handle(self, *args, **options):
        state_dict = dict()
        short_to_long_dict = dict()
        with open(os.path.join(settings.BASE_DIR, 'US.txt'), 'rb') as f:
            reader = csv.reader(f, delimiter='\t')

            for row in reader:
                if row[4] not in state_dict.keys():
                    short_to_long_dict[row[4]] = row[3]
                    state_dict[row[4]] = [row[1]]
                else:
                    state_dict[row[4]].append(row[1])

        print "Beginning import..."
        zip_list = list()
        for state, zips in state_dict.items():
            state_obj, created = AddressState.objects.get_or_create(
                short_name=state
            )
            if created:
                state_obj.long_name = short_to_long_dict[state]
                state_obj.save()
            for zip in zips:
                zip_list.append(AddressZip(zip_code=zip, state=state_obj))
        AddressZip.objects.bulk_create(zip_list)
