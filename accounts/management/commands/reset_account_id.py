from django.core.management.base import BaseCommand

from accounts.models import UserProfile


class Command(BaseCommand):
    args = ''
    help = 'Set account_id field on all UserProfiles to None'

    def handle(self, *args, **options):
        for profile in UserProfile.objects.all():
            profile.account_id = None
            profile.save()
