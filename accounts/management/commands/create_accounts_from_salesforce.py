from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from saasapp.salesforce import create_users_from_accounts


class Command(BaseCommand):
    args = ""
    help = "Create accounts from Salesforce."

    def handle(self,*args,**options):
        create_users_from_accounts()