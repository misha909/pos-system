from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from saasapp.salesforce import get_account_approval_by_email


class Command(BaseCommand):
    args = ''
    help = 'Update user activation status based on \
            Salesforce Account vArmory_Account_Approved__C'

    def handle(self, *args, **options):
        queryset = User.objects.all()
        for user in queryset:
            user.is_active = get_account_approval_by_email(user.email)
            user.save()
