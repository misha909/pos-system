from cryptography.fernet import Fernet
from kitchen.text.converters import to_bytes

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q

from accounts.models import UserProfile
from saas.fernet import FERNET_KEY


class Command(BaseCommand):
    args = ""
    help = "Encrypt passwords."

    def handle(self,*args,**options):
        fernet = Fernet(FERNET_KEY)
        users = UserProfile.objects.filter(
            Q(davidson_password__isnull=False) |
            Q(lipseys_password__isnull=False) |
            Q(green_password__isnull=False) |
            Q(rsr_password__isnull=False) |
            Q(zanders_password__isnull=False) |
            Q(camfour_password__isnull=False) |
            Q(sports_password__isnull=False)
        )
        for user in users:
            print "User:", user
            if user.davidson_password:
                user.davidson_password = fernet.encrypt(
                    to_bytes(user.davidson_password)
                )
            if user.lipseys_password:
                user.lipseys_password = fernet.encrypt(
                    to_bytes(user.lipseys_password)
                )
            if user.green_password:
                user.green_password = fernet.encrypt(
                    to_bytes(user.green_password)
                )
            if user.rsr_password:
                user.rsr_password = fernet.encrypt(
                    to_bytes(user.rsr_password)
                )
            if user.zanders_password:
                user.zanders_password = fernet.encrypt(
                    to_bytes(user.zanders_password)
                )
            if user.camfour_password:
                user.camfour_password = fernet.encrypt(
                    to_bytes(user.camfour_password)
                )
            if user.sports_password:
                user.sports_password = fernet.encrypt(
                    to_bytes(user.sports_password)
                )
            user.save()
