from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from saasapp.salesforce import update_shipping_info


class Command(BaseCommand):
    args = ""
    help = "Create accounts from Salesforce."

    def handle(self,*args,**options):
        update_shipping_info()