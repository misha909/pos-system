from django.core.management.base import BaseCommand

from accounts.models import Distributor
from accounts.utils import send_email
import mechanize
import urllib
import urllib2
from pyquery import PyQuery as pq

from django.template.loader import render_to_string


class Command(BaseCommand):
    args = ""
    help = "Verify Distributor Login is still working."

    def handle(self,*args,**options):
        distributors = Distributor.objects.exclude(login='')
        print distributors
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [
            ('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36')
        ]
        login = 'scott@gsadirect.com'
        password = 'gsadirect1'
        for d in distributors:
            print d
            if d.name == "Davidson's":
                url = "http://www11.davidsonsinc.com/Login/Login.aspx"
                br.open(url)
                br.select_form(name="aspnetForm")
                br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$UserName'] = login
                br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$Password'] = password
                req = br.submit()
                new_url = req.geturl()
                find_url = new_url.find('/Dealers/PowerSearch.aspx?')
            elif d.name == 'RSR Group':
                login = '89075'
                password = 'Gsadirect1'
                url = 'https://www.rsrgroup.com/cgi-bin/login?validate'
                user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
                values = {
                    "LOGIN": login,
                    "PASSWORD": password
                }
                headers = {
                    'User-Agent' : user_agent
                }
                data = urllib.urlencode(values)
                req = urllib2.Request(url, data, headers)
                response = urllib2.urlopen(req)
                new_url = response.read()
                find_url = new_url.find('dealer/clsddealerhome')
            elif d.name == 'Zanders':
                password = 'ID8327'
                url = 'https://shop2.gzanders.com/customer/account/login/'
                br.open(url)
                br.select_form(nr=2)  # get third form on the page
                br.form['login[username]'] = login
                br.form['login[password]'] = password
                req = br.submit()
                find_url = (req.geturl() == 'https://shop2.gzanders.com/customer/account/')
            elif d.name == 'Camfour':
                url = 'https://secure.camfour.com/Account/SignOn/'
                br.open(url)
                br.select_form(nr=1)  # get second form on the page
                br.form['cu'] = login
                br.form['pwd'] = password
                req = br.submit()
                text = req.get_data()
                find_url = 'Signed in as' in text
            elif d.name == 'Green Supply':
                url = "https://www.dealerease.net/catalog/cart/login/?ret_id=140067"
                br.open(url)
                br.select_form(name="form1")
                i=0
                for control in br.form.controls:
                    if i == 5:
                        ts = control._value
                        break
                    i = i+1
                br.form['txtLogin'] = login
                br.form['txtPass'] = password
                req = br.submit()
                new_url = req.geturl()
                find_url = new_url.find(ts)
            elif d.name == 'Lipseys':
                url = "http://www.lipseys.com/login.aspx"
                br.open(url)
                br.select_form(name="aspnetForm")
                br.form['ctl00$bodyContent$email'] = login
                br.form['ctl00$bodyContent$pass'] = password
                req = br.submit()

                content = req.get_data()
                dom = pq(content)
                contact_dom = dom('.dealer-econtact')
                if contact_dom:
                    find_url = 2
            if find_url == -1:
                print 'Sign In did not work... send email to Jason'
                text_content = render_to_string(
                    'accounts/email/distributor_error.txt',
                    {'distributor': d}
                )
                html_content = render_to_string(
                    'accounts/email/distributor_error.html',
                    {'distributor': d}
                )
                send_email(
                    text_content=text_content,
                    html_content=html_content,
                    subject='Error in Distributor Login',
                    to_email=['jason@gsadirect.com', 'david@primusmg.com']
                )
