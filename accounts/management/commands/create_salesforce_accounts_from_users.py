from pyforce import pyforce

from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from saasapp.salesforce import (
    create_account,
    create_search_history,
    create_order,
    create_purchase_history
)


class Command(BaseCommand):
    args = ""
    help = "Create Salesforce accounts from vArmory User."

    def handle(self,*args,**options):
        svc = pyforce.Client()
        svc.login(
            settings.SALESFORCE_LOGIN_EMAIL,
            "%s%s" % (settings.SALESFORCE_LOGIN_PASSWORD, settings.SALESFORCE_SECURITY_TOKEN)
        )
        users = User.objects.all()
        for user in users:
            account_id = create_account(user=user, update=False, svc=svc)
            if account_id:
                # Create search histories
                search_histories = user.searchhistory_set.all()
                for search_history in search_histories:
                    create_search_history(
                        search_history=search_history,
                        svc=svc,
                        account_id=account_id
                    )
                # Create Orders
                orders = user.order_set.all()
                for order in orders:
                    order_id = create_order(
                        order=order,
                        svc=svc,
                        account_id=account_id
                    )
                    purchase_histories = order.purchasehistory_set.all()
                    for purchase_history in purchase_histories:
                        create_purchase_history(
                            purchase_history=purchase_history,
                            svc=svc,
                            order_id=order_id
                        )
