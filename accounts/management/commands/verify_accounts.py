from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from accounts.models import Distributor
from accounts.utils import (
    verify_davidson,
    verify_lipseys,
    verify_green,
    verify_rsr
)


class Command(BaseCommand):
    args = ""
    help = "Verify Distributor Account Login."

    def handle(self,*args,**options):
        user = User.objects.get(username='kevinz')
        #d = Distributor.objects.get(name="Davidson's") #Davidson's
        #d = Distributor.objects.get(name='Lipseys') #Lipseys
        #d = Distributor.objects.get(name='Green Supply')
        d = Distributor.objects.get(name='RSR Group') #RSR
        print "Verify:", verify_rsr(user, d.login, d.password)