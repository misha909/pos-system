import requests
import json

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string


class Command(BaseCommand):
    args = ''
    help = 'Create corresponding Dealer Direct accounts for vArmory users'

    def handle(self, *args, **options):
        # api_url = 'http://localhost:8001/api/'
        api_url = 'http://dev-dealerdirect.ffldesign.com/api/'

        response = requests.post(api_url+'user-list/')
        emails = json.loads(response.content)
        queryset = User.objects.all()
        for user in queryset[:10]:
            if user.email not in emails:
                data = {
                    'email': user.email,
                    'password': get_random_string(length=30),
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'varmory_token': user.auth_token.key,
                    'is_active': True,
                    'is_admin': user.is_staff,
                }
                response = requests.post(api_url+'registration/', data=data)
                content = json.loads(response.content)

                token = content.get('token')
                headers = {'Authorization': 'Token '+token}
                data = {'password': user.password}
                response = requests.patch(
                    api_url+'update-user-info/',
                    headers=headers,
                    data=data
                )
                content = json.loads(response.content)
