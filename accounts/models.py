import requests

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.db import models
from django.db.models.signals import post_save, class_prepared
from django.dispatch import receiver
from django.template.loader import render_to_string

from rest_framework.authtoken.models import Token
import xml.etree.ElementTree as ET

from saasapp.constants import (
    CREDIT_CARD,
    BANK_ACCOUNT,
    WALLET_TYPE_CHOICES
)
from saasapp.tsys import (
    get_transaction_key,
    MyAdapter
)

from .constants import (
    PROTOCOL_CHOICES, FILE_TYPE_CHOICES, UPDATE_RESULT_CHOICES,
    CSV, NONE, STEP_CHOICES, REGISTRATION_STEP, FFL_VERIFICATION,
    REGISTER_SUPPLIERS, SUPPLIER_LOGIN, DONE
)

import os
import uuid


def create_banner_file_path(instance, filename):
    file_ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.user_id, file_ext)
    if not os.path.isfile(os.path.join('banners', filename)):
        return os.path.join('banners', filename)
    else:
        return create_file_path(instance, filename)


def create_file_path(instance, filename):
    file_ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), file_ext)
    if not os.path.isfile(os.path.join('images', filename)):
        return os.path.join('images', filename)
    else:
        return create_file_path(instance, filename)


def create_sot_file_path(instance, filename):
    file_ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), file_ext)
    if not os.path.isfile(os.path.join('sot', filename)):
        return os.path.join('sot', filename)
    else:
        return create_sot_file_path(instance, filename)


class ResetPasswordToken(models.Model):
    user = models.OneToOneField(User)
    token = models.CharField(max_length=100, null=True, blank=True)
    valid = models.BooleanField(default=True)

    def generate_token(self):
        self.token = default_token_generator.make_token(self.user)
        self.save()


class BlacklistedPassword(models.Model):
    text = models.CharField(max_length=100)


class RegistrationInfo(models.Model):
    """
    Represents the current step of registration.
    """
    step = models.CharField(
        max_length=14,
        choices=STEP_CHOICES,
        default=REGISTRATION_STEP
    )
    user = models.OneToOneField(User, null=True)

    def __unicode__(self):
        if self.user:
            return self.user.username
        return None

    def move_to_next_step(self):
        if self.step == REGISTRATION_STEP:
            self.step = FFL_VERIFICATION
        elif self.step == FFL_VERIFICATION:
            self.step = REGISTER_SUPPLIERS
        elif self.step == REGISTER_SUPPLIERS:
            self.step = SUPPLIER_LOGIN
        elif self.step == SUPPLIER_LOGIN:
            self.step = DONE
        self.save()


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    logo_url = models.CharField(blank=True, null=True, max_length=300)
    company_name = models.CharField(max_length=100, blank=True, null=True)
    company_banner = models.FileField(upload_to=create_banner_file_path, blank=True, null=True)
    margin = models.FloatField(default=30)
    account_id = models.CharField(max_length=50, blank=True, null=True)
    dealer_direct_token = models.CharField(max_length=80, blank=True, null=True)
    from_salesforce = models.BooleanField(default=False)
    ffl_design_employee = models.BooleanField(default=False)

    # Shipping Info
    address_street = models.CharField(max_length=255, blank=True, null=True)
    address_city = models.CharField(max_length=255, blank=True, null=True)
    address_state = models.CharField(max_length=255, blank=True, null=True)
    address_zip = models.CharField(max_length=255, blank=True, null=True)
    shipping_carrier = models.CharField(max_length=30, blank=True, null=True, default='UPS')
    scheduled_pickups = models.CharField(max_length=30, blank=True, null=True, default='UPS')
    carrier_dropoff = models.CharField(max_length=30, blank=True, null=True, default='UPS')
    is_varmory = models.BooleanField(default=False)
    varmory_account_approved = models.BooleanField(default=False)
    
    # Dealer Direct
    is_dealer_direct = models.BooleanField(default=False)
    #Dealer Exchange
    dealer_exchange = models.BooleanField(default=False)


    
    # FFL
    ffl_number_first_three = models.CharField(max_length=3, blank=True, null=True)
    ffl_number_last_five = models.CharField(max_length=5, blank=True, null=True)
    ffl_expiration_date = models.DateField(blank=True, null=True)
    ffl_premise_address = models.CharField(max_length=100, blank=True, null=True)
    ffl_mailing_address = models.CharField(max_length=100, blank=True, null=True)
    premise_zip_code = models.CharField(max_length=5, blank=True, null=True)
    ffl_is_uploaded = models.BooleanField(default=False)
    ffl_file = models.FileField(upload_to=create_file_path, null=True, blank=True)
    ffl_url = models.CharField(max_length=255, blank=True, null=True)

    # SOT
    sot_is_uploaded = models.BooleanField(default=False)
    sot_approved = models.BooleanField(default=False)
    sot_file = models.FileField(upload_to=create_sot_file_path, null=True, blank=True)
    sot_url = models.CharField(max_length=255, blank=True, null=True)
    
    # Distributor verifications
    davidson = models.BooleanField(default=False)
    davidson_verified = models.BooleanField(default=False)
    lipseys = models.BooleanField(default=False)
    lipseys_verified = models.BooleanField(default=False)
    rsr = models.BooleanField(default=False)
    rsr_verified = models.BooleanField(default=False)
    green = models.BooleanField(default=False)
    green_verified = models.BooleanField(default=False)
    zanders = models.BooleanField(default=False)
    zanders_verified = models.BooleanField(default=False)
    camfour = models.BooleanField(default=False)
    camfour_verified = models.BooleanField(default=False)
    sports = models.BooleanField(default=False)
    sports_verified = models.BooleanField(default=False)
    jerrys = models.BooleanField(default=False)
    jerrys_verified = models.BooleanField(default=False)
    ellet = models.BooleanField(default=False)
    ellet_verified = models.BooleanField(default=False)

    # Distributor Resp Emails
    davidson_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    lipseys_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    green_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    rsr_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    zanders_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    camfour_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    sports_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    jerrys_mail = models.EmailField(default='',max_length=100, blank=True, null=True)
    ellet_mail = models.EmailField(default='',max_length=100, blank=True, null=True)

    # Account Name for each distributor
    davidson_account_name = models.CharField(max_length=50, blank=True, null=True)
    lipseys_account_name = models.CharField(max_length=50, blank=True, null=True)
    green_account_name = models.CharField(max_length=50, blank=True, null=True)
    rsr_account_name = models.CharField(max_length=50, blank=True, null=True)
    zanders_account_name = models.CharField(max_length=50, blank=True, null=True)
    camfour_account_name = models.CharField(max_length=50, blank=True, null=True)
    sports_account_name = models.CharField(max_length=50, blank=True, null=True)
    jerrys_account_name = models.CharField(max_length=50, blank=True, null=True)
    ellet_account_name = models.CharField(max_length=50, blank=True, null=True)

    # Distributor Login
    davidson_login = models.CharField(max_length=50, blank=True, null=True)
    lipseys_login = models.CharField(max_length=50, blank=True, null=True)
    green_login = models.CharField(max_length=50, blank=True, null=True)
    rsr_login = models.CharField(max_length=50, blank=True, null=True)
    zanders_login = models.CharField(max_length=50, blank=True, null=True)
    camfour_login = models.CharField(max_length=50, blank=True, null=True)
    sports_login = models.CharField(max_length=50, blank=True, null=True)
    jerrys_login = models.CharField(max_length=50, blank=True, null=True)
    ellet_login = models.CharField(max_length=50, blank=True, null=True)

    # Distributor Password
    davidson_password = models.CharField(max_length=150, blank=True, null=True)
    lipseys_password = models.CharField(max_length=150, blank=True, null=True)
    green_password = models.CharField(max_length=150, blank=True, null=True)
    rsr_password = models.CharField(max_length=150, blank=True, null=True)
    zanders_password = models.CharField(max_length=150, blank=True, null=True)
    camfour_password = models.CharField(max_length=150, blank=True, null=True)
    sports_password = models.CharField(max_length=150, blank=True, null=True)
    jerrys_password = models.CharField(max_length=150, blank=True, null=True)
    ellet_password = models.CharField(max_length=150, blank=True, null=True)

    def __str__(self):
        return "%s" % self.user.email

    class Meta:
        db_table = 'saasapp_userprofile'
        ordering = ['-user__date_joined', ]

    def date_joined(self):
        if self.user:
            return self.user.date_joined
        return None
    date_joined.admin_order_field = '-user__date_joined'
    
    @property
    def ffl_number(self):
        if self.ffl_number_first_three and self.ffl_number_last_five:
            return "%s%s" % (self.ffl_number_first_three, self.ffl_number_last_five)
        return None

    @property
    def show_supplier_login_popup(self):
        return False
        # return (
        #     (self.davidson_verified and not self.davidson_login) or
        #     (self.lipseys_verified and not self.lipseys_login) or
        #     (self.green_verified and not self.green_login) or
        #     (self.rsr_verified and not self.rsr_login) or
        #     (self.sports_verified and not self.sports_login)
        # )

    @property
    def show_supplier_select(self):
        return not (
            self.davidson_verified or
            self.lipseys_verified or
            self.green_verified or
            self.rsr_verified or
            self.sports_verified or
            self.jerrys_verified or
            self.ellet_verified
        )

    def has_de_profile(self):
        from dealerexchange.models import ShippingInfo
        has_wallet = Wallet.objects.filter(user=self.user).exists()
        has_shipping = ShippingInfo.objects.filter(user_profile=self).exists()
        return has_wallet and has_shipping


class Wallet(models.Model):
    user = models.ForeignKey(User)
    wallet_type = models.CharField(
        max_length=15,
        choices=WALLET_TYPE_CHOICES,
        null=True,
        blank=True
    )
    represented_by_sales_rep = models.BooleanField(
        default=False
    )
    salesforce_account_email = models.EmailField(
        null=True,
        blank=True
    )
    cvv = models.CharField(max_length=4, null=True, blank=True)
    token = models.CharField(max_length=20, null=True, blank=True)
    masked_card_number = models.CharField(max_length=16, null=True, blank=True)
    expiration_month = models.IntegerField(null=True, blank=True)
    expiration_year = models.IntegerField(null=True, blank=True)
    account_name = models.CharField(max_length=255, null=True, blank=True)
    routing_number = models.CharField(max_length=20, null=True, blank=True)
    account_number = models.CharField(max_length=20, null=True, blank=True)

    def __unicode__(self):
        if self.wallet_type == CREDIT_CARD and self.token:
            return self.token
        elif self.wallet_type == BANK_ACCOUNT:
            return self.account_number
        return "Wallet for user %s %s" % (self.user.first_name, self.user.last_name)
    
    @property
    def address_line(self):
        if self.user and self.user.userprofile.ffl_premise_address:
            address = self.user.userprofile.ffl_premise_address.split(" - ")
            return address[0]
        return None

    @property
    def zip(self):
        if self.user and self.user.userprofile.ffl_premise_address:
            address = self.user.userprofile.ffl_premise_address.split(" - ")
            return address[-1]
        return None    

    def create_token(self, card_number):
        if self.wallet_type == CREDIT_CARD:
            transaction_key = get_transaction_key()

            xml_string = render_to_string( 'saas_app/tsys/token_request.xml', {
                'merchant_id': settings.MERCHANT_ID,
                'device_id': settings.DEVICE_ID,
                'developer_id': settings.DEVELOPER_ID,
                'transaction_key': transaction_key,
                'card_number': card_number
            })

            print 'Generate Token Request:', xml_string

            req = requests.Session()
            req.mount('https://', MyAdapter())
            response = req.post("%s/%s" % (
                    settings.TSYS_DOMAIN,
                    settings.TSYS_API_URL
                ),
                headers={'Content-Type': 'text/xml'},
                data=xml_string
            )

            print "Generate Token Response:", response.text

            root = ET.fromstring(response.text)
            api_interface = root.find('TransNox_API_Interface')
            if api_interface is not None:
                token_rs = api_interface.find('TokenRS')
                if token_rs is not None:
                    status = token_rs.find('Status')
                    if status is not None:
                        message = status.find('Message').text
                        if message == 'APPROVED':
                            card_info = token_rs.find('Card_Info')
                            if card_info is not None:
                                token = card_info.find('Token').text
                                self.masked_card_number = card_number[-4:]
                                self.token = token
                                self.save()

    def sale(self, order=None):
        from saasapp.models import TsysLog # To prevent circular imports

        if self.wallet_type == CREDIT_CARD:
            if self.token:
                transaction_key = get_transaction_key()
                xml_string = render_to_string('saas_app/tsys/sale.xml', {
                    'device_id': settings.DEVICE_ID,
                    'transaction_key': transaction_key,
                    'developer_id': settings.DEVELOPER_ID,
                    'wallet': self,
                    'order': order,
                    'purchase_histories': order.purchasehistory_set.all()
                })

                print "Sale Request:\n", xml_string

                req = requests.Session()
                req.mount('https://', MyAdapter())
                response = req.post("%s/%s" % (
                        settings.TSYS_DOMAIN,
                        settings.TSYS_API_URL
                    ),
                    headers={'Content-Type': 'text/xml'},
                    data=xml_string
                )

                print "Sale Response:\n", response.text

                # Check status of Sale method
                root = ET.fromstring(response.text)
                status = root.find('status')
                if status is not None:
                    status = status.text
                    if status == 'PASS':
                        # Check for Partially Approved Case
                        response_message = root.find('responseMessage').text
                        success = True
                        if response_message == 'Partially Approved':
                            # Do a Void Transaction
                            transaction_id = root.find('transactionID').text
                            processed_amount = root.find('processedAmount').text
                            success = self.void(order=order, transaction_id=transaction_id, processed_amount=processed_amount)
                        TsysLog.objects.create(
                            name='Sale',
                            request=xml_string,
                            response=response.text,
                            is_error=not success
                        )
                        return success
                    else:
                        TsysLog.objects.create(
                            name='Sale',
                            request=xml_string,
                            response=response.text,
                            is_error=True
                        )
                        raise Exception("Transaction Error: %s" % response.text)

                TsysLog.objects.create(
                    name='Sale',
                    request=xml_string,
                    response=response.text,
                    is_error=True
                )
                
                return False
            else:
                raise Exception("Credit Card number not available.")
        return False
    
    def void(self, order=None, transaction_id=None, processed_amount=None):
        from saasapp.models import TsysLog # To prevent circular imports

        transaction_key = get_transaction_key()
        xml_string = render_to_string('saas_app/tsys/void.xml', {
            'device_id': settings.DEVICE_ID,
            'transaction_key': transaction_key,
            'developer_id': settings.DEVELOPER_ID,
            'order': order,
            'transaction_id': transaction_id,
            'processed_amount': processed_amount,
            'purchase_histories': order.purchasehistory_set.all()
        })

        print "Void Request:\n", xml_string

        req = requests.Session()
        req.mount('https://', MyAdapter())
        response = req.post("%s/%s" % (
                settings.TSYS_DOMAIN,
                settings.TSYS_API_URL
            ),
            headers={'Content-Type': 'text/xml'},
            data=xml_string
        )

        print "Void Response:\n", response.text
        
        # Check status of Sale method
        root = ET.fromstring(response.text)
        status = root.find('status')
        success = False
        if status is not None:
            TsysLog.objects.create(
                name='Void',
                request=xml_string,
                response=response.text,
                is_error=True
            )
            success = status.text == 'PASS'
        TsysLog.objects.create(
            name='Void',
            request=xml_string,
            response=response.text,
            is_error=success
        )
        return success
        

class Distributor(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=200, null=True, blank=True)
    price_url = models.CharField(max_length=200, null=True, blank=True)
    login = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    protocol = models.CharField(
        max_length=8,
        choices=PROTOCOL_CHOICES
    )
    file_type = models.CharField(
        max_length=7,
        choices=FILE_TYPE_CHOICES,
        default=CSV
    )
    file_dir = models.CharField(max_length=200, null=True, blank=True)
    file_name = models.CharField(max_length=200, null=True, blank=True)
    pictures_url = models.CharField(max_length=200, null=True, blank=True)
    last_updated = models.DateTimeField()
    update_result = models.CharField(
        max_length=38,
        choices=UPDATE_RESULT_CHOICES,
        default=NONE
    )
    update_msg = models.TextField()
    parsing_id = models.CharField(max_length=3)
    update_date = models.DateField(null=True, blank=True)

    class Meta:
        db_table = 'distributors'

    def __unicode__(self):
        return self.name


class FFL(models.Model):
    ffl_number = models.CharField(max_length=8, unique=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    

    def __unicode__(self):
        return self.ffl_number


# User Profile Create
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, created, **kwargs):
    profile, created = UserProfile.objects.get_or_create(user=instance)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def longer_username(sender, *args, **kwargs):
    # you can't just do `if sender == django.contrib.auth.models.User`
    # because you have to import the model
    if sender.__name__ == 'User':
        sender._meta.get_field('username').max_length = 100


class AddressState(models.Model):
    short_name = models.CharField(max_length=4)
    long_name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.long_name


class AddressZip(models.Model):
    zip_code = models.CharField(max_length=10)
    state = models.ForeignKey(AddressState, related_name='zip_codes')

    def __unicode__(self):
        return self.zip_code


class_prepared.connect(longer_username)
