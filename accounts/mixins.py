from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

from braces.views import LoginRequiredMixin


class ApprovalRequiredMixin(LoginRequiredMixin):

    def handle_no_permission(self, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('login'))

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return self.handle_no_permission(request)
        elif not request.user.userprofile.varmory_account_approved:
            return HttpResponseRedirect(reverse_lazy('upload_ffl'))
        return super(ApprovalRequiredMixin, self).dispatch(
            request, *args, **kwargs)
