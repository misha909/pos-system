from django.contrib import admin
from django.contrib.auth.models import User

from saasapp.models import SearchHistory
from .models import RegistrationInfo, UserProfile, Distributor, Wallet
from .models import (
    RegistrationInfo,
    UserProfile,
    Distributor,
    BlacklistedPassword
)


def custom_titled_filter(title):
    class Wrapper(admin.RelatedFieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.RelatedFieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'ffl_design_employee', 'date_joined', )
    list_filter = (
        (
            'user__is_active',
            custom_titled_filter('Registered Users')
        ),
        'ffl_design_employee',
    )
    search_fields = ['user__email', 'company_name',]
    fieldsets = (
        (None, {
            'fields': (
                'user',
                'ffl_design_employee',
                'company_name',
                'margin',
                'is_dealer_direct',
                'from_salesforce',
                'account_id',
                'ffl_premise_address',
                'premise_zip_code',
                'ffl_mailing_address',
                'shipping_carrier',
                'scheduled_pickups',
                'varmory_account_approved',
                'ffl_is_uploaded',
                'ffl_file',
                'company_banner',
                )
        }),
        ('Supplier Verification',{
            'classes': ('wide', ),
            'fields': (
                'davidson_verified',
                'lipseys_verified',
                'rsr_verified',
                'green_verified',
                'zanders_verified',
                'camfour_verified',
                'sports_verified',
                'jerrys_verified',
                'ellet_verified'
            )
        }),
        ('Supplier Emails',{
            'classes': ('wide', ),
            'fields': (
                'davidson_mail',
                'lipseys_mail',
                'green_mail',
                'rsr_mail',
                'zanders_mail',
                'camfour_mail',
                'sports_mail',
                'jerrys_mail',
                'ellet_mail'
            )
        }),
        ('Supplier Accounts',{
            'classes': ('wide', ),
            'fields': (
                'davidson_account_name',
                'lipseys_account_name',
                'green_account_name',
                'rsr_account_name',
                'zanders_account_name',
                'camfour_account_name',
                'sports_account_name',
                'jerrys_account_name',
                'ellet_account_name'
            )
        }),
    )

    class Meta:
        model = UserProfile
        widgets = {
            'ffl_premise_address': admin.widgets.AdminTextareaWidget,
            'ffl_mailing_address': admin.widgets.AdminTextareaWidget
        }

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(RegistrationInfo)
admin.site.register(Distributor)
admin.site.register(Wallet)
admin.site.register(BlacklistedPassword)
