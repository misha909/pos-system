FTP = 'ftp'
HTTP = 'http'
PROTOCOL_CHOICES = (
    (FTP, FTP),
    (HTTP, HTTP),
)

CSV = 'csv'
XML = 'xml'
FILE_TYPE_CHOICES = (
    (CSV, CSV),
    (XML, XML),
)

SUCCESS = 'success'
WARNING = 'warning'
ERROR = 'error'
IN_PROGRESS = 'in progress'
NONE = 'none'

UPDATE_RESULT_CHOICES = (
    (SUCCESS, SUCCESS),
    (WARNING, WARNING),
    (ERROR, ERROR),
    (IN_PROGRESS, IN_PROGRESS),
    (NONE, NONE)
)

REGISTRATION_STEP = 'registration'
FFL_VERIFICATION = 'ffl'
REGISTER_SUPPLIERS = 'suppliers'
SUPPLIER_LOGIN = 'supplier_login'
DONE = 'done'

STEP_CHOICES = (
    (REGISTRATION_STEP, REGISTRATION_STEP),
    (FFL_VERIFICATION, FFL_VERIFICATION),
    (REGISTER_SUPPLIERS, REGISTER_SUPPLIERS),
    (SUPPLIER_LOGIN, SUPPLIER_LOGIN),
    (DONE, DONE)
)

DAVIDSONS = "Davidson's"
LIPSEYS = 'Lipseys'
GREEN = 'Green Supply'
RSR = 'RSR Group'
SPORT = "Sport's South"
ELLET = 'Ellet'
DEALER_DIRECT = 'Dealer Direct'
DISTRIBUTORS = (
   (DAVIDSONS, DAVIDSONS),
   (LIPSEYS, LIPSEYS),
   (GREEN, GREEN),
   (RSR, RSR),
   (SPORT, SPORT),
   # (ELLET, ELLET)
)
