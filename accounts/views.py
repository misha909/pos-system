# Python
import urllib
import urllib2
import json
# Django
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.base import View, TemplateView
# custom
from models import UserProfile
# 3rd-party
from cryptography.fernet import Fernet
from kitchen.text.converters import to_bytes
from pyquery import PyQuery as pq
from braces.views import LoginRequiredMixin
from bs4 import BeautifulSoup  # Bot Session Library
import mechanize  # Bot Session Library
from pyforce import pyforce  # Salesforce API Library
import requests  # Bot Session Library
from braces.views import JSONResponseMixin
import xml.etree.ElementTree as ET

from saasapp.models import ExceptionLog
from saasapp.forms import ForgotPasswordForm, ChangeBillingAddressForm
from saasapp.salesforce import create_account
from saas.fernet import FERNET_KEY

from .utils import (
    get_davidson_account_name,
    get_lipseys_account_name,
    get_green_account_name,
    get_zanders_account_name,
    get_camfour_account_name,
    get_sports_account_name,
    get_jerrys_account_name,
    get_ellet_account_name,
    send_intro_email
)
from .decorators import approval_required
from .mixins import ApprovalRequiredMixin
from .forms import SupplierRegForm, SignUpForm, LoginForm


""" Page to redirect when user want to sign up by entering information """
def signup(request):
    return render(request, 'signup1.html')


@login_required
def Logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))


class LoginView(FormView):
    template_name = 'signup1.html'
    form_class = LoginForm
    success_url = reverse_lazy('upload_ffl')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            profile = request.user.userprofile
            if not profile.varmory_account_approved and not profile.ffl_is_uploaded:
                return HttpResponseRedirect(reverse_lazy('upload_ffl'))
            elif profile.ffl_is_uploaded and not profile.varmory_account_approved:
                return HttpResponseRedirect(reverse_lazy('ffl_thank'))
            elif profile.varmory_account_approved and not profile.show_supplier_select:
                return HttpResponseRedirect(reverse_lazy('step-2'))
            return HttpResponseRedirect(reverse_lazy('product-search'))
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        next = request.GET.get('next')
        if next:
            request.session['next'] = next
        return super(LoginView, self).get(request, *args, **kwargs)

    def get_initial(self):
        initial = super(LoginView, self).get_initial()
        initial['username'] = self.request.COOKIES.get('username')
        return initial

    def get_success_url(self):
        next = self.request.session.get('next')
        return next or super(LoginView, self).get_success_url()

    def form_valid(self, form):
        user = form.get_user()
        profile = user.userprofile
        login(self.request, user)
        response = super(LoginView, self).form_valid(form)
        if not profile.varmory_account_approved:
            response = HttpResponseRedirect(reverse_lazy('upload_ffl'))
        elif profile.show_supplier_select:
            response = HttpResponseRedirect(reverse_lazy('step-2'))
        if form.cleaned_data.get('remember'):
            response.set_cookie(
                'username',
                value=form.cleaned_data.get('username'),
                max_age=99999999,
                httponly=True,
            )
        else:
            response.delete_cookie('username')
        return response

    def get_context_data(self, **kwargs):
        kwargs['login_form'] = kwargs['form']
        kwargs['forgot_password_form'] = ForgotPasswordForm()
        kwargs['sign_up_form'] = SignUpForm()
        kwargs['login'] = True
        del kwargs['form']
        return super(LoginView, self).get_context_data(**kwargs)


class SignUpView(FormView):
    template_name = 'signup1.html'
    form_class = SignUpForm
    success_url = reverse_lazy('upload_ffl')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            profile = request.user.userprofile
            if not profile.varmory_account_approved:
                return reverse_lazy('upload_ffl')
            elif profile.show_supplier_select:
                return HttpResponseRedirect(reverse_lazy('step-2'))
            return HttpResponseRedirect(reverse_lazy('product-search'))
        return super(SignUpView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        ticket = request.GET.get('ticket')
        if ticket:
            protocol = 'http://'
            if request.is_secure():
                protocol = 'https://'
            data = {
                'service': protocol + Site.objects.get_current().domain,
                'ticket': ticket
            }
            url = settings.DEALER_DIRECT_URL + 'cas/p3/serviceValidate/'
            response = requests.get(url, params=data)

            if response.status_code == 200:
                try:
                    xml = response.text.replace("'", "\\'").split('\n')[1]
                    root = ET.fromstring(xml)
                    if not root[0].get('code'):
                        user = User.objects.get(username=root[0][0].text)
                        user.backend = 'django.contrib.auth.backends.ModelBackend'
                        login(request, user)
                        return redirect('product-search')
                except Exception, e:
                    ExceptionLog.objects.create(
                        name='CASLogin',
                        exception=str(e)
                    )
        return super(SignUpView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        verified = form.cleaned_data['verified']
        new_user = form.create_new_user()
        if verified.get('company'):
            self.request.session['company'] = verified['company']
            # send_intro_email(to_email=[new_user.email])

        user = authenticate(
            username=new_user.username,
            password=form.cleaned_data['password']
        )
        login(self.request, user)
        return super(SignUpView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        initial = self.request.COOKIES.get('username')
        kwargs['sign_up_form'] = kwargs['form']
        kwargs['forgot_password_form'] = ForgotPasswordForm()
        kwargs['login_form'] = LoginForm(initial={'username': initial})
        del kwargs['form']
        return super(SignUpView, self).get_context_data(**kwargs)


class SupplierRegView(ApprovalRequiredMixin, FormView):
    template_name = 'signup3.html'
    form_class = SupplierRegForm
    success_url = reverse_lazy('step-3')

    def dispatch(self, request, *args, **kwargs):
        request.session['id'] = request.user.id
        return super(SupplierRegView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        distributors = form.cleaned_data['distributors']
        self.request.session['distributors'] = distributors
        return super(SupplierRegView, self).form_valid(form)


@login_required
@approval_required
def supplier_verification(request):
    user = request.user
    d = request.session.get('distributors')
    if d is None:
        return HttpResponseRedirect(reverse_lazy('step-2'))

    user_profile = UserProfile.objects.get(user=user)
    dist_checks = {}
    dist_checks['id'] = request.user.id

    if "Davidson's" in d:
        user_profile.davidson = True
        dist_checks.update({'davidson': True, 'davidson_success': ''})
    if 'Lipseys' in d:
        user_profile.lipseys = True
        dist_checks.update({'lipseys': True, 'lipseys_success': ''})
    if 'RSR Group' in d:
        user_profile.rsr = True
        dist_checks.update({'rsr': True, 'rsr_success': ''})
    if 'Green Supply' in d:
        user_profile.green = True
        dist_checks.update({'green': True, 'greens_success': ''})
    if 'Camfour' in d:
        user_profile.camfour = True
        dist_checks.update({'camfour': True, 'camfour_success': ''})
    if "Sport's South" in d:
        user_profile.sports = True
        dist_checks.update({'sports': True, 'sports_success': ''})
    if 'Zanders' in d:
        user_profile.zanders = True
        dist_checks.update({'zanders': True, 'zanders_success': ''})
    if "Jerry's Sports Center" in d:
        user_profile.jerrys = True
        dist_checks.update({'jerrys': True, 'jerrys_success': ''})
    if 'Ellet' in d:
        user_profile.ellet = True
        dist_checks.update({'ellet': True, 'ellet_success': ''})
    user_profile.save()

    # print "Dist Checks:", dist_checks
    # request.session['dist_checks'] = dist_checks

    userProfileData = UserProfile.objects.get(user__id=user.id)
    if request.session.get('company'):
        userProfileData.company_name = request.session['company']
    userProfileData.save()
    # Salesforce leads are generated from below code
    if userProfileData.company_name and not userProfileData.from_salesforce:
        create_account(user=user)

    del request.session['distributors']
    request.session.modified = True
    return render(request, 'signup4.html', {
        'distributors': json.dumps(dist_checks)
    })


# Auto login (bot) for Davidson
@csrf_exempt
def davidson(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']

        settings_source = request.POST.get('setting_id','')

        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=profile_data.id)

        if uname == '' or password == '':
            error= 1
            return HttpResponse(error)

        username = uname
        password = password

        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [
            ('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36')
        ]
        url = "http://www11.davidsonsinc.com/Login/Login.aspx"

        br.open(url)
        br.select_form(name="aspnetForm")
        br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$UserName'] = username
        br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$Password'] = password
        req = br.submit()
        new_url = req.geturl()
        find_url = new_url.find('/Dealers/PowerSearch.aspx?')

        if find_url != -1:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                davidson_verified=True,
                davidson_account_name=get_davidson_account_name(br, req),
                davidson_login=username,
                davidson_password=fernet.encrypt(to_bytes(password))
                #davidson_password=password
            )

            if settings_source:
                davidson_message = "Congratulations! Your account for Davidson's has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:
                return HttpResponse('success')
        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'davidson': True})

# Auto login (bot) for Lipseys
@csrf_exempt
def lipseys(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('setting_id', '')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            error = 1
            return HttpResponse(error)

        URL = "https://www.lipseys.com/login.aspx?ref=https%3a%2f%2fwww.lipseys.com%2fDefault.aspx"
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36"}

        username = uname
        password = password

        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [
            ('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36')
        ]
        url = "http://www.lipseys.com/login.aspx"

        br.open(url)
        br.select_form(name="aspnetForm")
        br.form['ctl00$bodyContent$email'] = username
        br.form['ctl00$bodyContent$pass'] = password
        req = br.submit()

        content = req.get_data()
        dom = pq(content)
        contact_dom = dom('.dealer-econtact')

        if contact_dom:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                lipseys_verified=True,
                lipseys_account_name=get_lipseys_account_name(br, req),
                lipseys_login=username,
                lipseys_password=fernet.encrypt(to_bytes(password))
                #lipseys_password=password
            )

            if settings_source:
                lipseys_message = "Congratulations! Your account for Lipsey's has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:
                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'lipseys': True})

@csrf_exempt
def green(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            # error = "please enter User Name and Password"
            error =1
            if settings_source:
                return HttpResponse(error)
            else:
                return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = "https://www.dealerease.net/catalog/cart/login/?ret_id=140067"

        br.open(url)
        br.select_form(name="form1")
        i=0
        for control in br.form.controls:
            if i == 5:
                ts = control._value
                break
            i = i+1
        br.form['txtLogin']=username
        br.form['txtPass']=password
        req = br.submit()
        new_url = req.geturl()
        print new_url
        find_url = new_url.find(ts)

        if find_url != -1:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=request.POST['id']).update(is_active=True)
            UserProfile.objects.filter(user_id=request.POST['id']).update(
                green_verified=True,
                green_account_name=get_green_account_name(br, req),
                green_login=username,
                green_password=fernet.encrypt(to_bytes(password))
                #green_password=password
            )

            if settings_source:
                greens_message = "Congratulations! Your account for Green Supply has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            # error = "Please enter the correct User Name or Password"
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'green': True})

@csrf_exempt
def rsr(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            # error = "please enter User Name and Password"
            error = 1
            return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.set_handle_redirect(mechanize.HTTPRedirectHandler)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = "http://www.rsrgroup.com/catalog/login"

        res = br.open(url)
        for form in br.forms():
            br.form = form
            break

        br.form['account_num'] = username
        br.form['password'] = password

        req = br.submit()
        new_url = req.geturl()

        # Go to home page
        url = 'http://www.rsrgroup.com/catalog/'
        res = br.open(url)
        text = res.get_data()
        find_url = "Log Out" in text

        if find_url:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                rsr_verified=True,
                rsr_account_name=username,
                rsr_login=username,
                rsr_password=fernet.encrypt(to_bytes(password))
                #rsr_password=password
            )
            if settings_source:
                greens_message = "Congratulations! Your account for RSR Group has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'rsr': True})


@csrf_exempt
def sports(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            # error = "please enter User Name and Password"
            error = 1
            return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = 'http://theshootingwarehouse.com/index.php/login'

        br.open(url)
        index = 0
        for form in br.forms():
            if index == 1:
                br.form = form
                break
            index = index + 1
        br.form['uName'] = username
        br.form['uPassword'] = password

        req = br.submit()
        text = req.get_data()
        find_url = 'Log Out' in text

        if find_url:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                sports_verified=True,
                sports_account_name=username,
                sports_login=username,
                sports_password=fernet.encrypt(to_bytes(password))
                #sports_password=password
            )

            if settings_source:
                sports_message = "Congratulations! Your account for Sport's South has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            # error = "Please enter the correct User Name or Password"
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'sports': True})


@csrf_exempt
def camfour(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=request.user.id)
            User_Profile = UserProfile.objects.get(user=request.user.id)

        if uname == '' or password == '':
            error = 1
            return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = 'https://secure.camfour.com/Account/SignOn/'

        br.open(url)
        br.select_form(nr=1)  # get second form on the page

        br.form['cu']=username
        br.form['pwd']=password
        req = br.submit()
        text = req.get_data()
        print 'Signed in as' in text
        find_url = 'Signed in as' in text

        if find_url:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                camfour_verified=True,
                camfour_account_name=get_camfour_account_name(br, req),
                camfour_login=username,
                camfour_password=fernet.encrypt(to_bytes(password))
                #camfour_password=password
            )

            if settings_source:
                camfour_message = "Congratulations! Your account for Camfour has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            # error = "Please enter the correct User Name or Password"
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'camfour': True})


@csrf_exempt
def zanders(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            # error = "please enter User Name and Password"
            error = 1
            return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = 'https://shop2.gzanders.com/customer/account/login/'

        br.open(url)
        br.select_form(nr=2)  # get third form on the page

        br.form['login[username]']=username
        br.form['login[password]']=password
        req = br.submit()
        find_url = (req.geturl() == 'https://shop2.gzanders.com/customer/account/')

        if find_url:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                zanders_verified=True,
                zanders_account_name=get_zanders_account_name(br, req),
                zanders_login=username,
                zanders_password=fernet.encrypt(to_bytes(password))
                #zanders_password=password
            )

            if settings_source:
                zanders_message = "Congratulations! Your account for Gzanders has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'zanders': True})


@csrf_exempt
def jerrys(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            # error = "please enter User Name and Password"
            error = 1
            return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = 'https://store.jerryssportscenter.com/storefrontCommerce/'

        br.open(url)
        br.select_form('loginForm')

        br.form['usr_name'] = username
        br.form['usr_password'] = password
        req = br.submit()
        text = req.get_data()
        find_url = 'If you are not' in text

        if find_url:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                jerrys_verified=True,
                jerrys_account_name=get_jerrys_account_name(br, req),
                jerrys_login=username,
                jerrys_password=fernet.encrypt(to_bytes(password))
            )

            if settings_source:
                sports_message = "Congratulations! Your account for Jerry's Sports has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            # error = "Please enter the correct User Name or Password"
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'jerrys': True})


@csrf_exempt
def ellet(request):
    if request.POST:
        uname = request.POST['uname']
        password = request.POST['password']
        id = request.session['id']
        settings_source = request.POST.get('settings','')
        if settings_source:
            profile_data = User.objects.get(id=id)
            User_Profile = UserProfile.objects.get(user__id=id)

        if uname == '' or password == '':
            # error = "please enter User Name and Password"
            error = 1
            return HttpResponse(error)

        username = uname
        password = password

        br=mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
        url = 'https://store.ellettbrothers.com/storefrontCommerce/login.do'

        br.open(url)
        br.select_form('loginForm')

        br.form['usr_name'] = username
        br.form['usr_password'] = password
        req = br.submit()
        text = req.get_data()
        find_url = 'If you are not' in text

        if find_url:
            fernet = Fernet(FERNET_KEY)
            User.objects.filter(id=id).update(is_active=True)
            UserProfile.objects.filter(user__id=id).update(
                ellet_verified=True,
                ellet_account_name=get_ellet_account_name(br, req),
                ellet_login=username,
                ellet_password=fernet.encrypt(to_bytes(password))
            )

            if settings_source:
                sports_message = "Congratulations! Your account for Ellet has been verified to see the effect please logout and login"
                return HttpResponse('success')
            else:

                return HttpResponse('success')

        if settings_source:
                error = 2
                return HttpResponse(error)
        else:
            # error = "Please enter the correct User Name or Password"
            error = 2
            return HttpResponse(error)

    return render(request, 'distributor_login.html', {'ellet': True})


class UpdateUserBillingAddress(JSONResponseMixin, View):

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = ChangeBillingAddressForm(request.POST)
            if form.is_valid():
                message = form.action(request.user)
                return self.render_json_response({
                    'success': 'success',
                    'message': message,
                })
