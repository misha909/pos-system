from django import forms
from django.core.validators import RegexValidator, validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.hashers import make_password

from products.utils import verify_ffl
from saasapp.salesforce import create_account
from .constants import DISTRIBUTORS
from .models import UserProfile, BlacklistedPassword
import re


class UsernameField(forms.CharField):

    def validate(self, value):
        super(UsernameField, self).validate(value)
        validate_email(value)
        check_unique = User.objects.filter(username=value)
        if check_unique.count() > 0:
            raise ValidationError(
                ('E-mail %(value)s is already registered to an account.'),
                params={'value': value},
                code='existing'
            )


class PasswordField(forms.CharField):
    validator = RegexValidator(regex=r'^[A-Za-z0-9@!()\/\\<>\.\-\?\'\_{}[\];|#$*%^&+=]+$')
    default_validators = [validator]

    def __init__(self, *args, **kwargs):
        kwargs['min_length'] = 8
        kwargs['error_messages'] = {
            'required': 'Please enter your password.',
            'invalid': 'Ensure password consists of letters or numbers.'
        }
        super(PasswordField, self).__init__(*args, **kwargs)

    def validate(self, value):
        super(PasswordField, self).validate(value)
        if BlacklistedPassword.objects.filter(text=value).count() > 0:
            raise ValidationError(
                ('Enter a less common password.'),
                code='common'
            )


class AuthenticationFormApproveFirst(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if user.userprofile.ffl_is_uploaded and not user.userprofile.varmory_account_approved:
            raise forms.ValidationError(
                'You cannot login until your account is approved.',
                code='inactive',
            )


class SupplierRegForm(forms.Form):
    distributors = forms.MultipleChoiceField(
        choices=DISTRIBUTORS,
        widget=forms.CheckboxSelectMultiple(),
        error_messages={
            'required': 'Select at least one distributor',
        }
    )


class LoginForm(AuthenticationFormApproveFirst):
    username = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={
                'name': 'username',
                'id': 'username',
                'class': 'username',
                'placeholder': 'Email address'
            }
        ),
        label='Email address',
        error_messages={
            'required': 'Please enter your e-mail address.'
        }
    )
    password = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput(
            attrs={
                'name': 'password',
                'id': 'password',
                'class': 'password',
                'placeholder': 'Password'
            }
        ),
        label='Password',
        error_messages={
            'required': 'Please enter your password.'
        }
    )
    remember = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'checked': 'checked',
                'name': 'remember',
            }
        ),
    )


class SignUpForm(forms.Form):
    first_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'name': 'first_name',
                'id': 'first_name',
                'placeholder': 'First Name',
            }
        ),
        label='First name',
        error_messages={
            'required': 'Please enter your first name.'
        }
    )
    last_name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'name': 'last_name',
                'id': 'last_name',
                'placeholder': 'Last Name',
            }
        ),
        label='Last name',
        error_messages={
            'required': 'Please enter your last name.'
        }
    )
    username = UsernameField(
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'name': 'username',
                'id': 'username',
                'placeholder': 'Email address'
            }
        ),
        label='Email address',
        error_messages={
            'required': 'Please enter your e-mail address.',
            'existing': 'E-mail address is already registered',
        }
    )
    ffl_number_first_three = forms.CharField(
        min_length=3,
        max_length=4,
        widget=forms.TextInput(
            attrs={
                'name': 'ffl_number_first_three',
                'id': 'ffl_number_first_three',
                'placeholder': 'First 3 of FFL',
            }
        ),
        label='First three digits of FFL',
        error_messages={
            'required': 'Please enter the first three digits of your FFL.',
            'min_length': 'Ensure first FFL field has three (3) digits.'
        }
    )
    ffl_number_last_five = forms.CharField(
        min_length=5,
        max_length=5,
        widget=forms.TextInput(
            attrs={
                'name': 'ffl_number_last_five',
                'id': 'ffl_number_last_five',
                'placeholder': 'Last 5 of FFL',
            }
        ),
        label='Last five digits of FFL',
        error_messages={
            'required': 'Please enter the last five digits of your FFL.',
            'min_length': 'Ensure second FFL field has five (5) digits.',
        }
    )
    premise_zip_code = forms.CharField(
        max_length=5,
        widget=forms.TextInput(
            attrs={
                'name': 'premise_zip_code',
                'id': 'premise_zip_code',
                'placeholder': 'Premise Zip Code',
            },
        ),
        label='Premise zip code',
        error_messages={
            'required': 'Please enter premise zip code.'
        }
    )
    password = PasswordField(
        max_length=100,
        widget=forms.PasswordInput(
            attrs={
                'name': 'password',
                'id': 'password',
                'placeholder': 'Password'
            }
        ),
        error_messages={
            'required': 'Please enter your password.',
            'invalid': 'Ensure password consists of letters or numbers.',
            'min_length': 'Ensure password has at least eight (8) characters.',
        }
    )
    password_confirm = PasswordField(
        max_length=100,
        widget=forms.PasswordInput(
            attrs={
                'name': 'password',
                'id': 'password',
                'placeholder': 'Confirm password'
            }
        ),
        error_messages={
            'required': 'Please confirm your password.',
            'invalid': 'Ensure password consists of letters or numbers.',
            'min_length': 'Ensure password has at least eight (8) characters.',
        }
    )

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        if not self.errors:
            first3 = cleaned_data.get('ffl_number_first_three')
            ffl_number_first_three = ''.join(re.findall(r'[0-9]', first3))
            last5 = cleaned_data.get('ffl_number_last_five')
            first1 = ffl_number_first_three[0]
            first2 = ffl_number_first_three[1:]
            invalid = 'Invalid FFL credentials.'
            fail = 'FFL EZ check is not responding. Please try again.'
            unique = 'FFL number already registered with vArmory.'
            match = 'Passwords must match.'

            if UserProfile.objects.filter(
                ffl_number_first_three=ffl_number_first_three,
                ffl_number_last_five=last5
            ).exists():
                self.add_error(
                    'ffl_number_first_three',
                    ValidationError(unique, code='unique')
                )
                return
            if cleaned_data.get('password') != cleaned_data.get('password_confirm'):
                self.add_error(
                        'password_confirm',
                        ValidationError(match, code='match')
                        )
                return
            try:
                verified = verify_ffl(first1, first2, last5)
                if not verified.get('verified'):
                    self.add_error(
                        'ffl_number_first_three',
                        ValidationError(invalid, code='invalid')
                    )
                self.cleaned_data['verified'] = verified
            except Exception, e:
                print e
                self.add_error(
                    'ffl_number_first_three',
                    ValidationError(fail, code='fail')
                )
        else:
            self.add_error(
                None,
                ValidationError(
                    'FFL will be verified once all other entries are valid.'
                )
            )

    def create_new_user(self):
        data = self.cleaned_data
        first3 = data['ffl_number_first_three']
        verified = data['verified']

        new_user = User.objects.create(
            first_name=data['first_name'],
            last_name=data['last_name'],
            username=data['username'],
            email=data['username'],
            password=make_password(data['password']),
        )
        new_user.is_active = False
        new_user.save()
        userprofile = UserProfile(
            user=new_user,
            ffl_number_first_three='%s%s' % (first3[0], first3[2:]),
            ffl_number_last_five=data['ffl_number_last_five'],
            ffl_expiration_date=verified['expiration_date'],
            ffl_premise_address=verified['premise_address'],
            ffl_mailing_address=verified['mailing_address'],
            company_name=verified['company'],
            premise_zip_code=data['premise_zip_code'],
            is_varmory=True
        )
        userprofile.save()
        # Salesforce leads are generated from the code below
        if userprofile.company_name and not userprofile.from_salesforce:
            create_account(user=new_user)
        return new_user
