from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse_lazy


def approval_required(function):
    dec = user_passes_test(
        lambda u: u.userprofile.varmory_account_approved,
        login_url=reverse_lazy('upload_ffl'),
        redirect_field_name=None
    )
    return dec(function)
