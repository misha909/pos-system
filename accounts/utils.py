import urllib
import urllib2
import mechanize
import requests

from pyquery import PyQuery as pq
from bs4 import BeautifulSoup
from cryptography.fernet import Fernet
from kitchen.text.converters import to_bytes

from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string

from accounts.models import ResetPasswordToken, AddressZip, AddressState
from saasapp.models import SearchHistory
from saasapp.constants import REGIONS, SALES_REPS
from saas.fernet import FERNET_KEY


def get_state_from_zip(zip):
    zip_obj = AddressZip.objects.get(zip_code=zip)
    state_obj = AddressState.objects.get(pk=zip_obj.state_id)
    return state_obj.short_name or ''


def get_davidson_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    """
    br = browser
    req = request

    # Retrieve Account Name
    content = req.get_data()
    dom = pq(content)
    link_element = dom("#linkai")
    link_element_url = link_element.attr('href')

    for link in br.links():
        if link.url == link_element_url:
            break

    res = br.follow_link(link)
    content = res.get_data()
    dom = pq(content)
    scripts = dom('script')
    for item in scripts.items():
        # Get source of iframe in script since initial value of src is ""
        if item.text().startswith("var pageURL = "):
            text = item.text()
            # Get value of pageURL
            page_url_start = text.find("var pageURL")
            page_url = text[page_url_start:]
            page_url_end = page_url.find(';')
            page_url = page_url[:page_url_end]
            # Get value of pageURL within quotes
            page_url_start = page_url.find('"')
            page_url_end = page_url.find('"', page_url_start + 1)
            page_url = page_url[page_url_start + 1:page_url_end]

            # Get value of dealer id
            dealer_id_start = text.find("var dealer_id")
            dealer_id = text[dealer_id_start:]
            dealer_id_end = dealer_id.find(';')
            dealer_id = dealer_id[:dealer_id_end]
            # Get value of dealer_id within quotes
            dealer_id_start = dealer_id.find('"')
            dealer_id_end = dealer_id.find('"', dealer_id_start + 1)
            dealer_id = dealer_id[dealer_id_start + 1:dealer_id_end]

            iframe_src = "%s.aspx?&pid=%s" % (page_url, dealer_id)
            iframe_url = "http://www11.davidsonsinc.com/MyAccount/%s" % iframe_src

            br.open(iframe_url)
            content = br.response().read()
            dom = pq(content)
            element = dom("span#lblCusID")
            return element.text()
    return None


def get_lipseys_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    """
    br = browser
    req = request

    # Retrieve Account Name
    target_url = "summary.aspx"
    for link in br.links():
        if link.url == target_url:
            break

    res = br.follow_link(link)
    content = res.get_data()
    dom = pq(content)
    table = dom("table.checkoutOrderDetail")
    td = table('td.detailLineItem:first')
    if td:
        values = td.text().split(' ')
        return values[0]

    return None


def get_green_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    """
    br = browser
    req = request

    content = req.get_data()
    dom = pq(content)
    element = dom("table.main_table .tpheader .mnu_top table.std td.navleft span small:first")
    if element:
        account_name = element.text()
        account_name_start = account_name.find("#")
        account_name_end = account_name.find(")")
        account_name = account_name[account_name_start + 1:account_name_end]
        return account_name
    return None


def get_zanders_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    """
    br = browser
    req = request

    content = req.get_data()
    dom = pq(content)
    element = dom("p.welcome-msg:first")
    if element:
        account_name = element.text()
        account_name_start = account_name.find(" ")
        account_name_end = account_name.find(" !")
        account_name = account_name[account_name_start + 1:account_name_end]
        return account_name
    return None


def get_camfour_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    """
    br = browser
    req = request

    content = req.get_data()
    dom = pq(content)
    element = dom("span.name:first")
    if element:
        account_name = element.text()
        return account_name
    return None


def get_sports_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    
    TODO:
    - Update function for sports.
      The current implementation is for Jerry's instead of theshootingwarehouse.
    """
    br = browser
    req = request

    content = req.get_data()
    dom = pq(content)
    details = dom(".userSessionDetail")

    ctr = 0
    for item in details.items():
        if ctr == 1:
            text = item.text()
            return text.split(' ')[0]
        ctr = ctr + 1
    return None

def get_jerrys_account_name(browser, request):
    """
    browser is the mechanize browser used during verification.
    request is the latest request from browser.
    """
    br = browser
    req = request

    content = req.get_data()
    dom = pq(content)
    details = dom(".userSessionDetail")

    ctr = 0
    for item in details.items():
        if ctr == 1:
            text = item.text()
            return text.split(' ')[0]
        ctr = ctr + 1
    return None


def get_ellet_account_name(browser, request):
    """
    The dom elements for Jerrys and Ellet are the same.
    A separate function is created for naming purposes and
    if there are changes in the future.
    """
    return get_jerrys_account_name(browser, request)


def verify_davidson(user=None, username=None, password=None):
    if not user and not username and not password:
        return False

    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [
        ('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36')
    ]
    url = "http://www11.davidsonsinc.com/Login/Login.aspx"

    br.open(url)
    br.select_form(name="aspnetForm")
    br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$UserName'] = username
    br.form['ctl00$ContentPlaceHolderNavPane$NavLoginForm$UserLogin$Password'] = password
    req = br.submit()
    new_url = req.geturl()
    find_url = new_url.find('/Dealers/PowerSearch.aspx?')

    if find_url != -1:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.davidson = True
        user_profile.davidson_verified = True
        user_profile.davidson_account_name = get_davidson_account_name(br, req)
        user_profile.davidson_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.davidson_password = fernet.encrypt(to_bytes(password))
        #user_profile.davidson_password = password
        user_profile.save()
        return True

    return False


def verify_lipseys(user=None, username=None, password=None):
    if not user and not username and not password:
        return False

    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [
        ('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36')
    ]
    url = "http://www.lipseys.com/login.aspx"

    br.open(url)
    br.select_form(name="aspnetForm")
    br.form['ctl00$bodyContent$email'] = username
    br.form['ctl00$bodyContent$pass'] = password
    req = br.submit()

    content = req.get_data()
    dom = pq(content)
    contact_dom = dom('.dealer-econtact')

    if contact_dom:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.lipseys = True
        user_profile.lipseys_verified = True
        user_profile.lipseys_account_name = get_lipseys_account_name(br, req)
        user_profile.lipseys_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.lipseys_password = fernet.encrypt(to_bytes(password))
        #user_profile.lipseys_password = password
        user_profile.save()
        return True

    return False


def verify_green(user=None, username=None, password=None):
    if not user and not username and not password:
        return False

    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [
        ('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')
    ]
    url = "https://www.dealerease.net/catalog/cart/login/?ret_id=140067"

    br.open(url)
    br.select_form(name="form1")
    i=0
    for control in br.form.controls:
        if i == 5:
            ts = control._value
            break
        i = i+1
    br.form['txtLogin']=username
    br.form['txtPass']=password
    req = br.submit()
    new_url = req.geturl()
    find_url = new_url.find(ts)

    if find_url != -1:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.green = True
        user_profile.green_verified = True
        user_profile.green_account_name = get_green_account_name(br, req)
        user_profile.green_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.green_password = fernet.encrypt(to_bytes(password))
        #user_profile.green_password = password
        user_profile.save()
        return True
    return False


def verify_rsr(user=None, username=None, password=None):
    if not user and not username and not password:
        return False

    br=mechanize.Browser()
    br.set_handle_robots(False)
    br.set_handle_redirect(mechanize.HTTPRedirectHandler)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
    url = "http://www.rsrgroup.com/catalog/login"

    res = br.open(url)
    for form in br.forms():
        br.form = form
        break

    br.form['account_num'] = username
    br.form['password'] = password
    
    req = br.submit()
    new_url = req.geturl()
    
    # Go to home page
    url = 'http://www.rsrgroup.com/catalog/'
    res = br.open(url)
    text = res.get_data()
    find_url = "Log Out" in text

    if find_url:
        user.is_active = True
        user.save()
        user_profile = user.userprofile
        user_profile.rsr = True
        user_profile.rsr_verified = True
        user_profile.rsr_account_name = username
        user_profile.rsr_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.rsr_password = fernet.encrypt(to_bytes(password))
        #user_profile.rsr_password = password
        user_profile.save()
        return True
    return False


def verify_sports(user=None, username=None, password=None):
    if not user and not username and not password:
        return False
    
    br=mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
    url = 'http://theshootingwarehouse.com/index.php/login'
    
    br.open(url)
    index = 0
    for form in br.forms():
        if index == 1:
            br.form = form
            break
        index = index + 1
    br.form['uName'] = username
    br.form['uPassword'] = password

    req = br.submit()
    text = req.get_data()
    find_url = 'Log Out' in text
    
    if find_url:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.sports = True
        user_profile.sports_verified = True
        user_profile.sports_account_name = username
        user_profile.sports_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.sports_password = fernet.encrypt(to_bytes(password))
        #user_profile.sports_password = password
        user_profile.save()
        return True
    return False


def verify_camfour(user=None, username=None, password=None):
    if not user and not username and not password:
        return False

    br=mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
    url = 'https://secure.camfour.com/Account/SignOn/'

    br.open(url)
    br.select_form(nr=1)  # get second form on the page

    br.form['cu']=username
    br.form['pwd']=password
    req = br.submit()
    text = req.get_data()
    find_url = 'Signed in as' in text

    if find_url:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.camfour = True
        user_profile.camfour_verified = True
        user_profile.camfour_account_name = get_camfour_account_name(br, req)
        user_profile.camfour_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.camfour_password = fernet.encrypt(to_bytes(password))
        #user_profile.camfour_password = password
        user_profile.save()
        return True
    return False


def verify_zanders(user=None, username=None, password=None):
    if not user and not username and not password:
        return False
    br=mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
    url = 'https://shop2.gzanders.com/customer/account/login/'

    br.open(url)
    br.select_form(nr=2)  # get third form on the page

    br.form['login[username]']=username
    br.form['login[password]']=password
    req = br.submit()
    find_url = (req.geturl() == 'https://shop2.gzanders.com/customer/account/')

    if find_url:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.zanders = True
        user_profile.zanders_verified = True
        user_profile.zanders_account_name = get_zanders_account_name(br, req)
        user_profile.zanders_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.zanders_password = fernet.encrypt(to_bytes(password))
        #user_profile.zanders_password = password
        user_profile.save()
        return True
    return False


def verify_jerrys(user=None, username=None, password=None):
    if not user and not username and not password:
        return False
    
    br=mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
    url = 'https://store.jerryssportscenter.com/storefrontCommerce/'
    
    br.open(url)
    br.select_form('loginForm')

    br.form['usr_name'] = username
    br.form['usr_password'] = password
    req = br.submit()
    text = req.get_data()
    find_url = 'If you are not' in text
    
    if find_url:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.jerrys = True
        user_profile.jerrys_verified = True
        user_profile.jerrys_account_name = get_jerrys_account_name(br, req)
        user_profile.jerrys_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.jerrys_password = fernet.encrypt(to_bytes(password))
        user_profile.save()
        return True
    return False


def verify_ellet(user=None, username=None, password=None):
    if not user and not username and not password:
        return False
    
    br=mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11')]
    url = 'https://store.ellettbrothers.com/storefrontCommerce/login.do'
    
    br.open(url)
    br.select_form('loginForm')

    br.form['usr_name'] = username
    br.form['usr_password'] = password
    req = br.submit()
    text = req.get_data()
    find_url = 'If you are not' in text
    
    if find_url:
        user.is_active = True
        user.save()

        user_profile = user.userprofile
        user_profile.ellet = True
        user_profile.ellet_verified = True
        user_profile.ellet_account_name = get_ellet_account_name(br, req)
        user_profile.ellet_login = username
        fernet = Fernet(FERNET_KEY)
        user_profile.ellet_password = fernet.encrypt(to_bytes(password))
        user_profile.save()
        return True
    return False


def send_email(text_content=None, html_content=None, subject=None, from_email='FFL Design <%s>' % settings.EMAIL_HOST_USER, to_email=[]):
    msg = EmailMultiAlternatives(
                subject,
                text_content,
                from_email,
                to_email
            )
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def send_intro_email(to_email=[]):
    text_content = render_to_string(
        'accounts/email/intro.txt',
        {}
    )
    html_content = render_to_string(
        'accounts/email/intro.html',
        {}
    )
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject='Welcome to vArmory by FFLDesign.com!',
        to_email=to_email
    )


def send_new_account_email(to_email=[], user=None, password=None):
    text_content = render_to_string(
        'accounts/email/new_account.txt',
        {
            'password': password,
            'user': user
        }
    )
    html_content = render_to_string(
        'accounts/email/new_account.html',
        {
            'password': password,
            'user': user
        }
    )
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject='Welcome to vArmory by FFLDesign.com!',
        to_email=to_email
    )


def send_search_alert_email(context):
    text_content = render_to_string(
        'search/email/search_alert.txt',
        context
    )
    html_content = render_to_string(
        'search/email/search_alert.html',
        context
    )
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject='User Keyword Search Alert',
        to_email=['jason@gsadirect.com', 'david@primusmg.com']
    )


def send_reset_password_email(to_email=[], user=None, reset_password_url=None):
    token = None
    if user:
        token = ResetPasswordToken.objects.get(user=user)

    text_content = render_to_string(
        'accounts/email/reset_password.txt',
        {
            'user': user,
            'token': token.token,
            'reset_password_url': reset_password_url
        }
    )
    html_content = render_to_string(
        'accounts/email/reset_password.html',
        {
            'user': user,
            'token': token.token,
            'reset_password_url': reset_password_url
        }
    )
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject='Reset your vArmory password',
        to_email=to_email
    )


def send_reset_password_email_missing(to_email=[], url=None):
    text_content = render_to_string(
        'accounts/email/reset_password_missing.txt',
        {'url': url}
    )
    html_content = render_to_string(
        'accounts/email/reset_password_missing.html',
        {'url': url}
    )
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject='Reset your vArmory password',
        to_email=to_email
    )


def send_order_notification_email(to_email=[], context={}):
    text_content = render_to_string(
        'saas_app/email/confirm_order_email.txt',
        context
    )
    html_content = render_to_string(
        'saas_app/email/confirm_order_email.html',
        context
    )
    subject = 'Order Received from FFL Design : '+ str(context['orders'][0].order_number_c)
    print subject
    if context.get('buyer'):
        subject = 'Order Placed on FFL Design'
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject=subject,
        to_email=to_email
        
    )


def send_search_result_report(to_email=[]):
    # Create report containing the search results with the name of users who used that keyword
    keywords = set()
    for result in SearchHistory.objects.exclude(user__email__in=settings.EXCLUDED_EMAILS_SEARCH_RESULTS):
        if result.searched_term:
            keywords.add(result.searched_term.lower())

    report_table = {}

    for keyword in keywords:
        user_set = set()
        results = SearchHistory.objects.filter(
            searched_term__iexact=keyword
        ).exclude(user__email__in=settings.EXCLUDED_EMAILS_SEARCH_RESULTS)
        for result in results:
            if result.user:
                user_set.add(result.user.email)
        # Add to list to have indexing
        user_list = []
        for user in user_set:
            user_list.append(user)
        report_table[keyword] = user_list

    text_content = render_to_string(
        'saas_app/email/search_result_report.txt',
        {
            'report_table': report_table
        }
    )
    html_content = render_to_string(
        'saas_app/email/search_result_report.html',
        {
            'report_table': report_table
        }
    )
    send_email(
        text_content=text_content,
        html_content=html_content,
        subject='Search Result Report',
        to_email=to_email
    )

def rep_finder(state=None):
    for region in REGIONS:
        if state in region:
            return SALES_REPS[REGIONS.index(region)]
    else:
        return 'David Lehman'
