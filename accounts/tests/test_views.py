from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core.management import call_command
from django.contrib.sessions.middleware import SessionMiddleware
from django.test import TestCase, Client, RequestFactory

from accounts.views import LoginView


def add_middleware_req(request, middleware_class):
    middleware = middleware_class()
    middleware.process_request(request)
    return request


def add_middleware_resp(response, middleware_class):
    middleware = middleware_class()
    middleware.process_response(response)
    return response


def setUpModule():
    call_command('loaddata', 'blacklisted_passwords')


class LoginTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        user = User(
            username='johndoe@bixly.com',
            password=make_password('longtestpassword')
        )
        user.is_active = True
        user.save()

    def test_login(self):
        response = self.client.post('/login/', {
            'username': 'johndoe@bixly.com',
            'password': 'longtestpassword',
            'remember': False,
        })
        self.assertEquals(response.status_code, 302)

    def test_login_remember(self):
        response = self.client.post('/login/', {
                'username': 'johndoe@bixly.com',
                'password': 'longtestpassword',
                'remember': True,
        })
        self.assertEqual(response.status_code, 302)
        print response.client.cookies.items()
        # self.assertIsNotNone(response.client.cookies.get('username'))
        # try:
        #     print response.client.cookies.items()
        #   print response.wsgi_request
        # except Exception, e:
        #     print e


class SignUpTestCase(TestCase):

    def setUp(self):
        self.data = {
            'first_name': 'FirstName',
            'last_name': 'LastName',
            'username': 'otheremail@address.com',
            'ffl_number_first_three': '9-82',
            'ffl_number_last_five': '01710',
            'premise_zip_code': '11111',
            'password': 'validpassword123',
            'password_confirm': 'validpassword123'
        }

    def test_signup(self):
        response = self.client.post('/', self.data)
        self.assertEquals(response.status_code, 302)
