from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.contrib.auth.models import User
from django.test import TestCase

from model_mommy import mommy

from accounts.forms import UsernameField, PasswordField, SignUpForm


def setUpModule():
    call_command('loaddata', 'blacklisted_passwords')


class SignUpFormTestCase(TestCase):

    def setUp(self):
        self.user = mommy.make(
            User,
            username='email@address.com',
            _fill_optional=True)
        self.data = {
            'first_name': 'FirstName',
            'last_name': 'LastName',
            'username': 'otheremail@address.com',
            'ffl_number_first_three': '9-82',
            'ffl_number_last_five': '01710',
            'premise_zip_code': '83642',
            'password': 'validpassword123',
            'password_confirm': 'validpassword123'
        }

    def test_invalid_username(self):
        field = UsernameField()
        self.assertEqual(field.clean('johndoe@bixly.com'), 'johndoe@bixly.com')
        self.assertRaises(ValidationError, field.clean, 'bademail@')

    def test_existing_username(self):
        field = UsernameField()
        self.assertRaises(ValidationError, field.clean, self.user.username)

    def test_common_password(self):
        field = PasswordField()
        self.assertRaises(ValidationError, field.clean, 'password')
        self.assertRaises(ValidationError, field.clean, '12345678')

    def test_valid_password(self):
        field = PasswordField()
        self.assertRaises(ValidationError, field.clean, 'abcdef~`g')
        self.assertEqual(field.clean('1pass3wor2d'), '1pass3wor2d')

    def test_length_password(self):
        field = PasswordField()
        self.assertRaises(ValidationError, field.clean, '')
        self.assertRaises(ValidationError, field.clean, 'soclose')

    def test_valid_ffl(self):
        form = SignUpForm(self.data)
        self.assertEqual(form.is_valid(), True)

    def test_invalid_ffl(self):
        self.data.update({'ffl_number_last_five': '11111'})
        form = SignUpForm(self.data)
        self.assertEqual(form.is_valid(), False)
        errors = form.errors.as_data()
        error = errors.get('ffl_number_first_three')[0]
        self.assertEqual(error.code, 'invalid')
