from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

from accounts.models import Wallet
from saasapp import tsys
from saasapp.constants import CREDIT_CARD
from saasapp.models import Order, PurchaseHistory


class TransitScriptTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(
            first_name='Kevin',
            last_name='Lorenzana',
            username='user',
            password=make_password('password'),
        )
        userprofile = user.userprofile
        userprofile.ffl_premise_address = '8320 - 85284'
        userprofile.save()
        order = Order.objects.create(
            user=user,
            payment_type='Credit Card',
        )
        PurchaseHistory.objects.create(
            user=user,
            order=order,
            upc='892756002373',
            model='GSG AK-47',
            qty=1
        )
        self.wallet = Wallet.objects.create(
            user=user,
            wallet_type=CREDIT_CARD,
            expiration_month=12,
            expiration_year=2016
        )

    def test_visa_one(self):
        print '\nVISA TEST 1'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=32.49,
            total_price=32.49
        )
        card_number = '4012000098765439'
        self.wallet.cvv = 999
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_visa_two(self):
        print '\nVISA TEST 2'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=25.00,
            total_price=25.00
        )
        card_number = '4012000098765439'
        self.wallet.cvv = 999
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_visa_three(self):
        print '\nVISA TEST 3'
        result = tsys.verify_card(
            card_holder_name='%s %s' % (
                self.wallet.user.first_name,
                self.wallet.user.last_name,
            ),
            card_number='4012000098765439',
            cvv=123,
            expiration_month=12,
            expiration_year=2016,
        )
        self.assertEqual(result, None)

    def test_mastercard_one(self):
        print '\nMASTERCARD TEST 1'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=7.00,
            total_price=7.00
        )
        card_number = '5400060000000041'
        self.wallet.cvv = 998
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_mastercard_two(self):
        print '\nMASTERCARD TEST 2'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=9.00,
            total_price=9.00
        )
        card_number = '5400060000000041'
        self.wallet.cvv = 998
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_mastercard_three(self):
        print '\nMASTERCARD TEST 3'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=11.10,
            total_price=11.10
        )
        card_number = '5400060000000041'
        self.wallet.cvv = 998
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_discover_one(self):
        print '\nDISCOVER TEST 1'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=6.00,
            total_price=6.00
        )
        card_number = '6011000993026909'
        self.wallet.cvv = 996
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_discover_two(self):
        print '\nDISCOVER TEST 2'
        result = tsys.verify_card(
            card_holder_name='%s %s' % (
                self.wallet.user.first_name,
                self.wallet.user.last_name,
            ),
            card_number='6011000993026909',
            cvv='illegible',
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.assertEqual(result, None)

    def test_amex_one(self):
        print '\nAMEX TEST 1'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=15.00,
            total_price=15.00
        )
        card_number = '371449635392376'
        self.wallet.cvv = 9997
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_amex_two(self):
        print '\nAMEX TEST 2'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=7.05,
            total_price=7.05
        )
        card_number = '371449635392376'
        self.wallet.cvv = 9997
        self.wallet.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_diners_one(self):
        print '\nDINERS TEST 1'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=10.00,
            total_price=10.00
        )
        card_number = '36004000400200'
        self.wallet.cvv = 996
        self.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))

    def test_jcb_one(self):
        print '\nJCB TEST 1'
        order = Order.objects.all()[0]
        PurchaseHistory.objects.all().update(
            unit_price=3.00,
            total_price=3.00
        )
        card_number = '3566002020360505'
        self.wallet.cvv = 996
        self.masked_card_number = card_number[-4:]
        result = tsys.verify_card(
            card_holder_name="%s %s" % (
                self.wallet.user.first_name,
                self.wallet.user.last_name
            ),
            card_number=card_number,
            cvv=self.wallet.cvv,
            expiration_month=self.wallet.expiration_month,
            expiration_year=self.wallet.expiration_year,
        )
        self.wallet.token = result
        self.wallet.save()
        self.assertTrue(self.wallet.sale(order=order))
