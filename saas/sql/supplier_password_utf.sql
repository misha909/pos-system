ALTER TABLE saasapp_userprofile CHANGE davidson_password davidson_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE saasapp_userprofile CHANGE green_password green_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE saasapp_userprofile CHANGE lipseys_password lipseys_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE saasapp_userprofile CHANGE rsr_password rsr_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE saasapp_userprofile CHANGE sports_password sports_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE saasapp_userprofile CHANGE zanders_password zanders_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE saasapp_userprofile CHANGE camfour_password camfour_password VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci;
