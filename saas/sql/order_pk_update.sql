SET FOREIGN_KEY_CHECKS=0;
UPDATE saasapp_purchasehistory SET order_id = order_id + 10000 WHERE order_id < 10000;
UPDATE saasapp_order SET id = id + 10000 WHERE id < 10000;
ALTER TABLE saasapp_order AUTO_INCREMENT = 11000;
SET FOREIGN_KEY_CHECKS=1;