DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pos',
        'USER': 'pos',
        'PASSWORD': 'password',
        'HOST': 'localhost',       # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'haystack',
        'TIMEOUT': 120
    }
}

#HAYSTACK_SEARCH_RESULTS_PER_PAGE = 48

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# SALESFORCE
SALESFORCE_LOGIN_EMAIL = 'jason@gsadirect.com.gsadff'
SALESFORCE_LOGIN_PASSWORD = 'Ffldesign1'
SALESFORCE_SECURITY_TOKEN = 'WuvvU1p8Qdw719fDAYow5rVq'
SALESFORCE_SERVER_URL = 'https://cs45.salesforce.com/services/Soap/u/20.0'

#SMTP
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'patrickfalvey40@gmail.com'
EMAIL_HOST_PASSWORD = 'ffldesign1'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

SITE_ID = 1
