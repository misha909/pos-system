import autocomplete_light

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required

from rest_framework import routers

from saasapp import views
from accounts import views as account_views

autocomplete_light.autodiscover()
admin.autodiscover()

# Examples:
# url(r'^admin_tools/', include('admin_tools.urls')),

urlpatterns = patterns(
    '',
    url(r'^$', account_views.SignUpView.as_view()),
    url(r'^index/$', account_views.SignUpView.as_view(), name='index'),
    url(r'^login/$', account_views.LoginView.as_view(), name='login'),
    url(
        r'^register/step-2/$',
        account_views.SupplierRegView.as_view(),
        name='step-2'
    ),
    url(
        r'^register/step-3/$',
        account_views.supplier_verification,
        name='step-3'
    ),
    url(
        r'^update-user-billing-address/',
        account_views.UpdateUserBillingAddress.as_view(),
        name='update_billing_address',
    ),
    url(r'^box/', views.box),
    url(r'^api/', include('api.urls')),
    url(r'^dealerexchange/', include('dealerexchange.urls')),
    url(r'^search/', include('haystack.urls')),
    url(
        regex=r'^product-search/',
        view=views.Product_Search,
        name='product-search'),
    url(
        regex=r'^forgot-password/',
        view=views.ForgotPassword.as_view(),
        name='forgot-password'
    ),
    url(
        regex=r'^reset-password/',
        view=views.ResetPassword.as_view(),
        name='reset-password'
    ),
    url(r'^cart/', views.CartView.as_view(), name='view_cart'),
    url(r'^upload-ffl/', views.UploadfflView.as_view(), name='upload_ffl'),
    url(r'^upload-sot/', views.UploadSOTView.as_view(), name='upload_sot'),
    url(r'^upload-ffl-thank/', views.UploadfflThankView.as_view(), name='ffl_thank'),
    url(r'^payment/', views.PaymentView.as_view(), name='view_payment'),
    url(r'^add-wallet/$', views.AddWalletView.as_view(), name='add_wallet'),
    url(r'^remove-wallet/$', views.RemoveWalletView.as_view(),
        name='remove_wallet'),
    url(r'^review/', views.ReviewView.as_view(), name="view_review"),
    url(
        r'^redeem-coupon/$',
        views.RedeemCouponView.as_view(),
        name='redeem_coupon'
    ),
    url(r'^thank/', views.ThankView.as_view(), name="view_thank"),
    url(r'^advance-search/', views.AdvanceSearch, name="advance-search"),
    url(r'^cart-add-product/', views.add_product, name='cart_add_product'),
    url(
        r'^cart-remove-product/',
        views.remove_product,
        name='cart_remove_product'
    ),
    url(
        r'^cart-update-product/',
        views.update_product,
        name='cart_update_product'
    ),
    url(
        regex=r'^set-customer-view/$',
        view=views.set_customer_view,
        name='set_customer_view'
    ),
    url(r'^resp_emails/', views.RespEmail),
    # url(r'^email-send/', views.EmailSendView.as_view()),
    # url(
    #     regex=r'^confirm-order/(?P<user_id>\d+)/(?P<order_id>\d+)/(?P<supplier>.*)/',
    #     view=views.ConfirmOrder.as_view(),
    #     name='confirm-order'
    # ),
    url(r'^order-history/', views.OrderHistoryView.as_view()),
    url(r'^history/', views.SearchHistoryView.as_view()),
    url(r'reports/product/no_image/', staff_member_required(views.NoImageProductReport.as_view())),
    url(r'^settings/', views.SettingsView.as_view(), name='settings'),
    url(r'^start_trial/', views.start_trial),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^session_security/', include('session_security.urls')),
    url(r'^cas/', include('mama_cas.urls')),
    # django-rest-framework
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # Accounts app
    url(r'^registration/', account_views.signup), # Signup page
    url(r'^logout/$', account_views.Logout, name='logout'),
    url(r'^davidson/', account_views.davidson),
    url(r'^lipseys/', account_views.lipseys),
    url(r'^greens/', account_views.green),
    url(r'^zanders/', account_views.zanders),
    url(r'camfour/', account_views.camfour),
    url(r'^sports/', account_views.sports),
    url(r'^rsr/', account_views.rsr),
    url(r'^jerrys/', account_views.jerrys),
    url(r'^ellet/', account_views.ellet),
    url(r'^testform/', views.TestFormView.as_view(), name='testform'),
)

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
