from django.conf import settings

from urllib import urlencode


def get_request(request):
    return {'request': request}


def get_domains(request):
    DEALER_DIRECT = urlencode(
        {'service': settings.DEALER_DIRECT_URL}
    )
    return {
        'DEALER_DIRECT': DEALER_DIRECT
    }
