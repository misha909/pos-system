"""
Django settings for saas project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.conf import settings


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k4=%)l4f35adu0wdwx350w^1)*!s+0bx-dwz(nd@2-*)s2c0ar'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    #'admin_tools',
    #'admin_tools.theming',
    #'admin_tools.menu',
    #'admin_tools.dashboard',
    'autocomplete_light',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'haystack',
    'rest_framework',
    'rest_framework.authtoken',
    'accounts',
    'import_stats',
    'products',
    'saasapp',
    'mptt',
    'django_cron',
    'carton',
    'corsheaders',
    'session_security',
    'mama_cas',
    'dealerexchange',
)

CRON_CLASSES = (
    "saasapp.views.MyCronJob",
)

#ADMIN_TOOLS_INDEX_DASHBOARD = 'saas.dashboard.CustomIndexDashboard'
#ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'saas.dashboard.CustomAppIndexDashboard'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'maintenancemode.middleware.MaintenanceModeMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'session_security.middleware.SessionSecurityMiddleware',
)

ROOT_URLCONF = 'saas.urls'

WSGI_APPLICATION = 'saas.wsgi.application'
# Database Server
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'saas_db',
#         'USER': 'saas_db_user',
#         'PASSWORD': 'i$Hu5k92',
#         'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
#         'PORT': '5432',
#     }
# }


# Database Local
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'cory',
        'PASSWORD': 'p@ssw0rd',
        'HOST': 'localhost',       # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',
    }
}


# Settings to be used when user to be mailed during sign up
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.office365.com'
EMAIL_HOST_USER = 'noreply@ffldesign.com'
EMAIL_HOST_PASSWORD = 'Dobu0580'
EMAIL_PORT = 587

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'


TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOGIN_URL = "/login/"
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'apptemplates.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "saas.context_processors.get_request",
    "saas.context_processors.get_domains",
)

TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\','/'),)

STATICFILES_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'static').replace('\\','/'),)
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media').replace('\\','/')
MEDIA_URL = '/media/'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Advanced',
    },
}

CKEDITOR_UPLOAD_PATH = MEDIA_ROOT+'/ckeditor/'

"""HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8983/solr'
    },
}"""

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9211/',
        'INDEX_NAME': 'haystack',
        'TIMEOUT': 120
    },
    'slave1': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9212/',
        'INDEX_NAME': 'haystack',
        'TIMEOUT': 120
    },
    'slave2': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9213/',
        'INDEX_NAME': 'haystack',
        'TIMEOUT': 120
    }
}

HAYSTACK_SEARCH_RESULTS_PER_PAGE = 48

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# FFL EZ Check
FFL_EZ_CHECK_URL = 'http://www.atfonline.gov/fflezcheck/'

# mama cas
DEALER_DIRECT_URL = 'https://dealerdirect.ffldesign.com/'

# REST Framework
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
}

# Image Server
IMAGE_URL = "http://166.78.105.176/images"

# Django Carton - shopping cart
CART_PRODUCT_MODEL = 'products.models.Product'

# SALESFORCE
SALESFORCE_LOGIN_EMAIL = 'jason@gsadirect.com'
SALESFORCE_LOGIN_PASSWORD = 'Ffldesign1'
SALESFORCE_SECURITY_TOKEN = 'R9qSRCD1063RuVAxJWsuHteF'
SALESFORCE_SERVER_URL = 'https://login.salesforce.com/services/Soap/u/20.0'
SALESFORCE_DOMAIN = 'na9.salesforce.com'
DISABLE_SALESFORCE = False

#Reports
EXCLUDED_EMAILS_SEARCH_RESULTS = [
    'david@gsadirect.com',
    'test@ffldesign.com',
    'kevinz@bixly.com'
]

# Maintenance Mode
MAINTENANCE_IGNORE_URLS = (
    r'^/admin/.*',
)

# TSYS Settings
MERCHANT_ID = '955009095000'
DEVICE_ID = '95500909500001'
DEVELOPER_ID = '002681'
TA = 'TA256592'
TSYS_PASSWORD = 'FFL_Design1'
TSYS_DOMAIN = 'https://gateway.transit-pass.com'
TSYS_PORT = 9300
TSYS_API_URL = 'servlets/TransNox_API_Server'

# GSA Direct CSV file
GSA_CSV_FILE = os.path.join(BASE_DIR, 'saas/imports/vArmory-Product-Table.csv').replace('\\','/')
GSA_DIRECT_EMAILS = [
    'jason@gsadirect.com'
]

# FFL Number CSV File
FFL_CSV_FILE = os.path.join(BASE_DIR, 'saas/imports/ffl-list.csv').replace('\\','/')

# Routing Number CSV File
ROUTING_NUMBER_CSV_FILE = os.path.join(BASE_DIR, 'saas/imports/Routing_Numbers.csv').replace('\\','/')

# Sport Settings
LAST_ITEM = -1

# Memcache
MEMCACHED_URL = '127.0.0.1:11211'

CORS_ORIGIN_ALLOW_ALL = True
#CORS_ALLOW_CREDENTIALS = False

# django-session-security
SESSION_SECURITY_WARN_AFTER = 840
SESSION_SECURITY_EXPIRE_AFTER = 900
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

try:
    from .local import *
except ImportError:
    pass
